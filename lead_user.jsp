<%@page import="com.servlets.*, com.bl.*, com.util.*, java.io.*, java.net.*, java.math.*, java.util.*"%>
<html>
<head>
   <script src="/jquery.js"></script>
   <script src="/main.js"></script>
</head>
<body>
<%
   /*--------------------------*/
   /*--- Capture Parameters ---*/
   /*--------------------------*/
   String      domain            =  Web.getDomainName( request );
   String      first_name        =  request.getParameter( "first_name"      );
               first_name        =  first_name==null?"":first_name;
   String      last_name         =  request.getParameter( "last_name"       );
               last_name         =  last_name==null?"":last_name;
   String      email             =  request.getParameter( "email"           );
               email             =  email==null?"":email;
   String      zip               =  request.getParameter( "zip_1"           );
               zip               =  zip==null?"":zip;
   try       { zip               =  zip.replace(" ", ""); } catch( Exception e ) {}
   String      service           =  request.getParameter( "service"         );
               service           =  service==null?"renew-drivers-license":service;
   String      state             =  request.getParameter( "state"           );
               state             =  state==null?"washington":state;
   String      ip                =  Web.getRemoteIP( request );
   String      userAgent         =  (""+request.getHeader("USER-AGENT"));
   

   String sql = "insert into user (ip, first_name, last_name, zip_1, email, created, active, user_agent) values ('"+ip+"', '"+first_name+"', '"+last_name+"', '"+zip+"', '"+email+"', sysdate(), 'Y', '"+userAgent+"')";
   DB.executeSQL( sql );
%>
   <script>
      try { parent.document.location.href = "/form/step1/<%=service%>/<%=state%>.html?first_name=<%=first_name%>&last_name=<%=last_name%>&email=<%=email%>&zip=<%=zip%>"; } catch(e) {}
   </script>
</body>
</html>