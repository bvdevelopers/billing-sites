<%@page import="java.util.*,java.io.*,java.net.*,com.servlets.*,com.oreilly.servlet.multipart.*,com.config.*,com.util.*"%><%
   if( request.getMethod().equals("GET") )
   {
      %><form action="/upload.jsp" enctype="multipart/form-data" method="post"><input type="file" name="datafile"><input type="submit" value="Send"></form><%
      return;
   }
   try
   {
      String                               id = User.getSessionProfile(request).get("id").toString();
      MultipartParser                mrequest = new MultipartParser(request, 5 * 1024 * 1024 * 1024, true, true);
      com.oreilly.servlet.multipart.Part part = null;
      while( (part = mrequest.readNextPart()) != null )
      {
         if( part.isFile() )
         {
            FilePart          fp =  (FilePart) part;
            InputStream       is =  fp.getInputStream();
            String          name =  fp.getFileName().toLowerCase();
            byte             b[] =  new byte[1024*1000];
            int               nr =  0;
            String    targetName = getServletContext().getRealPath( "../uploads/" + id + "@" + name );
            FileOutputStream fop = new FileOutputStream( targetName );
            while( (nr=is.read(b))!=-1 )
               fop.write( b, 0, nr );
            fop.close();
         }
      }
      Web.redirect( response, 302, "/uploaded.html" );
   }
   catch( Exception e )
   {
      e.printStackTrace();
      Web.send404( response );
   }
%>
