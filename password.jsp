<%@page import="java.util.*, com.util.*"%><%
   response.setContentType( "application/json" );
   
   String  user      = (String) session.getAttribute( "login"          );
   String  oldP      = Web.getParameterNoSQLInject( request, "old"     );
   String  newP      = Web.getParameterNoSQLInject( request, "new"     );
   String  confirm   = Web.getParameterNoSQLInject( request, "confirm" );
   String  domain    = Web.getDomainName( request );
   if( user==null || oldP.trim().equals("") || newP.trim().equals("") || confirm.trim().equals("") || !confirm.equals(newP) )
   {
      if( user==null )
      {
         out.print( "{\"status\":\"Session expired. Please login again\"}" );
      }
      else if( oldP.trim().equals("") )
      {
         out.print( "{\"status\":\"Current Password required\"}" );
      }
      else if( newP.trim().equals("") )
      {
         out.print( "{\"status\":\"New Password required\"}" );
      }
      else if( confirm.trim().equals("") )
      {
         out.print( "{\"status\":\"Confirm Password required\"}" );
      }
      else if( !confirm.equals(newP) )
      {
         out.print( "{\"status\":\"New Password does not match Confirm Password\"}" );
      }
      return;
   }
   if( newP.length() < 8 )
   {
      out.print( "{\"status\":\"New Password must be at least 8 Characters\"}" );
      return;
   }
   HashMap hm = DB.getRecord( "select * from login where user='"+user+"' and (password='"+oldP+"' or resetpass='"+oldP+"')" );
   if( hm.isEmpty() )
   {
      out.print( "{\"status\":\"Invalid Current Password\"}" );
      return;
   }
   
   DB.executeSQL( "update login set resetpass=null, password=\"" + newP + "\" where user='" + user + "' and domain='"+domain+"'" );
   
   out.print( "{\"status\":\"OK\"}" );
%>
