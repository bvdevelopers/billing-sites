<%
   String domain  = com.util.Web.getDomainName( request );
   response.setHeader( "Content-Disposition", "attachment;filename="+ domain + ".zip" );
   byte b[] = com.util.ZIP.zip( getServletContext().getRealPath( "/content/" + domain ) );
   response.getOutputStream().write( b );
%>
