<%@page import="java.net.*, java.util.*, com.util.*"%><%
   String f          = request.getParameter( "f" );
   String n          = Format.lastToken(      f, "/" );
   String domain     = Web.getDomainName(    request   );
   String user       = "" + session.getAttribute("login");
   String checklist  = "" + session.getAttribute("redirectchecklist");
   String nchecklist = request.getParameter( "newredirectchecklist" );
   
   if( f.toLowerCase().endsWith(".pdf") )
   {
      if(f.contains("/related-resources/") || f.contains("/related-forms/"))
      {
         checklist   = f;
      }
      if(nchecklist != null)
      {
         checklist   = nchecklist;
      }
      String sql = "insert into download_history values (sysdate(), '"+domain+"', '"+user+"', '"+f+"', '"+checklist+"')";
      DB.executeSQL( sql );
      response.setHeader("Content-Disposition", "attachment; filename=" + n);
      response.getOutputStream().write( Web.webGet(f) );
      response.getOutputStream().flush();
   }
%>
