<%@page import="com.util.*, com.bl.*, java.util.*, java.io.*" %>
<%!
   String showLocation( HashMap hm )
   {
      String r = "";
      r += "<p>";
      r += "<h2>" + hm.get("office") + "</h2>";
      r += "<div>" + hm.get("address") + "</div>";
      r += "<div>" + hm.get("city") + "</div>";
      r += "<div>" + hm.get("state") + ", " + hm.get("zip") + "</div>";
      r += "<div>Phone : " + hm.get("phone") + "</div>";
      r += "</p>";
      return r;
   }
%><%
   /*----------------------------------------------*/
   /*--- SORRY CANNOT DO PROCEDURE IN THIS CASE ---*/
   /*----------------------------------------------*/
   String zip = Web.getParameterNoSQLInject( request, "zip" );
   zip = zip==null?"":zip.trim();
   if( zip.length()==0 )
   {
      %><h2>Data Not Found</h2><%
   }
   else
   {
      Vector   v        = Queries.DMVlookup( zip );
      for( int i=0; i<v.size(); i++ )
         out.print( showLocation( (HashMap) v.elementAt(i) ) );
      if( v.size() == 0 )
         out.print( "<div class='errmsg'>No Match Found</div>" );
   }
%>