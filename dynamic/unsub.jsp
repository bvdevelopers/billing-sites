<%@page import="com.util.*, com.bl.*, java.util.*, java.io.*" %>
<form target="buffer" onSubmit="return confirm('Are you sure');" name="frm" method="post" action="/unsub">
   <input type="hidden" name="uuid"   value="<%=request.getParameter("uid")%>">
   <div style="padding-top:5px;">To remove your email address from our system, please enter it in the field below and click submit.</div>
   <div style="padding-top:5px;"><input required="required" type="email"  name="email" placeholder="Email Address" style="width:250px;"></div>
   <div style="padding-top:45px;"><input type="image" src="/img/unsubscribe_button.png"></div>
</form>
<iframe style="display:none" name="buffer"></iframe>
