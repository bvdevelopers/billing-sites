<%@page import="java.util.*, com.util.*, com.bl.*" %>
<%
   String domain = Web.getDomainName( request );
%>
var popcount    = 0;
var leaveSite   =  true;
var user_ip     = "<%=request.getRemoteAddr()%>";
var user_id     = "<%=session.getAttribute( "uid" )%>";
var user_agent  = "<%=request.getHeader("User-Agent")%>";
var transam     = "<%=session.getAttribute( "transam" )%>";
var coverdell   =  <%=session.getAttribute( "coverdell" )%>;
var forwarding_service = "<%=session.getAttribute( "forwarding_service")%>";


var __SPLASHZIP   = "<%=session.getAttribute( "splashzip"     )%>";
var __SPLASHSTATE = "<%=session.getAttribute( "splashstate"   )%>";
var quiz_points   = "<%=session.getAttribute( "quiz.points"   )%>";
var quiz_response = "<%=session.getAttribute( "quiz.response" )%>";
var upsellYN      = "<%=session.getAttribute( "upsellYN"      )%>";
var paymentTrans  =  <%=session.getAttribute( "transaction")!=null%>;



var _PAYMENT          = new Array();
var _PROFILE          = new Array();
var domainName        = "<%=domain%>";
var restaurant_coupon = "<%=session.getAttribute( "restaurant_coupons")%>";



var utm_source    =  "<%=session.getAttribute( "utm_source" )%>";
var utm_campaign  =  "<%=session.getAttribute( "utm_campaign" )%>";
var utm_content   =  "<%=session.getAttribute( "utm_content" )%>";
var utm_term      =  "<%=session.getAttribute( "utm_term" )%>";


<%-- GETTING POLLS RESULTS --%>
var poll = new Array();
<%
   try
   {
      java.util.Enumeration e = session.getAttributeNames();
      while( e.hasMoreElements() )
      {
         String k = (String) e.nextElement();
         String v = (String) session.getAttribute( k );
         if( k.startsWith("poll#") )
         {
            k = k.replace( "poll#", "" );
            %>poll[<%=k%>]=<%=v%>;<%
         }
      }
   }
   catch( Exception e )
   {
   }
%>




$(document).ready(function()
{
   $("#address_1").change( function() {
      $(this).val( $(this).val().replace("suite", "#") );
      $(this).val( $(this).val().replace("# ",    "#") );
      $(this).val( $(this).val().replace(" #",    "#") );
      var v  = $(this).val();
      var sp = v.split( "#" );
      if( sp.length==2 )
      {
         sp[0] = $.trim(sp[0]);
         sp[1] = $.trim(sp[1]);
         while( sp[0].charAt(sp[0].length-1)==',' )
            sp[0] = sp[0].substr( 0, sp[0].length - 1 );
         
         $("#address_1").val(       sp[0] );
         $("#suite_1"  ).val( "#" + sp[1] );
      }
   });



   $("#email").change( function() { correctEmail($(this));} );
   $("#card_number").change(function(){
      $(this).val( extractNumberJunk($(this).val()) );
   });
   


   
   try { <%=com.servlets.User.jsHash(request)%>    } catch(e) {}
   try { <%=com.servlets.Payment.jsHash(request)%> } catch(e) {}

   var hrf = document.location.href;
   if( hrf.indexOf("/adv-billing.html")!=-1 )
      track("adv_billing");

   if( hrf.indexOf("refund.html")!=-1 )
   {
      $("#button_section").click(function(){
         if ($.trim($("#first_name").val())    == "" ) {$("#first_name").css("border","1px solid red").focus();return false;}    else {$("#first_name").css("border","");}
         if ($.trim($("#last_name").val())     == "" ) {$("#last_name").css("border","1px solid red").focus();return false;}     else {$("#last_name").css("border","");}
         if ($.trim($("#email").val())         == "" ) {$("#email").css("border","1px solid red").focus();return false;}         else {$("#email").css("border","");}
         if ($.trim($("#reason").val())        == "" ) {$("#reason").css("border","1px solid red").focus();return false;}        else {$("#reason").css("border","");}
         
         $("#button_section").hide();
         $("#spin_section").show();
      });
   }
   else
   if( hrf.indexOf("/notice.html") != -1 )
   {
      track("notice");
   }
   else
   if( (hrf.indexOf("/dmv-locations.html")!=-1) || (hrf.indexOf("/licensing-centre-locations.html")!=-1) )
   {
      $("#submit").click( function(e){
         var zipcode = $("#zip").val();
         $("#dmvmap").attr("src","/googlemaps.jsp?q=dmv+" + zipcode)
      });
      
      //--- linking enter press with send button
      $(document).keypress(function(event){
         var keycode = (event.keyCode ? event.keyCode : event.which);
         if(keycode == '13')
         {
            $('#submit').click();   
         }
      });
   }
   else
   if( hrf.indexOf("/poll") != -1 )
   {
      $.ajax({ "url":"/ajax/trackPoll.jsp" });
   }
   else
   if( hrf.indexOf("/searchresults") != -1 )
   {
      var urlParams = parseParamsFromUrl();
      var kw = encodeURIComponent(urlParams['q']);
      $('#q').val	(urlParams['q']);
      $('#result').text('Displaying results for: ' + urlParams['q']);
      $.get('/search.jsp?q=' + kw, function(html)
      {
         $('#searchresults').html(html);
         paginate();
      });
   }
   else
   if( hrf.indexOf("/refund.html") != -1 )
   {
      $( "#__ID" ).val( hrf.split("?")[1] );
   }


   //--- colorbox
   $(document).bind('cbox_open', function () {     $('html').css({ overflow: 'hidden' }); }).bind('cbox_closed', function () {     $('html').css({ overflow: 'auto' }); });  



   //--- templied fields such as {state} {url[]}
   href = document.location.href.split("?")[0].split( "/" ).reverse();
   if( $(".templified").length > 0 )
   {
      lowState       =  $(".low-state");
      capState       =  $(".cap-state");
      initCapState   =  $(".init-cap-state");
      _state = href[0].replace( ".html", "" );
      
      if( lowState.length > 0 )     lowState.html(      _state.toLowerCase()   );
      if( capState.length > 0 )     capState.html(      _state.toUpperCase()   );
      if( initCapState.length > 0 ) initCapState.html(  initCap(_state )   );
      
      for( i=2; i<4; i++ )
      {
         initCapUrl   = $(".init-cap-url-" +  i);
         capUrl       = $(".cap-url-"      +  i);
         lowUrl       = $(".low-url-"      +  i);
         ndxVal       = href[i-1];
         if( lowUrl.length       > 0 ) lowUrl.html(      ndxVal.toLowerCase() );
         if( capUrl.length       > 0 ) capUrl.html(      ndxVal.toUpperCase() );
         if( initCapUrl.length   > 0 ) initCapUrl.html(  initCap(ndxVal) );
      }
   }
   
   if( document.location.href.indexOf("billing.html")!=-1 )
   {
      
      $("#card_number").change( function() {
         $("#processError").html( "" );
         $("#processError").fadeOut();
         if( $(this).val().charAt(0) == "3" )
         {
            $("#processError").html( "<img src='/img/error.png' width='25' align='left' id='errx'>American Express not accepted. Please try another card." );
            $("#processError").fadeIn();
         }
      });
      
      /*--------------------*/
      /*--- Billing page ---*/
      /*--------------------*/
      track( "billing" );
      try
      {
         var _stateService = _PROFILE["_FORM_"].split( "/" );
         var _state        = _stateService[2].split(".")[0];
         if( _state=="" || _state=="main" )
            _state = toStateName( _PROFILE["state_1"] ).toLowerCase();
         $("#s_state").val( _state );
         $("#s_service").val( _stateService[1] );
         $("#state").val( _state );
      }
      catch(e)
      {
      }


      $("#item").val(       "Processing Fee"       );
      $("#phone").val(      _PROFILE["phone_1"]    );
      $("#email").val(      _PROFILE["email"]      );
      $("#first_name").val( _PROFILE["first_name"] );
      $("#last_name").val(  _PROFILE["last_name"]  );
      $("#address").val(    _PROFILE["address_1"]  );
      $("#city").val(       _PROFILE["city_1"]     );
      $("#zip").val(        _PROFILE["zip_1"]      );
   }
   
});

function itemCart( icon, item, desc, cumulate )
{
   if( $("#price").val()=="" )
      $("#price").val("0");
   var _tot       =  eval( $("#displayPrice").text() );
   var _payTot    =  eval( $("#price").val()         );
   <%
      /*
      HashMap hm = DB.getHashData( "select * from new_pricepoint where domain='" + domain + "'", "type", "price" );
      */
      String state_1 = null;
      try {  state_1 = ((HashMap) session.getAttribute( "data" )).get("state_1").toString(); } catch( Exception e ) {}
      HashMap     hm = Queries.pricePoints( domain, state_1 );
      
      
      out.print( Format.jsHash( hm, "pricepoints" ) );
   %>
   if( document.location.href.indexOf("billing.html") ==-1 )
      return;
   var _h = "";
   
  
   splt_desc = desc.split("|"); 
   _h += "<div id='item'><table width='100%' border=0><tr>";
   _h +=    "<td style='vertical-align:middle;' width='20'><img src='https://s3.amazonaws.com/<%=domain%>/img/"+icon+".jpg' style='margin-right:10px;'></td>";
   _h +=    "<td style='vertical-align:middle;'>";
   _h +=    splt_desc[0];

   if( splt_desc.length > 1 )
      _h += "<div style='font-size:10px; color:gray;'>"+splt_desc[1]+"</div>"

   _h +=    "</td>";
   _h +=    "<td width='20' class='cartprice' style='vertical-align:middle; text-align:right; padding-right:10px;'>";
   if( typeof pricepoints[item]=== "undefined" )
      _h += "Free";
   else
      _h += "$"+pricepoints[item];
   _h += "</td>";
   _h += "</tr></table></div>";
   
   

   $("#items").append( _h );
   if( !isNaN(pricepoints[item]) )
      _tot += eval( pricepoints[item] );
   if( cumulate )
      _payTot += eval( pricepoints[item] );
   $("#displayPrice").text( parseFloat(_tot).toFixed(2) );
   $("#price").val(_payTot);
   $("#split").val( "<%=hm.get("Easy Guide")%>" );
}

function closeColorBox()
{
   parent.$.fn.colorbox.close();
}

function setDownload( key )
{
   u = "k=" + key;
   $.ajax({ type: "POST", url: "/dwn", data: u, async: false });
}

function redirect( u )
{
   try
   {
      parent.document.location.replace(u);
   }
   catch( e )
   {
   }   
}

function initCap(str)
{
   s = str.toLowerCase().replace(/\b[a-z]/g, function(letter){ return letter.toUpperCase(); });
   return s.replace(/-/g, " ");
}

function isoDate(str)
{
   dt = new Date(str);
   _m = dt.getUTCMonth() + 1;
   _d = dt.getUTCDate();
   _y = dt.getUTCFullYear();
   return "" + _y + "-" + (_m<10?"0":"") + _m + "-" + (_d<10?"0":"") + _d;
}

function getParam( p )
{
   var hrf = document.location.href;
   var u   = (hrf+"?n").split( "?" )[1].split("&");
   for( var _i=0; _i<u.length; _i++ )
   {
      pair = u[_i].split( "=" );
      if( pair[0]==p )
         return pair[1];
   }
   return "";
}

function isStateSource()
{
   var utm_source = getParam("utm_source").split("_");
   //return ( utm_source[0].length==2 && utm_source[0].toUpperCase()==utm_source[0] && utm_source[1].toLowerCase()=="Drivers" );
   return ( utm_source[0].length==2 && utm_source[1].toLowerCase()=="drivers" );
}


function OLD_getParam( p )
{
   var u = (document.location.href+"?n").split( "?" )[1].split("&");
   for( var _i=0; _i<u.length; _i++ )
   {
      pair = u[_i].split( "=" );
      if( pair[0]==p )
         return pair[1];
   }
   return "";
}

function getUrl( ndx )
{
   u = document.location.href.replace("http://","").replace("https://","").split("?")[0].split( "/" );
   return u[ndx];
}

function getState()
{
   u = document.location.href.split("?")[0].split( "/" );
   return u[u.length-1].split(".")[0];
}

function getPage()
{
   return getState();
}

function homePageRadio()
{
   $("#step1 input:radio").change(function(){
      $("#step1 :radio ").parent(".checkBox").removeClass("checked");
      $("#step1 :radio:checked").parent(".checkBox").addClass("checked");
      $("#step1 :radio:checked").blur();
   });
}

function go2CheckList()
{
   document.location.replace( "/checklist" + sessionData["_FORM_"] );
}



var reverseState = new Array();
<%
      java.util.Iterator it = com.util.Static.STATE_REVERSE_LOOKUP.keySet().iterator();
      while( it.hasNext() )
      {
         String k = (String) it.next();
         String v = (String) com.util.Static.STATE_REVERSE_LOOKUP.get(k);
         %>reverseState["<%=k%>"] = "<%=v.toUpperCase()%>";<%
      }
%>
function getStateCode()
{
   return reverseState[getState()];
}



//---- used for search only
function parseParamsFromUrl()
{
   var params  = new Array( );
   var parts   = window.location.search.substr(1).split('\x26');
   for( var i=0; i<parts.length; i++ )
   {
      var keyValuePair   = parts[i].split('=');
      var key            = decodeURIComponent(keyValuePair[0]);
      params[key]        = keyValuePair[1] ?decodeURIComponent(keyValuePair[1].replace(/\+/g, ' ')):keyValuePair[1];
   }
   return params;
}
pageIndex = 1;   
function paginate()
{
   var $ul = $('ul.pagination');
   $ul.children().remove();
   for( i=pageIndex * 10 - 9; i<=pageIndex * 10; i++ )
   {
      $ul.append('<li>'+i+'</li>');
   }
   $ul.find('li').click(function() {
      $ul.find('li').removeClass("currentPage");
      var no = $(this).text();
      if( no > 10 )
      {
         no = no - (pageIndex - 1) * 10;
	      var exp = 'li:nth-child(' +  no  + ')';
	      $ul.find('li:nth-child(' +  no  + ')').addClass("currentPage"); 
      }
      else
	      $ul.find('li:nth-child(' + no + ')').addClass("currentPage");
      var urlParams  = parseParamsFromUrl();
      var kw         = encodeURIComponent(urlParams['q']);
      $.get('/search.jsp?q=' + kw + '&qi=' + ((no * 10) +1), function(html)
      { 
         $('#searchresults').html(html);
      });
   });
   $ul.append('<li id="next">Next ></li>');
   $ul.find('li:first').addClass("currentPage");
   $ul.find('#next').click(function(){
      pageIndex++;
      paginate();
      var $ul = $('ul.pagination');
      $ul.find('li:first').click();
   });
}


function popWall( url, w, h )
{
   if( url.indexOf("addQ.")!=-1 )
   {
      var q = url.replace( "addQ.", "" );
      $.ajax( {"url":"/ajax/addQ.jsp?trg="+q} );
   }
   else
   {
      popupWindow = window.open(url,"",'height='+h+',width='+w+',resizable=yes,scrollbars=yes,toolbar=no,location=no')
      popupWindow.focus();
   }
}

function checkSurvey( oneYes )
{
   if( oneYes )
   {
      r = false;
      $('input:radio:checked').each(function(){
         if( (""+$(this).attr("onclick")) != "undefined")
         {
            r = true;
            return;
         }
      });
      return r;
   }
   else
      return ($('input:radio:checked').length*2 == $('input:radio').length);
}

function trackDown( typ, lnk )
{
}

function track( typ )
{
   $.ajax({ "url":"/ajax/track.jsp?type="+typ });
}


function getCreditCardType( accountNumber )
{
   if( /^5[1-5]/.test(accountNumber) )   return "mastercard";
   if( /^4/.test(accountNumber) )        return "visa";
   if( /^3[47]/.test(accountNumber) )    return "amex";
   return "other";
}

function isAmex( accountNumber )
{
   return (getCreditCardType(accountNumber)=="amex");
}
function isOther( accountNumber )
{
   if( accountNumber=="1234567890123456" )
      return false;
   return (getCreditCardType(accountNumber)=="other");
}


function cvv()
{
   $.colorbox({iframe:true, innerWidth:"280", innerHeight:"200", href:"/cvv.jsp", "close":"Close",  overlayClose: false});
}

function afterPost( cnt )
{
   $("#err_section").html( cnt );
   $("#err_section").show();

   $("#spin_section").hide();
   $("#button_section").show();
}

function extractNumberJunk( val )
{
   _v = "";
   for( var n=0; n<val.length; n++ )
   {
      if( val.charAt(n)==" " )
         continue;
      try
      {
         evl = eval( val.charAt(n) );
         _v += val.charAt(n);
      }
      catch(e)
      {
      }
   }
   return _v;
}


function correctEmail( o )
{
   trg  = o.val().toLowerCase().replace("www.", "");
   splt = trg.split( "@" );
   if( splt.length!=2 )
      return;
   trg = trg.replace( "..",    "." );
   trg = trg.replace( ".ccom",  ".com" );
   trg = trg.replace( ".comm",  ".com" );
   trg = trg.replace( ".coomm", ".com" );

   trg = trg.replace( ".cox",   ".com" );
   trg = trg.replace( ".xom",   ".com" );
   trg = trg.replace( ".cim",   ".com" );
   trg = trg.replace( ".con",   ".com" );
   
   trg = trg.replace( ".on",    ".com" );
   trg = trg.replace( ".cm",    ".com" );
   trg = trg.replace( ".nt",    ".net" );
   trg = trg.replace( ".co",    ".com" );

   
   trg = trg.replace( "gmial",  "gmail" );
   trg = trg.replace( "gamil",  "gmail" );
   
   if( splt.length == 2 )
   {
      splt = trg.split( "@" );
      dot  = splt[1].split(".")[1];
      dot  = dot==".co"?".com":dot;
      dot  = dot==".ne"?".net":dot;
      
      if( splt[1].indexOf("yaho")!=-1 )   splt[1] = "yahoo." + dot;
      else
      if( splt[1].indexOf("hotm")!=-1 )   splt[1] = "hotmail." + dot;
      else
      if( splt[1].indexOf("comca")!=-1 )  splt[1] = "comcast.net";
      else
      if( splt[1].indexOf("gma")!=-1 )    splt[1] = "gmail.com";
      else
      if( splt[1].indexOf("aol")!=-1 )    splt[1] = "aol.com";
      
   }
   rtn = splt[0] + "@" + splt[1];
   rtn = rtn.replace( ".comm", ".com" );
   rtn = rtn.replace( ".cpm",  ".com" );
   rtn = rtn.replace( ".undefined", ".com" );


   o.val( rtn );
}

var statesUS = new Array();
var statesCA = new Array();
<%
for( int i=0; i<Static.STATE_NAMES.length; i++ )
{
   %>statesUS[<%=i%>]="<%=Static.STATE_NAMES[i].toLowerCase().replace(" ", "-")%>"; <%
}
for( int i=0; i<Static.CA_STATE_NAMES.length; i++ )
{
   %>statesCA[<%=i%>]="<%=Static.CA_STATE_NAMES[i].toLowerCase().replace(" ", "-")%>"; <%
}
%>


function errorPop( ttl, cnt, width, height, callback, isCancel )
{
   html = "";
   html += "<div id='cboxMsg'>";
   html +=     "<div>";
   html +=        "<div style='text-align:center; background:#334872; color:#ffffff; font-size:30px; font-weight:bold; padding:10px;'>" + ttl + "</div>";
   html +=        "<div>"+cnt+"</div>";
   html +=        "<div>&nbsp;</div>";
   html +=     "</div>";
   html +=     "<div style='text-align:center; padding-top:10px;'>";
   html +=        "<span style='font-weight:bold; color:#FFFFFF; background-color:#43AB44; border:1px solid #000000; padding:10px 25px;' onClick='{closeColorBox(); "+callback+"();}'>OK</span>";
   if( isCancel )
   {
      html +=     "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
      html +=     "<span style='font-weight:bold; color:#FFFFFF; background-color:#43AB44; border:1px solid #000000; padding:10px 25px;' onClick='closeColorBox();'>CANCEL</span>";
   }
   html +=     "</div>";
   html += "</div>";
   $.colorbox({html:html, width:width, height:height, onLoad:function() { $('#cboxClose').remove(); }});
}

var addrValues = "";
function checkAddress( typ, titre, businessField, divMsg )
{
   return true;
}
function none()
{
}

function setAddress()
{
   arr = addrValues.split( "\n" );
   for( var i=0; i<arr.length; i++ )
   {
      pairs = arr[i].split( "\t" );
      try { document.frm[pairs[0]].value = pairs[1]; } catch(e) {}
   }
}

function checkBillingForm()
{
   var _hrf = document.location.href;
   var okPay = true;
   $("select:visible, input[type=text]:visible, input[type=tel]:visible").each(function() {
      isSearch = ($(this).attr("search")=="Y");
      if( !isSearch )
      {
         if( $(this).val() == "" )
         {
            $(this).css( "borderColor", "red" );
            okPay = false;
         }
         else
         {
            $(this).css( "borderColor", "" );
         }
      }
   });
   if( okPay )
   {
      $("#submitLoad").show();
      $("#billingSubmit").hide();
      document.frm.submit();
      return true;
   }
   return false;
}



function sCode(stateName)
{
   var s = new Array();
   s['alabama'] = 'AL'; s['alaska'] = 'AK'; s['arizona'] = 'AZ'; s['arkansas'] = 'AR'; s['california'] = 'CA'; s['colorado'] = 'CO'; s['connecticut'] = 'CT'; s['delaware'] = 'DE'; s['florida'] = 'FL'; s['georgia'] = 'GA'; s['hawaii'] = 'HI'; s['idaho'] = 'ID'; s['illinois'] = 'IL'; s['indiana'] = 'IN'; s['iowa'] = 'IA'; s['kansas'] = 'KS'; s['kentucky'] = 'KY'; s['louisiana'] = 'LA'; s['maine'] = 'ME'; s['maryland'] = 'MD'; s['massachusetts'] = 'MA'; s['michigan'] = 'MI'; s['minnesota'] = 'MN'; s['mississippi'] = 'MS'; s['missouri'] = 'MO'; s['montana'] = 'MT'; s['nebraska'] = 'NE'; s['nevada'] = 'NV'; s['new-hampshire'] = 'NH'; s['new-jersey'] = 'NJ'; s['new-mexico'] = 'NM'; s['new-york'] = 'NY'; s['north-carolina'] = 'NC'; s['north-dakota'] = 'ND'; s['ohio'] = 'OH'; s['oklahoma'] = 'OK'; s['oregon'] = 'OR'; s['pennsylvania'] = 'PA'; s['puerto-rico'] = 'PR';s['rhode-island'] = 'RI'; s['south-carolina'] = 'SC'; s['south-dakota'] = 'SD'; s['tennessee'] = 'TN'; s['texas'] = 'TX'; s['utah'] = 'UT'; s['vermont'] = 'VT'; s['virginia'] = 'VA'; s['washington'] = 'WA'; s['west-virginia'] = 'WV'; s['wisconsin'] = 'WI'; s['wyoming'] = 'WY';
   return s[stateName.toString()];
}

function toStateName(abbr) 
{
   var s = new Array();
   <%
      String arrC[] = null;
      String arrN[] = null;
      if( domain.endsWith(".ca") )
      {
         arrC = com.util.Static.CA_STATE_CODES;
         arrN = com.util.Static.CA_STATE_NAMES;
      }
      else
      {
         arrC = com.util.Static.STATE_CODES;
         arrN = com.util.Static.STATE_NAMES;
      }
      for( int i=0; i<arrC.length; i++ )
      {
         %>s["<%=arrC[i]%>"] = "<%=arrN[i]%>";<%
      }
   %>
   return s[abbr.toString()];
}
function toStateHyphen(abbr)
{
   var s = new Array();
   <%
      String arrHC[] = null;
      String arrHN[] = null;
      if( domain.endsWith(".ca") )
      {
         arrHC = com.util.Static.CA_STATE_CODES;
         arrHN = com.util.Static.CA_STATE_NAMES;
      }
      else
      {
         arrHC = com.util.Static.STATE_CODES;
         arrHN = com.util.Static.STATE_NAMES;
      }
      for( int i=0; i<arrC.length; i++ )
      {
         %>s["<%=arrHC[i]%>"] = "<%=arrHN[i].toLowerCase().replace(" ", "-")%>";<%
      }
   %>
   return s[abbr.toString()];
}

        

function getUrlParam(variable) 
{
   var query = window.location.search.substring(1);
   var vars = query.split('&');
   for (var i=0;i<vars.length;i++) 
   {
      var pair = vars[i].split('=');
      if (pair[0] == variable) 
      {
         return pair[1];
      }
   }
}

function capitalize(string) 
{ 
   return string.charAt(0).toUpperCase() + string.slice(1); 
} 

function capWords(str)
{ 
   var words = str.split(" "); 
   for (var i=0 ; i < words.length ; i++)
   { 
      var testwd = words[i]; 
      var firLet = testwd.substr(0,1); 
      var rest   = testwd.substr(1, testwd.length -1) 
      words[i]   = firLet.toUpperCase() + rest 
   } 
   return words.join(" "); 
} 

function postLead( channel, delay, action )
{
  var url = "/confirmLead";
  $.ajax({
      "url": url,
      "async": false,
      "type": "POST",
      "data": "channel=" + channel + "&delay=" + delay + "&action=" + action,
      "success": function(response){
         parent.closeColorBox();
      }
  });
}

function searchArb()
{
   tmp = hrf.split( "/" );
   ste = tmp[tmp.length-1];
   lnk = "/searcharb/" + ste;
   $.colorbox({iframe:true, innerWidth:"800", innerHeight:"80%", href:lnk, "close":"",  "overlayClose": false, onLoad: function() { $('#cboxClose').html("<font color=gray>No Thanks</font>").hide(2).delay(3500).show(2); }});
}

function go2( link )
{
   parent.document.location.replace( link );
}

function getCookie(c_name)
{
   var i,x,y,ARRcookies=document.cookie.split(";");
   for (i=0;i<ARRcookies.length;i++)
   {
      x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
      y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
      x=x.replace(/^\s+|\s+$/g,"");
      if ( x==c_name )
         return unescape(y);
   }
}

/*-------------------*/
/*--- postPay     ---*/
/*-------------------*/
function postPay( msg )
{
   parent.document.location.replace("/dl-confirmation.html");
}

/*---------------------*/
/*--- pdfFormExists ---*/
/*---------------------*/
function pdfFormExists()
{
   var r = false;
   var u = "/ajax/pdfExists.jsp?form=forms/" + _PROFILE["_FORM_"].split("/")[1] + "/" + toStateName(_PROFILE["state_1"]).toLowerCase();



   $.ajax({
      "url"     : u,
      "async"   : false,
      "success" : function(response) { r = (response.indexOf("true")!=-1); }
   });
   return r;
}

function billSwapBack()
{
   $("#submitLoad").hide();
   $("#billingSubmit").show();
}

function today()
{
   d = new Date();
   return new Date(d.getMonth()+1 + "/" + d.getDate() + "/" + d.getFullYear() + " 00:00:00");
}



function isillcPost( flow, first_name, last_name, address_1, city_1, state_1, zip_1, date_of_birth, gender, phone_1, email )
{
   var ifrU = "";
   $.ajax('/isillc.jsp', {
     async: false,
     type: "POST",
     contentType: "application/json",
     dataType: "json",
     data: JSON.stringify({
         "ProgramId": "BD1920F7-E54E-4405-B2EC-3CFFC456AF30",
         "CampaignSegmentId": "F270F159-9414-4474-B117-09CA94D46262",
         "Name": {
             "First": first_name,
             "Last":  last_name,
         },
         "Address": {
             "Line1": address_1,
             "Line2": null,
             "City": city_1,
             "StateOrProvince": state_1,
             "PostalCode": zip_1
         },
         "PersonalInfo": {
             "DateOfBirth": isoDate(date_of_birth),
             "Gender": gender
         },
         "ContactInfo": {
             "Telephone1": phone_1,
             "EMailAddress": email
         },
         "Elections": [{
             "OfferId": "5FFDF235-7A92-481F-9069-DE5C1BB9C9A7",
             "Amount": 1000000
         }],
         "Complete": true
     }),
     success: function (data) {
         var transLink = data.Uri.split("/");
         var transID   = transLink[transLink.length - 1];
         if( flow=="web" )
         {
            ifrU = data.Uri + "?_view=transamerica1&_redirect=https%3A//mydriverslicense.org/transam.jsp%3Fid%3D"+transID;
         }
     },
     error: function (data) {
         console.error(JSON.stringify(data).replace(/\r\n/g,'\n'));
     }
  });
  return ifrU;
}



function slideRestaurant(num)
{
   $("#restaurant-conf").html( num );
   $("#number").html( num );
   $("#gift-card-div").slideDown( "slow", function() {$(this).show();} ); 
}

function slideReward(num)
{
   $("#reward-conf").html( num );
   $("#number").html( num );
   $("#gift-card-div").slideDown( "slow", function() {$(this).show();} );
}



function validateCPF( cpf_field, msg, div )
{
   cpf_field.value = extractNumberJunk(cpf_field.value);
   cpf_number =  cpf_field.value;
   
   if(cpf_number.length != 11)
   {
      $("#errmsg").html( "CPF Inv&aacute;lido" );
      return false;
   }

   var result = 0;
   var      a = [10,9,8,7,6,5,4,3,2];


   
   for(var i = 0; i < a.length; i++)
      result = result + (a[i]*eval(cpf_number.charAt(i)));


   result = result%11;
   result = ((Math.round(result*1))/1);

   if(result < 2)
      first_dig = 0;
   else
      first_dig = (11-result);


   if(first_dig != eval( cpf_number.charAt(9)) )
   {
      $("#errmsg").html( "CPF Inv&aacute;lido" );
      return false;
   }
   a      = [11,10,9,8,7,6,5,4,3,2];
   result = 0;

   for(var i = 0; i < a.length; i++)
      result = result + (a[i]*eval(cpf_number.charAt(i)));

   result     = result%11;
   result     = ((Math.round(result*1))/1);
   second_dig = 11-result;
   return_val = (second_dig == cpf_number[10]);
   if( !return_val )
      $("#errmsg").html( "CPF Inv&aacute;lido" );
   return return_val;
}

function swapAddr()
{
   ship_name      =  document.frm.ship_name.value;
   ship_address   =  document.frm.ship_address.value;
   ship_suite     =  document.frm.ship_suite.value;
   ship_city      =  document.frm.ship_city.value;
   ship_state     =  document.frm.ship_state.value;
   ship_zip       =  document.frm.ship_zip.value;

   lbl_name       =  document.frm.lbl_name.value;
   lbl_address    =  document.frm.lbl_address.value;
   lbl_suite      =  document.frm.lbl_suite.value;
   lbl_city       =  document.frm.lbl_city.value;
   lbl_state      =  document.frm.lbl_state.value;
   lbl_zip        =  document.frm.lbl_zip.value;


   document.frm.ship_name.value     =  lbl_name;
   document.frm.ship_address.value  =  lbl_address;
   document.frm.ship_suite.value    =  lbl_suite;
   document.frm.ship_city.value     =  lbl_city;
   document.frm.ship_state.value    =  lbl_state;
   document.frm.ship_zip.value      =  lbl_zip;

   document.frm.lbl_name.value      =  ship_name;
   document.frm.lbl_address.value   =  ship_address;
   document.frm.lbl_suite.value     =  ship_suite;
   document.frm.lbl_city.value      =  ship_city;
   document.frm.lbl_state.value     =  ship_state;
   document.frm.lbl_zip.value       =  ship_zip;


   return false;
}


function dialogMail()
{
//   $.colorbox({iframe:true, innerWidth:"280", innerHeight:"200", href:"/emailsent.jsp", "close":"Close",  overlayClose: false});
     alert( "Message Sent:\nPlease allow 24 - 48 hours for our team to response.\nThank you" );
}



$(document).ready(function()
{
   var __id    = _PROFILE["id"   ];
   var __email = _PROFILE["email"];
   if( __id    =="" ) __id    = "<%=session.getAttribute( "id"    )%>";
   if( __email =="" ) __email = "<%=session.getAttribute( "email" )%>";
   if( __id=="null" || __email=="null" ) return;
   $('a.download').each(function (index, value) {
      var lnk = $(this).attr("href");
      if( lnk.indexOf("?") > 0 )
         lnk += "&";
      else
         lnk += "?";
      lnk += "id=" + __id + "&email=" + __email;
      lnk  = "/downloadPDF" + lnk;
      $(this).attr( "href", lnk );
   });
});

