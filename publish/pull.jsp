<%@page import="java.util.*, com.util.*"%><%
   String file   = request.getParameter( "file" );
   String domain = Web.getDomainName( request );
   if( file!=null )
   {
      String path = getServletContext().getRealPath( "/content/" + domain + "/" + file );
      byte   b[]  = IO.loadBinaryFile( path );
      response.getOutputStream().write(b);
      response.getOutputStream().flush();
   }
%>