<%@page import="java.net.*, java.util.*, java.io.*, com.util.*"%><%
   String  file   = request.getParameter( "file" );
           file   = file.replace( "|", "#" );
   String  bak    = request.getParameter( "bak"  );
   String  domain = Web.getDomainName( request );
   if( file!=null )
   {
      String            u     =  "http://dev." + domain + "/publish/pull.jsp?file=" + URLEncoder.encode(file, "UTF-8");
      out.println( "<strong>Pulling File from Dev</strong><br><a target='_blank' href='" + u + "'>" + u + "</a><br>" );
      byte              b[]   =  Web.webGet( u );
      out.println( "pulled " + b.length + " bytes<hr>" );
      String            trgF  =  getServletContext().getRealPath( "/content/" + domain + "/" + file );
      String            trgB  =  trgF.replace( File.separator + "content" + File.separator, File.separator + "content" + bak + File.separator );
      String            dirF  =  Format.extractLastToken(trgF, File.separator);
      String            dirB  =  Format.extractLastToken(trgB, File.separator);


      out.println("<p><strong>Create Project (if not exists)</strong><div style='white-space:nowrap;'>mkdir " + dirF+"</div>");
      try { (new File(dirF)).mkdirs(); } catch( Exception e ) { out.print("<div style='color:red'>Failure</div>"); }
      out.println("</p>");


      out.println("<p><strong>Prepares Backup</strong><div style='white-space:nowrap;'>mkdir " + dirB+"</div>");
      try { (new File(dirB)).mkdirs(); } catch( Exception e ) { out.print("<div style='color:red'>Failure</div>"); }
      out.println("</p>");


      out.println("<p><strong>Backup File</strong><div style='white-space:nowrap;'>copy  " + trgF + " to " + trgB+"</div>");
      try { IO.copyFile( trgF, trgB ); } catch( Exception e ) { out.print("<div style='color:red'>Failure</div>"); e.printStackTrace(); }
      out.println("</p>");


      out.println("<p><strong>Push to production</strong><div style='white-space:nowrap;'>create " + trgF+"</div>");
      try
      {
         FileOutputStream  fop = new FileOutputStream( trgF );
         fop.write( b );
         fop.close();
      }
      catch( Exception e )
      {
         out.println("<div style='color:red'>Failure</div>");
      }
      out.println("</p>");


      
   }
%>
