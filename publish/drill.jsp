<%@page import="java.util.*, java.text.*, java.io.*, com.util.*"%><%!
   String dt2String( Date dt ) throws Exception
   {
      SimpleDateFormat  formatter      = new SimpleDateFormat("yyyy-MM-dd");
      return formatter.format(dt);
   }
%><%
   String ip = request.getRemoteAddr().toString();
   if( !(ip.equals("127.0.0.1") || ip.equals("174.61.34.241") || ip.equals("66.229.69.163")) )
      return;
   
   
   response.setContentType( "application/json" );
   String domain  =  Web.getDomainName( request );
   String dir     =  request.getParameter( "ls" );
          dir     =  dir==null?"":dir;
   String path    =  getServletContext().getRealPath( "/content/" + domain + dir );
   
   String from    =  request.getParameter( "from" );
   String to      =  request.getParameter( "to"   );
   
   
   String ls[]    =  (new File(path)).list();
   String jSon    =  "[";
   if( ls!=null )
   {
      for( int i=0; i<ls.length; i++ )
      {
         File     fp    =  new File( path + "/" + ls[i] );
         Date     stamp =  (new Date(fp.lastModified()));
         boolean  range =  ((dt2String(stamp).compareTo(to) <= 0) && (dt2String(stamp).compareTo(from) >= 0));
         boolean  isDir =  fp.isDirectory();
         if( range )
         {
            jSon += (i==0?"":",");
            jSon += "{";
            jSon += "\"file\":\""   + ls[i] + "\",";
            jSon += "\"dir\":"      + isDir + ",";
            jSon += "\"stamp\":\""  + stamp + "\"";
            jSon += "}";
         }
      }
   }
   jSon    +=  "]";
   out.print( jSon );
%>
