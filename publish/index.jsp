<html>
   <%
      String from = request.getParameter( "from" );
             from = (from==null || from.equals(""))?com.util.Format.todayISO().split("@")[0]:from;
      String to   = request.getParameter( "to"   );
             to   = (to==null||to.equals(""))?com.util.Format.todayISO().split("@")[0]:to;
   %>
   <head>
      <script src="/jquery.js"></script>
      <style>
         * {
            cursor      :  default;
            font-family :  verdana, arial;
            font-size   :  10px;
         }
         
         .stamp {
            color:blue;
            font-style:italic;
         }
         
         a:link, a:visited, a:active, a:hover {
            text-decoration:none;
            color:navy;
         }
         
         a:hover {
            color:red;
         }
         
         .tr:hover {
            background:#FFFF00;
         }
         
         input::-webkit-inner-spin-button,
         input::-webkit-outer-spin-button {
            -webkit-appearance: none;
            margin: 0;
         }

         input.date {
            padding:3px;
            border:1px solid gray;
            border-radius:10px;
            outline: none;
         }
         
         .pad {
            width:24px;
         }
         
         .submit {
            display:none;
         }
      </style>
      <script>
         function drill( target, root )
         {
            $.ajax( {   url      : "drill.jsp?from=<%=from%>&to=<%=to%>&ls=" + root,
                        success  : function(data) {
                           htm = "<div><table cellpadding=0' cellspacing='0' border='0'>";
                           if( data=="" )
                           {
                              htm = "<tr><td class='pad'></td><td style='color:red; font-weight:bold;'>NO FILES FOUND</td></tr>";
                           }
                           else
                           for( var i=0; i<data.length; i++ )
                           {
                              path  = root + "/" + data[i].file;
                              subId = path.replace(/\//g, "");
                              
                              htm  += "<tr class='tr'>";
                              
                              if( data[i].dir )
                              {
                                 htm +=   "<td class='pad'>&nbsp;</td>";
                                 htm +=   "<td><img src='/img/dir.png'></td>";
                                 htm +=   "<td><a href='javascript:drill(\"" + subId + "\", \"" + path + "\")'>" + data[i].file  + "</a></td>";
                              }
                              else
                              {
                                 htm +=   "<td class='pad'><input onClick='trickle(this);' id='cbx_" + subId + "' name='cbx' value='" + root + "/" + data[i].file + "' type='checkbox'></td>";
                                 htm +=   "<td><img src='/img/file.png'></td>";
                                 htm +=   "<td>" + data[i].file  + "</td>";
                              }
                              
                              htm +=   "<td width='10'></td>";
                              htm +=   "<td class='stamp'>" + data[i].stamp + "</td>";
                              htm += "</tr>";
                              htm += "<tr id='" + subId + "' style='display:none;'><td></td><td class='content' colspan='4'></td></tr>";
                           }
                           htm += "</table></div>";
                           apnd = $(htm);
                           apnd.hide();
                           try { $("#" + target).show(); $("#" + target).find(".content").append( apnd ); } catch(e) {}
                           apnd.fadeIn();
                        }
                  });
         }
         
         $(document).ready(function()
         {
            drill( "main", "" );
         });
         
         function trickle( o )
         {
            if( $("input[type=checkbox]:checked").length > 0 )
               $(".submit").fadeIn();
            else
               $(".submit").fadeOut();
            if( true )
               return;
            chk = o.checked;
            oV = o.id.substring(4);
            $.each( $("input[type=checkbox]"), function( ndx, o ) {
               oId = o.id.substring(4);
               if( oId.substring(0,oV.length)==oV )
               {
                  o.checked = chk;
               }
            });
         }
         
         function rld()
         {
            lnk = "/publish/?from=" + $("#from").val() + "&to=" + $("#to").val();
            document.location.replace( lnk );
         }
      </script>
   </head>
   <body>
      <p>
         From Date : <input required='required' type="date" class="date" id="from" value="<%=from%>">
         &nbsp;&nbsp;&nbsp;
         To   Date : <input required='required' type="date" class="date" id="to" value="<%=to%>">
         &nbsp;&nbsp;&nbsp;
         <input type="button" onClick="rld();" class='date' value="re-load" style="width:100px; font-size:12px;">
      </p>
      <hr>
      <form action="publishFiles.jsp" target="buffer">
         <div style="height:20px;"><input class='submit' type="submit" value="Publish"></div>
         <div id="main"><div class="content"></div></div>
         <div style="height:20px;"><input class='submit' type="submit" value="Publish"></div>
      </form>
   </body>
   <iframe style="display:none" name="buffer"></iframe>
</html>
