import java.io.*;
import java.util.*;

class Generate
{
   /*-------------------------------------------------------------------------*/
   /*--- main ...                                                          ---*/
   /*-------------------------------------------------------------------------*/
   public static void main( String args[] ) throws Exception
   {
      ArrayList         al          =  DB.getData( "select * from domains d, ip i where id=ip_id and active='Y'" );
      FileOutputStream  fopWorker   =  new FileOutputStream( "./worker.properties" );
      ArrayList         domains     =  new ArrayList();
      String            workHead    =  "";
      String            workCnt     =  "";
      for( int i=0; i<al.size(); i++ )
      {
         HashMap  hm    =  (HashMap) al.get(i);
         String   name  =  hm.get("name"   ).toString().trim();
         String   priIP =  hm.get("private").toString().trim();
         String   pubIP =  hm.get("public" ).toString().trim();
         String   kyy   =  hm.get("kyy"    ).toString().trim();
         String   crt   =  hm.get("crt"    ).toString().trim();
         String   chain =  hm.get("chain"  ).toString().trim();
         String   work  =  name.replace(".", "");
         
         (new File("/usr/share/tomcat7/licenses/ROOT/content/" + name)).mkdirs();
         domains.add( name );
         
         workHead += "," + work;
         workCnt  += "worker." + work + ".type=ajp13";
         workCnt  += "\n";
         workCnt  += "worker." + work + ".host=" + name;
         workCnt  += "\n";
         workCnt  += "worker." + work + ".port=8009";
         workCnt  += "\n";
         
         workCnt  += "worker." + work + ".ping_timeout=1000";
         workCnt  += "\n";
         workCnt  += "worker." + work + ".ping_mode=A";
         workCnt  += "\n";
         workCnt  += "worker." + work + ".connection_pool_timeout=60";
         workCnt  += "\n";
         workCnt  += "worker." + work + ".socket_timeout=10";
         
         workCnt  += "\n\n";
         
         FileOutputStream fopNormal = new FileOutputStream( "./" + name + ".conf"     );
         fopNormal.write( header( name, priIP, "80", work ).getBytes() );
         fopNormal.write( footer().getBytes() );
         fopNormal.close();
         System.out.println( "OK " + name + ".conf" );
         
         if( crt.length()>0 && kyy.length()>0 && chain.length()>0 )
         {
            FileOutputStream fopSSL    = new FileOutputStream( "./" + name + "_SSL.conf" );
            fopSSL.write( header( name, priIP, "443", work ).getBytes() );
            fopSSL.write( ssl(    name, crt, kyy, chain    ).getBytes() );
            fopSSL.write( footer().getBytes() );
            fopSSL.close();
            System.out.print( "OK " );
         }
         else
         {
            System.out.print( "KO " );
         }
         System.out.println( name + "_SSL.conf" );
      }
      
      fopWorker.write( "worker.list=licenses\n\n".getBytes() );
      fopWorker.write( "worker.licenses.type=ajp13\n".getBytes() );
      fopWorker.write( "worker.licenses.host=localhost\n".getBytes() );
      fopWorker.write( "worker.licenses.port=8009\n".getBytes() );
      
      //---- tunning
      fopWorker.write( "worker.licenses.ping_timeout=1000\n".getBytes() );
      fopWorker.write( "worker.licenses.ping_mode=A\n".getBytes() );
      fopWorker.write( "worker.licenses.connection_pool_timeout=60\n".getBytes() );

      fopWorker.write( "worker.licenses.reply_timeout=30000\n".getBytes() );
      fopWorker.write( "worker.licenses.socket_connect_timeout=10000\n".getBytes() );
   // fopWorker.write( "worker.licenses.socket_timeout=10\n".getBytes() );
      
      fopWorker.close();
      
      tomcat( domains );
      hosts(  domains );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- header ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   static String header( String domain, String ip, String port, String jk )
   {
      String cnt = "";

      cnt += "NameVirtualHost " + ip + ":" + port;
      cnt += "\n<VirtualHost "  + ip + ":"  + port + ">";
      cnt += "\n";
      cnt += "\nRewriteEngine on";
      cnt += "\nRewriteCond %{REQUEST_METHOD} ^(TRACE|TRACK)";
      cnt += "\nRewriteRule .* - [F]";
      cnt += "\n";
      cnt += "\n# ------------------------------------------";
      cnt += "\n# --- General ...                        ---";
      cnt += "\n# ------------------------------------------";
      cnt += "\nServerAdmin webmaster@" + domain;
      cnt += "\nServerName www." + domain;
      cnt += "\nServerAlias "    + domain;
      cnt += "\n";
      cnt += "\n# ------------------------------------------";
      cnt += "\n# --- Logging ...                        ---";
      cnt += "\n# ------------------------------------------";
      cnt += "\nErrorLog logs/"  + domain + "-error_log";
      cnt += "\nCustomLog logs/" + domain + "-access_log common";
      cnt += "\n";
      cnt += "\n# ------------------------------------------";
      cnt += "\n# --- MOD JK ...                         ---";
      cnt += "\n# ------------------------------------------";
   // cnt += "\nJkMount /* " + jk.trim();
      cnt += "\nJkMount /* licenses";
      cnt += "\nSSLProtocol all -SSLv2 -SSLv3";


      return cnt;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- footer ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   static String footer()
   {
      return "\n\n</VirtualHost>\n";
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- ssl ...                                                           ---*/
   /*-------------------------------------------------------------------------*/
   static String ssl( String domain, String crt, String kyy, String chain )
   {
      FileOutputStream  fop       = null;
      String            crtFile   = "ssl/" + domain + ".crt";
      String            keyFile   = "ssl/" + domain + ".key";
      String            chainFile = "ssl/" + domain + ".chain";
      
      if(crt.length()  >0) {try { fop = new FileOutputStream(crtFile  ); fop.write( crt.getBytes()   ); fop.close(); } catch( Exception e ) {e.printStackTrace();}}
      if(kyy.length()  >0) {try { fop = new FileOutputStream(keyFile  ); fop.write( kyy.getBytes()   ); fop.close(); } catch( Exception e ) {e.printStackTrace();}}
      if(chain.length()>0) {try { fop = new FileOutputStream(chainFile); fop.write( chain.getBytes() ); fop.close(); } catch( Exception e ) {e.printStackTrace();}}
      
      String            cnt       = "";
                        cnt      += "\n# ------------------------------------------";
                        cnt      += "\n# --- SSL configuration -- need addition ---";
                        cnt      += "\n# ------------------------------------------";
                        cnt      += "\nSSLEngine on";
      
      if( crt.length()   > 0 )  cnt += "\nSSLCertificateFile /etc/httpd/conf.d/"         +  crtFile;
      if( kyy.length()   > 0 )  cnt += "\nSSLCertificateKeyFile /etc/httpd/conf.d/"      +  keyFile;
      if( chain.length() > 0 )  cnt += "\nSSLCertificateChainFile /etc/httpd/conf.d/"    +  chainFile;
      
      cnt += "\nSetEnvIf User-Agent \".*MSIE.*\" nokeepalive ssl-unclean-shutdown";
      cnt += "\nCustomLog logs/ssl_request_log \\";
      cnt += "\n   \"%t %h %{SSL_PROTOCOL}x %{SSL_CIPHER}x \\\"%r\\\" %b\"";
      return cnt;
   }


   /*-------------------------------------------------------------------------*/
   /*--- tomcat ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   static void tomcat( ArrayList name ) throws Exception
   {
      String          code = "";
      FileOutputStream fop = null;
      FileInputStream  fip = new FileInputStream( "./BASICS/server.xml" );
      byte             b[] = new byte[512];
      int               nr = 0;
      String      mainCode = "";
      
      for( int i=0; i<name.size(); i++ )
      {
         // --- create application basic file ---
         String dir = "/usr/share/tomcat7/licenses/ROOT";
         (new File(dir)).mkdirs();
         fop = new FileOutputStream( dir + "/ping.jsp" );
         fop.write( (new Date()).toString().getBytes() );
         fop.write( "<hr>".getBytes() );
         fop.write( "<%=new java.util.Date()%>".getBytes() );
         fop.write( "<hr>".getBytes() );
         fop.write( "<%=com.util.DB.getData(\"select sysdate()\")%>".getBytes() );
         fop.close();
         
         mainCode += "<Host name=\"www."+name.get(i)+"\" appBase=\"licenses\" unpackWARs=\"true\" autoDeploy=\"true\"><Alias>"+name.get(i)+"</Alias><Alias>dev."+name.get(i)+"</Alias>";
         mainCode += "</Host>\n";
      }
      
      while( (nr=fip.read(b))!=-1 )
         code += (new String(b,0,nr));
      
      code = code.replace( "###HOSTS###", mainCode );
      
      fip.close();
      
      fop = new FileOutputStream( "/etc/tomcat7/server.xml" );
      fop.write( code.getBytes() );
      fop.close();
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- hosts ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   static void hosts( ArrayList name ) throws Exception
   {
      FileOutputStream fop = new FileOutputStream( "/etc/hosts" );
      for( int i=0; i<name.size(); i++ )
         fop.write( ("52.205.70.190 dev." + name.get(i) + "\n").getBytes() ); 
      fop.close();
   }
   
}

