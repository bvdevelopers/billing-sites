var statenames = ['alabama','alaska','arizona','arkansas','california','colorado','connecticut','delaware','florida','georgia','hawaii','idaho','illinois','indiana','iowa','kansas','kentucky','louisiana','maine','maryland','massachusetts','michigan','minnesota','mississippi','missouri','montana','nebraska','nevada','new hampshire','new jersey','new mexico','new york','north carolina','north dakota','northern mariana islands','ohio','oklahoma','oregon','palau','pennsylvania','puerto rico','rhode island','south carolina','south dakota','tennessee','texas','utah','vermont','virgin island','virginia','washington','west virginia','wisconsin','wyoming'];
/*
var statenames = [
"alabama", "alaska", "arizona", "arkansas","california",
"colorado", "connecticut", "delaware", "georgia", "florida", "hawaii", 
 "illinois", "indiana", "iowa", "louisiana", "minnesota",
 "north-carolina", "texas", "west-virginia", "wyoming", "Wisconsin"
];
*/
var navi = "{"
    + "\"menu\": {"
    +   "\"category\": {"
    +       "\"hunting\": {"
    +           "\"subcategory\": ["
    +           "\"makeup-and-cosmetics\","
    +           "\"skincare\","
    +           "\"hairstylist-and-barber\","
    +            "\"nails\","
    +            "\"electrolysis\""
    +           "]"
    +        "},"
    +        "\"vehicles\": {"
    +           "\"subcategory\": ["
    +           "]"
    +        "},"
    +        "\"boating\": {"
    +           "\"subcategory\": ["
    +           "]"
    +        "},"
    +        "\"fishing\": {"
    +           "\"subcategory\": ["
    +           "\"commercial-fishing\","
    +           "\"freshwater-commercial-fishing\","
    +           "\"freshwater-fishing\","
    +            "\"new-fishing-license\","
    +           "\"saltwater-commercial-fishing\","
    +           "\"saltwater-fishing\","
    +           "\"sport-fishing\""
    +           "]"
    +        "},"
+            "\"gun-license\": {"
    +           "\"subcategory\": ["
    +           "]"
+            "},"
+            "\"birth-certificates\": {"
    +           "\"subcategory\": ["
    +           "]"
+            "}"
+        "}"
+    "}"
+"}";
//Marketing 2 click ypa Ad terms
var terms = '{'+
    '"category":{'+
      '"hunting":['+
           '{"link_name": "State License Application", "keyword": "hunting license"},'+
           '{"link_name": "Board Exam Study Guides", "keyword": "hunting written exam review"},'+
           '{"link_name": "Job Placement Services", "keyword": "hunting careers"},'+
           '{"link_name": "How-to-Network EBook", "keyword": "how to network"},'+
           '{"link_name": "Beauty School Directory", "keyword": "hunting school"},'+
           '{"link_name": "Scholarship Opportunities", "keyword": "scholarships for college"},'+
           '{"link_name": "Work Study Programs", "keyword": "hunting careers"},'+
           '{"link_name": "Take a Practice Exam", "keyword": "hunting practice test"},'+
           '{"link_name": "Get Financial Aid", "keyword": "financial aid for college"},'+
           '{"link_name": "hunting Job Listings", "keyword": "hunting careers"},'+
           '{"link_name": "Resume Writing Services", "keyword": "resume writing services"},'+
           '{"link_name": "Free Business Cards", "keyword": "free business cards"}'+
      '],'+
      '"vehicles":['+
           '{"link_name": "State License Application", "keyword": "nursing license"},'+
           '{"link_name": "CNA Exam Study Guides", "keyword": "nursing exam study guide"},'+
           '{"link_name": "Job Placement Services", "keyword": "nursing jobs"},'+
           '{"link_name": "Nursing CE Courses", "keyword": "nursing continuing education"},'+
           '{"link_name": "Nursing School Directory", "keyword": "nursing school"},'+
           '{"link_name": "Scholarship Opportunities", "keyword": "scholarships for college"},'+
           '{"link_name": "Work Study Programs", "keyword": "nursing internships"},'+
           '{"link_name": "Take a CNA Practice Exam", "keyword": "nursing exam study guide"},'+
           '{"link_name": "Get Financial Aid", "keyword": "financial aid for college"},'+
           '{"link_name": "Nursing Job Listings", "keyword": "nursing jobs"},'+
           '{"link_name": "CV Writing Services", "keyword": "resume writing services"},'+
           '{"link_name": "Renew Your Nursing License", "keyword": "renew your nursing license"}'+
      '],'+
      '"boating":['+
           '{"link_name": "New Business Licenses", "keyword": "business license"},'+
           '{"link_name": "Small Business Loans", "keyword": "small business loan"},'+
           '{"link_name": "Logo Design Services", "keyword": "business logo design"},'+
           '{"link_name": "Notary Services", "keyword": "notary public"},'+
           '{"link_name": "Get Merchant Processing", "keyword": "credit card processing companies"},'+
           '{"link_name": "Small Business Accountants", "keyword": "small business accounting"},'+
           '{"link_name": "Get Ranked in Google", "keyword": "website optimization"},'+
           '{"link_name": "File For Your EIN Number", "keyword": "ein number"},'+
           '{"link_name": "File Your Articles of Incorporation", "keyword": "articles of incorporation"},'+
           '{"link_name": "Apply For a Line of Credit", "keyword": "business line of credit"},'+
           '{"link_name": "Build Your Own Website", "keyword": "build your own website"},'+
           '{"link_name": "Free Business Cards", "keyword": "free business cards"}'+
      '],'+
      '"fishing":['+
           '{"link_name": "Get a Hunting License", "keyword": "hunting license", "url": "recreationallicenses.org/results.html?searchterm="},'+
           '{"link_name": "Get a Fishing License", "keyword": "fishing license", "url": "yahoo.com"},'+
           '{"link_name": "Buy Hunting Gear", "keyword": "hunting gear"},'+
           '{"link_name": "Find a Fishing Charter", "keyword": "fishing charters"},'+
           '{"link_name": "Check the Local Weather", "keyword": "local weather forecast"},'+
           '{"link_name": "Classifieds - Used Boats", "keyword": "buy used boat"},'+
           '{"link_name": "Find a Hunting Cabin", "keyword": "cabin rentals"},'+
           '{"link_name": "Best Fishing Destinations", "keyword": "fishing trips"},'+
           '{"link_name": "Learn Hunting Laws", "keyword": "hunting laws"},'+
           '{"link_name": "Best Hunting Destinations", "keyword": "hunting trips"},'+
           '{"link_name": "Great Deals on Fishing Rods", "keyword": "best fishing rods"},'+
           '{"link_name": "Reviews on Hunting Riffles", "keyword": "best hunting gear"}'+
      '],'+
      '"gun-license":['+
           '{"link_name": "Apply For a Marriage License", "keyword": "marriage license"},'+
           '{"link_name": "Board Exam Study Guides", "keyword": "marriage certificate copy"},'+
           '{"link_name": "Change Your Name", "keyword": "change my name"},'+
           '{"link_name": "Change Your Address", "keyword": "change my address"},'+
           '{"link_name": "Update Your Drivers License", "keyword": "change drivers license name"},'+
           '{"link_name": "For Sale By Owner", "keyword": "for sale by owner"},'+
           '{"link_name": "Get a Marriage License", "keyword": "marriage license"},'+
           '{"link_name": "Change Your Passport Name", "keyword": "change name on passport"},'+
           '{"link_name": "Last Minute Honeymoon Deals", "keyword": "all inclusive honeymoon packages"},'+
           '{"link_name": "Find Your Marriage Record", "keyword": "marriage license"},'+
           '{"link_name": "Donate Your Wedding Dress", "keyword": "donate my wedding dress"},'+
           '{"link_name": "Credit Card Debt Consolidation", "keyword": "credit card debt consolidation"}'+
      '],'+
      '"birth-certificates":['+
           '{"link_name": "Get A Copy of Your Birth Record", "keyword": "birth record"},'+
           '{"link_name": "Apply for a Passport", "keyword": "passport"},'+
           '{"link_name": "Social Security Benefits", "keyword": "apply for social security"},'+
           '{"link_name": "Background Checks", "keyword": "background checks"},'+
           '{"link_name": "Get Your Vital Records", "keyword": "vital records"},'+
           '{"link_name": "Duplicate Birth Certificate", "keyword": "birth certificate"},'+
           '{"link_name": "Renew Your Drivers License", "keyword": "drivers license renewal"},'+
           '{"link_name": "Apply For Government Programs", "keyword": "government aid"},'+
           '{"link_name": "Identity Theft Protection", "keyword": "identity theft"},'+
           '{"link_name": "Get a Birth Certificate", "keyword": "birth certificate"},'+
           '{"link_name": "Expedite Your Passport", "keyword": "expedite passport"},'+
           '{"link_name": "Get Your Credit Score", "keyword": "credit score"}'+
      ']'+
    '}'+
'}';
// Articles by Cateogry
var listings = '{'+
    '"article":{'+
      '"hunting":['+
           '\"displaying-your-cosmetology-license\",'+
           '\"study-tips\",'+ 
           '\"top-five-discounts\",'+
           '\"top-five-job-hunting-websites\"'+
      '],'+
      '"vehicles":['+
          '\"nurse-equipment-checklist\",'+
           '\"nurses-vs-physicians\",'+ 
           '\"ten-nursing-safety-tips\",'+
           '\"five-reasons-to-become-a-nurse\"'+
      '],'+
      '"boating":['+
           '\"business-license-checklist\",'+
           '\"five-reasons-to-start-a-business\",'+
           '\"how-to-choose-the-right-business-structure\",'+ 
           '\"tax-guide-for-businesses\"'+
      '],'+
      '"fishing":['+
           '\"five-reasons-to-get-a-fishing-license\",'+
           '\"hunting-checklist\",'+ 
           '\"ten-safety-tips-for-gun-owners\",'+
           '\"top-ten-us-fishing-destinations\"'+
      '],'+
      '"gun-license":['+
           '\"five-documents-that-prove-your-marriage\",'+
           '\"how-to-change-your-name-after-marriage\",'+ 
           '\"marriage-checklist\",'+
           '\"top-ten-destination-wedding-locations\"'+
      '],'+
      '"birth-certificates":['+
         '\"birth-certificate-vs-passport\",'+
         '\"destinations-that-allow-birth-certificates-as-id\",'+ 
         '\"five-government-parties-that-require-birth-certificates\",'+
         '\"where-to-store-your-birth-certificate\"'+
      ']'+
    '}'+
'}';

var nationwide = true;
var hrf = document.location.href;
var up = hrf.split("/");
var domain = up[2];
var directory = up[3]
var cat = up[4];
var sub_category = up[6];
var sub_sub_category = up[8];
var page_name = up[up.length-1].substring(0, up[up.length-1].length-5);

//var page = up[9];
/* ============
Note: The url is considered an array where the indexes are divided by '/'. 
in the example url: http://recreationallicenses.org/c/cosmetology/sc/skincare/ss/education/florida.html
ie up[2] equates to 'recreationallicenses.org'
below we take the array length, and use (-1) to get the last position. In this case it's 9, which gives us florida.html
we compare florida.html to index.html( index represents national content). If we are not on national page, set var nationwide to false.
Lastly, we use the array length (9) and use substring to extract the string we want. the (-5) will remove the last 5 characters ".html" to give you the state name
in the url.
============*/

var pageState = up[up.length-1];
var state = pageState.substring(0,pageState.length-5);
if(state != "index" && statenames.indexOf( state.replace(/-/g," ").replace(/\b\w/g, function(m){return m.toUpperCase() } ) ) > -1){
   nationwide = false;
   state = up[up.length-1].substring(0, up[up.length-1].length-5);
}
else
{
  state = pageState.split(".")[0];
}
var s_sub_cat = [
   "education",
   "exams",
   "license",
   "job-search"
];

//console.log(navi);

/*====== START: JSON Parsing ======*/
var navigation = JSON.parse(navi);
var ypa_ads = JSON.parse(terms);
var article_listings = JSON.parse(listings);
/*====== END: JSON Parsing ======*/

/*====== START: State Selection ======*/
function changeState(state_name){  
  state = state_name;
  makeNational = state.replace(/\b\w/g, function(m){return m.toUpperCase() } );
  $('#state-select-label').html(makeNational.replace("Index", "National"));

  $('.search-submit').click(function() {
    window.location = hrf.substring(0,hrf.length-pageState.length)+state_name+".html";
  }); 
}
/*====== END: State Selection ======*/

$(document).ready(function() {
  
/*====== START: Reusable Function ======*/
 //------- Extend JQuery to create a function / method to test if an element exists
 $.fn.exists = function() {
     return this.length !== 0;
 };

 //------- Shuffle Array Indexes
 function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex ;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  return array;
}
   // ====== Secondary menu responsive
$("#nav-trigger span").click(function() {
  if ($("nav#secondary-menu ul").hasClass("expanded")) {
     $("nav#secondary-menu ul.expanded").removeClass("expanded");
     $(this).removeClass("open");
     $("nav#secondary-menu").removeClass('show').slideUp(250);
  } else {
     $("nav#secondary-menu ul").addClass("expanded");
     $(this).addClass("open");
     $("nav#secondary-menu").addClass('show').slideDown(250);
  }
});

/*====== END: Reusable Function ======*/

  // Ad unique identifing class to each page
  if (cat){
    $("body > article").addClass("cat-" + cat.replace('.html',''));
  }
  // Make select 
    makeNational = state.replace(/\b\w/g, function(m){return m.toUpperCase() } );
  //$('#state-select-label').html(makeNational.replace("Index", "National"));
   
  //Change Main nav links to current state
   
  /*
  // Note: this default to index
  */
  $('#menu-main-menu a').each(function(){
    // var path = $(this).attr("href");
    // var navi_links = path.split("/");
    // var navi_statename = navi_links[navi_links.length-1].substring(0, navi_links[navi_links.length-1].length-5);
    // $(this).attr("href",path.replace(navi_statename, state));

  });
  
  // Send active states list to build secondary menu
  if($('.state-page').exists() == true){
    for( var c=0; c<statenames.length; c++ )
    {
       if (hrf.indexOf(statenames[c]) != -1) {
          $("#nav-contain").attr("style", "display: block");
          getSubMenu(statenames[c], "nav-contain");
          //getSubMenu(statenames[c], "sidebar-left");
          break;
       }
    }
  }

  // Hides items if the current page is not a state page
  if ($(".state-page").exists() == false) {
    $("#nav-contain").toggleClass("hide");
  }

  /*====== START: Dropdown Menu effect ======*/
   
  // Initialize function
  dropDown();
  
  // Call function on browser resize so conditional can be checked
  $(window).resize(function() {
     dropDown();
  });
  
  function dropDown() {
     if ($(window).width() > 750) {
         // jquery width is 17px less than the actual window size
         $('#menu-main-menu li').hoverIntent({
             over: function startHover(e) {
                 $(this).addClass('open');
             },
             out: function endHover() {
                 $(this).removeClass('open');
             },
             timeout: 500
         });
     } else {}
  };
  $(document).on('click', '.yamm .dropdown-menu', function(e) {
     e.stopPropagation()
  });

  /*====== START: Page Injection ======*/
  
  // Detects if there is a sidebar on the page
  if($('#secondary').exists() == false){
    $('body').addClass('full-width');
  }
  //alert( up[up.length-1].substring(0, up[up.length-1].length-5).replace(/-/g," ").replace(/\b\w/g, function(m){return m.toUpperCase() } ) );
   
  /*====== START: BREADCRUMBS ======*/
  var crumbs = "<li><a HREF=\"//" + domain + "/" + state + ".html\">Home</a></li>\n";
  if(cat != undefined && Object.keys(navigation.menu.category).indexOf(cat) > -1)
   {
      if(sub_category != undefined)
         crumbs += "<li><a HREF=\"//" + domain + "/c/" + cat + "/" + state + ".html\">"+cat.replace(/-/g," ").replace(/\b\w/g, function(m){return m.toUpperCase()})+"</a></li>\n";
      else
         crumbs += "<li class=\"active\">" + cat.replace(/-/g," ").replace(/\b\w/g, function(m){return m.toUpperCase()}) + "</li>\n";
   }
   
   // alert("Category:" + cat + "\n\n" + navigation.menu.category[cat].subcategory );

   if(sub_category != undefined && navigation.menu.category[cat].subcategory.indexOf(sub_category) > -1)
   {
      if(sub_sub_category != undefined)
         crumbs += "<li><a HREF=\"//" + domain + "/c/" + cat + "/sc/" + sub_category + "/" + state + ".html\">" + sub_category.replace(/-/g," ").replace(/\b\w/g, function(m){return m.toUpperCase()}) + "</a></li>\n";
      else
         crumbs += "<li class=\"active\">"+ sub_category.replace(/-/g," ").replace(/\b\w/g, function(m){return m.toUpperCase()}) +"</li>\n";
   }
   
   if(sub_sub_category != undefined)
   {
      // alert(state + " " + statenames.indexOf( state.replace(/-/g," ").replace(/\b\w/g, function(m){return m.toUpperCase()}) ) );
   
      if(state != undefined && statenames.indexOf( state.replace(/-/g," ").replace(/\b\w/g, function(m){return m.toUpperCase()}) ) > -1) 
         crumbs += "<li><a HREF=\"//" + domain + "/c/" + cat + "/sc/" + sub_category + "/ss/" + sub_sub_category + "/" + state + ".html\">"+sub_sub_category.replace(/-/g," ").replace(/\b\w/g, function(m){return m.toUpperCase()})+"</a></li>\n";
      else
         crumbs += "<li class=\"active\">"+sub_sub_category.replace(/-/g," ").replace(/\b\w/g, function(m){return m.toUpperCase() } )+"</li>\n";         
   }

   if( pageState != undefined && pageState != "index.html" && statenames.indexOf( pageState.replace(/-/g," ").replace(/\b\w/g, function(m){return m.toUpperCase()}) ) < 0 )
   {
      crumbs += "<li class=\"active\">" + pageState.split(".")[0].replace(/-/g," ").replace(/\b\w/g, function(m){return m.toUpperCase() } ) +"</li>\n";
   }
   $('.breadcrumb').html(crumbs);
   /*====== END: BREADCRUMBS ======*/
   
   /*====== START: SIDE MENU ======*/
   
   //alert( "Start Building Side Menu " + Object.keys(navigation.menu.category).length );
   
   if ($('.custom-landing').exists() == true){
   var submenu = document.getElementById("submenu");
   
   for(var m=0; m<Object.keys(navigation.menu.category).length; m++)
   {
      //alert( Object.keys(navigation.menu.category)[m] );
      
      if(cat != undefined && Object.keys(navigation.menu.category)[m] == cat)
      {
                
         var aText = document.createTextNode( Object.keys(navigation.menu.category)[m].replace(/-/g," ").replace(/\b\w/g, function(m){return m.toUpperCase() } ) );
         
         var a = document.createElement("a");
         a.setAttribute("href","//"+domain+"/c/"+Object.keys(navigation.menu.category)[m]+"/"+state+".html");
         a.appendChild(aText);
         
         var li = document.createElement("li");
         li.setAttribute("class","active");
         li.appendChild(a);
         
         var scul = document.createElement("ul");
         scul.setAttribute("class","level-2");
         
         //alert( m + " " + navigation.menu.category[Object.keys(navigation.menu.category)[m]]);
         var sm = navigation.menu.category[Object.keys(navigation.menu.category)[m]].subcategory;
         for(var sc=0; sc<sm.length; sc++){
                     
            if(sub_category != undefined && sub_category == sm[sc])
            {
               var scli = document.createElement("li");
               scli.setAttribute("class","active");

               var scaText = document.createTextNode(sm[sc].replace(/-/g," ").replace(/\b\w/g, function(m){return m.toUpperCase() } ));
                              
               //CHECK FOR SUB SUB MENU PAGE
               if( sub_sub_category != undefined)
               {
                  var sca = document.createElement("a");
                  sca.setAttribute("href", "//"+domain+"/c/"+cat+"/sc/"+sub_category+"/"+state+".html");
                  sca.appendChild(scaText);
                  scli.appendChild(sca);
               }
               //ON THIS PAGE
               else
               {
                  var scspan = document.createElement("span");
                  //scspan.setAttribute("class","blah");
                  scspan.appendChild(scaText);

                  scli.appendChild(scspan);
               }
               
               var ssul = document.createElement("ul");
               ssul.setAttribute("class","level-3");
               
               for(var ss=0; ss<s_sub_cat.length; ss++)
               {
                  var ssli = document.createElement("li");
                  
                  var ssaText = document.createTextNode(s_sub_cat[ss].replace(/-/g," ").replace(/\b\w/g, function(m){return m.toUpperCase() } ));
                  
                  if(sub_sub_category != s_sub_cat[ss]){
                     
                     var ssa = document.createElement("a");
                     ssa.appendChild(ssaText);
                     ssa.setAttribute("href","//"+domain+"/c/"+Object.keys(navigation.menu.category)[m]+"/sc/"+sm[sc]+"/ss/"+s_sub_cat[ss]+"/"+state+".html");
                  
                     ssli.appendChild(ssa);
                  
                  }
                  else
                  {
                     var ssSpan = document.createElement('span');
                     ssSpan.appendChild(ssaText);   
                     ssli.appendChild(ssSpan);   
                  }                     
                  
                  if(sub_sub_category == s_sub_cat[ss])
                     ssli.setAttribute("class","active");
                     
                  ssul.appendChild(ssli);
                  
               }
               
               scli.appendChild(ssul);
               
               scul.appendChild(scli);
               
            }
            else
            {
               var scaText = document.createTextNode(navigation.menu.category[Object.keys(navigation.menu.category)[m]].subcategory[sc].replace(/-/g," ").replace(/\b\w/g, function(m){return m.toUpperCase() } ));
               
               var sca = document.createElement("a");
               sca.setAttribute("href", "//"+domain+"/c/"+cat+"/sc/"+navigation.menu.category[Object.keys(navigation.menu.category)[m]].subcategory[sc]+"/"+state+".html");
               sca.appendChild(scaText);
               
               var scli = document.createElement("li");
               scli.appendChild(sca);
               
               scul.appendChild(scli);
                
            }
            
            li.appendChild(scul);           
         }
         
         submenu.appendChild(li);
        
      }
      else
      {
         var aText = document.createTextNode(Object.keys(navigation.menu.category)[m].replace(/-/g," ").replace(/\b\w/g, function(m){return m.toUpperCase() } ));
         var a = document.createElement("a");
         a.setAttribute("href","//"+domain+"/c/"+Object.keys(navigation.menu.category)[m]+"/"+state+".html");
         a.appendChild(aText);
         
         var li = document.createElement("li");
         li.appendChild(a);
         
         submenu.appendChild(li);

      }
   }
 }
 /*====== END: SIDE MENU ======*/
  
 
  //------- Set Featured Post Images
  if (sub_category === 'makeup-and-cosmetics'){
    $(".post-featured-img").html('<img src="http://s3.amazonaws.com/recreationallicenses.org/img/makeup-and-cosmetics.jpg" alt="">');
  } else if (sub_category === 'electrolysis'){
    $(".post-featured-img").html('<img src="http://s3.amazonaws.com/recreationallicenses.org/img/electrolysis.jpg" alt="">');
  }else if (sub_category === 'hairstylist-and-barber'){
    $(".post-featured-img").html('<img src="http://s3.amazonaws.com/recreationallicenses.org/img/hairstylist-and-barber.jpg" alt="">');
  }else if (sub_category === 'nails'){
    $(".post-featured-img").html('<img src="http://s3.amazonaws.com/recreationallicenses.org/img/nails.jpg" alt="">');
  }else if (sub_category === 'skincare'){
    $(".post-featured-img").html('<img src="http://s3.amazonaws.com/recreationallicenses.org/img/skin.jpg" alt="">');
  }else if (cat =="cosmetology" && sub_category == null){
    $(".post-featured-img").html('<img src="http://s3.amazonaws.com/recreationallicenses.org/img/cosmetology.jpg" alt="">');
  }

   // Creates HTML elements for each parent category, based on the amount of Sub Category and Sub-sub categories
  m1 = "";

  if (typeof navigation.menu.category[cat] !== "undefined")
  {
    for( var i=0; i<navigation.menu.category[cat].subcategory.length; i++ ){
      m1 += "," + navigation.menu.category[cat].subcategory[i];
    }
  }
  
  if ($(".parent-cat").exists() == true && $(".state-page").exists() == true ) {
   
   for( var i=0; i<menus[state].length; i++ )
   {
    lrc = menus[state][i].replace(/ /g, '-').toLowerCase();
    if( m1.indexOf(lrc)!=-1 )
    {
      $('.popular-posts').append('<article class="' + i + '"><h2></h2><p></p><span class="read-more"><span></article>');
    }
   }
/*
    for (i = 0; i < navigation.menu.category[cat].subcategory.length; i++) {
      $('.popular-posts').append('<article class="' + i + '"><h2></h2><p></p><span class="read-more"><span></article>');
    }
*/
   }else if ($(".sub-cat").exists() == true) {
      // for (i = 0; i < s_sub_cat.length; i++)
      if (typeof menus[state] !== "undefined"){
        for ( var i=0; i<menus[state].length; i++) {
          lrc = menus[state][i].replace(/ /g, '-').toLowerCase();
          if( m1.indexOf(lrc)!=-1 ){
            $('.popular-posts').append('<article class="' + i + '"><h2></h2><p></p><span class="read-more"><span></article>');
          }
        }
      } else{
        // Menu state is undefined, set it to the state of FL so the popular posts can show
        for ( var i=0; i<menus['florida'].length; i++) {
          lrc = menus['florida'][i].replace(/ /g, '-').toLowerCase();
          if( m1.indexOf(lrc)!=-1 ){
            $('.popular-posts').append('<article class="' + i + '"><h2></h2><p></p><span class="read-more"><span></article>');
          }
        }
      }
   } else{
      //.parent-cat doesn't exists
   }

   //CATEGORY BLOCK
   if ((hrf.indexOf("/c/") != -1) && (hrf.indexOf("/sc/") == -1)) {
      //------- Print Location
     $(".custom-landing").addClass("cat-" + cat);
     if($('.popular-posts').exists() ==true){
        // Populates newly created elements according to the number of categories
       for (i = 0; i < navigation.menu.category[cat].subcategory.length; i++) {
           //------- Popular posts Insert
           // Title
           $(".popular-posts article." + i + " h2").load("/c/"+cat+"/sc/" + navigation.menu.category[cat].subcategory[i] + "/" + state + ".html .page-title >* ");
           // Intro Content from Page
           $(".popular-posts article." + i + " p").load("/c/"+cat+"/sc/" + navigation.menu.category[cat].subcategory[i] + "/" + state + ".html #intro-para");
           // Read more link
           $(".popular-posts article." + i + " .read-more").load("/c/"+cat+"/sc/" + navigation.menu.category[cat].subcategory[i] + "/" + state + ".html .page-title >* ", function() {
               $(this).children('a').html('Read More');
           });
       }
     }
     //SUB CATEGORY BLOCK
   } else if ((hrf.indexOf("/c/") != -1) && (hrf.indexOf("/sc/") != -1) && (hrf.indexOf("/ss/") == -1)) {
      //------- Print Location
      $(".custom-landing").addClass("cat-" + cat+ " sub-cat-" + sub_category);
       // Populates newly created elements according to the number of categories
      if($('.popular-posts').exists() ==true){
       for (var i=1; i<menus[state].length; i++) {
         transformed_subcat = menus[state][i].replace(/ /g, '-').toLowerCase();
         //------- Popular posts Insert
         // Title
         $(".popular-posts article." + i + " h2").load("/c/"+cat+"/sc/" + transformed_subcat+ "/" + state +".html .page-title >* ");
         // Intro Content from Page
         $(".popular-posts article." + i + " p").load("/c/"+cat+"/sc/" + transformed_subcat+ "/" + state + ".html #intro-para");
         // Read more link
         $(".popular-posts article." + i + " .read-more").load("/c/"+cat+"/sc/" + transformed_subcat+ "/" + state + ".html .page-title >* ", function() {
             $(this).children('a').html('Read More');
         });
       }
     }
     //SUB SUB CATEGORY BLOCK
   } else if ((hrf.indexOf("/c/") != -1) && (hrf.indexOf("/sc/") !== -1) && (hrf.indexOf("/ss/") != -1)) {
         //------- Print Location
        $(".custom-landing").addClass("cat-" + cat + " sub-cat-" + sub_category + " s-sub-cat-" + sub_sub_category);
        
   }

  //------- Insert PDF Article
  // PDFs listed by page name
  var pdf_page_select = [
  'what-type-of-cosmetologist-are-you', 
  'fishing-trip-checklist', 
  'checklist-for-starting-a-business',
  'making-sure-your-birth-certificate-is-official',
  'nursing-school-education-tracks',
  '5-tips-for-a-healthy-marriage'
  ]; 

  var blog_posts = ['displaying-your-cosmetology-license', 'study-tips', 'top-five-discounts', 'top-five-job-hunting-websites'];
  // Randomly select Pdf page
  //shuffle(pdf_page_select);


  // Create clever way to use all possible terms for fishing pages
  var rec_terms = ['gun','fire','fish','hunt','job'];
  var rec_list;
   
   // for (i = 0; i < rec_terms.length; i++) {
   //      rec_list += ("|| cat.indexOf('"+rec_terms[i]+"') != -1");
   // };
  //alert(rec_list);
  if($('.child-page').exists() == true){
      // if in recreation
      /*
      if(cat =='fishing' || cat.indexOf('gun') != -1 || cat.indexOf('fire') != -1 || cat.indexOf('fish') != -1 || cat.indexOf('hunt') != -1 && cat.indexOf('job') == -1){
       createChildPage( pdf_page_select[1]);
      } else if(cat =='boating'  || cat.indexOf('boating') != -1){
       createChildPage( pdf_page_select[2]);
      } else if(cat =='birth-certificates' || cat.indexOf('birth') != -1){
       createChildPage( pdf_page_select[3]);
      } else if(cat =='vehicles' || cat.indexOf('vehicles') != -1){
       createChildPage( pdf_page_select[4]);
      } else if(cat =='gun-license' || cat.indexOf('gun-license') != -1){
       createChildPage( pdf_page_select[5]);
      }else{
        createChildPage( pdf_page_select[0]);
      }
      */
        // ==================================
        // Temporarily force all content to fishing
        createChildPage( pdf_page_select[1]);
  }

  function createChildPage (selected_pdf) {
     // Title
     $(".child-page h3").load("/pdf/" + selected_pdf + ".html .page-title >* ");
     // Intro Content from Article
     $(".child-page .summary .excerpt").load("/pdf/" + selected_pdf + ".html #intro-para");
     // PDF Btn
     $(".child-page .pdf-btn").load("/pdf/" + selected_pdf + ".html .print-pdf-wrap >* ", function() {
         $(this).children('a').addClass('btn btn-block btn-primary');
         $(this).children('a').html('<i class="glyphicon glyphicon-print"></i> <span>Print PDF</span>');
     });
     // Read More Btn
     $(".child-page .read-more").load("/pdf/" + selected_pdf + ".html .page-title >* ", function() {
         $(this).children('a').addClass('btn btn-block btn-primary');
         $(this).children('a').html('<i class="glyphicon glyphicon-chevron-right"></i> <span>Read More</span>');
     });
  }
  //------- Insert Blog Articles
  
  // .child-category should only exist on pages that wants to populate articles
  if($('.child-category').exists() == true){
     // If the directory isn't 'article' the program assumes we are in a category page
     if (directory != 'article'){
        //createBlogExcerpts(article_listings.article[cat]);
        // ==================================
        // Temporarily force all content to fishing
        createBlogExcerpts(article_listings.article['fishing']);
     }
     //cat is represented as the file-name.html in the url
     // the code below search for a keyword in the page name. Then sets the JSON category based on that.
     /*
     else if (cat.indexOf('nurse') != -1 || cat.indexOf('vehicles') != -1){
      // createBlogExcerpts(article_listings.article['vehicles']);
     
     }else if (cat.indexOf('boating') != -1){
      createBlogExcerpts(article_listings.article['boating']);
     
     }else if (cat.indexOf('gun') != -1 || cat.indexOf('fire') != -1 || cat.indexOf('fish') != -1 || cat.indexOf('hunt') != -1 && cat.indexOf('job') == -1) {
      createBlogExcerpts(article_listings.article['fishing']);
     
     }else if (cat.indexOf('gun-license') != -1){
      createBlogExcerpts(article_listings.article['gun-license']);
     
     }else if (cat.indexOf('birth') != -1){
      createBlogExcerpts(article_listings.article['birth-certificates']);
     }
     */
     else{
      // Display fishing as the default
      createBlogExcerpts(article_listings.article['fishing']);
     }
  }
  function createBlogExcerpts(sort_by){
      // loop is based on the current category defined by the url. The program will loop through all articles for that category
      for (i = 0; i < sort_by.length; i++) {
           // Create blog post html elements
           $('.child-category .well .row').append('<article class="col-xs-12 ' + i + '"><div></div><h4></h4><p></p><span class="read-more"><span></article>');
           $('.child-category article div').addClass("img-wrap pull-left")

           // Post Feature image
           $("article.col-xs-12." + i + " .img-wrap").load("/article/"+sort_by[i]+".html .post-featured-img >* ");
           // Title
           $("article.col-xs-12." + i + " h4").load("/article/"+sort_by[i]+".html .page-title >* ");
           // Intro Content from Page
           /*
           --- trying to truncate the string by word count */
           $("article.col-xs-12." + i + " p").load("/article/"+sort_by[i]+".html  #intro-para", function(){
              string = $(this).text();
              var res = string.substring(0, 350);
              //wordcount = string.trim().replace(/\s+/gi, ' ').split(' ').length;
              //alert(wordcount);
              $(this).html(res+"...");

           });
           // Read more link
           $("article.col-xs-12." + i + " .read-more").load("/article/"+sort_by[i]+".html  .page-title >* ", function() {
               $(this).children('a').html('Continue Reading');
           });
       }
  }
  //------- Create and Populate 2 click ads
  // Sort through JSON objects like an array:  Object.keys(ypa_ads.category).length;
  // if page is not a pdf and the 2-click ads via related-search doesn't exist

  if(directory != 'pdf' || $('.related-search-title').exists() == true){
    if((hrf.indexOf("/c/") != -1)){
      // You are in category pages. use cat based on url
      // createTwoClickAds(ypa_ads.category[cat]);
      // ==================================
      // Temporarily force all content to fishing
      createTwoClickAds(ypa_ads.category['fishing']);
    } else if(directory =='article'){
        // Directory is in articles
        /*
        if (cat.indexOf('nurse') != -1 || cat.indexOf('vehicles') != -1){
          createTwoClickAds(ypa_ads.category['vehicles']);
        
        }else if (cat.indexOf('boating') != -1){
          createTwoClickAds(ypa_ads.category['boating']);

        }else if (cat.indexOf('gun') != -1 || cat.indexOf('fire') != -1 || cat.indexOf('fish') != -1 || cat.indexOf('hunt') != -1){
        createTwoClickAds(ypa_ads.category['fishing']);

        }else if (cat.indexOf('gun-license') != -1){
        createTwoClickAds(ypa_ads.category['gun-license']);

        }else if (cat.indexOf('birth') != -1){
        createTwoClickAds(ypa_ads.category['birth-certificates']);      

        }else{
          //default to cosmetolgy category
          createTwoClickAds(ypa_ads.category['hunting']);
        }
        */

         //default to cosmetolgy category
          createTwoClickAds(ypa_ads.category['fishing']);
    } else{
      // You're on a corporate pages; don't build links
    }
  }
  function createTwoClickAds(filter_by){
     for(i = 0; i<filter_by.length; i++){
        // Create 2-click ad html elements
        $('ol#wrapper').append('<div class="block ' + i + '"><li class="textBlock"><a class="adlink"></a></li></div>');         
        $('ol#wrapper div.block.'+i+' a').attr('href','/results.html?searchterm='+filter_by[i].keyword);
        //$('ol#wrapper div.block.'+i+' a').attr('href','//'+filter_by[i].url+filter_by[i].keyword);
        $('ol#wrapper div.block.'+i+' a').append(filter_by[i].link_name);
      }
  }
 /*====== START: Mini Ypa ======*/
  if($('.ad-block').exists() == true){
  // If the search term is empty or undefined, pass the page name to the variable; if not, use the search term
  var searching = (mini_ypa_term != null && mini_ypa_term !='')? mini_ypa_term: $('.page-title a').html();
  // Show fewer ads in Cosmetology category
  var desktop_ad_range = (cat == 'hunting' || cat == 'fishing')? '1-1': '1-4';
  /*
  if (cat == 'fishing'){
    if(page_name =='index' || page_name =='colorado' || page_name =='florida' || page_name =='texas' || page_name =='california'){
      aff_id = 259248;
    }
  }else{
      aff_id = 259247;
  }
  */
  aff_id = 259249;
  window.ypaAds.insertMultiAd({
     ypaAdConfig: '000000510',
     ypaAdTypeTag: aff_id,
     //ypaAdTrafficType:"test",
     SponsoredSearch: {
         Mobile: {
             SrcTag: "litmus_search-ypa-mobile1"
         }
     }, 
     ypaPubParams: {
         query: searching  // variable assigned from Landing pages
     },
     ypaAdSlotInfo: [{
         ypaAdSlotId: 'licenses',
         ypaAdDivId: 'ypaAdWrapper-licenses',
         ypaAdWidth: '600',
         ypaAdHeight: '451',
         ypaSlotOptions: {
             AdOptions: {
                 Mobile: {
                     AdRange: "1-1",
                     Lat: false,
                     Favicon: true,
                     LocalAds: true,
                     SiteLink: false,
                     MerchantRating: false,
                     ImageInAds: false
                 },
                 DeskTop: {
                     AdRange: desktop_ad_range,
                     Lat: false,
                     Favicon: true,
                     LocalAds: false,
                     SiteLink: false,
                     MerchantRating: true,
                     ImageInAds: false
                 }
             }, //Ad Options end
             TemplateOptions: {
                 Mobile: {
                     AdUnit: {
                         // backgroundColor: "#ABABAB",
                         // borderColor: "#ABABAB",
                         lineSpacing: 15, // valid values 8-25
                         adSpacing: 15, // valid values 5-30
                         //font: "Verdana",
                         urlAboveDescription: false,
                         adLayout: 1 // or 2/3, show ad in 1 line, 2 lines or 3 lines 
                         //cssLink: "partnercsslocaiton.com/mycss.css"
                     },
                      AdUnitLabel: {
                        position: "TopLeft", //"TopRight"/"BottomLeft"/"BottomRight"
                        fontsize: 8, // allowed values 6-24
                        color: "#123123"
                     },
                     Title: {
                         fontsize: 15, // valid value 8-18
                         // color: "#ABABAB",
                         // underline: false,
                         // bold: false,
                         // onHover: {
                         //     color: "#BCBCBC",
                         //     underline: false
                         // }
                     },
                     Description: {
                         fontsize: 14, // valid value 8-18
                         // color: "#ABABAB",
                         // underline: false,
                          bold: false,
                         // onHover: {
                         //     color: "#BCBCBC",
                         //     underline: false
                         // }
                     },
                     URL: {
                         fontsize: 14, // valid value 8-18
                         // color: "#ABABAB",
                          underline: true,
                         // bold: false,
                         // onHover: {
                         //     color: "#BCBCBC",
                         //     underline: false
                         // }
                     },
                     LocalAds: {
                         fontsize: 13, // valid value 8-18
                         // color: "#ABABAB",
                         // underline: false,
                         // bold: false,
                         // onHover: {
                         //     color: "#BCBCBC",
                         //     underline: false
                         // }
                     },
                     MerchantRating: {
                         fontsize: 12, // valid value 8-18
                         // color: "#ABABAB",
                         // underline: false,
                         // bold: false,
                         // onHover: {
                         //     color: "#BCBCBC",
                         //     underline: false
                         // }
                     },
                     SiteLink: {
                         fontsize: 12, // valid value 8-18
                         // color: "#ABABAB",
                         // underline: false,
                         // bold: false,
                         // onHover: {
                         //     color: "#BCBCBC",
                         //     underline: false
                         // }
                     },
                     // ImageInAds: {
                     //     align: "left" // or "right", 
                     //     size: "30x50", // widthxheight, default 20X20 formobile
                     //     appendToImageSrcUrl: "a=5&b=home"
                     // }
                 },
                 DeskTop: {
                     AdUnit: {
                         // backgroundColor: "#ABABAB",
                         // borderColor: "#ABABAB",
                         // lineSpacing: 15,  valid values 8-25)
                         // adSpacing: 15, // valid values 5-30)
                         // font: "Verdana",
                         // urlAboveDescription: true,
                         cssLink: "http://recreationallicenses.org/cdn/css/ypa-mods.css"
                     },
                     /*
                     AdUnitLabel: {
                         position: "TopLeft", //"TopRight"/"BottomLeft"/"BottomRight"
                         fontSize: 8, // allowed values 6-24
                         color: "#123123"
                     },
                     */
                     Title: {
                         fontsize: 14, // valid value 8-18
                         // color: "#ABABAB",
                         // underline: false,
                         // bold: false,
                         // onHover: {
                         //     color: "#BCBCBC",
                         //     underline: false
                         // }
                     },
                     Description: {
                         fontsize: 13, // valid value 8­18
                         // color: "#ABABAB",
                         // underline: false,
                         // bold: false,
                         // onHover: {
                         //     color: "#BCBCBC",
                         //     underline: false
                         // }
                     },
                     URL: {
                         fontsize: 12, // valid value 8-18
                         // color: "#ABABAB",
                         // underline: false,
                         // bold: false,
                         // onHover: {
                         //     color: "#BCBCBC",
                         //     underline: true
                         // }
                     },
                     // ImageInAds: {
                     //     align: left, //left or right 
                     //     size: "50x50", //widthXheight, default 50X50 for Desktop
                     //     appendToImageSrcUrl: "a=5&b=home"
                     // }
                 }
             }
         } // ypaSlotOptions
     }]
  });
}
/*====== END: Mini Ypa ======*/

for( var i=0; i<statenames.length; i++ )
{
  $("#listate").append( "<li><a href='/c/fishing/sc/general-information/"+statenames[i] + ".html'>" + initCap(statenames[i])+ "</a></li>" );
}

}); // end ready function
var submenuJSON = "{" 
+ "\"alabama\":[\"General Information\",\"Freshwater Fishing\",\"Saltwater Fishing\"]," 
+ "\"alaska\":[\"General Information\",\"Sport Fishing\",\"Commercial Fishing\"]," 
+ "\"arizona\":[\"General Information\",\"New Fishing License\",\"Commercial Fishing\"]," 
+ "\"arkansas\":[\"General Information\",\"New Fishing License\",\"Commercial Fishing\"]," 
+ "\"florida\":[\"General Information\",\"Freshwater Fishing\",\"Saltwater Fishing\"]," 
+ "\"georgia\":[\"General Information\",\"Freshwater Fishing\",\"Saltwater Fishing\"]," 
+ "\"california\":[\"General Information\",\"Commercial Fishing\",\"Sport Fishing\"],"
+ "\"colorado\":[\"General Information\",\"Commercial Fishing\",\"New Fishing License\"]," 
+ "\"connecticut\":[\"General Information\",\"Freshwater Fishing\",\"Saltwater Fishing\"]," 
+ "\"connecticut\":[\"General Information\",\"Freshwater Fishing\",\"Saltwater Fishing\"]," 
+ "\"delaware\":[\"General Information\",\"Commercial Fishing\",\"New Fishing License\"],"
+ "\"hawaii\":[\"General Information\",\"Commercial Fishing\",\"Freshwater Fishing\"],"
+ "\"idaho\":[\"General Information\",\"Commercial Fishing\",\"New Fishing License\"],"
+ "\"illinois\":[\"General Information\",\"Commercial Fishing\",\"New Fishing License\"],"
+ "\"indiana\":[\"General Information\",\"Commercial Fishing\",\"New Fishing License\"],"
+ "\"iowa\":[\"General Information\",\"Standard Fishing\",\"Trout Fishing License\"],"
+ "\"kentucky\":[\"General Information\",\"Commercial Fishing\",\"New Fishing License\"],"
+ "\"louisiana\":[\"General Information\",\"Freshwater Fishing\",\"Saltwater Fishing\"]," 
+ "\"maine\":[\"General Information\",\"Freshwater Fishing\",\"Saltwater Fishing\"]," 
+ "\"maryland\":[\"General Information\",\"Freshwater Fishing\",\"Saltwater Fishing\"]," 
+ "\"massachusetts\":[\"General Information\",\"Freshwater Fishing\",\"Saltwater Fishing\"]," 
+ "\"michigan\":[\"General Information\",\"Commercial Fishing\",\"New Fishing License\"]," 
+ "\"minnesota\":[\"General Information\",\"Commercial Fishing\",\"New Fishing License\"]," 
+ "\"mississippi\":[\"General Information\",\"Freshwater Fishing\",\"Saltwater Fishing\"]," 
+ "\"missouri\":[\"General Information\",\"Freshwater Fishing\",\"Saltwater Fishing\"]," 
+ "\"montana\":[\"General Information\",\"Commercial Fishing\",\"Recreational Fishing\"]," 
+ "\"new-hampshire\":[\"General Information\",\"Freshwater Fishing\",\"Saltwater Fishing\"],"
+ "\"new-jersey\":[\"General Information\",\"Freshwater Fishing\",\"Saltwater Fishing\"],"
+ "\"new-mexico\":[\"General Information\",\"Commercial Fishing\",\"New Fishing License\"],"
+ "\"nevada\":[\"General Information\",\"Commercial Fishing\",\"New Fishing License\"]," 
+ "\"north-carolina\":[\"General Information\",\"Freshwater Fishing\",\"Saltwater Fishing\"],"
+ "\"texas\":[\"General Information\",\"Freshwater Fishing\",\"Saltwater Fishing\"],"
+ "\"west-virginia\":[\"General Information\",\"Standard Fishing\",\"Trout Fishing License\"],"
+ "\"wyoming\":[\"General Information\",\"Commercial Fishing\",\"New Fishing License\"],"
+ "\"wisconsin\":[\"General Information\",\"Commercial Fishing\",\"New Fishing License\"]" + "}";


var pagename = "";
var menus = JSON.parse(submenuJSON);
function getSubMenu(state, submenu) {
  var menu = menus[state];

  for (var m = 0; m < menu.length; m++) {
    var _lnk = "/c/"+cat+"/sc/" + menu[m].replace(/ /g, '-').toLowerCase() + "/" + state + ".html";
    $("#" + submenu + " ul").append("<li><a href=\""+_lnk+"\">" + menu[m] + "</a></li>");
  }
}

function setMapLinks(state) {
    // var _lnk = "/c/"+cat+"/" + state + ".html";
    var _lnk = "/c/"+cat+"/sc/general-information/" + state + ".html";
    return _lnk;
}

function getPageName(hrf) {
  pagename = hrf.split("/")[hrf.split("/").length - 1].split(".")[0].replace(/-/g, ' ');

  if (pagename == "index")
    pagename = hrf.split("/")[hrf.split("/").length - 2]

  return pagename;
}

function go2State(o) {
  document.location.href = "/" + o.value + ".html";
}


