jQuery(function($) {
  
   //==== Sticky Sidebar
   var $sidebar = $(".sidebar"),
      $window = $(window),
      offset = $sidebar.offset(),
      topPadding = 20,
      $beginScrollAfter = 322;

   // Gets height of main nav and secondary and combines them in order to give appropriate margin to sticky sidebar
   // note: when the secondary nav isn't present, the height is 0.
   var headerHeight = $('#header').height() + $('#nav-contain').height();

   /* ========= Removing Sticky sidebar*/
   $window.scroll(function() {
      // only make sidbar sticky on desktop view
      if ($(window).width() >= 990) {
         var start = $("#content > .content-wrap > .container").offset().top,
            end = $("body").height() - ($(".sidebar.right").height() + $("footer").height()),
            distance = 0;

         //console.log("Tracking Pos: " + $beginScrollAfter + " WINDOW scroll: " + $window.scrollTop() + " CONTAIN Offset:" + $sidebar.offset().top + " Sidebar height:" + $sidebar.height());

         //console.log("start value: " + start + " end value: " + end + " ");

         if ($window.scrollTop() > start && $window.scrollTop() < end) {
            $sidebar.css({
               "top": $window.scrollTop() - start,
               "position": "relative",
               "margin-top": headerHeight

            });
         } else if ($window.scrollTop() < start) {
            $sidebar.css({
               "position": "static",
               "margin-top": 0
            });
         }
      }
   });
   
   // ====== Secondary menu responsive
   $("#nav-trigger span").click(function() {
      if ($("nav#secondary-menu ul").hasClass("expanded")) {
         $("nav#secondary-menu ul.expanded").removeClass("expanded");
         $(this).removeClass("open");
         $("nav#secondary-menu").removeClass('show').slideUp(250);
      } else {
         $("nav#secondary-menu ul").addClass("expanded");
         $(this).addClass("open");
         $("nav#secondary-menu").addClass('show').slideDown(250);
      }
   });


   // ====== Form Validate: add '*' to labels that have required fields
   requiredLabel = $('label[req="Y"]');
   
   // if ($('label[req="Y"]').exists() == true) {
   //    requiredLabel.append('<span> *</span>');
   // }


});
//Bread crumb script - Kevin Lynn Brown
//Duplicate directory names bug fix by JavaScriptKit.com
function breadCrumb() {
   var path = "";
   var href = document.location.href;
   var s = href.split("?")[0].split("/");

   var stateindex = false;

   if (s[s.length - 1].replace(/.html|-/g, "").toLowerCase() == "index")
      stateindex = true;

   for (var i = 2; i < (s.length - 1); i++) {

      if (stateindex && i == (s.length - 2))
         path += "<li class=\"active\">" + s[i] + "</li> ";
      else
      {
      // path += "<li><a HREF=\"" + href.substring(0, href.indexOf("/" + s[i]) + s[i].length + 1) + ".html\">" + s[i] + "</a></li> ";
         path += "<li>" + s[i] + "</li> ";
      }
   }

   i = s.length - 1;

   // last path in the array will contain the active class
   if (!stateindex)
      path += "<li class=\"active\">" + s[i].replace(/.html|-/g, " ") + "</li>";

   document.writeln(path);
}
/*
 * Usage: create your link calling the function and passing it ID
 * <a href="javascript:scrollToElementId('here')">Read more</a>
 * Then give an element <div id="here"> the id name you want to link to
 */
function scrollToElementId(elm) {
   $('html, body').animate({
      scrollTop: $('#' + elm).offset().top
   }, 500);
}
