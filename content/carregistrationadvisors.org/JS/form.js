$(document).ready(function()
{
   // --- phone_1 focus
   $("#phone_1").focus( function() { $("#_msg_phone_1").removeClass("err_on").addClass("hint_msg"); $("#_msg_phone_1").html("Can be same as cell Phone");  } );
   $("#phone_1").blur(  function() { $("#_msg_phone_1").removeClass("hint_msg").addClass("err_on"); $("#_msg_phone_1").html("");                           } );
   
   // --- blueKai
   var _h = document.location.href;
   if( _h.indexOf("/forms/") != -1 )
   {
      // --- set H1 content (start)
      var mapH1 = new Array();
      mapH1[ "new-registration" ]         =  "Get Your [State] Car Registration Handbook";
      mapH1[ "renew-registration" ]       =  "Get Your [State] Car Registration Handbook";
      mapH1[ "replace-registration" ]     =  "Get Your [State] Car Registration Handbook";
      mapH1[ "change-of-name" ]           =  "Get Your [State] Car Registration Handbook";
      mapH1[ "change-of-address" ]        =  "Get Your [State] Car Registration Handbook";
      splt = document.location.href.split( "/" );
      cnt  = mapH1[ splt[5] ].replace( "[State]", initCap(splt[6].replace(".html", "")) );
      $("#formh1").html( cnt );
   }   
   
   if( _h.indexOf("/step1/") != -1 )
   {
      try
      {
         var state = initCap(document.location.href.split("/")[6].split(".")[0]);  //get state from url
         //$("#formh1").html( "Get your "+ state +" car registration application and checklist" );
         $("#state_licensed").val( state );
      }
      catch(e){}
      
      $("#agree_tc").css('margin-left','5px');
      
      var map =   {  "new-registration"      : ["New Registration",                    "Licensed Driver"],
                     "renew-registration"    : ["Renew Registration",                  "Licensed Driver"],
                     "replace-registration"  : ["Replace Registration",                "Licensed Driver"],
                     "change-of-name"        : ["Registration Change of Name",         "Licensed Driver"],
                     "change-of-address"     : ["Registration Change of Address",      "Licensed Driver"]
                  };
      __spt = _h.split( "/" );
      __zip = $("#zip_1").val();
      __sta = $("#state_1").val();
      __act = __spt[__spt.length-2];

      _imSrc = "https://stags.bluekai.com/site/5279?phint=wiki%3Ddriving&phint=wiki%3Ddriverslicense&phint=local%20services%3Dgovernment%20services&phint=local%20services%3Dinsurance&phint=in%20market%3Dauto%20insurance&phint=in%20market%3DAutomotive Services&phint=intent%3D" + escape(map[__act][0]) + "&phint=driver%20class%3D" + escape(map[__act][1]) + "&phint=state%3D" + escape(__sta) + "&phint=zip%3D" + __zip;
      if( __act=="change-of-address" )
         _imSrc += "&phint=in%20market%3Dmoving";
      $('body').append( "<img height=\"1\" width=\"1\" src=\"" + _imSrc + "\">" );
   }
});

function customValidate()
{
   oZip   = $("#zip_1"   );
   oState = $("#state_1" );
   vZip   = oZip.val();
   vState = oState.val();
   if( vZip=="" || vState=="" )
   {
      m = $("#_msg_zip_1");
      m.html( "Missing Data" );
      if( vZip=="" )
         focusField( $("#zip_1"), null, null );
      else
         focusField( $("#state_1"), null, null );
      fadeErr(m);
      errs = true;
      return false;
   }
   return true;
}
function extraValidate()
{
   return true;
}
