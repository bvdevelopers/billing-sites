<%@page import="java.net.*, com.util.*, com.bl.*, java.util.*" %>
<%
   HashMap hm      =  (HashMap) request.getAttribute( "data"    );
   String  pageNum =  (String) hm.get( "_PAGENUM_" );
   pageNum         =  (pageNum==null)?"0":pageNum;
%>

<%-- ############################################ --%>
<%-- MSN Code for Registration Conversion Page    --%>
<%-- ############################################ --%>
<%
   if( pageNum.equals("02") )
   {
      %><script type="text/javascript"> if (!window.mstag) mstag = {loadTag : function(){},time : (new Date()).getTime()};</script> <script id="mstag_tops" type="text/javascript" src="//flex.atdmt.com/mstag/site/4b5bd494-0b2b-4aef-a954-74da6b5ac8ae/mstag.js"></script> <script type="text/javascript"> mstag.loadTag("analytics", {dedup:"1",domainId:"1454534",type:"1",actionid:"47318"})</script> <noscript> <iframe src="//flex.atdmt.com/mstag/tag/4b5bd494-0b2b-4aef-a954-74da6b5ac8ae/analytics.html?dedup=1&domainId=1454534&type=1&actionid=47318" frameborder="0" scrolling="no" width="1" height="1" style="visibility:hidden;display:none"> </iframe> </noscript><%
   }
%>


<%-- ############################################ --%>
<%-- Google Code for Registration Conversion Page --%>
<%-- ############################################ --%>
<%
   if( pageNum.equals("02") )
   {
      %>
      <%-- ############################################ --%>
      <script type="text/javascript">
      /* <![CDATA[ */
      var google_conversion_id = 1016074789;
      var google_conversion_language = "en";
      var google_conversion_format = "2";
      var google_conversion_color = "ffffff";
      var google_conversion_label = "1PTgCPPesgIQpaTA5AM";
      var google_conversion_value = 0;
      /* ]]> */
      </script>
      <script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion.js">
      </script>
      <noscript>
      <div style="display:inline;">
      <img height="1" width="1" style="border-style:none;" alt="" src="https://www.googleadservices.com/pagead/conversion/1016074789/?label=1PTgCPPesgIQpaTA5AM&amp;guid=ON&amp;script=0"/>
      </div>
      </noscript>
      <%
   }
%>

 
<%-- ######## --%>
<%-- BLUEKAI  --%>
<%-- ######## --%>
<%
   if( pageNum.equals("01") )
   {
      StringBuffer imSrc = new StringBuffer();
      imSrc.append( "https://stags.bluekai.com/site/5279?phint=wiki%3Ddriving&phint=wiki%3Ddriverslicense&phint=local%20services%3Dgovernment%20services&phint=local%20services%3Dinsurance&phint=in%20market%3Dauto%20insurance&phint=in%20market%3DAutomotive%20Services" );
      try { String city_1           =  URLEncoder.encode( hm.get("city_1" ).toString(),         "UTF-8" ); imSrc.append("&phint=city%3D").append(city_1);                      } catch(Exception e) {}
      try { String state_1          =  URLEncoder.encode( hm.get("state_1").toString(),         "UTF-8" ); imSrc.append("&phint=state%3D").append(state_1);                    } catch(Exception e) {}
      try { String zip_1            =  URLEncoder.encode( hm.get("zip_1").toString(),           "UTF-8" ); imSrc.append("&phint=zip%3D").append(zip_1);                        } catch(Exception e) {}
      try { String gender           =  URLEncoder.encode( hm.get("gender").toString(),          "UTF-8" ); imSrc.append("&phint=gender%3D").append(gender);                    } catch(Exception e) {}
      try { String marital_status   =  URLEncoder.encode( hm.get("marital_status").toString(),  "UTF-8" ); imSrc.append("&phint=marital%20status%3D").append(marital_status);  } catch(Exception e) {}
      try { String education        =  URLEncoder.encode( hm.get("education").toString(),       "UTF-8" ); imSrc.append("&phint=education%3D").append(education);              } catch(Exception e) {}
      try { String credit           =  URLEncoder.encode( hm.get("credit").toString(),          "UTF-8" ); imSrc.append("&phint=credit%20rating%3D").append(credit);           } catch(Exception e) {}
      try { String occupation       =  URLEncoder.encode( hm.get("occupation").toString(),      "UTF-8" ); imSrc.append("&phint=occupation%3D").append(occupation);            } catch(Exception e) {}
      try
      {
         int age =  Format.currentYear() - Integer.parseInt(  hm.get("date_of_birth").toString().split("/")[2]);
         imSrc.append( "&phint=age%3D" ).append( age );
      }
      catch(Exception e) {}
      %><img src="<%=imSrc%>"><%
   }
   else
   if( pageNum.equals("02") )
   {
      StringBuffer imSrc = new StringBuffer();
      imSrc.append( "https://stags.bluekai.com/site/5279?phint=wiki%3Ddriving&phint=wiki%3Ddriverslicense&phint=local%20services%3Dgovernment%20services&phint=local%20services%3Dinsurance&phint=in%20market%3Dauto%20insurance&phint=in%20market%3DAutomotive%20Services" );
      try { String car_year         =  URLEncoder.encode( hm.get("car_year" ).toString(),          "UTF-8" ); imSrc.append("&phint=vehicle%20year%3D").append(car_year);}                          catch(Exception e) {}
      try { String car_make_lbl_    =  URLEncoder.encode( hm.get("car_make_lbl_" ).toString(),     "UTF-8" ); imSrc.append("&phint=vehicle%20make%3D").append(car_make_lbl_);}                     catch(Exception e) {}
      try { String car_model_lbl_   =  URLEncoder.encode( hm.get("car_model_lbl_" ).toString(),    "UTF-8" ); imSrc.append("&phint=vehicle%20model%3D").append(car_model_lbl_);}                   catch(Exception e) {}
      try { String car_trim_lbl_    =  URLEncoder.encode( hm.get("car_trim_lbl_" ).toString(),     "UTF-8" ); imSrc.append("&phint=vehicle%20submodel%3D").append(car_trim_lbl_);}                 catch(Exception e) {}
      try { String lease            =  URLEncoder.encode( hm.get("vehicle_status" ).toString(),    "UTF-8" ); imSrc.append("&phint=lease%20vehicle%3D").append(lease.equals("Lease")?"Yes":"No");} catch(Exception e) {}
      try { String car_usage        =  URLEncoder.encode( hm.get("car_usage" ).toString(),         "UTF-8" ); imSrc.append("&phint=vehicle%20use%3D").append(car_usage);}                          catch(Exception e) {}
      try { String mileage_trip     =  URLEncoder.encode( hm.get("mileage_trip" ).toString(),      "UTF-8" ); imSrc.append("&phint=daily%20mileage%3D").append(mileage_trip);}                     catch(Exception e) {}
      try { String annual_mileage   =  URLEncoder.encode( hm.get("annual_mileage" ).toString(),    "UTF-8" ); imSrc.append("&phint=annual%20mileage%3D").append(annual_mileage);}                  catch(Exception e) {}
      %><img src="<%=imSrc%>"><%
   }
   else
   if( pageNum.equals("02") )
   {
      StringBuffer imSrc = new StringBuffer();
      imSrc.append( "https://stags.bluekai.com/site/5279?phint=wiki%3Ddriving&phint=wiki%3Ddriverslicense&phint=local%20services%3Dgovernment%20services&phint=local%20services%3Dinsurance&phint=in%20market%3Dauto%20insurance&phint=in%20market%3DAutomotive%20Services" );
      try
      {
         String own_home =  hm.get("own_home" ).toString();
         imSrc.append( "&phint=homeowner%3D" ).append( own_home.equals("Y")?"Yes":"No" );
      }
      catch( Exception e )
      {
      }
      try
      {
         String ticket_or_accident =  hm.get("ticket_or_accident" ).toString();
         if( ticket_or_accident.equals("Y") )
         {
            imSrc.append( "&phint=In%20Market%3DAttorney%20Services" );
            imSrc.append( "&phint=In%20Market%20Retail%20Shoppers%3DFinancial%20and%20Legal%20Services" );
            imSrc.append( "&phint=In%20Market%20Retail%20Shoppers%3DAttorneys" );
         }
      }
      catch( Exception e )
      {
      }
      %><img src="<%=imSrc%>"><%
   }
%>
