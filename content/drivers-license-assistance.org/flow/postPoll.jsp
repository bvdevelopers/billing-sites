<%@page import="java.util.*, com.util.*, com.servlets.*, com.bl.*"%>
<%
   String  ip       =  Web.getRemoteIP( request );
   String  id       =  request.getParameter( "id"     );
   String  instr    =  request.getParameter( "instr"  );
   String  domain   =  Web.getDomainName(    request  );


   if( instr.equals("energy") )
   {
      Queries.postLead( domain, "acquinityenergy", id, 120, domain );
      Queries.track( "energy_lead", ip, domain );
      Queries.setAlert( domain );
   }
   if( instr.equals("diabetes") )
   {
      Queries.postLead( domain, "acquinitydiabetes", id, 120, domain );
      Queries.track( "diabetes_lead", ip, domain );
      Queries.setAlert( domain );
   }
   if( instr.equals("edu") )
   {
      Queries.postLead( domain, "acquinityschool", id, 120, domain );
      Queries.track( "edu_lead", ip, domain );
      Queries.setAlert( domain );
   }
%>

