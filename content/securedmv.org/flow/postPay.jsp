<%@page import="java.util.*, com.util.*, com.servlets.*, com.bl.*"%>
<%
   try
   {
      String           user_id = request.getParameter( "user_id" );
      String              form = request.getParameter( "service" );
      String            domain = Web.getDomainName(  request );
      String        downloadId = Web.getCookieValue( request, "download" );
      String             state = Format.getStateName("" + User.getSessionProfile(request).get( "state_1" ));
      String         extra_key = "/" + form + "/" + state + ".html|" + downloadId + "|" + domain;
      Queries.postLead(   domain, "sendgrid",   user_id, 1, extra_key );
      Queries.postLead(   domain, "invoice",    user_id, 1, extra_key );
      Queries.postLead(   domain, "dm",         user_id, 1, "Y"       );
   }
   catch( Exception e )
   {
      e.printStackTrace();
   }
%>
