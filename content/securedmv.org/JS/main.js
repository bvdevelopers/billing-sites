hrf = document.location.href;
$(document).ready(function()
{
   if( (hrf.indexOf("index.html")!=-1) )
   {
      //
   }
   else
   if( hrf.indexOf("/form/")!=-1 )
   {
      fixOrderSummary( "form" );
   }
   else
   if( hrf.indexOf("/billing.html")!=-1 )
   {
      fixOrderSummary( "billing" );
      $( "#billingSubmit" ).on( "click", function()
      {
          $("#billingForm").submit();
      });
   }
});

function fixOrderSummary( page )
{
   var price = "9.99";
   if ( page == "form" )
   {
      var pathArray  =  window.location.pathname.split('/');
      var state      =  pathArray[pathArray.length-1].split('.')[0];
      state          =  state.replace(/-/g,' ');
      if ( state == "washington dc" ) state = "washington dC";
      $("#item-practice").html( state + " DMV Practice Tests 2018" );

      if( hrf.indexOf("/deluxe/")!=-1 )
      {
         $(".order-deluxe").show();
         price = "14.99";
      }
   }
   else
   if ( page == "billing" )
   {
      var state      =  $("#s_state").val();
      state          =  state.replace(/-/g,' ');
      if ( state == "washington dc" ) state = "washington dC";
      $("#item-practice").html( state + " DMV Practice Tests 2018" );
      if ( _GLOBAL_SESSION_["service"] == "deluxe")
      {
         $(".order-deluxe").show();
         price = "14.99";
      }
   }
   $("#price-product, #price-total").html( "$" + price );
}