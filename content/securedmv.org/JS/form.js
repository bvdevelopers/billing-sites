$(document).ready(function()
{
   var _h = document.location.href;
   if( _h.indexOf("/form/") != -1 )
   {
      var pathArray  =  window.location.pathname.split('/');
      var state      =  pathArray[pathArray.length-1].split('.')[0];
      $("#state option[value=" + state + "]").prop('selected', true);

      $( "#submit-index" ).on( "click", function()
      {
         var step = $("input[name=_PAGENUM_]").val();
         console.log("Mainor --> step: "+step);
         if( step === "01")
         {
            console.log("Mainor --> let submit form 01");
            $("#frm").submit();
         }
         else
         if( step === "02")
         {
            console.log("Mainor --> let submit form 02");
            $("#frm").submit();
         }
      });
   }
});

function showSecondStep()
{
   $("input[name=_PAGENUM_]").val("02");
   $("#basic").removeClass("active");
   $("#billing").addClass("active");
   $('ul.nav.nav-pills li.text-center a[href$="#basic"]').parent().removeClass("active");
   $('ul.nav.nav-pills li.text-center a[href$="#billing"]').parent().addClass("active");
   var body = $("html, body");
   body.stop().animate({scrollTop:0}, 500, 'swing', function(){});
}