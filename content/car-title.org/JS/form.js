$(document).ready(function()
{
   // --- phone_1 focus
   $("#phone_1").focus( function() { $("#_msg_phone_1").removeClass("err_on").addClass("hint_msg"); $("#_msg_phone_1").html("Can be same as cell Phone");  } );
   $("#phone_1").blur(  function() { $("#_msg_phone_1").removeClass("hint_msg").addClass("err_on"); $("#_msg_phone_1").html("");                           } );
   
   // --- blueKai
   var _h = document.location.href;
   if( _h.indexOf("/forms/") != -1 )
   {
      // --- set H1 content (start)
      var mapH1 = new Array();
      mapH1[ "new-title" ]         =  "Get Your [State] Car Title Easy Guide Package";
      mapH1[ "replace-title" ]     =  "Get Your [State] Car Title Easy Guide Package";
      mapH1[ "change-of-name" ]    =  "Get Your [State] Car Title Easy Guide Package";
      mapH1[ "change-of-address" ] =  "Get Your [State] Car Title Easy Guide Package";
      
      splt = document.location.href.split( "/" );
      cnt  = mapH1[ splt[5] ].replace( "[State]", initCap(splt[6].replace(".html", "")) );
      $("#formh1").html( cnt );
      // --- set H1 content (end)
   }
   
   if( _h.indexOf("forms/step1/") != -1 )
   {
      try
      {
         var state = initCap(document.location.href.split("/")[6].split(".")[0]);  //get state from url
         //$("#formh1").html( "Get your "+ state +" car title application and checklist" );
         $("#state_licensed").val( state );
      }
      catch(e){}     
   
      //hide good student
      $("#main_18d").hide();
      $("#main_18e").hide();
      $("#main_18f").hide();
      $("#main_18g").hide();
   
      $("#state_licensed option[value='Not Licensed']").remove();
      
      //terms and conditions checkbox move right
      $("#agree_tc").css('margin-left','5px');
      
      var map =   {  "new-title"          : ["New Car Title",              "Licensed Driver"],
                     "replace-title"      : ["Replace Car Title",          "Licensed Driver"],
                     "change-of-name"     : ["Change Name Car Title",      "Licensed Driver"],
                     "change-of-address"  : ["Change Address Car Title",   "Licensed Driver"]
                  };

      __spt = _h.split( "/" );
      __zip = $("#zip_1").val();
      __sta = $("#state_1").val();
      __act = __spt[__spt.length-2];
      
      _imSrc = "https://stags.bluekai.com/site/5280?phint=wiki%3Ddriving&phint=wiki%3Ddriverslicense&phint=local%20services%3Dgovernment%20services&phint=local%20services%3Dinsurance&phint=in%20market%3Dauto%20insurance&phint=in%20market%3DAutomotive Services&phint=intent%3D" + escape(map[__act][0]) + "&phint=driver%20class%3D" + escape(map[__act][1]) + "&phint=state%3D" + escape(__sta) + "&phint=zip%3D" + __zip;
      if( __act=="change-of-address" )
         _imSrc += "&phint=in%20market%3Dmoving";
      $('body').append( "<img height=\"1\" width=\"1\" src=\"" + _imSrc + "\">" );
   }
   
});

function customValidate()
{
   oZip   = $("#zip_1"   );
   oState = $("#state_1" );
   vZip   = oZip.val();
   vState = oState.val();
   if( vZip=="" || vState=="" )
   {
      m = $("#_msg_zip_1");
      m.html( "Missing Data" );
      if( vZip=="" )
         focusField( $("#zip_1"), null, null );
      else
         focusField( $("#state_1"), null, null );
      fadeErr(m);
      errs = true;
      return false;
   }
   return true;
}
