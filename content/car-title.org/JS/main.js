hrf = document.location.href;
$(document).ready(function(){

    // check for california to change disclaimers
    if( utm_source == "CA_Drivers" || _PROFILE["state_1"] == "CA" )
    {
     $(".disclaimer").css("position","relative");
     $(".disclaimer").html('THIS PRODUCT OR SERVICE HAS NOT BEEN APPROVED OR ENDORSED BY ANY GOVERNMENTAL AGENCY, <br>AND THIS OFFER IS NOT BEING MADE BY AN <a href="http://dmv.ca.gov/" style="color:blue" target="_blank">AGENCY OF THE GOVERNMENT</a>.');
    }

    
   if( (hrf.indexOf("index.html")!=-1) )
   {
      //--- popup ---
      var referrer =  document.referrer;
      if( referrer.indexOf(domainName) ==-1 )
      {
		/*Index Modal*/
        $('#myModal').modal({backdrop: 'static', keyboard: false});  
        $('#myModal').modal('show');
      }
      
      $("#step1 input:radio").change(function(){
         $("#step1 :radio ").parent(".checkBox").removeClass("checked");
         $("#step1 :radio:checked").parent(".checkBox").addClass("checked");
         $("#step1 :radio:checked").blur();
      });
      
      //--- home page validation ---
      $("#submit").click(function(e){
         var _state  = $("#state").val();
         var _action = $('input[name=action]:checked','#first-form').val();
         if( !_action )
         {
            alert( "Please Select an Action" );
            return false;
         }
         if( _state=="" )
         {
            alert( "Please Select a State" );
            $("#state").focus();
            return false;
         }
         if( _state.length>0 )
         {
            if( _action == "learners-permit")
               document.location.href = "/dmv-practice-tests.html";
            else if ( _action == "change-of-address" )
               document.location.href = "http://"+domainName+"/form/coa/" + _action + "/" + _state + ".html";
            else
               document.location.href = "http://"+domainName+"/form/step1/" + _action + "/" + _state + ".html";
         }
         return false;
      });
   }
   else
   if( (hrf.indexOf("/easyguide.html")!=-1) )
   {
      if( utm_source == "CA_Drivers" || _PROFILE["state_1"] == "CA" )
      {
         $("#ca-disc").show();
      }
   }
   else
   if( (hrf.indexOf("/billing.html")!=-1) )
   {
      itemCart( "processing",       "Processing Fee",  "Registration Fee",             true  );
      
      // check for california cookie to change disclaimers
      if( utm_source == "CA_Drivers" || _PROFILE["state_1"] == "CA" )
      {
         $(".disclaimer").css("position","relative");
         $(".disclaimer").html('THIS PRODUCT OR SERVICE HAS NOT BEEN APPROVED OR ENDORSED BY ANY GOVERNMENTAL AGENCY, <br>AND THIS OFFER IS NOT BEING MADE BY AN <a href="http://dmv.ca.gov/" style="color:blue" target="_blank">AGENCY OF THE GOVERNMENT</a>.');
         $("#termsBoxCalifornia").show();
         itemCart( "checklist-thumb",  "Easy Guide",      "License Easy Guide &trade;|State specific guide containing helpful information",   false );
      }
      else
      {
         itemCart( "checklist-thumb",  "Easy Guide",      "License Easy Guide &trade;",   false );
          $("#termsBox").show();
      }
      
      if( pdfFormExists() )
         itemCart( "form-thumb",    "Pre-filled Form", "Pre-filled Form",              false );
         
      
   }
   else
   if( hrf.indexOf("/checklist/")!=-1 )
   {         
      if($('.pdfynY').length == 0)
         $("#formDiv").hide();
      if( paymentTrans )
         $("#dl-success").show();
      else
         $("#aside").css("margin-top","40px");
      
      // --- make all links under download boxes target _blank
       $("#checklistPar").find("a").each(function(){
         $(this).attr("target","_blank");
       });
      
      // "Click here to finish your application" DIV link open in new window
      if ( $('.linksynY').length > 0 )
      {
         var a_href = $(".linksynY").closest('a[href]').attr("href");
             $('#outsideLink').click( function() {
                window.open( a_href );
                return false;
            });
      }
   }
   else
   if( hrf.indexOf("/splash/")!=-1 || hrf.indexOf("/landing/")!=-1 )
   {
      $("#step1 input:radio").change(function(){
         $("#step1 :radio ").parent(".checkBox").removeClass("checked");
         $("#step1 :radio:checked").parent(".checkBox").addClass("checked");
         $("#step1 :radio:checked").blur();
      });
      
      try
      {
         var x = hrf.split("?")[1].split(":")[0];
         var opt1 = "new-title";
         var opt2 = "replace-title";
         var opt3 = "change-of-address";
         var opt4 = "change-of-name";
         var opt5 = "salvage-title";
         if( x == opt1 || x == opt2 || x == opt3 || x == opt4 || x == opt5 )
         {
            $("#"+x.toString()).parent(".checkBox").addClass("checked");
            $("#"+x.toString()).attr("checked", true);
         }
      }
      catch(e){}
      
      $("#submit").click( function(){
         _action = $('input[name=action]:checked',   '#splashform');
         
         if( _action.length==0 )
         {
            alert( "Please Choose a Service" );
            return;
         }
         action = _action.val();
         _link = "/form/step1/" + action + "/" + getState() + ".html";
         document.location.href = _link;
      });
   }
   else
   if( (hrf.indexOf("/servsplash/")!=-1) || (hrf.indexOf("/servlanding/")!=-1))
   {
      try 
      {
         var val        = hrf.split('/');
         var _service   = val[4];
         var urlState   = val[5].split('.');
         var _state     = urlState[0];
      }
      catch(e) {}
      
      $(".splash_startTX").click(function() {
         if(_service == "change-of-address")
            document.location.href = "http://"+domainName+"/form/coa/" + _service + "/" +_state+ ".html";
         else
            document.location.href = "http://"+domainName+"/form/step1/" + _service + "/" +_state+ ".html";
      });
   }
   else
   if( hrf.indexOf("/docs.html")!=-1 || hrf.indexOf("/guide.html")!=-1 )
   {
      if( hrf.indexOf("guide.html")!=-1 )
         track("guide");
      
      //--- checkbox ---
      $("#step1 input:radio").change(function(){
         $("#step1 :radio ").parent(".checkBox").removeClass("checked");
         $("#step1 :radio:checked").parent(".checkBox").addClass("checked");
         $("#step1 :radio:checked").blur();
      });
      
      //--- home page validation ---
      $("#submit").click(function(e){
         var state  = $("#state").val();
         var serv = $('input[name=action]:checked','#first-form').val();
         
         if( !serv )
         {
            alert( "Please Select an Action" );
            return false;
         }
         if( state=="" )
         {
            alert( "Please Select a State" );
            $("#state").focus();
            return false;
         }
         if( state.length>0 )
         {
            // --- if has form, show in shopping cart
            var url = "/ajax/pdfExists.jsp?form=form/" + serv + "/" + state;
            $.ajax({
               "url": url,
               "success": function(response){
                  if( response.indexOf("true") !=-1 )
                  {
                     $("#form").show();
                     $("#form").click(function(){
                        document.location.href = "/PDForm/" + serv + "/" + state + ".pdf";
                     });
                  }
               }
            });
            
            $("#checklist").click(function(){
               document.location.href = "/checklist/" + serv + "/" + state + ".pdf";
            });
            
            $("form, #seals").hide();
            $("#dlh1").text("Your Documents are Ready!");
            $("#download-par").text("Your Driver License Easy Guide and form (if applicable) are available below for download.");
            $("#progress").attr("src", "http://s3.amazonaws.com/"+domainName+"/img/progress2.png");
            if( serv == "change-of-address" ) 
               $("#genericChangeAddress, #coabr").show();
            $("#show-downloads").show();
         }
      });
   }
   
   //-----------------------------------------
   //--- Service Splash Pages ---
   //-----------------------------------------
   if( (hrf.indexOf(".org/new-title/")!=-1) || (hrf.indexOf(".org/replace-title/")!=-1) || (hrf.indexOf(".org/change-of-address/")!=-1) || (hrf.indexOf(".org/change-of-name/")!=-1) )
   {
   	$("#stateinfo-cont").click(function() {
   		document.location.href = "/form/step1/" + getUrl(1) + "/" + getState() + ".html";
   	});
   }
});

/*-----------------*/
/*--- postQuiz  ---*/
/*-----------------*/
function postQuiz( q )
{
   var n = "";
   if ( parent.document.location.href.indexOf("s=california")!=-1 )
      n = "?s=california";
   parent.document.location.replace( "/pt-results.html"+n );
}


