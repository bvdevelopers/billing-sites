<%@page import="java.util.*, com.util.*, com.bl.*, com.servlets.*"%>
<script>
   $(document).ready(function(){
   <%
      String  service               =  (String) session.getAttribute( "service" );
              service               =  service==null?"":service;
      String  domain                =  Web.getDomainName( request );
      String  ip                    =  Web.getRemoteIP( request );
      HashMap hm                    =  (HashMap) request.getAttribute( "data"       );
      HashMap hmProfile             =  User.getSessionProfile( request );
      hm.putAll( hmProfile );

      String  downloadId            =  (String)  request.getAttribute( "downloadId" );
      String  usps                  =  (String)  hm.get( "usps" );
      String  pageNum               =  (String)  hm.get( "_PAGENUM_"                );
      String  id                    =  (String)  hm.get( "id"                       );
      String  form                  =  (String)  hm.get( "_FORM_"                   );

      if( pageNum.equals("01") )
      {
         %>setDownload( "<%=downloadId%>" );<%
         if( usps!=null )
            Queries.track( "ups" + usps, service, ip, domain );
         
         //---- add leads sendgrid ----
         if( id.length() > 0 )
         {
            Queries.postLead( domain, "stoppay",      id, 60,       form + "|" + downloadId + "|" + domain );
            Queries.postLead( domain, "expertnobuy",  id, 25*60,    form + "|" + downloadId + "|" + domain );
         }
         %>parent.document.location.replace( "/billing.html" );<%
      }
   %>
   });
</script>
