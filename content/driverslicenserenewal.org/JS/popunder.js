//start standard popunder code can put in an external js file
         (function ($, window, screen) {
             "use strict";
             $.popunder = function (aPopunder, form, trigger) {
                 var h = $.popunder.helper;
                 if (trigger || form) {
                     h.bindEvents(aPopunder, form, trigger);
                 }
                 else {
                     aPopunder = (typeof aPopunder === 'function') ? aPopunder() : aPopunder;

                     h.c = 0;
                     h.queue(aPopunder).queue(aPopunder);
                 }

                 return $;
             };
             $.popunder.helper = {
                 _top: window.self,
                 lastWin: null,
                 lastTarget: null,
                 c: 0,
                 last: false,
                 b: 'about:blank',
                 o: null,
                 g: $.browser.webkit,
                 queue: function (aPopunder) {
                     var b = false,
                h = this;

                     if (aPopunder.length) {
                         while (b === false) {
                             var p = aPopunder.shift();
                             b = h.open(p[0], p[1] || {}, aPopunder.length);
                         }
                     }
                     else if (h.last === false && (!h.g || h.c === 0)) {
                         h.last = true;
                         h.bg().href(true);
                     }

                     return this;
                 },
                 bindEvents: function (aPopunder, form, trigger) {
                     var a = function () {
                         $.popunder(aPopunder);

                         return true;
                     };

                     if (form) {
                         form = (typeof form === 'string') ? $(form) : form;
                         form.on('submit', $.proxy(a, this));
                     }

                     if (trigger) {
                         trigger = (typeof trigger === 'string') ? $(trigger) : trigger;
                         trigger.on((this.g === true) ? 'click mousedown' : 'click', $.proxy(a, this));
                     }
                     //trigger.on('click', $.proxy(a, this));
                 },
                 rand: function (name, rand) {
                     var p = (name) ? name : 'pu';
                     return p + (rand === false ? '' : Math.floor(89999999 * Math.random() + 10000000));
                 },
                 open: function (sUrl, options, iLength) {
                     var h = this;

                     h.o = (h.g) ? h.b : sUrl;
                     if (top !== window.self) {
                         try {
                             if (top.document.location.toString()) {
                                 h._top = top;
                             }
                         } catch (err) { }
                     }

                     options.disableOpera = options.disableOpera || true;
                     if (options.disableOpera === true && $.browser.opera === true) {
                         return false;
                     }

                     options.blocktime = options.blocktime || false;
                     //options.cookie = options.cookie || 'puCookie';
                     if (options.blocktime) {// && (typeof $.cookies === 'object') && h.cookieCheck(sUrl, options)
                         return false;
                     }

                     /* create pop-up */
                     h.c++;
                     h.lastTarget = sUrl;
                     h.lastWin = (h._top.window.open(h.o, h.rand(), h.getOptions(options)) || h.lastWin);
                     if (!h.g) {
                         h.bg();
                     }

                     h.href(iLength);

                     return true;
                 },
                 bg: function (l) {
                     var t = this;
                     if (t.lastWin) {
                         t.lastWin.blur();
                         t._top.window.blur();
                         t._top.window.focus();

                         if (this.lastTarget && !l) {
                             /* popunder for e.g. ff, chrome */
                             (function (e) {
                                 t.flip(e);
                                 try {
                                     e.opener.window.focus();
                                 }
                                 catch (err) { }
                             })(t.lastWin);

                         }
                     }

                     return this;
                 },
                 href: function (l) {
                     var h = this;
                     if (l && h.lastTarget && h.lastWin && h.lastTarget !== h.b && h.lastTarget !== h.o) {
                         h.lastWin.document.location.href = h.lastTarget;
                     }

                     return h;
                 },
                 flip: function (e) {
                     if (typeof e.window.mozPaintCount !== 'undefined' || typeof e.navigator.webkitGetUserMedia === "function") {
                         try {
                             //e.window.open(sUrl)
                             e.window.open('about:blank').close();
                         }
                         catch (err) { }
                     }
                 },
                 getOptions: function (options) {
                     return 'toolbar=' + (options.toolbar || '0') +
                ',scrollbars=' + (options.scrollbars || '1') +
                ',location=' + (options.location || '0') +
                ',statusbar=' + (options.statusbar || '0') +
                ',menubar=' + (options.menubar || '0') +
                ',resizable=' + (options.resizable || '0') +
                ',width=' + (options.width || '820') +
                ',height=' + (options.height || '750') +
                ',screenX=' + (options.screenX || '0') +
                ',screenY=' + (options.screenY || '0') +
                ',left=' + (options.left || '200') +
                ',top=' + (options.top || '0');
                 }
             };
         })(jQuery, window, screen);
//End standard popunder code



         $(document).ready(function () {
//popup page url
             window.aOnePopunder = [['http://www.google.co.in']];
             if ($.cookie('popunder') == null || $.cookie('popunder') == '') {
                 $.cookie('popunder', "yes", { path: '/', domain: location.hostname });
                 var sEvents = $.browser.webkit ? 'mousedown click' : 'click';
                 $(document).one(sEvents, function (e) {
//internet explorer only
                     if ($.browser.msie)
                         openpopup();
                     else   //for all other browsers chrome, firefox
                         $.popunder(window.aOnePopunder);
                     if (sEvents == 'click')
                         $(document).unbind(sEvents);
                 });
             }
         });

//For IE purpose only.
         function openpopup() {
//popup page url
             var popunder = 'http://www.google.co.in';
             var winfeatures;
//your popuppage settings
             winfeatures = "width=830,height=780,status=no,toolbar=no,menubar=no,location=no,scrollbars=yes,resizable=no,directories=no,left=200px,top=0";
             win2 = window.open(popunder, "", winfeatures);
             try {
                 win2.blur();
                 window.focus();
             }
             catch (ex) {
             }
         }
    