hrf = document.location.href;
$(document).ready(function(){
	
	//Blog alt and title tags
	if(hrf.indexOf("/blog/") != -1) {
		blog_site = window.location.hostname;
		blog_title = $("#blogtitle").text();
		the_title = blog_site + " blog: " + blog_title;
		$("#blogimg").attr("title", the_title);
		$("#blogimg").attr("alt", the_title);
	}	
	
   /******** GLOBAL FUNCTIONS ********/
   //get url parameters
   function getUrlParameter(sParam){
       var sPageURL = window.location.search.substring(1);
       var sURLVariables = sPageURL.split('&');
       for (var i = 0; i < sURLVariables.length; i++) 
       {
           var sParameterName = sURLVariables[i].split('=');
           if (sParameterName[0] == sParam) 
           {
               return sParameterName[1];
           }
       }
   }   	
	
	/*Remove States*/
	/*var href = $(location).attr("href");
	var lastSegment = href.substring(href.lastIndexOf('/') + 1);
	var nonValidStates = ["iowa.html","kansas.html","kentucky.html","louisiana.html","new-jersey.html","maine.html","maryland.html","michigan.html","mississippi.html"];
	if ($.inArray(lastSegment,nonValidStates) != -1){
		$("#submit2").remove();
	    $("#audiotxt").remove();
		$("#audio_book").remove();
		$(".dld-block").css("width", "33.33%");
		$(".img-dld").css("margin", "5px 35%");
	}*/
	  
   /******** HOME PAGE ********/
/*   if( (hrf.indexOf("index.html")!=-1) )
   {
      //--- auto select service and state from url ---
      var url_service   = getUrlParameter('service');
      var url_state     = getUrlParameter('state');
      if(url_service != undefined && url_state != undefined)
      {
         var radioElement = $("#"+url_service);
         if(radioElement.length > 0)
            radioElement.prop("checked", true);
         
         $("#state").val(url_state);
      }
      
      //--- home submit process ---
      $("#submit").click(function(e){
         var _state     = "";
         var _service   = "";
         if($('#state').length > 0)
            _state      = $("#state").val();
         if($('input[type="radio"].services-input:checked').length > 0)
            _service    = $('input[type="radio"].services-input:checked').attr('id');
         if(_state != "" && _service != ""){
            if( (hrf.indexOf("dev."+domainName)!=-1) )
            {
               document.location.href = "http://dev."+domainName+"/form/step1/" + _service + "/" + _state + ".html";
            }
            else if( (hrf.indexOf("stage."+domainName)!=-1) )
            {
               document.location.href = "http://stage."+domainName+"/form/step1/" + _service + "/" + _state + ".html";
            }
            else
            {
               document.location.href = "http://"+domainName+"/form/step1/" + _service + "/" + _state + ".html";
            }
         }
         else{
            if(_service == ""){
               $('#homeValidationModal h4.modal-title').html('Please Select a Service');
               $('#homeValidationModal').modal('show');
               return false; 
            }else if(_state == ""){
               $('#homeValidationModal h4.modal-title').html('Please Select a State');
               $('#homeValidationModal').modal('show');
               return false;
            }
         }
         return false;
      });
   }
   */
/******** HOME PAGE ********/
if( (hrf.indexOf("index.html")!=-1) )
{  
   //--- auto select service and state from url ---
      var url_service   = getUrlParameter('service');
      var url_state     = getUrlParameter('state');

      if(url_service != undefined && url_state != undefined)
      {
         var fix_url_service = url_service;
         if(url_service=="new-driver-license") fix_url_service = "new-drivers-license";
         else if(url_service=="renew-driver-license") fix_url_service = "renew-drivers-license";
         else if(url_service=="replace-driver-license") fix_url_service = "replace-drivers-license";
         var radioElement = $("#"+url_service);
         if(radioElement.length > 0)
            radioElement.prop("checked", true);     
         $("#state").val(url_state);

            if( (hrf.indexOf("dev."+domainName)!=-1) )
            {
               document.location.href = "http://dev."+domainName+"/form/step1/" + fix_url_service + "/" + url_state + ".html";
            }
            else if( (hrf.indexOf("stage."+domainName)!=-1) )
            {
               document.location.href = "http://stage."+domainName+"/form/step1/" + fix_url_service + "/" + url_state + ".html";
            }
            else
            {
               document.location.href = "http://"+domainName+"/form/step1/" + fix_url_service + "/" + url_state + ".html";
         }
      }
	  else
	  {
			/*Index Modal*/
        if( hrf.indexOf("?")==-1 )
         {
            $('#myModal').modal({backdrop: 'static', keyboard: false});
            $('#myModal').modal('show');
         }
	  }

    //--- home submit process ---
         $( '.row #form > span > input[type="radio"], #state ').change(function() {
            var _state     = "";
            var _service   = "";
            var _service   = $('.row #form > span > input[type="radio"]:checked').attr('id');         

            if($('#state').length > 0){
               var _state   = $('#state').val();              
            }

            if(_state != "" && _service != "" && typeof _state != 'undefined' && typeof _service != 'undefined'){
               if( (hrf.indexOf("dev."+domainName)!=-1) )
               {
                  document.location.href = "http://dev."+domainName+"/form/step1/" + _service + "/" + _state + ".html";
               }
               else if( (hrf.indexOf("stage."+domainName)!=-1) )
               {
                  document.location.href = "http://stage."+domainName+"/form/step1/" + _service + "/" + _state + ".html";
               }
               else
               {
                  document.location.href = "http://"+domainName+"/form/step1/" + _service + "/" + _state + ".html";
               }
            }
            else
            {
               if (typeof _state == 'undefined')
               {
                  alert("Please select a state");
               }
               else if (typeof _service == 'undefined')
               {
                  alert("Please select a service");
               }
            }
      });
}
   
/******** PERSONAL INFORMATION PAGE ********/
   else if( hrf.indexOf("/form/step1/")!=-1 )
{ 

	

      var pathArray= window.location.pathname.split('/');
      var stateArray=pathArray[4];
      var stateOption=  stateArray.split('.');

      var service= pathArray[3];
      var state= stateOption[0];
      
      //alert(state);

      var radioElement = $("#"+service);
          radioElement.prop("checked", true); 
    
      setTimeout(function(){ $('#state').val(state); }, 800);
       
      $("#email").change( function() { correctEmail($(this));} );
      
	  $.validator.addMethod("dobValid", function (value, element, options){
         var anyEmpty = ( $("#_dd_date_of_birth").val() == 0 || $("#_mm_date_of_birth").val() == 0 || $("#_yy_date_of_birth").val() == 0 );

         if(!anyEmpty)
            return true;
      }, "Birth Date Required");
      
      $.validator.addMethod("dobCheck", function (value, element, options){
         var val = $("#date_of_birth").val();
         if( dateCheck(val) )
            return true;
      }, "Invalid Date");

	  
	function correctEmail( o )
	{
	   trg  = o.val().toLowerCase().replace("www.", "");
	   splt = trg.split( "@" );
	   if( splt.length!=2 )
		  return;
	   trg = trg.replace( "..",    "." );
	   trg = trg.replace( ".ccom",  ".com" );
	   trg = trg.replace( ".comm",  ".com" );
	   trg = trg.replace( ".coomm", ".com" );

	   trg = trg.replace( ".cox",   ".com" );
	   trg = trg.replace( ".xom",   ".com" );
	   trg = trg.replace( ".cim",   ".com" );
	   trg = trg.replace( ".con",   ".com" );
	   
	   trg = trg.replace( ".on",    ".com" );
	   trg = trg.replace( ".cm",    ".com" );
	   trg = trg.replace( ".nt",    ".net" );
	   trg = trg.replace( ".co",    ".com" );

	   
	   trg = trg.replace( "gmial",  "gmail" );
	   trg = trg.replace( "gamil",  "gmail" );
	   
	   if( splt.length == 2 )
	   {
		  splt = trg.split( "@" );
		  dot  = splt[1].split(".")[1];
		  dot  = dot==".co"?".com":dot;
		  dot  = dot==".ne"?".net":dot;
		  
		  if( splt[1].indexOf("yaho")!=-1 )   splt[1] = "yahoo." + dot;
		  else
		  if( splt[1].indexOf("hotm")!=-1 )   splt[1] = "hotmail." + dot;
		  else
		  if( splt[1].indexOf("comca")!=-1 )  splt[1] = "comcast.net";
		  else
		  if( splt[1].indexOf("gma")!=-1 )    splt[1] = "gmail.com";
		  else
		  if( splt[1].indexOf("aol")!=-1 )    splt[1] = "aol.com";
		  
	   }
	   rtn = splt[0] + "@" + splt[1];
	   rtn = rtn.replace( ".comm", ".com" );
	   rtn = rtn.replace( ".cpm",  ".com" );
	   rtn = rtn.replace( ".undefined", ".com" );


	   o.val( rtn );
	}
	  

      $.validator.addMethod("mailValid", function (value, element, options)
      {
         var emailData = $("#email").val(),
             anyEmpty  = ( emailData.indexOf("@") == -1 || emailData.indexOf(".") == -1 );

         if(anyEmpty)
           return false;
         else{
            $("#email-error").css({"display":"none"});
            return true;
         }
      }, "Email Invalid");

      $.validator.addMethod("phoneValid", function (value, element, options)
      {
         var msisdn = extractNumberJunk(value);
         if( msisdn.length!=0 )
         {
            if( (msisdn.length!=10 && "US"=="US") || (msisdn.length!=10 && "US"=="BR") )
               return false;
            else
               return true;
         }
         else
            return true;
      }, "Invalid Phone Number");
      
      $("#frm").validate({
        onfocusout: false,
         onkeyup: false,
         rules: {
            first_name: {
               required: true
            },
            last_name: {
               required: true
            },
            address_1: {
               required: true
            },
            city_1: {
               required: true
            },
            state_1: {
               required: true
            },
            zip_1: {
               required: true,
               minlength: 5
            },
            gender: {
               required: true
            },
            phone_1: {
               required: true,
               phoneValid: true
            },
            _yy_date_of_birth: {
               required: true,
               dobValid: true,
               dobCheck: true
            },
            email: {
               required: true,
               mailValid: true
            }
         },
         messages: {
            first_name: {
               required: "First Name is Required"
            },
            last_name: {
               required: "Last Name is Required"
            },
            address_1: {
               required: "Address is Required"
            },
            city_1: {
               required: "City is Required"
            },
            state_1: {
               required: "Required"
            },
            zip_1: {
               required: "Required",
               minlength: "Invalid ZIP"
            },
            gender: {
               required: "Gender is Required"
            },
            phone_1: {
               required: "Primary Phone is Required"
            },
            _yy_date_of_birth: {
               required: "Birth Date is Required"
            },
            email: {
               required: "Email Address is Required"
            }
         },
         submitHandler: function(form) {
            $("#frm label.error").hide();
            
            $("#btnsubmit").css({"display":"none"});
            $("#frmBadges").css({"display":"none"});
            $("#frmCancel").css({"display":"none"});
            $("#frmLoadingGif").css({"display":"block"});
            form.submit();            
         }
      });
      
      //mask only number fields
      $("#zip_1").mask("99999");
   }
   else if( hrf.indexOf("/billing.html")!=-1 )
   {
      //itemCart();
	  itemCart( "processing", 		"Processing Fee", 	"Processing Fee",				true	);
	  itemCart( "checklist-thumb", 	"Easy Guide",      	"Driver's Assistant Guide",   	false	);
      
      /*try
      {
         $("#state").val( _PROFILE["state_1"] );
      }
      catch(e){}*/
      
      $(".openCard").click(function(e){
         $('#cvvModal').modal('show');
      });
      
      jQuery.validator.addMethod("vmcardsonly", function(value, element, param) {
         if( value.charAt(0) == 4 || value.charAt(0) == 5 || value.charAt(0) == 6){
            $("#card_number-error").hide();
            return true;
         }else{
            $("#card_number-error").show();
            $('#card_number').val("");
            $("#card_number").focus();
            return false;
         }
      }, "We do not support American Express Cards");
      
      $("#billingForm").validate({
        onfocusout: false,
         onkeyup: false,
         rules: {
            first_name: {
               required: true
            },
            last_name: {
               required: true
            },
            state: {
               required: true
            },
            zip: {
               required: true,
               minlength: 5
            },
            card_number: { 
               required: true,
               minlength:16,
               vmcardsonly: true
            },
            month: {
               required: true
            },
            year: {
               required: true
            },
            security_code: {
               required: true,
               minlength: 3
            }
         },
         messages: {
            first_name: {
               required: "First Name is Required"
            },
            last_name: {
               required: "Last Name is Required"
            },
            state: {
               required: "Required"
            },
            zip: {
               required: "Required",
               minlength: "Invalid ZIP"
            },
            card_number: {
               required: "Credit Card number is Required",
               minlength: "Invalid Credit Card number"
            },
            month: {
               required: "Required"
            },
            year: {
               required: "Required"
            },
            security_code: {
               required: "Security Code is Required",
               minlength: "Invalid Security Code"
            }
         },
		 
         submitHandler: function(form) {
            $("#billingForm label.error").hide();
            $("#card_number-error").html("");

            var valid    = $.payment.validateCardNumber($('#card_number').val());
            if (!valid) {     
               $("#card_number-error").html("Invalid Credit Card number");
               $("#card_number-error").show();
               $("#card_number").focus();
               return false;
            }
                       
            $("#billingForm #billing-error").html("");
            $("#billingSubmit").css({"display":"none"});
            $("#submitLoad").css({"display":"block"});
            
            /*var path = hrf;
            if( (path.indexOf("dev."+domainName)!=-1) )
            {
               document.location.href = "http://dev."+domainName+"/thank-you.html";
            }
            else if( (path.indexOf("stage."+domainName)!=-1) )
            {
               document.location.href = "http://stage."+domainName+"/thank-you.html";
            }
            else
            {
               document.location.href = "http://"+domainName+"/thank-you.html";
            }*/
            //window.location = "/dl-confirmation.html";
			
            form.submit();
         }
      });
      
      //mask only number fields
      $("#zip").mask("99999");
      $("#card_number").mask("9999 9999 9999 9999");
      $("#security_code").mask("999");
      
      //fix when typing letters on credit card field
      $("#card_number").keyup(function() {
         if(this.value == "   ") this.value = "";
      });
      
      //gas coupon exit intent
      /*$(".modalClose, .couponSubmit").on('click', function(e) {
         $('#ouibounce-modal').hide();
         e.stopPropagation();
      });
      ouibounce(document.getElementById('ouibounce-modal'), {
         aggressive: true,
         sitewide: true,
         timer: 0
      });*/
   }
});

