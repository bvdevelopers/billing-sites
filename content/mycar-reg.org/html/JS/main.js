hrf = document.location.href;
$(document).ready(function(){

   /* GLOBAL VARIABLE */
   var divice = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
   var iPhone = /iPhone/i.test(navigator.userAgent);

   /***************************************************************************/
   /******************************** FUNCTIONS ********************************/
   /***************************************************************************/
   //set new height to both top boxes on service/state landing page
   function infoBoxesSize ()
   {
      var leftBox     = $(".left-box").height();
      var rightBox    = $(".right-box").height();
      var bigBox;
      var boxHeight;
      var windowWidth = $(window).width(); 

      if (divice == false)
      {
         if (windowWidth < 992){
            if(leftBox > rightBox)
            {
               bigBox = leftBox;
               boxHeight = bigBox + 10;
               $(".left-box").css("height",bigBox+"px");
               $(".service-info-container").css("height",boxHeight+"px");
            }
            else
            {
               bigBox = rightBox;
               boxHeight = bigBox + 10;
               $(".left-box").css("height",bigBox+"px");
               $(".service-info-container").css("height",boxHeight+"px");
            }
         }
         else{
            if(leftBox > rightBox)
            {
               bigBox = leftBox;
               boxHeight = bigBox -50;
               $(".left-box").css("height",bigBox+"px");
               $(".service-info-container").css("height",boxHeight+"px");
            }
            else
            {
               bigBox = rightBox;
               boxHeight = bigBox -50;
               $(".left-box").css("height",bigBox+"px");
               $(".service-info-container").css("height",boxHeight+"px");
            }
         }
      }
   }
   
   //adjust vertical position to buttons on top boxes on service/state landing page
   function buttonTopPosition (){
      /*var leftBox        = $(".service-information-boxes .info_boxes.left-box");
      var leftButton     = $(".service-information-boxes .info_boxes.left-box .serviceCTA");
      var rightButton    = $(".service-information-boxes .info_boxes.right-box .btncontinue");
      if(rightButton.length > 0 && leftButton.length > 0)
      {
         var leftButtonTop  = $(".service-information-boxes .info_boxes.left-box .serviceCTA").offset().top;
         var rightButtonTop = $(".service-information-boxes .info_boxes.right-box .btncontinue").offset().top;
         if(leftButtonTop < rightButtonTop)
         {
            var buttonRealWidth = leftButton.width() + (parseInt(leftButton.css("padding-left").replace("px", "")))*2;
            var newMargin = ($(".service-information-boxes .info_boxes.left-box").width() - buttonRealWidth)/2;
            leftButton.css("margin-left",newMargin+"px");
            leftButton.css("margin-right",newMargin+"px");
            var newTop = (rightButtonTop - leftButtonTop) + parseInt(leftButton.css("margin-top").replace("px", ""));
            leftButton.css("margin-top",newTop+"px");
         }
         else
         {
            var buttonRealWidth = leftButton.width() + (parseInt(leftButton.css("padding-left").replace("px", "")))*2;
            var newMargin = ($(".service-information-boxes .info_boxes.left-box").width() - buttonRealWidth)/2;
            leftButton.css("margin-left",newMargin+"px");
            leftButton.css("margin-right",newMargin+"px");
            var newTop = (leftButtonTop - rightButtonTop) + parseInt(rightButton.css("margin-top").replace("px", ""));
            rightButton.css("margin-top",newTop+"px");
         }
      }*/
      var rightButton    = $(".service-information-boxes .info_boxes.right-box .serviceCTA");
      if(rightButton.length > 0)
      {
         var buttonRealWidth = rightButton.width() + (parseInt(rightButton.css("padding-left").replace("px", "")))*2;
         var newMargin = ($(".service-information-boxes .info_boxes.right-box").width() - buttonRealWidth)/2;
         rightButton.css("margin-left",newMargin+"px");
         rightButton.css("margin-right",newMargin+"px");
      }
   }
   
   //center bootstrap modal
   function centerModal() {
       $(this).css('display', 'block');
       var $dialog = $(this).find(".modal-dialog");
       var offset = ($(window).height() - $dialog.height()) / 2;
       // Center modal vertically in window
       $dialog.css("margin-top", offset);
   }
   
   //function to center (horizontal) element by adding margin
   function centerElementMarginH(element, container){
      if(element.length > 0 && container.length > 0)
      {
         //clean margins
         element.css("margin-left","0");
         element.css("margin-right","0");
         
         var realContainerWidth = container.width();
         var realElementWidth = element.width() + parseInt(element.css("padding-left").replace("px", "")) + parseInt(element.css("padding-right").replace("px", ""));
         var newMargin = (realContainerWidth - realElementWidth)/2;
         element.css("margin-left",newMargin+"px");
         element.css("margin-right",newMargin+"px");
      }
   }

   function getZipForMap ()
   {

      if( (hrf.indexOf("dmv-locations")!=-1) )
      {
         if (divice)
         {
            $("#submitMob").click(function(){
               var zipMVal = $("#zipM").val();
               $("#submitMob").attr("href", "https://maps.google.com/maps?q=dmv+" + zipMVal); 
            });
         }
      }
   }
   getZipForMap ();

   // generate width of the new disclaimer in form page
   function newDisclaimerWidth()
   {
      var formWidth = $(".personal-form .personal-frm.radiusBoxContainer").width();
      $('#main_14, #main_14a').css({"width":formWidth+"px "});
   }

   function mobileFormsPersonal ()
   {
      if (divice)
      {
         $(".personal-frm").css({"width":"100%","margin":"0 0"});
         $(".personal-form form table").first().css({"position":"absolute"});
         //$("#main_14").replaceWith("<p>MyDriversLicenses.org is a third party website not affiliated with any government agencies. The services that we provide can be available for free in the official sites or local offices. You can purchase &amp; download for USD 19.98 our guide and resources, which contains processed information in order to perform any DMV service. By continuing using our website, you accept our <a href='/tc.html'>Terms and Conditions</a> and <a href='/privacy.html'>Privacy Policy</a>.&nbsp;</p>")
         //$("#main_14").replaceWith("<p>MyDriversLicenses.org is a third party website not affiliated with any government agencies. The services that we provide can be available for free in the official sites or local offices. You can purchase &amp; download for USD 19.98 our guide and resources, which contains processed information in order to perform any DMV service. By continuing using our website, you accept our <a href='/tc.html'>Terms and Conditions</a> and <a href='/privacy.html'>Privacy Policy</a>.&nbsp;</p>");
         newDisclaimerWidth();
      }
   }

   function mobileFormsCOA ()
   {
      if (divice)
      {
         $(".personal-form-coa, .personal-form-coa .personal-frm").css({"width":"100%","margin":"0 0"});
         $("#main_26a, #main_27a, #extra_1a, #extra_1e, #extra_6a, #extra_17a, #extra_23b, #extra_25b, #extra_27a").css({"width":"295px"});
         $(".personal-form-coa #main_30").css({"margin":"0"});
         console.log("device");
      }
   }
   
   //function to change color on agree input on billing page
   function changeAgreeColor(change){
      if(change){
         $(".billing-page .billing-bottom p.billing-disclaimer").css({"color":"#ff0000"});
         $(".billing-page .billing-bottom #agree-tc").css({"outline":"1px solid #ff0000"});
      }else{
         $(".billing-page .billing-bottom p.billing-disclaimer").css({"color":"#333"});
         $(".billing-page .billing-bottom #agree-tc").css({"outline":"none"});
      }
   }
   
   //fix display of disclaimer in FF and IE on the first form
   if(hrf.indexOf("/step1/") != -1){
      function personalFormDisc(){
         $("#main_14a").replaceWith("<p>MyCar-Reg.org is a third party website not affiliated with any government agencies. The services that we provide can be available for free in the official sites or local offices. You can purchase &amp; download for USD 19.99 our guide and resources, which contains processed information in order to perform any DMV service. By continuing using our website, you accept our <a href='/tc.html'>Terms and Conditions</a> and <a href='/privacy.html'>Privacy Policy</a>.</p>");
      }
      personalFormDisc();

      function mask_phone_number ()
      {
         var get_the_phone = $("#phone_1");
         get_the_phone.on('keyup', function()
         {
            if( event.keyCode == 8 ) {
               event.preventDefault();
               if( get_the_phone.val().indexOf("-") && get_the_phone.val().length == 3  || get_the_phone.val().length == 7 ){
                  var mask_number = get_the_phone.val() + "";
                  get_the_phone.val(mask_number);
               }
            }
            else if( get_the_phone.val().length == 3  || get_the_phone.val().length == 7 ){
               var mask_number = get_the_phone.val() + "-";
               get_the_phone.val(mask_number);
            }
         });
      }
      mask_phone_number();

      var pFormWidth = $(".personal-frm").width();
      $(".outter_disclaimer").css({"width": pFormWidth+"px"});

      function htmlForm ()
      {
         centerElementMarginH($(".personal-frm.radiusBoxContainer"), $("#frm"));
         centerElementMarginH($(".outter_disclaimer"), $("#frm"));
      }
      htmlForm ();
   }

   // coupon step1
   /*
   if( hrf.indexOf("/step1/") !=-1  )
   {
      centerElementMarginH($("#cModal"), $(".ouibounce-modal"));

      function autoFillStep1()
      {
         $("#first_name").val("Name");
         $("#last_name").val("Last");
         $("#address_1").val("Address");
         $("#city_1").val("City");
         $("#state_1").val("CA");
         $("#zip_1").val("00000");
         $("#gender").val("M");
         $("#phone_1").val("8888888888");
         $("#email").val("email@email.com");
         $("#_mm_date_of_birth").val("2");
         $("#_dd_date_of_birth").val("20");
         $("#_yy_date_of_birth").val("1991");
      }

      function bringCouponModal(){
         ouibounce(document.getElementById('ouibounce-modal'), {
            aggressive: true,
            sitewide: true,
            timer: 0
         });
         var validateCoupon = $("#couponForm").validate({
            rules: {
               mail_coupon: {
                  required: true,
                  email: true
               }
            },
            messages: {
               mail_coupon: {
                  required: "Email is Required"
               }
            },
            submitHandler: function(form) {
               console.log("sumbit");
               $(".coupons_inputs, .btn-getCoupon").css({"display":"none"});
               $("#spin_coupon").css({"display":"block"});
               autoFillStep1();
               $("#email").val($("#mail_coupon").val());
               setTimeout(function() {
                  $("#frm").submit();
               }, 100);   
            }
         });
      }
      function closeCouponModal()
      {
         $(".modalClose").on('click', function(e) {
           $('#ouibounce-modal').hide();
           e.stopPropagation();
         });         
      }
      if(hrf.indexOf("postForm") == -1){
         bringCouponModal();
         closeCouponModal();
      }
   }
   */
   // coupon billing
   if( hrf.indexOf("/billing") != -1  )
   {
      var firstNamePreD = $("#first_name").val();
      var lastNamePreD  = $("#last_name").val();
      var zipPreD       = $("#zip").val();

      function closeCouponModal2()
      {
         $(".modalClose, .couponSubmit").on('click', function(e) {
           $('#ouibounce-modal').hide();
           e.stopPropagation();
         });         
      }

      function bringCouponModal2(){ 
         ouibounce(document.getElementById('ouibounce-modal'), {
            aggressive: true,
            sitewide: true,
            timer: 0
         });
      }
      closeCouponModal2();
      bringCouponModal2();

   }
   
   if( hrf.indexOf("/redeem-gas") !=-1  )
   {
      var validateCoupon = $("#couponRedeemForm").validate({
         rules: {
            mail_coupon_redeem: {
               required: true,
               email: true
            }
         },
         messages: {
            mail_coupon_redeem: {
               required: "Email is Required"
            }
         },
         submitHandler: function(form) {
            form.submit();  
         }
      });
   }
   //function to validate billing form
   function validateBillingForm(){
      var   required    =  "";
      var   errors      =  "";
      var   firstField  =  "";
      $("#billingForm #billing-error").html("");
      $("#billingForm label").removeClass("error");
      changeAgreeColor(false);
      if($("#billingForm #first_name").val() == ""){
         if(required != ""){required += ", ";}
         required += "First name";
         $("#billingForm label[for='first_name']").addClass("error");
         if(firstField  ==  ""){firstField = $("#billingForm #first_name");}
      }
      if($("#billingForm #last_name").val() == ""){
         if(required != ""){required += ", ";}
         required += "Last name";
         $("#billingForm label[for='last_name']").addClass("error");
         if(firstField  ==  ""){firstField = $("#billingForm #last_name");}
      }
      if($("#billingForm #address").val() == ""){
         if(required != ""){required += ", ";}
         required += "Address";
         $("#billingForm label[for='address']").addClass("error");
         if(firstField  ==  ""){firstField = $("#billingForm #address");}
      }
      if($("#billingForm #city").val() == ""){
         if(required != ""){required += ", ";}
         required += "City";
         $("#billingForm label[for='city']").addClass("error");
         if(firstField  ==  ""){firstField = $("#billingForm #city");}
      }
      if($("#billingForm #state").val() == ""){
         if(required != ""){required += ", ";}
         required += "State";
         $("#billingForm label[for='state']").addClass("error");
         if(firstField  ==  ""){firstField = $("#billingForm #state");}
      }
      if($("#billingForm #zip").val() == ""){
         if(required != ""){required += ", ";}
         required += "Zip Code";
         $("#billingForm label[for='zip']").addClass("error");
         if(firstField  ==  ""){firstField = $("#billingForm #zip");}
      }
      if($("#billingForm #card_number").val() == ""){
         if(required != ""){required += ", ";}
         required += "Credit Card Number";
         $("#billingForm label[for='card_number']").addClass("error");
         if(firstField  ==  ""){firstField = $("#billingForm #card_number");}
      }else{
         var creditCardLength = $("#billingForm #card_number").val().length;
         if( creditCardLength < 13 || creditCardLength > 25 ){
            errors += "Invalid Credit Card Number. ";
            $("#billingForm label[for='card_number']").addClass("error");
            if(firstField  ==  ""){firstField = $("#billingForm #card_number");}
         }else if( $("#billingForm #card_number").val().substring(0, 1) === "3" ){
            errors += "Only VISA or MasterCard cards are accepted. ";
            $("#billingForm label[for='card_number']").addClass("error");
            if(firstField  ==  ""){firstField = $("#billingForm #card_number");}
         }
      }
      if($("#billingForm #month").val() == "" || $("#billingForm #year").val() == ""){
         if(required != ""){required += ", ";}
         required += "Expiration Date";
         $("#billingForm label[for='expires']").addClass("error");
         if(firstField  ==  ""){firstField = $("#billingForm #month");}
      }
      if($("#billingForm #security_code").val() == ""){
         if(required != ""){required += ", ";}
         required += "Security Code";
         $("#billingForm label[for='security-code']").addClass("error");
         if(firstField  ==  ""){firstField = $("#billingForm #security_code");}
      }else{
         var codeLength = $("#billingForm #security_code").val().length;
         if( codeLength < 3 || codeLength > 4 ){
            errors += "Invalid Security Code. ";
            $("#billingForm label[for='security-code']").addClass("error");
            if(firstField  ==  ""){firstField = $("#billingForm #security_code");}
         }
      }
      /*
      if(!$("#agree-tc").is(':checked')){
         if(required != ""){required += ", ";}
         required += "Agree Terms and Conditions";
         changeAgreeColor(true);
         if(firstField  ==  ""){firstField = $("#agree-tc");}
      }
      */
      if(required.indexOf(",") != -1){
         required += " are required. ";
      }
      else{
         if(required != ""){required += " is required. ";}
      }
      errors = required + errors;
      
      if(errors != ""){
         $("#billingForm #billing-error").html(errors);
         firstField.focus();
         console.log(firstField.attr('id'));
         return false;
      }
      else{
         $("#submitBilling").css({"display":"none"});
         $("#billingLoadingGif").css({"display":"block"});
         $("#billingForm").submit();
         return true;
      }
   }

   //remove header menu from flow pages desktop and mobile
   function removeHeaderMenu()
   {
      //affected urls
      var stepOne   = hrf.indexOf("/forms/step1/")!=-1;
      var billing   = hrf.indexOf("/billing")!=-1;
      var loading   = hrf.indexOf("/dl-confirmation")!=-1;
      var checklist = hrf.indexOf("/checklist/")!=-1;

      if( stepOne || billing || loading || checklist )
      {
         $(".navigation.desktopElements, .mobile-nav.mobileMenu.mobileElements, .footer-link-container").css({"display":"none"});
      }
   }
   
   //function to fix elements on landing page
   function fixLandingPage()
   {
      //adjust elements on service/state landing page
      if($(".service-information-boxes").length > 0){
         setTimeout(function(){
            if( divice == false ) {
               infoBoxesSize();
            }
            buttonTopPosition();
            
            //center buttons on mobile
            if( divice == true ) {
               centerElementMarginH($(".service-information-boxes .left-box a.serviceCTA.showMobile"), $(".service-information-boxes .info_boxes.left-box"));
               centerElementMarginH($(".middle-content.addressChange a.btncontinue.continueFlow"), $(".middle-content.addressChange"));
            }
         },100);
      }
   }
   
   function fixMenuMobileLandscape()
   {
      var viewportWidth = $(window).width();
      var viewportHeight = $(window).height();

      if ( viewportWidth < 993 )
      {
         $(".rmm.minimal").css({"max-width":"100%"});
         $(".rmm-toggled").css({"display":"block"});
         $(".rmm-toggled.rmm-closed").css({"display":"block"});
         $(".rmm-main-list").css({"display":"none"});
      }
   }

   //fix landscape on iphone
   function fixiPhoneLandscape(){
      
      var winWidth = $(window).width();
      var winHeight = $(window).height();

      if ( iPhone && winWidth > winHeight ){
         $("html").addClass("iphone-landscape");
      }else{
         $("html").removeClass("iphone-landscape");
      }
   }
   
   //get url parameters
   function getUrlParameter(sParam)
   {
       var sPageURL = window.location.search.substring(1);
       var sURLVariables = sPageURL.split('&');
       for (var i = 0; i < sURLVariables.length; i++) 
       {
           var sParameterName = sURLVariables[i].split('=');
           if (sParameterName[0] == sParam) 
           {
               return sParameterName[1];
           }
       }
   }

   /*fix the form on resize*/
   if(hrf.indexOf("/step1/") != -1){
      function removeNonNeened()
      {
         $("#main_5, #main_7, #main_9, #main_11, #main_13").remove();
      }
      function fixFormResize(){
         var targetForm        = $(".personal-form .personal-frm");
         var targetWindowWidth = $(window).width();
         var targetFormOffset  = targetForm.offset().left; 
         var targetMaxWidth    = targetWindowWidth - targetFormOffset;

         if( hrf.indexOf("/step1/") ){
            if( targetWindowWidth < 993 ){
               targetForm.parent().parent().css({"width":"100%"});
               targetForm.children().first().css({"width":"100%"});
               targetForm.css({"margin":"0","width":"100%","max-width": +targetMaxWidth+"px","margin-right":+targetFormOffset+"px"});
            }
         } 
      }
      
      $(window).on("resize", function () {
         fixFormResize();
         validateStep1FormFields();
      });
      removeNonNeened();
   }
   
   /********************************************************************************/
   /******************************** GLOBAL CHANGES ********************************/
   /********************************************************************************/
   //init mobile menu
   if (typeof responsiveMobileMenu === "function") {
      if( (hrf.indexOf("/billing.html")!=-1) )
      {
         setTimeout(function(){
            responsiveMobileMenu();
            getMobileMenu();
            adaptMenu();
            /* slide down mobile menu on click */
            $('.rmm-toggled, .rmm-toggled .rmm-button').click(function(){
               if ( $(this).is(".rmm-closed")) {
                  $(this).find('ul').stop().show(300);
                  $(this).removeClass("rmm-closed");
               }
               else {
                  $(this).find('ul').stop().hide(300);
                  $(this).addClass("rmm-closed");
               }
            });
         },100);
         
      }
      else
      {
         responsiveMobileMenu();
         getMobileMenu();
         adaptMenu();
         /* slide down mobile menu on click */
         $('.rmm-toggled, .rmm-toggled .rmm-button').click(function(){
            if ( $(this).is(".rmm-closed")) {
               $(this).find('ul').stop().show(300);
               $(this).removeClass("rmm-closed");
            }
            else {
               $(this).find('ul').stop().hide(300);
               $(this).addClass("rmm-closed");
            }
         });
      }
   }
   
   
   //event on resize window
   $(window).on("resize", function () {
      htmlForm ();
      adaptMenu();
      $('.modal:visible').each(centerModal);
      $(".left-box").css("height","auto");
      $(".service-info-container").css("height","auto");
      infoBoxesSize ();
      buttonTopPosition();
      //centerElementMarginH( $(".footer-options ul, .footer-options p") , $(".footer-options"));
      if( (hrf.indexOf("/forms/step1")!=-1) )
      {
            centerElementMarginH($(".personal-frm.radiusBoxContainer"), $("#frm"));
      }
      if( (hrf.indexOf("/forms/coa")!=-1) )
      {
         centerElementMarginH($(".personal-frm.radiusBoxContainer"), $(".col-md-12.align-wrapper h1"));
      }
      //we call function to fix elements on landing page:
      fixLandingPage();
      //we call function to fix menu on tablets:
      fixMenuMobileLandscape();

      //center service container
      //centerElementMarginH($(".select-service-container.radiusBoxContainer"), $(".container.main-content"));

      fixiPhoneLandscape();

      newDisclaimerWidth();
   });
   
   //center footer
   //centerElementMarginH( $(".footer-options ul, .footer-options p") , $(".footer-options"));   
   
   //change disclaimer when there is a service and a state selected
   if(dmv_link != "null" && $(".disclaimer.pages").length != 0){
      $(".disclaimer.pages").html($(".disclaimer.pages").html().replace("government agency", "<a href='"+dmv_link+"'>government agency</a>"));
   }
   
   //modal event on show
   $('.modal').on('show.bs.modal', centerModal);
   
   //button actions
   $(".goto-per-form").click(function(e) {
      document.location.href = "/forms/step1/" + getUrl(1) + "/" + getState() + ".html";
      //document.location.href = "http://"+domainName+"/index.html";
      return false;
   });
   $(".goto-coa-form").click(function(e) {
      document.location.href = "/forms/coa/" + getUrl(1) + "/" + getState() + ".html";
      return false;
   });
   $(".goto-serv-spec").click(function(e) {
      var btnId = $(this).attr('id');
      //document.location.href = "/" + btnId + "/" + getState() + ".html";
      document.location.href = "/forms/step1/" + btnId + "/" + getState() + ".html";
      return false;
   });

   //select box actions
   if($(".comboBoxSelection.showMobile.drivers-license").length > 0){
      var serviceSel   = $("#main-service");
      if(serviceSel.length > 0)
         serviceSel.css({"margin":"10px"});
      serviceSel.on("change",function()
      {
         if ( serviceSel.val() != ""){
            var getServiceVal = serviceSel.val();
            if(hrf.indexOf("/index.html") == -1 && hrf.indexOf("/dmv-services.html") == -1)
               document.location.href = "/forms/step1/" + getServiceVal + "/" + getState() + ".html";
         }else{
            return false;             
         }
      });
   }
   else if($(".comboBoxSelection.showMobile.car-registration").length > 0){
      var serviceSel   = $("#main-service");
      if(serviceSel.length > 0)
         serviceSel.css({"margin":"10px"});
      serviceSel.on("change",function()
      {
         if ( serviceSel.val() != ""){
            var getServiceVal = serviceSel.val();
            if(hrf.indexOf("/index.html") == -1 && hrf.indexOf("/dmv-services.html") == -1)
               document.location.href = "https://mycar-reg.org/forms/step1/" + getServiceVal + "/" + getState() + ".html";
         }else{
            return false;             
         }
      });
   }
   else if($(".comboBoxSelection.showMobile.car-title").length > 0){
      var serviceSel   = $("#main-service");
      if(serviceSel.length > 0)
         serviceSel.css({"margin":"10px"});
      serviceSel.on("change",function()
      {
         if ( serviceSel.val() != ""){
            var getServiceVal = serviceSel.val();
            if(hrf.indexOf("/index.html") == -1 && hrf.indexOf("/dmv-services.html") == -1)
               document.location.href = "https://cartitlenow.org/forms/step1/" + getServiceVal + "/" + getState() + ".html";
         }else{
            return false;             
         }
      });
   }
   
   //we call function to fix elements on landing page:
   fixLandingPage();
   
   //we call function to fix menu on tablets
   fixMenuMobileLandscape();
   
   //center service container
   //centerElementMarginH($(".select-service-container.radiusBoxContainer"), $(".container.main-content"));

   //we call function to fix iphone landscape layout
   fixiPhoneLandscape();

   // we call function to remove header menu on the selected pages
   removeHeaderMenu();

   //we call function to rezise width of the disclaimer on the form page
   newDisclaimerWidth();

      //code for each specific page
   if( (hrf.indexOf("index.html")!=-1)  || (hrf.indexOf("/dmv-services.html")!=-1) || (hrf.indexOf("/guide.html")!=-1) )
   {
      function getTitleHome(){
         $(".btnSelection button").on("click",function(){
            var getBtnService = $(this).text();
            $(".get-d-service").text(getBtnService);
         });
         $("#state").on("change",function(){
            var getBtnState = $(this).val();
            if(getBtnState.indexOf("-") != -1){
               getBtnState = getBtnState.replace(/\-/g, ' ');
            }
            var statePressed = $(".get-d-state").text(getBtnState).css({"text-transform":"capitalize"});
         });      
      }
      //getTitleHome();
      
      //--- popup ---
      /*var referrer =  document.referrer;
      if( referrer.indexOf(domainName) ==-1 )
      {
         $.colorbox({ innerWidth:"750", innerHeight:"380", href:"/indexpop.html", "close":"",  "overlayClose": false, onLoad: function() { $('#cboxClose').html('<img src="http://s3.amazonaws.com/'+domainName+'/img/btncontinue.png" />'); }});
         $("#cboxOverlay").css("background","#888");
      }*/
      
      /*$("#step1 input:radio").change(function(){
         $("#step1 :radio ").parent(".checkBox").removeClass("checked");
         $("#step1 :radio:checked").parent(".checkBox").addClass("checked");
         $("#step1 :radio:checked").blur();
      });*/
      
      //preselected values from URL
      var url_service   = getUrlParameter('service');
      var url_state     = getUrlParameter('state');
      
      if(url_service != undefined && url_state != undefined)
      {         
         var service = $(".select-service-container button");
         service.removeClass("serviceWanted");
         $("#"+url_service).addClass("serviceWanted");
         var id = $(this).attr("id");
         var serviceSelected = id;
         
         if(hrf.indexOf("/dmv-services.html")!=-1 || hrf.indexOf("/index.html")!=-1 || hrf.indexOf("/guide.html")!=-1 )
         {
            var radioElement = $("#"+url_service);
            if(radioElement.length > 0)
               radioElement.prop("checked", true);
         }
         
         $("#state").val(url_state);
      }
      
      if ( divice == true )
      {
         $(".btnSelection").css({"display":"none"});
         $(".comboBoxSelection").css({"display":"block"});

         var serviceSel = $("#main-service");

         serviceSel.on("change",function()
         {
            if ( serviceSel.val() != ""){
               var getServiceVal = serviceSel.val();
            }else{
               return false;
            }
         });
         
      }else{
         
         $(".btnSelection").css({"display":"block"});
         $(".comboBoxSelection").css({"display":"none"});
         
         //mark service as selected
         $(".select-service-container button").click(function()
         {
            if($(this).hasClass("serviceWanted"))
               $(this).removeClass("serviceWanted");
            else{
               var service = $(".select-service-container button");
               service.removeClass("serviceWanted");
               $(this).addClass("serviceWanted");
            }
         });         
      }
      //--- home page validation ---
      $("#submit").click(function(e){

         var _state     = "";
         var _action    = "";
         var _serviceD  = "";
         var _specificS = "";
         
         if($("#state").length > 0)
            _state     = $("#state").val();
         if($('div.select-service-container button.service.serviceWanted').length > 0)
            _action    = $('div.select-service-container button.service.serviceWanted').attr('id');
         if($("#main-service").length > 0)
            _serviceD  = $("#main-service").val();
         if($('input[type="radio"]:checked').length > 0)
            _specificS = $('input[type="radio"]:checked').val();

         if( divice == false)
         {
            if( _specificS == "" )
            {
               if( _action == "" )
               {
                  $('#homeValidationModal h4.modal-title').html('Please Select a Service');
                  $('#homeValidationModal').modal('show');
                  return false;
               }
            }else{
               _action = _specificS;
            }
         }else{
            if( !_serviceD )
            {
               $('#homeValidationModal h4.modal-title').html('Please Select a Service');
               $('#homeValidationModal').modal('show');
               return false; 
            }else{
               _action = _serviceD;
            }
         }

         if( _state=="" )
         {
            $('#homeValidationModal h4.modal-title').html('Please Select a State');
            $('#homeValidationModal').modal('show');
            $("#state").focus();
            return false;
         }
         if( _state.length>0 )
         {
            if(hrf.indexOf("index.html")!=-1)
            {
               if( (hrf.indexOf("dev.mycar-reg.org/")!=-1) )
               {
                  document.location.href = "http://dev."+domainName+"/forms/step1/" + _action + "/" + _state + ".html";
               }
               else if( (hrf.indexOf("stage.mycar-reg.org/")!=-1) )
               {
                  document.location.href = "http://stage."+domainName+"/forms/step1/" + _action + "/" + _state + ".html";
               }
               else
               {
                  document.location.href = "http://"+domainName+"/forms/step1/" + _action + "/" + _state + ".html";
               }
            }
            else
            {
               if( (hrf.indexOf("dev.mycar-reg.org/")!=-1) )
               {
                  document.location.href = "http://dev."+domainName+"/" + _action + "/" + _state + ".html";
               }
               else if( (hrf.indexOf("stage.mycar-reg.org/")!=-1) )
               {
                  document.location.href = "http://stage."+domainName+"/" + _action + "/" + _state + ".html";
               }
               else
               {
                  document.location.href = "http://"+domainName+"/" + _action + "/" + _state + ".html";
               }
            }
            if(hrf.indexOf("guide.html")!=-1)
            {
               if( (hrf.indexOf("dev.mycar-reg.org/")!=-1) )
               {
                  document.location.href = "http://dev."+domainName+"/download/" + _action + "/" + _state + ".html";
               }
               else if( (hrf.indexOf("stage.mycar-reg.org/")!=-1) )
               {
                  document.location.href = "http://stage."+domainName+"/download/" + _action + "/" + _state + ".html";
               }
               else
               {
                  document.location.href = "http://"+domainName+"/download/" + _action + "/" + _state + ".html";
               }
            }
         }
         return false;
      });
   }

   var unkService = $("#insurance-registration");
   unkService.on("click",function(){
      document.location.href = "/insurance-registration.html";
   });
   //display box
   if ( divice == true )
      {
         $(".btnSelection, .state_btnSelection").css({"display":"none"});
         $(".comboBoxSelection").css({"display":"block"});

         var serviceSel   = $("#main-service");
         var insuranceReg = "insurance-registration";

         serviceSel.on("change",function()
         {
            if ( serviceSel.val() != ""){
               var getServiceVal = serviceSel.val();
               if( getServiceVal == insuranceReg ){
                  document.location.href = "/insurance-registration.html"
               }else{
                  if(hrf.indexOf("/index.html") == -1)
                     document.location.href = "/forms/step1/" + getServiceVal + "/" + getState() + ".html";
               }
            }else{
               return false;             
            }
         });
      }else{
         
         $(".btnSelection, .state_btnSelection").css({"display":"block"});
         $(".comboBoxSelection").css({"display":"none"});
         
         //mark service as selected
         $(".select-service-container button").click(function()
         {
            var service = $(".select-service-container button");
            service.removeClass("serviceWanted");
            $(this).addClass("serviceWanted");
            
            var id = $(this).attr("id");
            var serviceSelected = id;
         });         
      }
   //code for each specific page
   if( (hrf.indexOf("index.html")!=-1) )
   {
      //--- popup ---
      /*var referrer =  document.referrer;
      if( referrer.indexOf(domainName) ==-1 )
      {
         $.colorbox({ innerWidth:"750", innerHeight:"380", href:"/indexpop.html", "close":"",  "overlayClose": false, onLoad: function() { $('#cboxClose').html('<img src="http://s3.amazonaws.com/'+domainName+'/img/btncontinue.png" />'); }});
         $("#cboxOverlay").css("background","#888");
      }*/
      
      /*$("#step1 input:radio").change(function(){
         $("#step1 :radio ").parent(".checkBox").removeClass("checked");
         $("#step1 :radio:checked").parent(".checkBox").addClass("checked");
         $("#step1 :radio:checked").blur();
      });*/
      var unkService = $("#insurance-registration");
      unkService.on("click",function(){
         document.location.href = "/insurance-registration.html";
      });
      //preselected values from URL
      var url_service   = getUrlParameter('service');
      var url_state     = getUrlParameter('state');
      
      if(url_service != undefined && url_state != undefined)
      {
         console.log("url_service-->"+url_service);
         console.log("url_state-->"+url_state);
         
         var service = $(".select-service-container button");
         service.removeClass("serviceWanted");
         $("#"+url_service).addClass("serviceWanted");
         var id = $(this).attr("id");
         var serviceSelected = id;
         
         $("#state").val(url_state);
      }
      /*
      //--- home page validation ---
      $("#submit").click(function(e){

         var _state    = $("#state").val();
         var _action   = $('div.select-service-container button.service.serviceWanted').attr('id');
         var _serviceD = $("#main-service").val();

         if( divice == false)
         {
            if( !_action )
            {
               $('#homeValidationModal h4.modal-title').html('Please Select a Service');
               $('#homeValidationModal').modal('show');
               return false;
            }
         }else{
            if( !_serviceD )
            {
               $('#homeValidationModal h4.modal-title').html('Please Select a Service');
               $('#homeValidationModal').modal('show');
               return false; 
            }else{
               _action = _serviceD;
            }
         }

         if( _state=="" )
         {
            $('#homeValidationModal h4.modal-title').html('Please Select a State');
            $('#homeValidationModal').modal('show');
            $("#state").focus();
            return false;
         }
         if( _state.length>0 )
         {
            if( (hrf.indexOf("dev.mycar-reg.org/")!=-1) )
            {
               document.location.href = "http://dev."+domainName+"/forms/step1/" + _action + "/" + _state + ".html";
            }
            else if( (hrf.indexOf("stage.mycar-reg.org/")!=-1) )
            {
               document.location.href = "http://stage."+domainName+"/forms/step1/" + _action + "/" + _state + ".html";
            }
            else
            {
               document.location.href = "http://"+domainName+"/forms/step1/" + _action + "/" + _state + ".html";
            }
         }
         return false;
      });
*/
   }/*
   else
   if( (hrf.indexOf("/step1/")!=-1) )//personal form resize function
   {
      var errorM = $("label.err_msg").text();

      function pfromWidth ()
      {
         var errorM = $("label.err_msg").text();

         if (errorM < 1){
           var rowWidth = $(".personal-frm table tbody tr").width();
           var newWidth = rowWidth + 150;
           $(".personal-frm").css("width", newWidth+"px");
         }
         else
         {
           var rowWidth = $(".personal-frm table tbody tr").width();
           var newWidth = rowWidth + 170;
           $(".personal-frm").css("width", newWidth+"px");
         }
         centerElementMarginH($(".personal-frm.radiusBoxContainer"), $("#frm"));

         //centerCTAbtn
         $(".personal-form .personal-frm table").css({"margin":"0 auto"});

         mobileFormsPersonal ();
      }
      pfromWidth();

      function getErrors()
      {
         $(".personal-frm input, .personal-frm select").on( "focusout", function(){
            var errorLabel = $("label.err_msg");

            errorLabel.each(function(){
               var errrorM = $(this).text();
               if( errrorM < 1 )
               {
                  var rowWidth = errorLabel.parent().parent().width();
                  errorLabel.parent().parent().css({"width":"100%"});
                  var newWidth = rowWidth + 150;
                  $(".personal-frm").css("width", newWidth+"px");
               }
            });
            pfromWidth();
         });
      }
      getErrors();

      function btnAboveFold()
      {
         var stepOneFormWidth = $( ".personal-frm" ).width();

         $( ".personal-frm" ).children().first().addClass("firstStepForm");
         $( "#main_17" ).clone().appendTo( ".firstStepForm" );
         $( "#main_14" ).clone().appendTo( ".firstStepForm" );
         $( "#main_14" ).first().remove();
         $( "#main_16d" ).prev().parent().next().remove();
         $( ".firstStepForm" ).width(stepOneFormWidth);
         $("#main_17").on('click',function(){
            pfromWidth();
         });

      }

      $(window).on('resize',function(){
         pfromWidth();
         getErrors();
      });

      $("#main_17").on('click',function(){
         pfromWidth();
      });

      newDisclaimerWidth();
      btnAboveFold();
   }*/
   else
   if( (hrf.indexOf("/coa/")!=-1) )//coa form resize function
   {

      function arrangeCOAForm ()
      {
         if (divice)
         {
            $("#main_23").after("<tr id='main_new_address'></tr>");
            var newCol = $("#main_new_address");
            // to clone
            var naTitle        = $("#main_15f").clone().appendTo(newCol);
            var streetAddLabel = $("#main_16f").clone().appendTo(newCol);
            var streetAddInput = $("#main_16g").clone().appendTo(newCol);
            var aptSuitLabel   = $("#main_18f").clone().appendTo(newCol);
            var aptSuitInput   = $("#main_18g").clone().appendTo(newCol);
            var cityLabel      = $("#main_20f").clone().appendTo(newCol);
            var cityInput      = $("#main_20g").clone().appendTo(newCol);
            var stateLabel     = $("#main_22f").clone().appendTo(newCol);
            var stateInput     = $("#main_22g").clone().appendTo(newCol);
            var zipTotal       = $("#main_22h").clone().appendTo(newCol);
            // remove for mobile - next to structure
            $("#main_19, #main_15 #main_15f, #main_16 #main_16f, #main_16 #main_16g, #main_18 #main_18f, #main_18 #main_18g, #main_20 #main_20f, #main_20 #main_20g, #main_22 #main_22f, #main_22 #main_22g, #main_22 #main_22h").remove();
         }  
      }
      arrangeCOAForm();

      function centerCOActaBtn ()
      {
         $(".personal-form-coa .personal-frm #main_30").css({"width":"350px"});
         var contBtnCont   = $(".personal-form-coa .personal-frm #main_30").width();
         var coaFormWidth  = $(".personal-form-coa .personal-frm").width();
         var centerContBtn = (coaFormWidth - contBtnCont)/2;
         $(".personal-form-coa .personal-frm #main_30").css({"margin-left": centerContBtn+"px"});
         $(".personal-form-coa .personal-frm #main_29 .personal-frm.radiusBoxContainer").css({"width":"100%","margin-left":"0","margin-right":"0"});
      }

      $("div").find(".personal-form").addClass("personal-form-coa").removeClass("personal-form");
      $("#main_29 .personal-frm table").last().remove();
      $(".personal-form-coa, .personal-form-coa .personal-frm").css("width", "950px");

      function coaformWidth ()
      {
         var movingCont = $("#main_29 .personal-frm table").width();
         var movingContMatch = movingCont + 50;

         $("#forwarding_service").change(function(){
            if( $(this).val() == "Y" )
            {
               $(".personal-frm").css("width", movingContMatch+"px");
               $(".personal-form-coa, #main_29, #main_29 .personal-frm").css("width", movingCont+"px");
            }    
            else
            {
              $(".personal-form-coa, .personal-form-coa .personal-frm").css("width", "950px");
            }
         });
         centerElementMarginH($(".personal-frm.radiusBoxContainer"), $(".col-md-12.align-wrapper h1"));
      }
      coaformWidth();
      centerCOActaBtn();
      mobileFormsCOA();

      $(window).on('resize',function(){
         coaformWidth();
         centerCOActaBtn();
         mobileFormsCOA();
      });

      $("#main_31").on('click',function(){
         coaformWidth();
      });
   }
   else
   if( (hrf.indexOf("/road-advisors.html")!=-1) )
   {
      //update price
      itemCart();
      
      //change span on road advisors page when the user has gone through the flow before
      if(flow_last_step != "null"){
         $("span.linkToLastStep").html("<a href='"+flow_last_step+"'>complete the online application assistant</a>");
      }
   }
   else
   if( (hrf.indexOf("/billing.html")!=-1) )
   {
      /*$( ".finish-step" ).clone().appendTo( ".billing-form-right" );
      $( ".billing-bottom" ).next().remove();
      $( ".billing-page input.btncontinue" ).css({"float":"left"});*/

      itemCart();
      $("#card_number").css("border","2px solid #ea9351");
      $( window ).scrollTop( 0 );
      $(".openCard").click(function(e){
         $('#cvvModal').modal('show');
      });
      /*$("#billingForm #submitBilling").click(function(e){
         validateBillingForm();
         return false;
      });*/

      jQuery.validator.addMethod("vmcardsonly", function(value, element, param) {

         if( value.charAt(0) == 4 || value.charAt(0) == 5){
            $("#card_number-error").hide();
            return true;
         }else{
            $("#card_number-error").show();
            $('#card_number').val("");
            $("#card_number").focus();
            return false;
         }

      }, "We do not support American Express Cards");
      //, "Only Visa or MasterCard allowed");
      
      var validatorBilling = $("#billingForm").validate({
         onfocusout: false,
         onkeyup: false,
         rules: {
            first_name: {
               required: true
            },
            last_name: {
               required: true
            },
            state: {
               required: true
            },
            zip: {
               required: true,
               minlength: 5
            },
            "card_number": { 
               required: true,
               minlength:16,
               vmcardsonly: true 
            },/*
            card_number: {
               required: true,
               minlength: 16
            },*/
            month: {
               required: true
            },
            year: {
               required: true
            },
            security_code: {
               required: true,
               minlength: 3
            }
         },
         messages: {
            first_name: {
               required: "First Name is Required"
            },
            last_name: {
               required: "Last Name is Required"
            },
            state: {
               required: "State is Required"
            },
            zip: {
               required: "Zip Code is Required",
               minlength: "Invalid US ZIP Code"
            },
            card_number: {
               required: "Credit Card number is Required",
               minlength: "Invalid Credit Card number"
            },
            month: {
               required: "Month is Required"
            },
            year: {
               required: "Year is Required"
            },
            security_code: {
               required: "Security Code is Required",
               minlength: "Invalid Security Code"
            }
         },
         submitHandler: function(form) {
            $("ul.billing-form-left label.error").hide();
            //check credit card on client side
            $("#card_number-error").html("");
            var valid = $.payment.validateCardNumber($('#card_number').val());
            if (!valid) {               
               $("#card_number-error").html("Invalid Credit Card number");
               $("#card_number-error").show();
               $("#card_number").focus();
               return false;
            }
            $("#billingForm #billing-error").html("");
            $("#submitBilling").css({"display":"none"});
            $("#billingLoadingGif").css({"display":"block"});
            form.submit();
         }
      });
      
      //mask only number fields
      $("#zip").mask("99999");
      $("#card_number").mask("9999 9999 9999 9999");
      $("#security_code").mask("999");
      
      //fix when typing letters on credit card field
      $("#card_number").keyup(function() {
         if(this.value == "   ") this.value = "";
      });
      /*
      //validate when losing focus on form fields
      $("#first_name, #last_name, #state, #zip, #card_number, #month, #year, #security_code").focusout(function() {
         if(this.value == ""){
            var field_id    = $(this).attr('id');
            validatorBilling.element("#"+field_id);
         }
      });*/
   }
   else if( hrf.indexOf("/forms/step1/")!=-1 )
   {

      $.validator.addMethod("dobValid", function (value, element, options)
      {
          var anyEmpty = ( $("#_dd_date_of_birth").val() == 0 || $("#_mm_date_of_birth").val() == 0 || $("#_yy_date_of_birth").val() == 0 );

          if(!anyEmpty)
            return true;
      },
         "Birth Date Required"
      );

      $.validator.addMethod("mailValid", function (value, element, options)
      {
         var emailData = $("#email").val(),
             anyEmpty  = ( emailData.indexOf("@") == -1 || emailData.indexOf(".") == -1 );

         if(anyEmpty)
           return false;
         else{
            $("#email-error").css({"display":"none"});
            return true;
         }
      },
         "Email Invalid"
      );

      $.validator.addMethod("addressValid", function (value, element, options)
      {
          var addressData = $("#address_1").val(),
              anyEmpty = ( addressData.indexOf("#") != -1 || addressData.indexOf(" SUITE ") != -1 || addressData.indexOf(" APT ") != -1 || addressData.indexOf(" Apt ") != -1 || addressData.indexOf(" Suite ") != -1);
          if(!anyEmpty)
            return true;
      },
         "Invalid Address"
      );

      $.validator.addMethod("phoneValid", function (value, element, options)
      {
          var phoneData = $("#phone_1").val(),
              anyEmpty = ( phoneData == "" || phoneData.length < 10 || phoneData.length > 10 && phoneData.indexOf("-") == -1 );
          if(!anyEmpty)
            return true;
      },
         "Invalid Phone Number"
      );

      var validatorPersonal = $("#frm").validate({
         onfocusout: false,
         onkeyup: false,
         rules: {
            first_name: {
              required: true
            },
            last_name: {
              required: true
            },
            address_1: {
              required: true,
              addressValid: true
            },
            city_1: {
              required: true
            },
            state_1: {
              required: true
            },
            zip_1: {
              required: true,
              minlength: 5
            },
            gender: {
              required: true
            },
            phone_1: {
              required: true,
              phoneValid: true
            },
            date_of_birth: {
               required: true
            },
            email: {
              required: true,
              mailValid: true
            },
            "dob-year": { 
               required: true,
               dobValid: true 
            }
            /*
            "dob-month": { required: "required", dobValid: true },
            "dob-day":   { required: "required", dobValid: true },
            "dob-year":  { required: "required", dobValid: true }
            */
         },
         messages: {
            first_name: {
              required: "First Name required"
            },
            last_name: {
              required: "Last Name required"
            },
            address_1: {
              required: "Address required"
            },
            city_1: {
              required: "City required"
            },
            state_1: {
              required: "State required"
            },
            zip_1: {
              required: "Zip Code required",
              minlength: "Invalid Zip Code"
            },
            gender: {
              required: "Gender required"
            },
            phone_1: {
              required: "Phone required"
            },
            email: {
              required: "Email required"
            }
         },
         submitHandler: function(form) {
           form.submit();
         }
      });
      
      //mask only number fields
      $("#zip").mask("99999");
      $("#card_number").mask("9999 9999 9999 9999");
      $("#security_code").mask("999");
   }
   
   //ie help
   if( hrf.indexOf("billing.html")!=-1 || hrf.indexOf("fullfill.html")!=-1)
   {

      var ie = (function(){

         var undef,
            v = 3,
            div = document.createElement('div'),
            all = div.getElementsByTagName('i');

         while (
            div.innerHTML = '<!--[if gt IE ' + (++v) + ']><i></i><![endif]-->',
            all[0]
         );

         return v > 4 ? v : undef;

      }());

      if (ie<9){
         $("#billing_form").css("width","80%");
         $(".billing_content .right_subtitle").css("display","none");
         $(".billing_page #transaction_container .right_subtitle").css("display","block");
         $(".billing_page #transaction_container").css("margin","0 10%");
         $(".fullfill_page .left_container").css("width","65%");
         $(".fullfill_page .right_container").css({"width":"45%","margin":"1% 4%"});
      }

   }

   /*else
   if( hrf.indexOf("/checklist/")!=-1 )
   {         
      if( upsellYN == "Y" )
      {
         alert( "For all users, particularly for those accessing the site via Tablet or Mobile device, we have sent you a confirmation email with a link to download your information.\n\nYour download is also available on this page." );
      }
      var url = "/ajax/pdfExists.jsp?form=forms/" + getUrl(2) + "/" + getState();
      $.ajax({
         "url": url,
         "success": function(response){
            if( response.indexOf("true") ==-1 )
               $("#formDiv").hide();
         }
      });
      if( paymentTrans )
         $("#dl-success").show();
      else
         $("#aside").css("margin-top","40px");
      
      if( _PROFILE["usps"] == "N" )
      {
         $("#uspsDiv").show(); //aside column explanation
         $("#step5icon").attr("src","http://s3.amazonaws.com/"+domainName+"/img/step5checklist.png").show();  //step5
         $(".uspsY").show(); //usps image
      }
      
      if( getUrl(2) == "change-of-address" )
      {
         $("#genericCOA").show(); //aside column explanation
         $(".genericChangeAddress").show(); //change of address image
      }
      
      if ( $('.linksynY').length > 0 )
      {
         var a_href = $(".linksynY").closest('a[href]').attr("href");
         $('#outsideLink').click( function() {
            window.open( a_href );
            return false;
         });
      }
      
      //--- check if coverdell is successful --> show congrats pop
      if( coverdell )
      {
           //show gpp alert for 30 seconds and then close it
           $(".black_overlay, .gppalert").show();
           
           window.setTimeout(function() {
               $(".black_overlay, .gppalert").hide();
           }, 30000);
           
           $("#alertclose").click(function(){
               $(".black_overlay, .gppalert").hide();
           });
      }

   }
   else
   if( hrf.indexOf("/gpp.html") != -1 )
   {
       //--- checkbox ---
       $(".checkpair input:radio").change(function(){
          $(".checkpair :radio ").parent(".checkBox").removeClass("checked");
          $(".checkpair :radio:checked").parent(".checkBox").addClass("checked");
          $(".checkpair :radio:checked").blur();
       });
   
       $("#userstate").text(initCap(toStateName(_PROFILE["state_1"])));
       
      //--- check for million dollar policy cookie - if yes, show dropdown
      if( getCookie("selectedYes") == "y" )
      {
           $("#gpp-yes").parent(".checkBox").addClass("checked");
           $("#gpp-yes").attr("checked", true);
           
           $("#gpp-hide-confirm").slideDown("slow");
           
           $("#gpp-confirm").parent(".checkBox").addClass("checked");
           $("#gpp-confirm").attr("checked", true);
      }
       
       var serv = getUrlParam("service");
       
       $('input:radio[name="coverage"]').change(function(){
           if ($(this).is(':checked') && $(this).val() == 'y') {
               $("#gpp-hide-confirm").slideDown("slow");
               
                //---set cookie so you know user clicked yes already
               document.cookie="selectedYes=y;path=/";
           }
           else
           {
               $("#gpp-hide-confirm").slideUp("fast");
           }
       });
   
       $("#gpp-submit").click(function(){
           //---make sure checkbox is checked
           if (!$("input[name='coverage']:checked").val()) 
           {
              alert('In order to continue, please select either Yes or No.');
              return false;
           }
           else 
           {
               if ($("#gpp-yes").is(':checked') && !$("#gpp-confirm").is(':checked'))
               {
                   alert("To confirm your selection, please select the Step 2 checkbox below.");
                   return false;
               }
           }
           //if they selected yes to both, move on to gpp confirmation
           if( $("#gpp-yes").is(':checked') && $("#gpp-confirm").is(':checked'))
           {
               document.location.href = "/gpp-confirm.html";
           }
           else if( $("#gpp-no").is(':checked') ) //if not was selected - go to checklist page
           {
               document.location.href = "/checklistpage";
           }
      });
   }
   else
   if( hrf.indexOf("/gpp-confirm.html") != -1 )
   {
       var serv = getUrlParam("service");
       try
       {
           $("#fname").text( _PROFILE["first_name"] );
           $("#lname").text( _PROFILE["last_name"] );
           $("#address").text( _PROFILE["address_1"] );
           $("#city").text( _PROFILE["city_1"] );
           $("#state").text( _PROFILE["state_1"] );
           $("#zip").text( _PROFILE["zip_1"] );
           $("#email").text( _PROFILE["email"] );
           $("#phone").text( _PROFILE["phone_1"] );
       }
       catch(e){}
   
       $("#gpp-submit").click(function(){  
           $("#gpp-submit").hide();
           $("#spinner").show();
           
           //---add user to gpp queue
           $.ajax({ "url":  "/ajax/addQ.jsp?trg=gppQ",  "success": function(response){
                  document.location.href = "/coverdell.jsp";
               }
           });
       });
       $("#gpp-nothanks").click(function(){  
           document.location.href = "/checklistpage";
       });
   }
   else
   if( hrf.indexOf("/splash/")!=-1 || hrf.indexOf("/landing/")!=-1 )
   {
      $("#step1splash input:radio").change(function(){
         $("#step1splash :radio ").parent(".checkBox").removeClass("checked");
         $("#step1splash :radio:checked").parent(".checkBox").addClass("checked");
         $("#step1splash :radio:checked").blur();
      });
      
      //---all states have id-cards and suspneded now
      $("#reinstate-checkbox").show();
      $("#idcards-checkbox").show();
      
      if( hrf.indexOf("/splash/")!=-1 || hrf.indexOf("/landing/")!=-1 )
      {
         $("#indexBox").css("margin-top","25px");
         var theState = capWords(getState().replace("-"," ").replace("-"," "));
         if(theState == "Puerto Rico")
         {
            $('.all-states').hide();
            $('.pr').show();
            $("#idcards-checkbox").hide();
         }
         else
         {
            $("#idcards-checkbox").show();
            $('.pr').hide();
         }
      }
      try
      {
         var x = hrf.split("?")[1].split(":")[0];
         var opt1 = "new-drivers-license";
         var opt2 = "renew-drivers-license";
         var opt3 = "replace-drivers-license";
         var opt4 = "learners-permit";
         var opt5 = "change-of-address";
         var opt6 = "change-of-name";
         var opt7 = "reinstate-suspended-license";
         var opt8 = "id-cards";
         if(x == opt1 || x == opt2 || x == opt3 || x == opt4 || x == opt5 || x == opt6 || x == opt7 || x == opt8)
         {
            $("#"+x.toString()).parent(".checkBox").addClass("checked");
            $("#"+x.toString()).attr("checked", true);
         }
      }
      catch(e){}
      
      $("#stateName").text(capWords(getState().replace("-"," ").replace("-"," ")));
      
      //--- state splash pages validation ---
      $("#continue").click(function(e){
         var _service = $('input[name=action]:checked',   '#splash-form').val();
         if( !_service )
         {
            alert( "Please Select an Action" );
            return false;
         }
         
         if(_service == "change-of-address")
            document.location.href = "http://"+domainName+"/forms/coa/" + _service + "/" +getState()+ ".html";
         else
            document.location.href = "http://"+domainName+"/forms/step1/" + _service + "/" +getState()+ ".html";
      });
   }
   else
   if( (hrf.indexOf("/servsplash/")!=-1) || (hrf.indexOf("/servlanding/")!=-1))
   {
      try 
      {
         var val        = hrf.split('/');
         var _service   = val[4];
         var urlState   = val[5].split('.');
         var _state     = urlState[0];
      }
      catch(e) {}
      
      $(".splash_startTX").click(function() {
         if(_service == "change-of-address")
            document.location.href = "http://"+domainName+"/forms/coa/" + _service + "/" +_state+ ".html";
         else
            document.location.href = "http://"+domainName+"/forms/step1/" + _service + "/" +_state+ ".html";
      });
   }
   else
   if( hrf.indexOf("/docs.html")!=-1 || hrf.indexOf("/guide.html")!=-1 )
   {
      if( hrf.indexOf("guide.html")!=-1 )
         track("guide");
      
      //--- checkbox ---
      $("#step1 input:radio").change(function(){
         $("#step1 :radio ").parent(".checkBox").removeClass("checked");
         $("#step1 :radio:checked").parent(".checkBox").addClass("checked");
         $("#step1 :radio:checked").blur();
      });
      
      //--- home page validation ---
      $("#submit").click(function(e){
         var state  = $("#state").val();
         var serv = $('input[name=action]:checked','#first-form').val();
         
         if( !serv )
         {
            alert( "Please Select an Action" );
            return false;
         }
         if( state=="" )
         {
            alert( "Please Select a State" );
            $("#state").focus();
            return false;
         }
         if( state.length>0 )
         {
            // --- if has form, show in shopping cart
            var url = "/ajax/pdfExists.jsp?form=forms/" + serv + "/" + state;
            $.ajax({
               "url": url,
               "success": function(response){
                  if( response.indexOf("true") !=-1 )
                  {
                     $("#form").show();
                     $("#form").click(function(){
                        document.location.href = "/PDForm/" + serv + "/" + state + ".pdf";
                     });
                  }
               }
            });
            
            $("#checklist").click(function(){
               document.location.href = "/checklist/" + serv + "/" + state + ".pdf";
            });
            
            $("form, #seals").hide();
            $("#dlh1").text("Your Documents are Ready!");
            $("#download-par").text("Your Driver License Easy Guide and form (if applicable) are available below for download.");
            $("#progress").attr("src", "http://s3.amazonaws.com/"+domainName+"/img/progress2.png");
            if( serv == "change-of-address" ) 
               $("#genericChangeAddress, #coabr").show();
            $("#show-downloads").show();
         }
      });
   }*/
   
   //-----------------------------------------
   //--- Service Splash Pages ---
   //-----------------------------------------
   /*if( (hrf.indexOf(".org/new-drivers-license/")!=-1) || (hrf.indexOf(".org/renew-drivers-license/")!=-1) || (hrf.indexOf(".org/replace-drivers-license/")!=-1) || (hrf.indexOf(".org/change-of-address/")!=-1) )
   {
    $("#stateinfo-cont").click(function() {
      document.location.href = "/forms/step1/" + getUrl(1) + "/" + getState() + ".html";
    });
   }*/
});

/*-----------------*/
/*--- postQuiz  ---*/
/*-----------------*/
/*function postQuiz( q )
{
   var n = "";
   if ( parent.document.location.href.indexOf("s=california")!=-1 )
      n = "?s=california";
   parent.document.location.replace( "/pt-results.html"+n );
}*/
