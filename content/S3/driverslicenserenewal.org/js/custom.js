$(document).ready(function() {
    var today = new Date();

    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
        $('.article').readmore({
          speed: 75,
          moreLink: '<a href="#" class="bottom-buffer-20">Read more</a>',
          lessLink: '<a href="#" class="bottom-buffer-20">Read less</a>',
          heightMargin: 20
        });
        $('select[name="birth_month"] option:contains("Birth Month")').text('Month');
        $('select[name="birth_day"] option:contains("Birth Day")').text('Day');
        $('input[name="birth_year"]').attr('placeholder', 'Year');

        $('select[name="issued_month"] option:contains("DL/ID Issued Month")').text('Month');
        $('select[name="issued_day"] option:contains("DL/ID Issued Day")').text('Day');
        $('input[name="issued_year"]').attr('placeholder', 'Year');
    }
    else
    {
        $('select[name="birth_month"] option:contains("Month")').text('Birth Month');
        $('select[name="birth_day"] option:contains("Day")').text('Birth Day');
        $('input[name="birth_year"]').attr('placeholder', 'Birth Year');

        $('select[name="issued_month"] option:contains("Month")').text('DL/ID Issued Month');
        $('select[name="issued_day"] option:contains("Day")').text('DL/ID Issued Day');
        $('input[name="issued_year"]').attr('placeholder', 'DL/ID Issued Year');
    }


    $('.requirement_error_3').hide();
    $('.requirement_error_4').hide();
    $('.requirement_error_5').hide();

    $('input[type=radio][name=requirement_3]').change(function() {
        if (this.value == 'no') {
            $('.requirement_error_3').hide();
            if ( $('input[type=radio][name=requirement_4]:checked').val() == 'no' && $('input[type=radio][name=requirement_5]:checked').val() == 'yes' )
            {
                $('.submit-colorado-renew').show();
            }
        }
        if (this.value == 'yes') { $('.requirement_error_3').show(); $('.submit-colorado-renew').hide();}
    });

    $('input[type=radio][name=requirement_4]').change(function() {
        if (this.value == 'no') {
            $('.requirement_error_4').hide();
            if ( $('input[type=radio][name=requirement_3]:checked').val() == 'no' && $('input[type=radio][name=requirement_5]:checked').val() == 'yes' )
            {
                $('.submit-colorado-renew').show();
            }
        }
        if (this.value == 'yes') { $('.requirement_error_4').show(); $('.submit-colorado-renew').hide();}
    });

    $('input[type=radio][name=requirement_5]').change(function() {
        if (this.value == 'yes') {
            $('.requirement_error_5').hide();
            if ( $('input[type=radio][name=requirement_4]:checked').val() == 'no' && $('input[type=radio][name=requirement_3]:checked').val() == 'no' )
            {
                $('.submit-colorado-renew').show();
            }
        }
        if (this.value == 'no') { $('.requirement_error_5').show(); $('.submit-colorado-renew').hide();}
    });


    function handler(e) {
        var jqEl = $(e.currentTarget);
        var tag = jqEl.parents('.other-state-license');
        switch (jqEl.attr("data-action")) {
        case "add":
            // tag.clone().appendTo('#other-state-section');
            tag.after(tag.clone().find("input").val("").end());
            break;
        case "delete":
            tag.remove();
            break;
        }
        return false;
    }

    $( document ).on('touchstart click', 'a.duplicate, a.remove', handler);

    $('#other-state-section').hide();

    $('input[type=radio][name=other-states]').bind('touchstart click', function() {
        if ($(this).val() == 1)
        { $('#other-state-section').show(); }
        else
        { $('#other-state-section').hide(); }
    });


    $('a[data-toggle="tab"].next').on('shown.bs.tab', function (e) {
      var total = $('.progress-bar').data('total');
      var step = $(e.target).data('step');
      var percent = (parseInt(step) / parseInt(total)) * 100;
      $('.progress-bar').css({width: percent + '%'});
      $('html, body').animate({scrollTop: 0}, 'slow');
    })

    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })

    $('.mailing_address_wrap').toggle();
    $('#continue').hide();

    $('.options').bind('touchstart click', function() {
        $('#continue').show();
        $('.options i').removeClass("fa-check-square-o").addClass("fa-square-o");
        $("i", this).toggleClass("fa-square-o fa-check-square-o");
        $('a[data-toggle="tab"].next').removeClass('disabled');
    });

    $('input[type=checkbox][name=same_address]').change(function() {
        $('.billing_address_wrap, .mailing_address_wrap').toggle();
    });

    if ($('input[type=checkbox][name=same_address]').is(':checked')) {
        $('.billing_address_wrap, .mailing_address_wrap').hide();
    }


    $('#map').usmap({
      stateStyles: {fill: '#F3F6F9'},
      stateHoverStyles: {fill: '#00557A'},
      stateHoverAnimation: 300,
      showLabels: true,
      mouseover: function(event, data)
      {
        $('#us-map-alert').text(toStateName(data.name) + ' ( ' + data.name + ' )');
      },
      click: function(event, data)
      {
        window.location.href = "form/step1/start/" + toStateHyphen(data.name) + ".html";
      }

    });

    $("#continue-stateselect").click(function() {
      var state = $("#statedrop").val();
      if( state == "" || state == null )
      {
        $("#statedrop").css("border","1px solid red");
        alert("Please select a State");
        return false;
      }
      else
      {
        alert(state)
        window.location.href = "form/step1/start/" + toStateHyphen(data.name) + ".html";
      }
    });


    $('#driver_license_state').on('change', function() {
      $('#orderForm').formValidation('revalidateField', 'driver_license');
      $('#orderForm').formValidation('revalidateField', 'driver_license_state');
      if ($(this).val() == 'TX')
      {
        $('input[name="audit_number"]').show();
      }
      else
      {
        $('input[name="audit_number"]').hide();
      }
    });

    $('#orderForm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'fa fa-check',
            invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        },
        live: 'submitted',
        fields: {
            full_name: {
                validators: {
                    notEmpty: {
                        message: 'The full name is required'
                    },
                    regexp: {
                        message: 'The full name can only contain the letters, digits and spaces',
                        regexp: /^[A-Za-z0-9\s\-+\.]+$/
                    }
                }
            },
            emergency_full_name: {
                validators: {
                    regexp: {
                        message: 'The full name can only contain the letters, digits and spaces',
                        regexp: /^[A-Za-z0-9\s\-+\.]+$/
                    }
                }
            },
            agreement: {
                validators: {
                    notEmpty: {
                        message: 'You need to agree to our terms.'
                    }
                }
            },

            vision: {
                validators: {
                    notEmpty: {
                        message: 'You need to agree to the vision terms.'
                    }
                }
            },
            medical: {
                validators: {
                    notEmpty: {
                        message: 'You need to agree to the medical terms.'
                    }
                }
            },
            service: {
                validators: {
                    notEmpty: {
                        message: 'You need to pick one.'
                    }
                }
            },
            gender: {
                validators: {
                    notEmpty: {
                        message: 'You need to pick one.'
                    }
                }
            },
            eye: {
                validators: {
                    notEmpty: {
                        message: 'You need to pick one.'
                    }
                }
            },
            hair: {
                validators: {
                    notEmpty: {
                        message: 'You need to pick one.'
                    }
                }
            },
            height_feet: {
                validators: {
                    notEmpty: {
                        message: 'You need to pick one.'
                    }
                }
            },
            height_inches: {
                validators: {
                    notEmpty: {
                        message: 'You need to pick one.'
                    }
                }
            },
            weight: {
                validators: {
                    notEmpty: {
                        message: 'You need to pick one.'
                    },
                    between: {
                        min: 1,
                        max: 1200,
                        message: 'Weight is out of range'
                    }
                }
            },

            requirement_1: {
                validators: {
                    notEmpty: {
                        message: 'You need to pick one.'
                    }
                }
            },
            requirement_2: {
                validators: {
                    notEmpty: {
                        message: 'You need to pick one.'
                    }
                }
            },
            requirement_3: {
                validators: {
                    notEmpty: {
                        message: 'You need to pick one.'
                    }
                }
            },
            requirement_4: {
                validators: {
                    notEmpty: {
                        message: 'You need to pick one.'
                    }
                }
            },
            requirement_5: {
                validators: {
                    notEmpty: {
                        message: 'You need to pick one.'
                    }
                }
            },
            first_name: {
                validators: {
                    notEmpty: {
                        message: 'The first name is required'
                    },
                    regexp: {
                        message: 'The first name can only contain the letters, digits and spaces',
                        regexp: /^[A-Za-z0-9\s\-+\.]+$/
                    }
                }
            },
            middle_name: {
                validators: {
                    regexp: {
                        message: 'The middle name can only contain the letters, digits and spaces',
                        regexp: /^[A-Za-z0-9\s\-+\.]+$/
                    }
                }
            },
            last_name: {
                validators: {
                    notEmpty: {
                        message: 'The last name is required'
                    },
                    regexp: {
                        message: 'The last name can only contain the letters, digits and spaces',
                        regexp: /^[A-Za-z0-9\s\-+\.]+$/
                    }
                }
            },
            email: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    },
                    identical: {
                        field: 'verifyemail',
                        message: 'Enter the same email on next field'
                    }
                }
            },
            verifyemail: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'The email address is required'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    },
                    identical: {
                        field: 'email',
                        message: 'The email and its confirm are not the same'
                    }
                }
            },
            phone: {
                verbose: false,
                validators: {
                    notEmpty: {
                        message: 'The phone is required'
                    },
                    regexp: {
                        message: 'The phone number can only have 10 digits',
                        regexp: /^\d{10}$/
                    }
                }
            },
            emergency_phone: {
                verbose: false,
                validators: {
                    regexp: {
                        message: 'The phone number can only have 10 digits',
                        regexp: /^\d{10}$/
                    }
                }
            },
            driver_license: {
                validators: {
                    callback: {
                        message: 'DL Number is not valid',
                        callback: function(value, validator, $field) {
                            var current_dl_state = $('#driver_license_state').val();
                            checkDL = ValidateDriverLicenseByState(value, current_dl_state.toUpperCase());
                            if(checkDL != "valid") {
                              return {
                                  valid: false,
                                  message: checkDL
                              };
                            }
                            return true;
                        }
                    }

                }
            },
            driver_license_state: {
                validators: {
                    notEmpty: {
                        message: 'DL State is required'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z]+$/i,
                        message: 'Select a State'
                    }
                }
            },
            social_number: {
                validators: {
                    notEmpty: {
                        message: 'The SSN is required'
                    },
                    regexp: {
                        message: 'Enter only numbers',
                        regexp: /^([0-9]{4}|[0-9]{5}|[0-9]{9})$/,
                    }
                }
            },
            signature: {
                validators: {
                    notEmpty: {
                        message: 'Electronic signature is required'
                    }
                }
            },
            affirmation: {
                validators: {
                    notEmpty: {
                        message: 'Affirmation is required'
                    }
                }
            },
            audit_number: {
                validators: {
                    notEmpty: {
                        message: 'Audit number is required'
                    },
                    regexp: {
                        regexp: /^([0-9]{11}|[0-9]{20})$/,
                        message: 'Audit number should contain 11 or 20 numbers.'
                    }
                }
            },
            mailing_address: {
                validators: {
                    notEmpty: {
                        message: 'The address is required'
                    }
                }
            },
            mailing_apto: {
                validators: {
                    regexp: {
                        message: 'The apto seem to have invalid characters',
                        regexp: /^[A-Za-z0-9\s\-+\.]+$/
                    }
                }
            },
            mailing_city: {
                validators: {
                    notEmpty: {
                        message: 'The city is required'
                    },
                    regexp: {
                        message: 'The city seem to have invalid characters',
                        regexp: /^[A-Za-z0-9\s\-+\.]+$/
                    }
                }
            },
            mailing_state: {
                validators: {
                    regexp: {
                        message: 'The state is required',
                        regexp: /^[a-zA-Z]{2}$/
                    }
                }
            },
            mailing_zipcode: {
                validators: {
                    notEmpty: {
                        message: 'The zipcode is required'
                    },
                    regexp: {
                        message: 'The zip code can only have 5 digits',
                        regexp: /^\d{5}$/
                    }
                }
            },
            mailing_county: {
                validators: {
                    notEmpty: {
                        message: 'The county is required'
                    }
                }
            },
            on_driver_license_address: {
                validators: {
                    notEmpty: {
                        message: 'The address is required'
                    }
                }
            },
            on_driver_license_apto: {
                validators: {
                    regexp: {
                        message: 'The apto seem to have invalid characters',
                        regexp: /^[A-Za-z0-9\s\-+\.]+$/
                    }
                }
            },
            on_driver_license_city: {
                validators: {
                    notEmpty: {
                        message: 'The city is required'
                    },
                    regexp: {
                        message: 'The city seem to have invalid characters',
                        regexp: /^[A-Za-z0-9\s\-+\.]+$/
                    }
                }
            },
            on_driver_license_state: {
                validators: {
                    regexp: {
                        message: 'The state is required',
                        regexp: /^[a-zA-Z]{2}$/
                    }
                }
            },
            on_driver_license_zipcode: {
                validators: {
                    notEmpty: {
                        message: 'The zipcode is required'
                    },
                    regexp: {
                        message: 'The zip code can only have 5 digits',
                        regexp: /^\d{5}$/
                    }
                }
            },
            on_driver_license_county: {
                validators: {
                    notEmpty: {
                        message: 'The county is required'
                    }
                }
            },
            current_address: {
                validators: {
                    notEmpty: {
                        message: 'The address is required'
                    }
                }
            },
            current_apto: {
                validators: {
                    regexp: {
                        message: 'The apto seem to have invalid characters',
                        regexp: /^[A-Za-z0-9\s\-+\.]+$/
                    }
                }
            },
            current_city: {
                validators: {
                    notEmpty: {
                        message: 'The city is required'
                    },
                    regexp: {
                        message: 'The city seem to have invalid characters',
                        regexp: /^[A-Za-z0-9\s\-+\.]+$/
                    }
                }
            },
            current_state: {
                validators: {
                    regexp: {
                        message: 'The state is required',
                        regexp: /^[a-zA-Z]{2}$/
                    }
                }
            },
            current_zipcode: {
                validators: {
                    notEmpty: {
                        message: 'The zipcode is required'
                    },
                    regexp: {
                        message: 'The zip code can only have 5 digits',
                        regexp: /^\d{5}$/
                    }
                }
            },
            current_county: {
                validators: {
                    notEmpty: {
                        message: 'The county is required'
                    }
                }
            },
            birth_month: {
                validators: {
                    notEmpty: {
                        message: 'Month is required'
                    },
                    between: {
                        min: 1,
                        max: 12,
                        message: 'Month is required'
                    }
                }
            },
            birth_day: {
                validators: {
                    notEmpty: {
                        message: 'Day is required'
                    },
                    between: {
                        min: 1,
                        max: 31,
                        message: 'Day is required'
                    }
                }
            },
            birth_year: {
                validators: {
                    notEmpty: {
                        message: 'Year is required'
                    },
                    integer: {
                        message: 'The value is not a Year'
                    },
                    stringLength: {
                        min: 4,
                        max: 4,
                        message: 'Year must be 4 digits long'
                    },
                    between: {
                        min: 1900,
                        max: 2015,
                        message: 'Year is out of range'
                    }
                }
            },
            issued_month: {
                validators: {
                    notEmpty: {
                        message: 'Month is required'
                    },
                    between: {
                        min: 1,
                        max: 12,
                        message: 'Month is required'
                    }
                }
            },
            issued_day: {
                validators: {
                    notEmpty: {
                        message: 'Day is required'
                    },
                    between: {
                        min: 1,
                        max: 31,
                        message: 'Day is required'
                    }
                }
            },
            issued_year: {
                validators: {
                    notEmpty: {
                        message: 'Year is required'
                    },
                    integer: {
                        message: 'The value is not a Year'
                    },
                    stringLength: {
                        min: 4,
                        max: 4,
                        message: 'Year must be 4 digits long'
                    },
                    between: {
                        min: 1900,
                        max: today.getFullYear(),
                        message: 'Year is out of range'
                    }
                }
            },
            state: {
                validators: {
                    notEmpty: {
                        message: 'The State is required'
                    }
                }
            }
        }
    })
    .on('success.field.fv', function(e, data) {
        if (data.field === 'driver_license' && !data.fv.isValidField('driver_license_state')) {
            data.fv.revalidateField('driver_license_state');
        }

        if (data.field === 'driver_license_state' && !data.fv.isValidField('driver_license')) {
            data.fv.revalidateField('driver_license');
        }
    })
    .on('success.form.fv', function(e, data) {
        e.preventDefault();
        const $form = $(e.target);
        var state = $("#state").val();
        window.location.href = "form/step1/start/" + state + ".html";
    });


    $('#payForm').formValidation({
        framework: 'bootstrap',
        icon: {
            valid: 'fa fa-check',
            invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        },
        live: 'submitted',
        fields: {
            credit_card: {
                validators: {
                    notEmpty: {
                        message: 'The credit card is required'
                    },
                    creditCard: {
                        message: 'The credit card number is not valid'
                    }
                }
            },
            month: {
                validators: {
                    notEmpty: {
                        message: 'The month is required'
                    }
                }
            },
            year: {
                validators: {
                    notEmpty: {
                        message: 'The year is required'
                    }
                }
            },
            cvv: {
                validators: {
                    notEmpty: {
                        message: 'The CVV is required'
                    },
                    cvv: {
                        creditCardField: 'credit_card',
                        message: 'The CVV number is not valid'
                    }
                }
            },
            address: {
                validators: {
                    notEmpty: {
                        message: 'The address is required'
                    }
                }
            },
            city: {
                validators: {
                    notEmpty: {
                        message: 'The city is required'
                    },
                    regexp: {
                        message: 'The city seem to have invalid characters',
                        regexp: /^[A-Za-z0-9\s\-+\.]+$/
                    }
                }
            },
            state: {
                validators: {
                    regexp: {
                        message: 'The state is required',
                        regexp: /^[a-zA-Z]{2}$/
                    }
                }
            },
            zipcode: {
                validators: {
                    notEmpty: {
                        message: 'The zipcode is required'
                    },
                    regexp: {
                        message: 'The zip code can only have 5 digits',
                        regexp: /^\d{5}$/
                    }
                }
            }
        }
    })
    .on('success.field.fv', function(e, data) {

    });



    function toStateName(abbr)
    {
       var s = new Array();
       s["AL"] = "Alabama";s["AK"] = "Alaska";s["AZ"] = "Arizona";s["AR"] = "Arkansas";s["CA"] = "California";s["CO"] = "Colorado";s["CT"] = "Connecticut";s["DE"] = "Delaware";s["DC"] = "District of Columbia";s["FL"] = "Florida";s["GA"] = "Georgia";s["HI"] = "Hawaii";s["ID"] = "Idaho";s["IL"] = "Illinois";s["IN"] = "Indiana";s["IA"] = "Iowa";s["KS"] = "Kansas";s["KY"] = "Kentucky";s["LA"] = "Louisiana";s["ME"] = "Maine";s["MD"] = "Maryland";s["MA"] = "Massachusetts";s["MI"] = "Michigan";s["MN"] = "Minnesota";s["MS"] = "Mississippi";s["MO"] = "Missouri";s["MT"] = "Montana";s["NE"] = "Nebraska";s["NV"] = "Nevada";s["NH"] = "New Hampshire";s["NJ"] = "New Jersey";s["NM"] = "New Mexico";s["NY"] = "New York";s["NC"] = "North Carolina";s["ND"] = "North Dakota";s["OH"] = "Ohio";s["OK"] = "Oklahoma";s["OR"] = "Oregon";s["PA"] = "Pennsylvania";s["PR"] = "Puerto Rico";s["RI"] = "Rhode Island";s["SC"] = "South Carolina";s["SD"] = "South Dakota";s["TN"] = "Tennessee";s["TX"] = "Texas";s["UT"] = "Utah";s["VA"] = "Virginia";s["VT"] = "Vermont";s["WA"] = "Washington";s["WV"] = "West Virginia";s["WI"] = "Wisconsin";s["WY"] = "Wyoming";
       return s[abbr.toString()];
    }

    function toStateHyphen(abbr)
    {
       var s = new Array();
       s["AL"] = "alabama";s["AK"] = "alaska";s["AZ"] = "arizona";s["AR"] = "arkansas";s["CA"] = "california";s["CO"] = "colorado";s["CT"] = "connecticut";s["DE"] = "delaware";s["DC"] = "district-of-columbia";s["FL"] = "florida";s["GA"] = "georgia";s["HI"] = "hawaii";s["ID"] = "idaho";s["IL"] = "illinois";s["IN"] = "indiana";s["IA"] = "iowa";s["KS"] = "kansas";s["KY"] = "kentucky";s["LA"] = "louisiana";s["ME"] = "maine";s["MD"] = "maryland";s["MA"] = "massachusetts";s["MI"] = "michigan";s["MN"] = "minnesota";s["MS"] = "mississippi";s["MO"] = "missouri";s["MT"] = "montana";s["NE"] = "nebraska";s["NV"] = "nevada";s["NH"] = "new-hampshire";s["NJ"] = "new-jersey";s["NM"] = "new-mexico";s["NY"] = "new-york";s["NC"] = "north-carolina";s["ND"] = "north-dakota";s["OH"] = "ohio";s["OK"] = "oklahoma";s["OR"] = "oregon";s["PA"] = "pennsylvania";s["PR"] = "puerto-rico";s["RI"] = "rhode-island";s["SC"] = "south-carolina";s["SD"] = "south-dakota";s["TN"] = "tennessee";s["TX"] = "texas";s["UT"] = "utah";s["VA"] = "virginia";s["VT"] = "vermont";s["WA"] = "washington";s["WV"] = "west-virginia";s["WI"] = "wisconsin";s["WY"] = "wyoming";
       return s[abbr.toString()];
    }

    function ValidateDriverLicenseByState(DLNumber, state) {
      var sErrorMsg = 'valid';
      if (DLNumber === "") {
        sErrorMsg = 'DL Number is required';
        return sErrorMsg;
      }
      switch (state) {
        case "AL":
        case "AK":
          if (!RegExp("^\\d{7}$").test(DLNumber)) {
            sErrorMsg = "7 Numeric.";
          }
          break;
        case "AZ":
          if (!RegExp("^[A-Za-z]\\d{8}$|^\\d{9}$").test(DLNumber)) {
            sErrorMsg = "1Alpha+8Numeric (for example - A12345678) \nor \n9Numeric (for example - 123456789).";
          }
          break;
        case "AR":
          if (!RegExp("^\\d{8}$|^\\d{9}$").test(DLNumber)) {
            sErrorMsg = "8Numeric (for example - 12345678) \nor \n9Numeric (for example - 123456789).";
          }
          break;
        case "CA":
          if (!RegExp("^[A-Za-z]{1}\\d{7}$").test(DLNumber)) {
            sErrorMsg = "1Alpha+7Numeric (for example - A1234567).";
          }
          break;
        case "CO":
          if (!RegExp("^\\d{9}$|^[A-Za-z]{1,2}\\d{1,6}$").test(DLNumber)) {
            sErrorMsg = "9Numeric (for example - 123456789) \nor \n1-2Alpha+1-6Numeric (for example - A123456).";
          }
          break;
        case "CT":
          if (!RegExp("^\\d{9}$").test(DLNumber)) {
            sErrorMsg = "9Numeric (for example - 123456789).";
          }
          break;
        case "DE":
          if (!RegExp("^\\d{1,7}$").test(DLNumber)) {
            sErrorMsg = "1 to 7 Numeric. (for example - 1234567)";
          }
          break;
        case "DC":
          if (!RegExp("^\\d{7}$|^\\d{9}$|^[A-z]{2}\\d{8}$").test(DLNumber)) {
            sErrorMsg = "7 Numeric  (for example - 1234567) \nor \n9 Numeric. (for example - 123456789) \nor\n 2 Alpha + 8 Numeric (for example AA12345678)";
          }
          break;
        case "FL":
          if (!RegExp("^[A-Za-z]{1}\\d{12}$").test(DLNumber)) {
            sErrorMsg = "1 Alpha + 12 Numeric. (for example - A123456789012)";
          }
          break;
        case "GA":
          if (!RegExp("^\\d{9}$").test(DLNumber)) {
            sErrorMsg = "9 Numeric. (for example - 123456789)";
          }
          break;
        case "HI":
          if (!RegExp("^[A-Za-z]{1}\\d{8}$|^\\d{9}$").test(DLNumber)) {
            sErrorMsg = "1Alpha+8Numeric (for example - A12345678) \nor \n9Numeric. (for example - 123456789)";
          }
          break;
        case "ID":
          if (!RegExp("^[A-Za-z]{2}[0-9]{6}[A-Za-z]{1}$|^[0-9]{9}$", "gm").test(DLNumber)) {
            sErrorMsg = "2 Alpha + 6 Numeric + 1 Alpha (for example - AA123456A), \nor 9 Numeric (for example - 123456789).";
          }
          break;
        case "IL":
          if (!RegExp("^[A-Za-z]{1}\\d{11}$").test(DLNumber)) {
            sErrorMsg = "1Alpha+11Numeric (for example - A12345678901) \nor \n1Alpha+12Numeric (for example - A123456789012).";
          }
          break;
        case "IN":
          if (!RegExp("^\\d{10}$|^[A-Za-z]{1}\\d{9}$").test(DLNumber)) {
            sErrorMsg = "1Alpha+9Numeric (for example - A123456789) \nor \n10Numeric (for example - 1234567890).";
          }
          break;
        case "IA":
          if (!RegExp("^\\d{9}$|^[A-Za-z0-9]{9}$").test(DLNumber)) {
            sErrorMsg = "9 Numeric (for example - 123456789), \nor \n9 Alpha or 9 Numeric (for example - 123AA1234).";
          }
          break;
        case "KS":
          if (!RegExp("^[A-Za-z0-9]{6}$|^[A-Za-z]{1}\\d{8}$|^\\d{9}$").test(DLNumber)) {
            sErrorMsg = "6 alternating alpha and numeric characters (for example - A1A1A) \nor \n1Alpha+8Numeric (for example - A12345678) \nor \n9Numeric (for example - 123456789).";
          }
          break;
        case "KY":
          if (!RegExp("^[A-Za-z]{1}\\d{8}$|^\\d{9}$").test(DLNumber)) {
            sErrorMsg = "1Alpha+8Numeric (for example - A12345678) \nor \n9Numeric (for example - 123456789).";
          }
          break;
        case "LA":
          if (!RegExp("^\\d{1,9}$").test(DLNumber)) {
            sErrorMsg = "1-9 Numeric (for example - 123456789).";
          }
          break;
        case "ME":
          if (!RegExp("^\\d{7}$").test(DLNumber)) {
            sErrorMsg = "7Numeric (for example - 1234567)";
          }
          break;
        case "MD":
          if (!RegExp("^[A-Za-z]{1}\\d{12}$").test(DLNumber)) {
            sErrorMsg = "1 Alpha + 12 Numeric (for example - A123456789012).";
          }
          break;
        case "MA":
          if (!RegExp("^\\d{9}$|^[A-Za-z]{1}\\d{8}$").test(DLNumber)) {
            sErrorMsg = "1Alpha+8Numeric (for example - A12345678) \nor \n9Numeric (for example - 123456789).";
          }
          break;
        case "MI":
          if (!RegExp("^[A-Za-z]{1}\\d{12}$").test(DLNumber)) {
            sErrorMsg = "1Alpha+12Numeric (for example - A123456789012).";
          }
          break;
        case "MN":
          if (!RegExp("^[A-Za-z]{1}\\d{12}$").test(DLNumber)) {
            sErrorMsg = "1 Alpha + 12 Numeric (for example - A1234567890).";
          }
          break;
        case "MS":
          if (!RegExp("^\\d{9}$").test(DLNumber)) {
            sErrorMsg = "9 Numeric (for example - 123456789).";
          }
          break;
        case "MO":
          if (!RegExp("^[A-Z]{1}[0-9]{5,9}$|(^[A-Z]{1}[0-9]{6}[R]{1}$)|(^[0-9]{8}[A-Z]{2}$)|(^[0-9]{9}[A-Z]{1}$)|^[0-9]{9}$").test(DLNumber)) {
            sErrorMsg = "1 Alpha + 5-9 Numeric \nor\n1 Alpha + 6 Numeric + 'R'\nor\n8 Numeric + 2 Alpha\nor\n9 Numeric + 1 Alpha\nor\n9 Numeric";
          }
          break;
        case "MT"://!!!
          if (!RegExp("^\\d{13}$|^[A-Za-z]{1}\\d{8}$|^\\d{9}$|^\\d{14}$").test(DLNumber)) {
            sErrorMsg = "1Alpha+8Numeric (for example - A12345678) \nor \n13Numeric (for example - 1234567890123) \nor \n9Numeric (for example - 123456789) \nor \n14Numeric (for example - 12345678901234).";
          }
          break;
        case "NE":
          if (!RegExp("^[A-Za-z]{1}\\d{3,8}$").test(DLNumber)) {
            sErrorMsg = "1 Alpha + 3-8Numeric (for example - A123).";
          }
          break;
        case "NV":
          if (!RegExp("^\\d{10}$|^\\d{12}$").test(DLNumber)) {
            sErrorMsg = "10 Numeric (for example - 1234567890) \nor \n12 Numeric (for example - 123456789012).";
          }
          break;
        case "NH":
          if (!RegExp("^\\d{2}[A-Za-z]{3}\\d{5}$").test(DLNumber)) {
            sErrorMsg = "2Numeric+3Alpha+5Numeric (for example - 12AAA12345).";
          }
          break;
        case "NJ":
          if (!RegExp("^[A-Za-z]{1}\\d{14}$").test(DLNumber)) {
            sErrorMsg = "1 Alpha + 14 Numeric (for example - A12345678901234).";
          }
          break;
        case "NM":
          if (!RegExp("^\\d{8,9}$").test(DLNumber)) {
            sErrorMsg = "8Numeric (for example - 12345678) \nor \n9Numeric (for example - 123456789).";
          }
          break;
        case "NY"://!!!
          if (!RegExp("^\\d{9}$|^[A-Za-z]{1}\\d{7}$|^[A-Za-z]{1}\\d{18}$|^\\d{8,9}$|^\\d{16}$|^[A-Za-z]{8}$").test(DLNumber)) {
            sErrorMsg = "9Numeric (for example 123456789) \n or \n1Alpha+7Numeric (for example - A1234567) \nor \n1Alpha+18Numeric (for example - A123456789012345678) \nor \n8Numeric (for example - 12345678) \nor \n9Numeric (for example - 123456789) \nor \n16 Numeric (for example - 1234567890123456) \nor \n8Alpha (for example - AAAAAAAA).";
          }
          break;
        case "NC":
          if (!RegExp("^\\d{1,12}$").test(DLNumber)) {
            sErrorMsg = "1-12Numeric (for example - 123456789012).";
          }
          break;
        case "ND":
          if (!RegExp("^\\d{9}$|^[A-Za-z]{3}\\d{6}$|^[A-z]{1}\\d{8}$").test(DLNumber)) {
            sErrorMsg = "9 Numeric (for example - 123456789), \nor \n3 Alpha + 6 Numeric (for example - AAA123456) \nor\n 1 Alpha + 8 Numeric (for example A12345678).";
          }
          break;
        case "OH":
          if (!RegExp("^[A-z]{2}\\d{6}$").test(DLNumber)) {
            sErrorMsg = "2 Alpha + 6 Numeric (for example - AA123456).";
          }
          break;
        case "OK":
          if (!RegExp("^[A-Za-z]{1}\\d{9}$|^\\d{9}$").test(DLNumber)) {
            sErrorMsg = "1Alpha+9Numeric (for example - A123456789) \nor \n9Numeric (for example - 123456789).";
          }
          break;
        case "OR":
          if (!RegExp("^\\d{6,9}$").test(DLNumber) && !RegExp("^[A-Za-z]{1}\\d{6,9}$").test(DLNumber)) {
            sErrorMsg = "6-9 Numeric OR 1Alpha+6-9Numeric (for example - 123456789 or A123456789).";
          }
          break;
        case "PA":
          if (!RegExp("^\\d{8}$").test(DLNumber)) {
            sErrorMsg = "8 Numeric (for example - 12345678).";
          }
          break;
        case "RI":
          if (!RegExp("^\\d{7}$|^[Vv]{1}\\d{6}$").test(DLNumber)) {
            sErrorMsg = "7Numeric (for example - 1234567) \nor \nV + 6 Numeric (for example - V123456).";
          }
          break;
        case "SC":
          if (!RegExp("^\\d{1,10}$").test(DLNumber)) {
            sErrorMsg = "1-10 Numeric (for example - 1234567890).";
          }
          break;
        case "SD":
          if (!RegExp("^\\d{6}$|^\\d{8,9}$").test(DLNumber)) {
            sErrorMsg = "6 Numeric (for example - 123456) \nor \n8-9 Numeric (for example - 12345678).";
          }
          break;
        case "TN":
          if (!RegExp("^\\d{8,9}$").test(DLNumber)) {
            sErrorMsg = "8-9Numeric (for example - 123456789).";
          }
          break;
        case "TX":
          if (!RegExp("^\\d{8}$").test(DLNumber)) {
            sErrorMsg = "8 Numeric (for example - 12345678).";
          }
          break;
        case "UT":
          if (!RegExp("^\\d{4,9}$").test(DLNumber)) {
            sErrorMsg = "4 to 9 Numeric (for example - 123456789).";
          }
          break;
        case "VT":
          if (!RegExp("^\\d{8}$|^\\d{7}[Aa]{1}$").test(DLNumber)) {
            sErrorMsg = "8Numeric (for example - 12345678) \nor \n7Numeric+A (for example - 1234567A).";
          }
          break;
        case "VA":
          if (!RegExp("^[A-z]{1}\\d{8}$|^\\d{9}$|^\\d{12}$").test(DLNumber)) {
            sErrorMsg = "1 Alpha + 8 Numeric (for example - A12345678) \nor \n9 Numeric (for example - 123456789) \nor \n12 Numeric (for example - 123456789012).";
          }
          break;
        case "WA":
          if (!RegExp("^[A-z0-9]{12}$").test(DLNumber)) {
            sErrorMsg = "12 Alpha or Numeric (for example 123456789012).";
          }
          break;
        case "WV":
          if (!RegExp("^[AaBbCcDdEeFfIiSsXx01]{1,2}\\d{5,6}$").test(DLNumber)) {
            sErrorMsg = "A, B, C, D, E, F, I, S, 0, 1, XX, or 1X + 5-6 Numeric (for example A12345)";
          }
          break;
        case "WI":
          if (!RegExp("^[A-Za-z]{1}\\d{13}$").test(DLNumber)) {
            sErrorMsg = "1 Alpha + 13 Numeric (for example - A1234567890123).";
          }
          break;
        case "WY":
          if (!RegExp("^[0-9\-]{10}$").test(DLNumber)) {
            sErrorMsg = "10Numeric `Hyphens are allowed in the seventh position` (for example - 1234567890).";
          }
          break;
        case "0":
          sErrorMsg = "Please Select a DL State to Validate the DL Number"; //"State not found for DL validation!";
          break;
        default:
          //sErrorMsg = "State not found for DL validation:";
          sErrorMsg = 'valid';
          break;
      }
      return sErrorMsg;
    }

})
