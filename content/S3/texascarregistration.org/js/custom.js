$(document).ready(function() {

	$(function() {
		$('[data-toggle="tooltip"]').tooltip()
	})

	$('#enforcement').hide();

	$('input[type=radio][name=vehicle_out_state]').change(function() {
		if (this.value == 'N') {
			$('#enforcement').hide();
		}
		if (this.value == 'Y') {
			$('#enforcement').show();
		}
	});

	$('input[type=checkbox][name=same_address]').change(function() {
		$('.billing_address_wrap').toggle();
	});

	if ($('input[type=checkbox][name=same_address]').is(':checked')) {
		$('.billing_address_wrap').hide();
	}

	$('#start_datePicker').datepicker({
		format : 'mm/dd/yyyy'
	}).on('changeDate', function(e) {
		// Revalidate the start date field
		$('#orderForm').formValidation('revalidateField', 'start_date');
	});

	$('#end_datePicker').datepicker({
		format : 'mm/dd/yyyy'
	}).on('changeDate', function(e) {
		$('#orderForm').formValidation('revalidateField', 'end_date');
	});

	$('#orderForm').formValidation({
		framework : 'bootstrap',
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		live : 'enabled',
		fields : {
			first_name : {
				validators : {
					notEmpty : {
						message : 'The first name is required'
					},
					regexp : {
						message : 'The first name can only contain the letters, digits and spaces',
						regexp : /^[A-Za-z0-9\s\-+\.]+$/
					}
				}
			},
			middle_name : {
				validators : {
					notEmpty : {
						message : 'The middle name is required'
					},
					regexp : {
						message : 'The middle name can only contain the letters, digits and spaces',
						regexp : /^[A-Za-z0-9\s\-+\.]+$/
					}
				}
			},
			last_name : {
				validators : {
					notEmpty : {
						message : 'The last name is required'
					},
					regexp : {
						message : 'The last name can only contain the letters, digits and spaces',
						regexp : /^[A-Za-z0-9\s\-+\.]+$/
					}
				}
			},
			email : {
				verbose : false,
				validators : {
					notEmpty : {
						message : 'The email address is required'
					},
					emailAddress : {
						message : 'The input is not a valid email address'
					},
					identical : {
						field : 'verifyemail',
						message : 'The email and its confirm are not the same'
					}
				}
			},
			verifyemail : {
				verbose : false,
				validators : {
					notEmpty : {
						message : 'The email address is required'
					},
					emailAddress : {
						message : 'The input is not a valid email address'
					},
					identical : {
						field : 'email',
						message : 'The email and its confirm are not the same'
					}
				}
			},
			phone : {
				verbose : false,
				validators : {
					notEmpty : {
						message : 'The phone is required'
					},
					regexp : {
						message : 'The phone number can only have 10 digits',
						regexp : /^\d{10}$/
					}
				}
			},
			address : {
				validators : {
					notEmpty : {
						message : 'The address is required'
					}
				}
			},
			city : {
				validators : {
					notEmpty : {
						message : 'The city is required'
					},
					regexp : {
						message : 'The city seem to have invalid characters',
						regexp : /^[A-Za-z0-9\s\-+\.]+$/
					}
				}
			},
			state : {
				validators : {
					notEmpty : {
						message : 'The state is required'
					}
				}
			},
			county : {
				validators : {
					notEmpty : {
						message : 'The county is required'
					}
				}
			},
			zipcode : {
				validators : {
					notEmpty : {
						message : 'The zipcode is required'
					},
					regexp : {
						message : 'The zip code can only have 5 digits',
						regexp : /^\d{5}$/
					}
				}
			},
			insurance_name : {
				validators : {
					notEmpty : {
						message : 'The Insurance name is required'
					}
				}
			},
			policy_number : {
				validators : {
					notEmpty : {
						message : 'The Policy number is required'
					}
				}
			},
			insurance_phone : {
				validators : {
					notEmpty : {
						message : 'The phone is required'
					},
					regexp : {
						message : 'The phone number can only have 10 digits',
						regexp : /^\d{10}$/
					}
				}
			},
			vin : {
				validators : {
					notEmpty : {
						message : 'The vin is required'
					},
					regexp : {
						message : 'Enter last 4 characters of the VIN',
						regexp : /^\w{4}$/
					}
				}
			},
			license_plate : {
				validators : {
					notEmpty : {
						message : 'The license plate is required'
					},
					regexp : {
						message : 'The license plate is 7 characters long',
						regexp : /^\w{6,7}$/
					}
				}
			},
			vehicle : {
				validators : {
					notEmpty : {
						message : 'The type of vehicle is required'
					}
				}
			},
			ticket : {
				validators : {
					notEmpty : {
						message : 'This field is required.'
					}
				}
			},
			declaration : {
				validators : {
					notEmpty : {
						message : 'This field is required.'
					}
				}
			},
			vehicle_out_state : {
				validators : {
					notEmpty : {
						message : 'This field is required.'
					}
				}
			},
			enforcement : {
				validators : {
					notEmpty : {
						message : 'This field is required.'
					}
				}
			},
			inspection : {
				validators : {
					notEmpty : {
						message : 'This field is required.'
					}
				}
			},
			confirm : {
				validators : {
					notEmpty : {
						message : 'Confirm the information you entered.'
					}
				}
			},
			agreement : {
				validators : {
					notEmpty : {
						message : 'You need to agree to our terms.'
					}
				}
			},
			start_date : {
				validators : {
					notEmpty : {
						message : 'The start date is required'
					},
					date : {
						format : 'MM/DD/YYYY',
						max : 'end_date',
						message : 'The start date is not a valid'
					}
				}
			},
			end_date : {
				validators : {
					notEmpty : {
						message : 'The end date is required'
					},
					date : {
						format : 'MM/DD/YYYY',
						min : 'start_date',
						message : 'The end date is not a valid'
					}
				}
			},
			signature : {
				verbose : false,
				validators : {
					notEmpty : {
						message : ''
					},
					callback : {
						message : 'Signature is not valid',
						callback : function(value, validator, $field) {
							var full_name = $('input[name=first_name]').val() + ' ' + $('input[name=last_name]').val();
							if (value != full_name) {
								return {
									valid : false,
									message : 'Your electronic signature must match your  first and last name.'
								};
							}
							return true;
						}
					}
				}
			}
		}
	}).on('success.field.fv', function(e, data) {
		if (data.field === 'start_date' && !data.fv.isValidField('end_date')) {
			// We need to revalidate the end date
			data.fv.revalidateField('end_date');
		}

		if (data.field === 'end_date' && !data.fv.isValidField('start_date')) {
			// We need to revalidate the start date
			data.fv.revalidateField('start_date');
		}
	});

	$('#payForm').formValidation({
		framework : 'bootstrap',
		icon : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		live : 'enabled',
		fields : {
			credit_card : {
				validators : {
					notEmpty : {
						message : 'The credit card is required'
					},
					creditCard : {
						message : 'The credit card number is not valid'
					}
				}
			},
			month : {
				validators : {
					notEmpty : {
						message : 'The month is required'
					}
				}
			},
			year : {
				validators : {
					notEmpty : {
						message : 'The year is required'
					}
				}
			},
			cvv : {
				validators : {
					notEmpty : {
						message : 'The CVV is required'
					},
					cvv : {
						creditCardField : 'credit_card',
						message : 'The CVV number is not valid'
					}
				}
			},
			billing_address : {
				validators : {
					notEmpty : {
						message : 'The address is required'
					}
				}
			},
			billing_city : {
				validators : {
					notEmpty : {
						message : 'The city is required'
					},
					regexp : {
						message : 'The city seem to have invalid characters',
						regexp : /^[A-Za-z0-9\s\-+\.]+$/
					}
				}
			},
			billing_state : {
				validators : {
					notEmpty : {
						message : 'The state is required'
					}
				}
			},
			billing_zipcode : {
				validators : {
					notEmpty : {
						message : 'The zipcode is required'
					},
					regexp : {
						message : 'The zip code can only have 5 digits',
						regexp : /^\d{5}$/
					}
				}
			}
		}
	}).on('success.field.fv', function(e, data) {

	});

})