
$(document).ready(function(){
   /*----------------------*/
   /*--- fixAddress ... ---*/
   /*----------------------*/
   $("#address_1").change(function(){ fixAddress( $(this) ); });
   $("#address_2").change(function(){ fixAddress( $(this) ); });
   
   /*----------------------------------------*/
   /*--- city, state, zip from form ...   ---*/
   /*----------------------------------------*/
   
   /*----------------------------------------*/
   /*--- form type ...                    ---*/
   /*----------------------------------------*/
   uSplit = document.location.href.split("/").reverse();
   try { $("#_FORM_").val( "/" + uSplit[1] + "/" + uSplit[0].split("?")[0] ); } catch(e) {}
   
   /*----------------------------------------*/
   /*--- debug ...                        ---*/
   /*----------------------------------------*/
   if( document.location.href.indexOf("DEBUG=") != -1 )
   {
      $("#buffer").show();
   }
   
   /*----------------------------------------*/
   /*--- Car Stuff ...                    ---*/
   /*----------------------------------------*/
   var CAR_YEAR_START  = 1980;
   var CAR_URL         = "/ajax/cars.jsp";
   
   function loadCarData( element, url, data, type )
   {
      if(element.length == 0)
         return;
      
      $.ajax({
         "url": url,
         "type": type || "POST",
         "format": "json",
         "data": data,
         "success": function(response, message){
            $.each(response, function(value, display)
            {
               element.append( $("<option>").attr("value", value).html(display) );
            });
            element.css( "width","" );
         },
         "complete": function(){
            element.removeAttr("disabled");
         }
      });
   }
   
   //--------- buildSelector ---------
   function buildSelector(element, key, attribute)
   {
      var selector = String(element.attr(key)).replace(key, "");
      return "select" + ((selector != "")? "#" + selector: "") + "[" + attribute +"]";
   }
   
   //--------- pre-set ---------
   (function(){
      //--------- populate years ---------
      var select        =  $( "select[car_year]" );
      var currentYear   =  (new Date()).getFullYear();
      //currentYear++;
      select.empty().append(
         $("<option>").attr("value", "").html("--Select--")
      );
      
      $("select[car_make], select[car_model], select[car_trim]").attr("disabled", "disabled");
      
      for( var year = currentYear; year >= CAR_YEAR_START; year-- )
         select.append(
         $("<option>").attr("value", year).html(year)
      );
   })();
   
   //--------- on Change car year ---------
   $("#state_1,#state_2").change(function(){
      var _o = $(this).attr("id");
      var _v = $(this).val();
      $.each($(".county"), function( index, value ) {
        if( $(this).attr("state") == _o )
        {
            var _cnt = $(this);
            _cnt.empty();
            _cnt.append( $("<option>").attr("value", "").html("--- select ---") );
            $.ajax({
               "url": "/ajax/county.jsp?state="+_v,
               "format": "json",
               "success": function(response, message){
                  $.each(response, function(value, display)
                  {
                     _cnt.append( $("<option>").attr("value", display.county).html(initCap(display.county)) );
                  });
               }
            });
        }
      });
   });
   
   
   $("select[car_year]").change(function(){
      var year  = $(this);
      var make  = $(buildSelector(year,  "car_year",  "car_make")  );
      var model = $(buildSelector(make,  "car_make",  "car_model") );
      var trim  = $(buildSelector(model, "car_model", "car_trim")  );
      make.removeAttr("disabled" ).empty().append($("<option>").attr("value", "").html("--Select--")).attr("disabled", "disabled");
      model.removeAttr("disabled").empty().append($("<option>").attr("value", "").html("--Select--")).attr("disabled", "disabled");
      trim.removeAttr("disabled" ).empty().append($("<option>").attr("value", "").html("--Select--")).attr("disabled", "disabled");
      loadCarData(make, CAR_URL, { "year": year.val() });
   });
   
   //--------- on Change car make ---------
   $("select[car_make]").change(function(){
      var make  = $(this);
      var year  = $("select[car_year=" + make.attr("id") + "]");
      var model = $(buildSelector(make,  "car_make",  "car_model") );
      var trim  = $(buildSelector(model, "car_model", "car_trim")  );
      model.removeAttr("disabled").empty().append($("<option>").attr("value", "").html("--Select--")).attr("disabled", "disabled");
      trim.removeAttr("disabled" ).empty().append($("<option>").attr("value", "").html("--Select--")).attr("disabled", "disabled");
      loadCarData(model, CAR_URL, { "year": year.val(), "make": make.val() });
      $("#"+$(this).attr("id")+"_lbl_").val( $(this).find("option:selected").text() );
   });
   
   //--------- on Change car model ---------
   $("select[car_model]").change(function(){
      var model = $(this);
      var make  = $("select[car_make=" + model.attr("id") + "]");
      var year  = $("select[car_year=" + make.attr("id") + "]");
      var trim  = $(buildSelector(model, "car_model", "car_trim"));
      trim.removeAttr("disabled").empty().append($("<option>").attr("value", "").html("--Select--")).attr("disabled", "disabled");
      loadCarData(trim, CAR_URL, { "year": year.val(), "make": make.val(), "model": model.val() });
      $("#"+$(this).attr("id")+"_lbl_").val( $(this).find("option:selected").text() );
   });
   
   //--------- on Change car trim ---------
   $("select[car_trim]").change(function(){
      $("#"+$(this).attr("id")+"_lbl_").val( $(this).find("option:selected").text() );
   });
   
   /*----------------------------------------*/
   /*--- state ...                        ---*/
   /*----------------------------------------*/
   u = document.location.href.split( "/" );
   u = stateLookup( u[u.length-1].split(".")[0] );
   if( typeof u === "undefined" )
      u = "";
   try { if(u!="") $("#state").val(u);                            } catch(e) {}
   try { if(u!="") $("#state_1").val(u); $("#state_1").change();  } catch(e) {}
   try { if(u!="") $("#state_2").val(u);                          } catch(e) {}

   try { if(""!="") $("#city").val("");   } catch(e) {}
   try { if(""!="") $("#city_1").val(""); } catch(e) {}
   try { if(""!="") $("#city_2").val(""); } catch(e) {}


   try { if(""!="") $("#zip").val("");   } catch(e) {}
   try { if(""!="") $("#zip_1").val(""); } catch(e) {}
   try { if(""!="") $("#zip_2").val(""); } catch(e) {}

   
   /*----------------------------------------*/
   /*--- trim on change ...               ---*/
   /*----------------------------------------*/
   $(":input").change(function(){
      $(this).val( $.trim( $(this).val() ) );
   });
   
   /*----------------------------------------*/
   /*--- Phone field ...                  ---*/
   /*----------------------------------------*/
// $("[field=PHONE],[field=ZIP]").blur(function(){
   $("[field=PHONE]").blur(function(){
      /*
      if( $(this).val()==$(this).attr("placeholder") )
         return;
      */
      var el = $(this);
      var cleanNumber = el.val().replace(/[^\d]+/g, "");
      
      if( (cleanNumber.length!=10 && "US"=="US") ||
          (cleanNumber.length!=10 && "US"=="BR")
      )
      {
         el.val(cleanNumber);
         return;
      }
      if( "US"=="US" )
         el.val(cleanNumber.replace(/(\d{3})(\d{3})(\d{4,})/g, "$1-$2-$3"));
      else
      if( "US"=="BR" )
         el.val(cleanNumber.replace(/(\d{2})(\d{4})(\d{4,})/g, "($1) $2-$3"));
   })
   
   /*----------------------------------------*/
   /*--- Number field ...                 ---*/
   /*----------------------------------------*/
   $("[field=NUMBER]").keypress(function(event){
      return true;
   }).blur(function(){
      if( $(this).val()==$(this).attr("placeholder") )
         return;
      var el = $(this);
      el.val(el.val().replace(/[^\d]/g, ""));
   });
   popForm();
});

/*----------------------------------------------------------------------------*/
/*--- setHeight ...                                                        ---*/
/*----------------------------------------------------------------------------*/
function setHeight( o )
{
   $("#"+o).val( $("#_feet_"+o).val() + "'" + $("#_inch_"+o).val() );
}

/*----------------------------------------------------------------------------*/
/*--- btnSwap ...                                                          ---*/
/*----------------------------------------------------------------------------*/
function btnSwap()
{
   $("#submit").toggle();
   $("#spin").toggle();
}

/*----------------------------------------------------------------------------*/
/*--- setDate ...                                                          ---*/
/*----------------------------------------------------------------------------*/
function setDate(o)
{
   objId    = o.id.substr(4);
   mainObj  = $("#" + objId );
   mObj     = $("#_mm_" + objId );
   dObj     = $("#_dd_" + objId );
   yObj     = $("#_yy_" + objId );
   
   m        =  mObj.val();
   d        =  dObj.val();
   y        =  yObj.val();
   full     =  m + "/" + d + "/" + y;
   if( full=="//" )
      full = "";
   mainObj.val( full );
}

/*----------------------------------------------------------------------------*/
/*--- setMonthYear ...                                                     ---*/
/*----------------------------------------------------------------------------*/
function setMonthYear(o)
{
   objName = o.id.substr(4);
   mV      = $("#_mm_"+objName).val();
   yV      = $("#_yy_"+objName).val();
   v       = mV + "/" + yV;
   v       = v=="/"?"":v;
   $("#"+objName).val( v );
}

/*----------------------------------------------------------------------------*/
/*--- postForm ...                                                         ---*/
/*----------------------------------------------------------------------------*/
function postForm()
{
   leaveSite = false;
   
   $(".lbls").removeClass( "errorlabel" );
   if( document.location.href.indexOf("DEBUG=") != -1 )
   {
      document.frm.submit();
      return;
   }
   
   var errs       =   false;
   var els        =   document.frm.elements;
   /*
   if( $("#address_1").length == 1 )
   {
      try { validateAddress(1); } catch(e) {}
   }
   if( $("#address_2").length == 1 )
   {
      try { validateAddress(2); } catch(e) {}
   }
   */

   for( var i=0; i<els.length; i++ )
   {
      try
      {
         var elId = els[i].id;
         var o    = $("#"+elId);
         var t    = o.attr( "field" );
         var v    = $.trim( o.val() );
         var m    = $("#_msg_"+elId);
         
         var r    = o.attr( "req" );
         m.html( "&nbsp;" ).removeClass("err_on");
         
         if( t=="RADIO" )
         {
            fieldName = o.attr("name");
            v = $('input:radio[name="'+fieldName+'"]:checked').val();
            m = $('#_msg_'+fieldName);
            m.html( "" ).removeClass("err_on");
            if( typeof v === "undefined" )
               v = "";
         }
         else
         if( t=="MONTH_YEAR" )
         {
            if( $("#_mm_"+elId).is(":visible") && (r=='Y') && (els[i].value.length!=7) )
            {
               m.html( "Missing Data" );
               focusField( o, t, elId )
               fadeErr(m);
               errs = true;
            }
         }
         else
         if( t=="HEIGHT" )
         {
            _ff = $("#_feet_"+elId).val();
            _ii = $("#_inch_"+elId).val();
            if( $("#_feet_"+elId).is(":visible") && (r=='Y') && (_ff=="" || _ii=="") )
            {
               m.html( "Missing Data" );
               focusField( o, t, elId )
               fadeErr(m);
               errs = true;
            }
         }
         
         if(   o.is(":visible") || ((t=="DATE" || t=="DOB") && $('#_mm_'+elId).is(":visible")) )
         {
            if( r=="Y" && v=="" )
            {
               if( !errs )
                  focusField( o, t, elId )
               m.html( "Missing Data" );
               fadeErr(m);
               errs = true;
            }
            else
            {
               if( !validateField( o, v, t, r, m ) && !errs )
               {
                  focusField( o, t, elId );
                  errs = true;
               }
            }
         }
      }
      catch(e)
      {
      }
   }


   if( !customValidate() || errs )
      return;
  
   try
   {
      if( !extraValidate() )
         return;
   }
   catch(e)
   {
   }
 
   btnSwap();
   document.frm.submit();
}

/*----------------------------------------------------------------------------*/
/*--- fixAddress ...                                                       ---*/
/*----------------------------------------------------------------------------*/
function fixAddress( o )
{
   val      = o.val().toUpperCase();
   msg_div  = $("#_msg_"+o.attr("id"));
   msg_div.html( "" );
   if( val.indexOf("#")!=-1 || val.indexOf(" SUITE ")!=-1 || val.indexOf(" APT ")!=-1 )
   {
      msg_div.html( "Invalid Address" );
      fadeErr(msg_div);
      return false;
   }
   else
   {
      tmp = val.replace(/[^a-zA-Z]+/g,'').replace( /\s/g, "" );
      if( tmp=="POBOX" )
      {
         msg_div.html( "Invalid Address" );
         fadeErr(msg_div);
         return false;
      }
   }
   return true;
}

/*----------------------------------------------------------------------------*/
/*--- validateField ...                                                    ---*/
/*----------------------------------------------------------------------------*/
function validateField( obj, val, typ, req, msg_div )
{
   oId = obj.attr("id");
   if( oId=="address_1" || oId=="address_2" )
   {
      if( !fixAddress(obj) )
         return false;
   }
   else
   if( typ == "EMAIL" )
   {
      if( !emailCheck(val) )
      {
         msg_div.html( "Invalid e-Mail" );
         fadeErr(msg_div);
         return false;
      }
   }
   else
   if( typ == "PHONE" )
   {
      msisdn = extractNumberJunk(val);
      if( msisdn.length!=0 )
      {
         if( (msisdn.length!=10 && "US"=="US") ||
             (msisdn.length!=10 && "US"=="BR")
         )
         {
            msg_div.html( "Invalid Phone #" );
            fadeErr(msg_div);
            return false;
         }
      }
   }
   else
   if( typ=="DOB" || typ=="DATE" )
   {
      if( val!="" && !dateCheck(val) )
      {
         msg_div.html( "Invalid Date" );
         fadeErr(msg_div);
         return false;
      }
   }
   return true;
   
}

/*----------------------------------------------------------------------------*/
/*--- focusField ...                                                       ---*/
/*----------------------------------------------------------------------------*/
function focusField( o, t, elId )
{
   var _o = o;
   try
   {
      if( t=='DOB' || t=='DATE' )
         _o = $("#_mm_" + elId);
      else
      if( t=='HEIGHT' )
         _o = $( "#_feet_" + elId );
      _o.focus();
      _o.select();
   }
   catch(e)
   {
   }
}


/*----------------------------------------------------------------------------*/
/*--- setErrMsg ...                                                        ---*/
/*----------------------------------------------------------------------------*/
function setErrMsg( objName, errMsg )
{
   var m = $("#_msg_"+objName);
   m.html( errMsg );
   fadeErr(m);

   o = $( "#"+objName );

   focusField( o, o.attr("field"), objName );
}



/*----------------------------------------------------------------------------*/
/*--- validateAddress ...                                                  ---*/
/*----------------------------------------------------------------------------*/
function validateAddress( typ )
{

   var HTTPS_KEY = "ABQIAAAAM2NwXjUwIF0_whEjmxC0gxSyWfVkRxUL0C1eztfnALDs_jWcsRSPvZbobvKnJ4ul8R_LwGkpUpzRdQ";
   var HTTP_KEY  = "ABQIAAAAM2NwXjUwIF0_whEjmxC0gxS-aTHDcIL1RztHO-uynIJmCVzAuhRgBXeVrLFmUDnr9E21q-77V8aPIw";
   var protocol  = document.location.protocol;
   var dbg = ( document.location.href.indexOf("DEBUG=") != -1 );
   try
   {
      address_ =  $("#address_"+typ).val();
      city_    =  $("#city_"+typ).val();
      state_   =  $("#state_"+typ).val();
      zip_     =  $("#zip_"+typ).val();

      u  = protocol + '//maps.google.com/maps/geo?output=json&v=2&sensor=false';
      u += '&q=';

      uri      = address_ + " " + city_ + " " + state_ + " " + zip_;
      u += escape(uri);
      if( protocol=="https:" )
         u += "&key=" + HTTPS_KEY;
      else
         u += "&key=" + HTTP_KEY;

      if( dbg )
         alert( u );

      $.ajax({
         "url": u,
         dataType: 'jsonp',
         cache: false,
         "async":false,
         "success": function(response, message){
            try
            {
               $("#_msg_address_"+typ).html( "&nbsp;" ).removeClass("err_on");
               $("#_msg_city_"+typ).html( "&nbsp;" ).removeClass("err_on");
               $("#_msg_state_"+typ).html( "&nbsp;" ).removeClass("err_on");
               $("#_msg_zip_"+typ).html( "&nbsp;" ).removeClass("err_on");
            }
            catch(e)
            {
            }
           
            if( dbg )
            {
               alert( response.Placemark );
               alert( response.Placemark[0] );
               alert( response.Placemark[0].address );
            }
            if( !response.Placemark )
               return;
            fullAddr   = response.Placemark[0].address.split( "," );
            if( dbg )
               alert( fullAddr );
            __country  = $.trim( fullAddr[fullAddr.length - 1] );
            if( __country == "USA" )
            {
               fullAddr.pop();
               __statezip = $.trim( fullAddr[fullAddr.length - 1] ).split(" ");
               __state    = __statezip[0];
               __zip      = __statezip[1];


               fullAddr.pop();
               __city    = $.trim( fullAddr[fullAddr.length - 1] );

               fullAddr.pop();
               __address = $.trim( fullAddr[fullAddr.length - 1] );


               
               $("#address_" + typ).val( __address );
               $("#city_"    + typ).val( __city    );
               $("#state_"   + typ).val( __state   );
               $("#zip_"     + typ).val( __zip     );
            }
         },
         "error": function(){
            if( dbg )
            {
               alert( "ERROR" );
            }
         }
      });
   }
   catch(e)
   {
   }
}




/*----------------------------------------------------------------------------*/
/*--- fadeErr ...                                                          ---*/
/*----------------------------------------------------------------------------*/
function fadeErr(m)
{
   $("#"+m.attr("id").replace("_msg_", "label_")).addClass( "errorlabel" );

   m.addClass( "err_on" );
   m.fadeIn(1000, function() {
      m.fadeOut(500, function() {
         m.fadeIn(1000);
      } );
   });
}



/*###############################*/
/*###############################*/
/*###### U T I L I T I E S ######
/*###############################*/
/*###############################*/


/*----------------------------------------------------------------------------*/
/*--- emailCheck ...                                                       ---*/
/*----------------------------------------------------------------------------*/
function emailCheck( email )
{
   var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
   return pattern.test(email);
}

/*----------------------------------------------------------------------------*/
/*--- dateCheck ...                                                        ---*/
/*----------------------------------------------------------------------------*/
function dateCheck( dt )
{
   var _dt = new Date(dt);
   var spl = dt.split( "/" );
   return ( parseInt(spl[0])===(_dt.getMonth()+1) && parseInt(spl[2])===_dt.getFullYear() );
}

/*----------------------------------------------------------------------------*/
/*--- isValidText ...                                                      ---*/
/*----------------------------------------------------------------------------*/
function isValidText(str)
{
   if( str=="" ) return false;
   var re = /[^a-zA-Z\.\- ]/g
   return re.test(str);
}

/*----------------------------------------------------------------------------*/
/*--- containsVowels ...                                                   ---*/
/*----------------------------------------------------------------------------*/
function containsVowels(str)
{
   var vwls = "euioay";
   for( i=0; i<vwls.length; i++ )
   {
      if( str.indexOf(""+vwls.charAt(i)) != -1 )
         return true;
   }
   return false;
}

/*----------------------------------------------------------------------------*/
/*--- isNumeric ...                                                        ---*/
/*----------------------------------------------------------------------------*/
function isNumeric(str){
   if( str=="" )
      return true;
  var re = /[\D]/g
  if (re.test(str)) return false;
  return true;
}

/*----------------------------------------------------------------------------*/
/*--- isAlphaNumeric ...                                                   ---*/
/*----------------------------------------------------------------------------*/
function isAlphaNumeric(str){
   if( str=="" )
      return true;
  var re = /[^a-zA-Z0-9]/g
  if (re.test(str)) return false;
  return true;
}

/*----------------------------------------------------------------------------*/
/*--- allSameChar ...                                                      ---*/
/*----------------------------------------------------------------------------*/
function allSameChar(str)
{
   if( str.length==0 )
      return false;
   zeroChar = str.charAt(0);
   for( i=1; i<str.length; i++ )
      if( str.charAt(i) != zeroChar )
         return false;
   return true;
}

/*----------------------------------------------------------------------------*/
/*--- hideField ...                                                        ---*/
/*----------------------------------------------------------------------------*/
function hideField(dId)
{
   $("#"+dId).hide();
}
/*----------------------------------------------------------------------------*/
/*--- showField ...                                                        ---*/
/*----------------------------------------------------------------------------*/
function showField(dId)
{
   $("#label_"+dId).show();
   $("#"+dId).show();
}


/*----------------------------------------------------------------------------*/
/*--- stateLookup ...                                                      ---*/
/*----------------------------------------------------------------------------*/
var sLookup = new Array();
sLookup["maranhao"] = "MA";sLookup["sao-paulo"] = "SP";sLookup["delaware"] = "DE";sLookup["rio-de-janeiro"] = "RJ";sLookup["nebraska"] = "NE";sLookup["virginia"] = "VA";sLookup["missouri"] = "MO";sLookup["south-dakota"] = "SD";sLookup["florida"] = "FL";sLookup["amapa"] = "AP";sLookup["california"] = "CA";sLookup["oregon"] = "OR";sLookup["oklahoma"] = "OK";sLookup["iowa"] = "IA";sLookup["indiana"] = "IN";sLookup["texas"] = "TX";sLookup["hawaii"] = "HI";sLookup["new-hampshire"] = "NH";sLookup["mississippi"] = "MS";sLookup["illinois"] = "IL";sLookup["mato-grosso"] = "MT";sLookup["maine"] = "ME";sLookup["newfoundland"] = "NL";sLookup["british-columbia"] = "BC";sLookup["idaho"] = "ID";sLookup["alagoas"] = "AL";sLookup["rondonia"] = "RO";sLookup["utah"] = "UT";sLookup["montana"] = "MT";sLookup["new-brunswick"] = "NB";sLookup["massachusetts"] = "MA";sLookup["manitoba"] = "MB";sLookup["piaui"] = "PI";sLookup["alaska"] = "AK";sLookup["ceara"] = "CE";sLookup["alberta"] = "AB";sLookup["kentucky"] = "KY";sLookup["saskatchewan"] = "SK";sLookup["district-of-columbia"] = "DC";sLookup["colorado"] = "CO";sLookup["parana"] = "PR";sLookup["georgia"] = "GA";sLookup["mato-grosso-do-sul"] = "MS";sLookup["nevada"] = "NV";sLookup["arizona"] = "AZ";sLookup["north-carolina"] = "NC";sLookup["para"] = "PA";sLookup["goias"] = "GO";sLookup["wyoming"] = "WY";sLookup["quebec"] = "QC";sLookup["roraima"] = "RR";sLookup["yukon"] = "YT";sLookup["minas-gerais"] = "MG";sLookup["arkansas"] = "AR";sLookup["northwest-territories"] = "NT";sLookup["north-dakota"] = "ND";sLookup["alabama"] = "AL";sLookup["sergipe"] = "SE";sLookup["new-mexico"] = "NM";sLookup["west-virginia"] = "WV";sLookup["prince-edward-island"] = "PE";sLookup["acre"] = "AC";sLookup["pennsylvania"] = "PA";sLookup["maryland"] = "MD";sLookup["washington"] = "WA";sLookup["louisiana"] = "LA";sLookup["new-york"] = "NY";sLookup["connecticut"] = "CT";sLookup["south-carolina"] = "SC";sLookup["espirito-santo"] = "ES";sLookup["michigan"] = "MI";sLookup["distrito-federal"] = "DF";sLookup["tocantins"] = "TO";sLookup["rio-grande-do-sul"] = "RS";sLookup["puerto-rico"] = "PR";sLookup["new-jersey"] = "NJ";sLookup["rio-grande-do-norte"] = "RN";sLookup["nova-scotia"] = "NS";sLookup["bahia"] = "BA";sLookup["amazonas"] = "AM";sLookup["wisconsin"] = "WI";sLookup["minnesota"] = "MN";sLookup["kansas"] = "KS";sLookup["paraiba"] = "PB";sLookup["nunavut"] = "NU";sLookup["tennessee"] = "TN";sLookup["pernambuco"] = "PE";sLookup["santa-catarina"] = "SC";sLookup["ontario"] = "ON";sLookup["rhode-island"] = "RI";sLookup["ohio"] = "OH";
function stateLookup(str)
{
   return sLookup[str];
}


/*----------------------------------------------------------------------------*/
/*--- titleField ...                                                       ---*/
/*----------------------------------------------------------------------------*/
function titleField()
{
   $(':input[title]').each(function() {
      var $this = $(this);
      if($this.val() === '') {
         $this.val($this.attr('title'));
         $this.css("color", "silver");
      }
      $this.focus(function() {
         if($this.val() === $this.attr('title')) {
           $this.val('');
           $this.css("color", "black");
         }
      });
      $this.blur(function() {
         if($this.val() === '') {
            $this.val($this.attr('title'));
            $this.css("color", "silver");
         }
      });
   });   
}

/*----------------------------------------------------------------------------*/
/*--- checkFixEmail ...                                                    ---*/
/*----------------------------------------------------------------------------*/
function checkFixEmail()
{
   if( !emailCheck(document.frm.fixEmail.value) )
   {
      alert( "Invalid Email" );
      document.frm.fixEmail.focus();
      document.frm.fixEmail.select();
      return false;
   }
   
   if( !emailCheck(document.frm.fixConfirmEmail.value) )
   {
      alert( "Invalid Email" );
      document.frm.fixConfirmEmail.focus();
      document.frm.fixConfirmEmail.select();
      return false;
   }
   
   if( document.frm.fixEmail.value != document.frm.fixConfirmEmail.value )
   {
      alert( "Email not matching confirmation" );
      document.frm.fixEmail.focus();
      document.frm.fixEmail.select();
      return false;
   }
   $("#fixemail").colorbox.close();
   try { $("#email").html(document.frm.fixConfirmEmail.value) } catch(e) {}
   return true;
}


function popForm()
{
   try
   {
      for( var v in _PROFILE )
      {
         if( v=="state_1" && _PROFILE[v]=="" )
            continue;
         if( v.charAt(0)!="_" )
            $("#"+v).val( _PROFILE[v] );
         if( v=="date_of_birth" )
         {
            _dob = _PROFILE[v].split("/");
            $("#_mm_date_of_birth").val( _dob[0] );
            $("#_dd_date_of_birth").val( _dob[1] );
            $("#_yy_date_of_birth").val( _dob[2] );
         }
      }
   }
   catch(e)
   {
   }
}


function placeholders()
{
   try
   {
      if( navigator.appName.indexOf("Explorer")>0  && parseInt($.browser.version, 10) <=9 )
      {
         try
         {
            $('input, textarea').placeholder();
         }
         catch(e)
         {
         }
      }
   }
   catch(e)
   {
   }
}
$(document).ready(function(){
   
});