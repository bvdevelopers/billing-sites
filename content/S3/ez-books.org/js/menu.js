function openNav() {
    document.getElementById("mySidenav").style.width = "100%";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

function openNavright() {
    document.getElementById("mySidenavright").style.width = "35%";
   	document.getElementById("wrapperMain").style.opacity=  "0.4";
}

function closeNavright() {
    document.getElementById("mySidenavright").style.width = "0";
    document.getElementById("wrapperMain").style.opacity=  "0.9";
}


var modal = document.getElementById('id01');

// When the user clicks anywhere outside of the modal, close it
// window.onclick = function(event) {
    // if (event.target == modal) {
        // modal.style.display = "none";
    // }
// }

$(document).ready(function () {

    // var tab = $('.tabs-section');
     // var origOffsetY = tab.offset().top;

    // function scroll() {
        // if ($(window).scrollTop() >= origOffsetY) {
            // $('.tabs-section').addClass('sticky');
            // $('.tab-content').addClass('menu-padding');
        // } else {
            // $('.tabs-section').removeClass('sticky');
            // $('.tab-content').removeClass('menu-padding');
        // }
    // }

    // document.onscroll = scroll;

    $("#state-slider-related-videos").mouseover(function() {
        $(this).animate({
            left: '-400'
        }, 400);
    });

    $("#state-slider-related-videos").mouseout(function(){
        $(this).stop(true,true).animate({ left: "0" }, 400);
    });

    $("#state-slider-image").mouseover(function() {
        $(this).animate({
            right: '0'
        }, 400);
    });

    $("#state-slider-image").mouseout(function(){
        $(this).stop(true,true).animate({ right: "-360" }, 400);
    });

});


var __slice = [].slice;

(function($, window) {
    var Starrr;

    Starrr = (function() {
        Starrr.prototype.defaults = {
            rating: void 0,
            numStars: 5,
            change: function(e, value) {}
        };

        function Starrr($el, options) {
            var i, _, _ref,
                _this = this;

            this.options = $.extend({}, this.defaults, options);
            this.$el = $el;
            _ref = this.defaults;
            for (i in _ref) {
                _ = _ref[i];
                if (this.$el.data(i) != null) {
                    this.options[i] = this.$el.data(i);
                }
            }
            this.createStars();
            this.syncRating();
            this.$el.on('mouseover.starrr', 'i', function(e) {
                return _this.syncRating(_this.$el.find('i').index(e.currentTarget) + 1);
            });
            this.$el.on('mouseout.starrr', function() {
                return _this.syncRating();
            });
            this.$el.on('click.starrr', 'i', function(e) {
                return _this.setRating(_this.$el.find('i').index(e.currentTarget) + 1);
            });
            this.$el.on('starrr:change', this.options.change);
        }

        Starrr.prototype.createStars = function() {
            var _i, _ref, _results;

            _results = [];
            for (_i = 1, _ref = this.options.numStars; 1 <= _ref ? _i <= _ref : _i >= _ref; 1 <= _ref ? _i++ : _i--) {
                _results.push(this.$el.append("<i class='fa fa-star-o'></i>"));
            }
            return _results;
        };

        Starrr.prototype.setRating = function(rating) {
            if (this.options.rating === rating) {
                rating = void 0;
            }
            this.options.rating = rating;
            this.syncRating();
            return this.$el.trigger('starrr:change', rating);
        };

        Starrr.prototype.syncRating = function(rating) {
            var i, _i, _j, _ref;

            rating || (rating = this.options.rating);
            if (rating) {
                for (i = _i = 0, _ref = rating - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
                    this.$el.find('i').eq(i).removeClass('fa-star-o').addClass('fa-star');
                }
            }
            if (rating && rating < 5) {
                for (i = _j = rating; rating <= 4 ? _j <= 4 : _j >= 4; i = rating <= 4 ? ++_j : --_j) {
                    this.$el.find('i').eq(i).removeClass('fa-star').addClass('fa-star-o');
                }
            }
            if (!rating) {
                return this.$el.find('i').removeClass('fa-star').addClass('fa-star-o');
            }
        };

        return Starrr;

    })();
    return $.fn.extend({
        starrr: function() {
            var args, option;

            option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
            return this.each(function() {
                var data;

                data = $(this).data('star-rating');
                if (!data) {
                    $(this).data('star-rating', (data = new Starrr($(this), option)));
                }
                if (typeof option === 'string') {
                    return data[option].apply(data, args);
                }
            });
        }
    });
})(window.jQuery, window);

$(function() {
    return $(".starrr").starrr();
});

$( document ).ready(function() {
      
  $('#stars').on('starrr:change', function(e, value){
    $('#count').html(value);
  });
  
  $('#stars-existing').on('starrr:change', function(e, value){
    $('#count-existing').html(value);
  });
  
  $('#Carousel').carousel({
        interval: 3000
    })
	
	// $('#carousel-generic').carousel({
		// interval: 3000
	// });
	 
})

function colorchange(id,id2,id3)
{

  var color = document.getElementById(id).style.color;
  var x = id;
 
  
  if(color == "gray")
  {
	document.getElementById(id).style.color = "orange";

	if(x == x){
		var alert = document.getElementById(id2);
		alert.classList.add("in");
		var txt = document.getElementById(id3);
		txt.innerHTML = "Added to your favorites..."
	
		setTimeout(function(){

		alert.classList.remove("in");

		}, 1500);
	}
	
  }
  
  if(color == "orange")
  {
    document.getElementById(id).style.color = "gray";
	
		if(x = x){
			var alert = document.getElementById(id2);
			alert.classList.add("in");
			var txt = document.getElementById(id3);
			txt.innerHTML = "Removed from your favorites..."
	
			setTimeout(function(){
		
				alert.classList.remove("in");

			}, 1500);
		}
	}
}


var i = 0;

function contador()
{
	i = i + 1;
	var btn = document.getElementById("add-car");
	var mostrar = document.getElementById("total-car");
	mostrar.classList.remove('hidden');
	mostrar.innerHTML = i;
}	


var mediaquery = window.matchMedia("(max-width: 767px)");
if (mediaquery.matches) {
   var carousel = $('#carousel-generic .carousel-inner');
   var columns = $('#carousel-generic .carousel-inner .col-xx-small');

   
   console.log(columns);
      
	carousel.append('<div class= "item" id="new-item"></div>');
	$("<div class='row'></div>").appendTo("#new-item");
	carousel.append('<div class= "item" id="new-item-1"></div>');
	$("<div class='row'></div>").appendTo("#new-item-1");
	
	$('#col-3').appendTo('#new-item');
	$('#col-4').appendTo('#new-item');
	$('#col-7').appendTo('#new-item-1');
	$('#col-8').appendTo('#new-item-1');
	
	var item = $(".carousel-inner #new-item");
	
	var x = $('#carousel-generic .item');
	
	for (i = 0; i < x.length; i++) { 
		id = x[i].id;
		
		if(id == 'new-item'){
			$("<li data-target='#carousel-generic' data-slide-to='2'></li>").appendTo('.carousel-indicators');
		}else if(id == 'new-item-1'){
			$("<li data-target='#carousel-generic' data-slide-to='3'></li>").appendTo('.carousel-indicators');
		}
		
	}

  
}



 // VALIDACION FORMULARIO STEP1

 
$(document).ready(function(){
   /*----------------------*/
   /*--- fixAddress ... ---*/
   /*----------------------*/
   $("#address_1").change(function(){ fixAddress( $(this) ); });
   $("#address_2").change(function(){ fixAddress( $(this) ); });
   
   /*----------------------------------------*/
   /*--- city, state, zip from form ...   ---*/
   /*----------------------------------------*/
   
   /*----------------------------------------*/
   /*--- form type ...                    ---*/
   /*----------------------------------------*/
   uSplit = document.location.href.split("/").reverse();
   try { $("#_FORM_").val( "/" + uSplit[1] + "/" + uSplit[0].split("?")[0] ); } catch(e) { }
   
   /*----------------------------------------*/
   /*--- debug ...                        ---*/
   /*----------------------------------------*/
   if( document.location.href.indexOf("DEBUG=") != -1 )
   {
      $("#buffer").show();
   }
   
   /*----------------------------------------*/
   /*--- Car Stuff ...                    ---*/
   /*----------------------------------------*/
   var CAR_YEAR_START  = 1980;
   var CAR_URL         = "/ajax/cars.jsp";
   
   function loadCarData( element, url, data, type )
   {
      if(element.length == 0)
         return;
      
      $.ajax({
         "url": url,
         "type": type || "POST",
         "format": "json",
         "data": data,
         "success": function(response, message){
            $.each(response, function(value, display)
            {
               element.append( $("<option>").attr("value", value).html(display) );
            });
            element.css( "width","" );
         },
         "complete": function(){
            element.removeAttr("disabled");
         }
      });
   }
   
   //--------- buildSelector ---------
   function buildSelector(element, key, attribute)
   {
      var selector = String(element.attr(key)).replace(key, "");
      return "select" + ((selector != "")? "#" + selector: "") + "[" + attribute +"]";
   }
   
   //--------- pre-set ---------
   (function(){
      //--------- populate years ---------
      var select        =  $( "select[car_year]" );
      var currentYear   =  (new Date()).getFullYear();
      //currentYear++;
      select.empty().append(
         $("<option>").attr("value", "").html("--Select--")
      );
      
      $("select[car_make], select[car_model], select[car_trim]").attr("disabled", "disabled");
      
      for( var year = currentYear; year >= CAR_YEAR_START; year-- )
         select.append(
         $("<option>").attr("value", year).html(year)
      );
   })();
   
   //--------- on Change car year ---------
   $("#state_1,#state_2").change(function(){
      var _o = $(this).attr("id");
      var _v = $(this).val();
      $.each($(".county"), function( index, value ) {
        if( $(this).attr("state") == _o )
        {
            var _cnt = $(this);
            _cnt.empty();
            _cnt.append( $("<option>").attr("value", "").html("--- select ---") );
            $.ajax({
               "url": "/ajax/county.jsp?state="+_v,
               "format": "json",
               "success": function(response, message){
                  $.each(response, function(value, display)
                  {
                     _cnt.append( $("<option>").attr("value", display.county).html(initCap(display.county)) );
                  });
               }
            });
        }
      });
   });
   
   
   $("select[car_year]").change(function(){
      var year  = $(this);
      var make  = $(buildSelector(year,  "car_year",  "car_make")  );
      var model = $(buildSelector(make,  "car_make",  "car_model") );
      var trim  = $(buildSelector(model, "car_model", "car_trim")  );
      make.removeAttr("disabled" ).empty().append($("<option>").attr("value", "").html("--Select--")).attr("disabled", "disabled");
      model.removeAttr("disabled").empty().append($("<option>").attr("value", "").html("--Select--")).attr("disabled", "disabled");
      trim.removeAttr("disabled" ).empty().append($("<option>").attr("value", "").html("--Select--")).attr("disabled", "disabled");
      loadCarData(make, CAR_URL, { "year": year.val() });
   });
   
   //--------- on Change car make ---------
   $("select[car_make]").change(function(){
      var make  = $(this);
      var year  = $("select[car_year=" + make.attr("id") + "]");
      var model = $(buildSelector(make,  "car_make",  "car_model") );
      var trim  = $(buildSelector(model, "car_model", "car_trim")  );
      model.removeAttr("disabled").empty().append($("<option>").attr("value", "").html("--Select--")).attr("disabled", "disabled");
      trim.removeAttr("disabled" ).empty().append($("<option>").attr("value", "").html("--Select--")).attr("disabled", "disabled");
      loadCarData(model, CAR_URL, { "year": year.val(), "make": make.val() });
      $("#"+$(this).attr("id")+"_lbl_").val( $(this).find("option:selected").text() );
   });
   
   //--------- on Change car model ---------
   $("select[car_model]").change(function(){
      var model = $(this);
      var make  = $("select[car_make=" + model.attr("id") + "]");
      var year  = $("select[car_year=" + make.attr("id") + "]");
      var trim  = $(buildSelector(model, "car_model", "car_trim"));
      trim.removeAttr("disabled").empty().append($("<option>").attr("value", "").html("--Select--")).attr("disabled", "disabled");
      loadCarData(trim, CAR_URL, { "year": year.val(), "make": make.val(), "model": model.val() });
      $("#"+$(this).attr("id")+"_lbl_").val( $(this).find("option:selected").text() );
   });
   
   //--------- on Change car trim ---------
   $("select[car_trim]").change(function(){
      $("#"+$(this).attr("id")+"_lbl_").val( $(this).find("option:selected").text() );
   });
   
   /*----------------------------------------*/
   /*--- state ...                        ---*/
   /*----------------------------------------*/
   u = document.location.href.split( "/" );
   u = stateLookup( u[u.length-1].split(".")[0] );
   if( typeof u === "undefined" )
      u = "";

   try { if(u!="") $("#state").val(u);                            } catch(e) {}
   try { if(u!="") $("#state_1").val(u); $("#state_1").change();  } catch(e) {}
   try { if(u!="") $("#state_2").val(u);                          } catch(e) {}

   try { if(""!="") $("#city").val("");   } catch(e) {}
   try { if(""!="") $("#city_1").val(""); } catch(e) {}
   try { if(""!="") $("#city_2").val(""); } catch(e) {}


   try { if(""!="") $("#zip").val("");   } catch(e) {}
   try { if(""!="") $("#zip_1").val(""); } catch(e) {}
   try { if(""!="") $("#zip_2").val(""); } catch(e) {}

   
   /*----------------------------------------*/
   /*--- trim on change ...               ---*/
   /*----------------------------------------*/
   $(":input").change(function(){
      $(this).val( $.trim( $(this).val() ) );
   });
   
   /*----------------------------------------*/
   /*--- Phone field ...                  ---*/
   /*----------------------------------------*/
// $("[field=PHONE],[field=ZIP]").blur(function(){
   $("[field=PHONE]").blur(function(){
      /*
      if( $(this).val()==$(this).attr("placeholder") )
         return;
      */
      var el = $(this);
      var cleanNumber = el.val().replace(/[^\d]+/g, "");
      
      if( (cleanNumber.length!=10 && "US"=="US") ||
          (cleanNumber.length!=10 && "US"=="BR")
      )
      {
         el.val(cleanNumber);
         return;
      }
      el.val(cleanNumber.replace(/(\d{3})(\d{3})(\d{4,})/g, "$1-$2-$3"));
   })
   
   /*----------------------------------------*/
   /*--- Number field ...                 ---*/
   /*----------------------------------------*/
   $("[field=NUMBER]").keypress(function(event){
      return true;
   }).blur(function(){
      if( $(this).val()==$(this).attr("placeholder") )
         return;
      var el = $(this);
      el.val(el.val().replace(/[^\d]/g, ""));
   });
   popForm();
});

/*----------------------------------------------------------------------------*/
/*--- setHeight ...                                                        ---*/
/*----------------------------------------------------------------------------*/
function setHeight( o )
{
   $("#"+o).val( $("#_feet_"+o).val() + "'" + $("#_inch_"+o).val() );
}

/*----------------------------------------------------------------------------*/
/*--- btnSwap ...                                                          ---*/
/*----------------------------------------------------------------------------*/
function btnSwap()
{
   $("#submit").toggle();
   $("#spin").toggle();
}

/*----------------------------------------------------------------------------*/
/*--- setDate ...                                                          ---*/
/*----------------------------------------------------------------------------*/
function setDate(o)
{
   objId    = o.id.substr(4);
   mainObj  = $("#" + objId );
   mObj     = $("#_mm_" + objId );
   dObj     = $("#_dd_" + objId );
   yObj     = $("#_yy_" + objId );
   
   m        =  mObj.val();
   d        =  dObj.val();
   y        =  yObj.val();
   full     =  m + "/" + d + "/" + y;
   if( full=="//" )
      full = "";
   mainObj.val( full );
}

/*----------------------------------------------------------------------------*/
/*--- setMonthYear ...                                                     ---*/
/*----------------------------------------------------------------------------*/
function setMonthYear(o)
{
   objName = o.id.substr(4);
   mV      = $("#_mm_"+objName).val();
   yV      = $("#_yy_"+objName).val();
   v       = mV + "/" + yV;
   v       = v=="/"?"":v;
   $("#"+objName).val( v );
}

/*----------------------------------------------------------------------------*/
/*--- postForm ...                                                         ---*/
/*----------------------------------------------------------------------------*/
function postForm()
{
   if( validateForm() )
   {
      btnSwap();
      document.frm.submit();
      /*
      try
      {
         postMediata("FINAL");
      }
      catch(e)
      {
         document.frm.submit();
      }
      */
   }
}
function postDM()
{
   document.frm.submit();
}


function validateForm()
{
   $(".lbls").removeClass( "errorlabel" );
   if( document.location.href.indexOf("DEBUG=") != -1 )
   {
      document.frm.submit();
      return true;
   }
   
   var errs       =   false;
   var els        =   document.frm.elements;
   for( var i=0; i<els.length; i++ )
   {
      try
      {
         var elId = els[i].id;
         var o    = $("#"+elId);
         var t    = o.attr( "field" );
         var v    = $.trim( o.val() );
         var m    = $("#_msg_"+elId);
         var l    = $("#label_"+elId);
         
         var r    = o.attr( "req" );
         m.html( "&nbsp;" ).removeClass("err_on");
         
         if( t=="RADIO" )
         {
            fieldName = o.attr("name");
            v = $('input:radio[name="'+fieldName+'"]:checked').val();
            m = $('#_msg_'+fieldName);
            m.html( "" ).removeClass("err_on");
            if( typeof v === "undefined" )
               v = "";
         }
         else
         if( t=="MONTH_YEAR" )
         {
            if( $("#_mm_"+elId).is(":visible") && (r=='Y') && (els[i].value.length!=7) )
            {
               m.html( "Missing Data" );
               focusField( o, t, elId )
               fadeErr(m);
               errs = true;
            }
         }
         
         if(   o.is(":visible") || ((t=="DATE" || t=="DOB") && $('#_mm_'+elId).is(":visible")) )
         {
            if( r=="Y" && v=="" )
            {
               if( !errs )
                  focusField( o, t, elId )
               
               l.addClass("errorlabel");
               
               if( m.length )
               {
                  m.html( "Missing Data" );
                  fadeErr(m);
               }
               
               errs = true;
            }
            else
            {
               if( !validateField( o, v, t, r, m ) && !errs )
               {
                  focusField( o, t, elId );
                  errs = true;
               }
            }
         }
      }
      catch(e)
      {
      }
   }


   if( !customValidate() || errs )
   {
   
      if( $("#globalerror").length )
      {
         $("#globalerror").html( "Missing Data" );
         $("#globalerror").fadeIn(1000, function() {
            $("#globalerror").fadeOut(500, function() {
               $("#globalerror").fadeIn(1000);
            });
         });  
      }
      
      return false;
   
   }
   
   try
   {
      if( !extraValidate() )
         return false;
   }
   catch(e)
   {
   }
   return true; 
}

/*----------------------------------------------------------------------------*/
/*--- fixAddress ...                                                       ---*/
/*----------------------------------------------------------------------------*/
function fixAddress( o )
{
   val      = o.val().toUpperCase();
   msg_div  = $("#_msg_"+o.attr("id"));
   msg_div.html( "" );
   if( val.indexOf("#")!=-1 || val.indexOf(" SUITE ")!=-1 || val.indexOf(" APT ")!=-1 )
   {
      msg_div.html( "Invalid Address" );
      fadeErr(msg_div);
      return false;
   }
   else
   {
      tmp = val.replace(/[^a-zA-Z]+/g,'').replace( /\s/g, "" );
      if( tmp=="POBOX" )
      {
         msg_div.html( "Invalid Address" );
         fadeErr(msg_div);
         return false;
      }
   }
   return true;
}

/*----------------------------------------------------------------------------*/
/*--- validateField ...                                                    ---*/
/*----------------------------------------------------------------------------*/
function validateField( obj, val, typ, req, msg_div )
{
   if( obj.attr("type")=="radio" && obj.attr("req")=="Y" )
   {
      var r = true;
      try
      {
         var radName = obj.attr("name");
         var radVal  = $('input[name=' + radName + ']:checked').val() || "";
         if( radVal=="" )
         {
            $('input[name=' + radName + ']').each(function() {
               $("#label_"  + radName ).addClass( "errorlabel" );
               r = false;
            });
         }
      }
      catch(e)
      {
      }
      return r;
   }

   oId = obj.attr("id");
   if( oId=="address_1" || oId=="address_2" )
   {
      if( !fixAddress(obj) )
         return false;
   }
   else
   if( typ == "EMAIL" )
   {
      if( !emailCheck(val) )
      {
         msg_div.html( "Invalid e-Mail" );
         fadeErr(msg_div);
         return false;
      }
   }
   else
   if( typ == "PHONE" )
   {
      msisdn = extractNumberJunk(val);
      if( msisdn.length!=0 )
      {
         if( (msisdn.length!=10 && "US"=="US") ||
             (msisdn.length!=10 && "US"=="BR")
         )
         {
            msg_div.html( "Invalid Phone #" );
            fadeErr(msg_div);
            return false;
         }
      }
   }
   else
   if( typ=="DOB" || typ=="DATE" )
   {
      if( val!="" && !dateCheck(val) )
      {
         msg_div.html( "Invalid Date" );
         fadeErr(msg_div);
         return false;
      }
   }
   return true;
   
}

/*----------------------------------------------------------------------------*/
/*--- focusField ...                                                       ---*/
/*----------------------------------------------------------------------------*/
function focusField( o, t, elId )
{
   var _o = o;
   try
   {
      if( t=='DOB' || t=='DATE' )
         _o = $("#_mm_" + elId);
      else
      if( t=='HEIGHT' )
         _o = $( "#_feet_" + elId );
      _o.focus();
      _o.select();
   }
   catch(e)
   {
   }
}


/*----------------------------------------------------------------------------*/
/*--- setErrMsg ...                                                        ---*/
/*----------------------------------------------------------------------------*/
function setErrMsg( objName, errMsg )
{
   var m = $("#_msg_"+objName);
   m.html( errMsg );
   fadeErr(m);

   o = $( "#"+objName );

   focusField( o, o.attr("field"), objName );
}



/*----------------------------------------------------------------------------*/
/*--- validateAddress ...                                                  ---*/
/*----------------------------------------------------------------------------*/
function validateAddress( typ )
{

   var HTTPS_KEY = "ABQIAAAAM2NwXjUwIF0_whEjmxC0gxSyWfVkRxUL0C1eztfnALDs_jWcsRSPvZbobvKnJ4ul8R_LwGkpUpzRdQ";
   var HTTP_KEY  = "ABQIAAAAM2NwXjUwIF0_whEjmxC0gxS-aTHDcIL1RztHO-uynIJmCVzAuhRgBXeVrLFmUDnr9E21q-77V8aPIw";
   var protocol  = document.location.protocol;
   var dbg = ( document.location.href.indexOf("DEBUG=") != -1 );
   try
   {
      address_ =  $("#address_"+typ).val();
      city_    =  $("#city_"+typ).val();
      state_   =  $("#state_"+typ).val();
      zip_     =  $("#zip_"+typ).val();

      u  = protocol + '//maps.google.com/maps/geo?output=json&v=2&sensor=false';
      u += '&q=';

      uri      = address_ + " " + city_ + " " + state_ + " " + zip_;
      u += escape(uri);
      if( protocol=="https:" )
         u += "&key=" + HTTPS_KEY;
      else
         u += "&key=" + HTTP_KEY;

      if( dbg )
         alert( u );

      $.ajax({
         "url": u,
         dataType: 'jsonp',
         cache: false,
         "async":false,
         "success": function(response, message){
            try
            {
               $("#_msg_address_"+typ).html( "&nbsp;" ).removeClass("err_on");
               $("#_msg_city_"+typ).html( "&nbsp;" ).removeClass("err_on");
               $("#_msg_state_"+typ).html( "&nbsp;" ).removeClass("err_on");
               $("#_msg_zip_"+typ).html( "&nbsp;" ).removeClass("err_on");
            }
            catch(e)
            {
            }
           
            if( dbg )
            {
               alert( response.Placemark );
               alert( response.Placemark[0] );
               alert( response.Placemark[0].address );
            }
            if( !response.Placemark )
               return;
            fullAddr   = response.Placemark[0].address.split( "," );
            if( dbg )
               alert( fullAddr );
            __country  = $.trim( fullAddr[fullAddr.length - 1] );
            if( __country == "USA" )
            {
               fullAddr.pop();
               __statezip = $.trim( fullAddr[fullAddr.length - 1] ).split(" ");
               __state    = __statezip[0];
               __zip      = __statezip[1];


               fullAddr.pop();
               __city    = $.trim( fullAddr[fullAddr.length - 1] );

               fullAddr.pop();
               __address = $.trim( fullAddr[fullAddr.length - 1] );


               
               $("#address_" + typ).val( __address );
               $("#city_"    + typ).val( __city    );
               $("#state_"   + typ).val( __state   );
               $("#zip_"     + typ).val( __zip     );
            }
         },
         "error": function(){
            if( dbg )
            {
               alert( "ERROR" );
            }
         }
      });
   }
   catch(e)
   {
   }
}




/*----------------------------------------------------------------------------*/
/*--- fadeErr ...                                                          ---*/
/*----------------------------------------------------------------------------*/
function fadeErr(m)
{
   $("#"+m.attr("id").replace("_msg_", "label_")).addClass( "errorlabel" );
   
   m.addClass( "err_on" );
   m.fadeIn(1000, function() {
      m.fadeOut(500, function() {
         m.fadeIn(1000);
      } );
   });
}



/*###############################*/
/*###############################*/
/*###### U T I L I T I E S ######
/*###############################*/
/*###############################*/


/*----------------------------------------------------------------------------*/
/*--- emailCheck ...                                                       ---*/
/*----------------------------------------------------------------------------*/
function emailCheck( email )
{
   var pattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
   return pattern.test(email);
}

/*----------------------------------------------------------------------------*/
/*--- dateCheck ...                                                        ---*/
/*----------------------------------------------------------------------------*/
function dateCheck( dt )
{
   var _dt = new Date(dt);
   var spl = dt.split( "/" );
   return ( parseInt(spl[0])===(_dt.getMonth()+1) && parseInt(spl[2])===_dt.getFullYear() );
}

/*----------------------------------------------------------------------------*/
/*--- isValidText ...                                                      ---*/
/*----------------------------------------------------------------------------*/
function isValidText(str)
{
   if( str=="" ) return false;
   var re = /[^a-zA-Z\.\- ]/g
   return re.test(str);
}

/*----------------------------------------------------------------------------*/
/*--- containsVowels ...                                                   ---*/
/*----------------------------------------------------------------------------*/
function containsVowels(str)
{
   var vwls = "euioay";
   for( i=0; i<vwls.length; i++ )
   {
      if( str.indexOf(""+vwls.charAt(i)) != -1 )
         return true;
   }
   return false;
}

/*----------------------------------------------------------------------------*/
/*--- isNumeric ...                                                        ---*/
/*----------------------------------------------------------------------------*/
function isNumeric(str){
   if( str=="" )
      return true;
  var re = /[\D]/g
  if (re.test(str)) return false;
  return true;
}

/*----------------------------------------------------------------------------*/
/*--- isAlphaNumeric ...                                                   ---*/
/*----------------------------------------------------------------------------*/
function isAlphaNumeric(str){
   if( str=="" )
      return true;
  var re = /[^a-zA-Z0-9]/g
  if (re.test(str)) return false;
  return true;
}

/*----------------------------------------------------------------------------*/
/*--- allSameChar ...                                                      ---*/
/*----------------------------------------------------------------------------*/
function allSameChar(str)
{
   if( str.length==0 )
      return false;
   zeroChar = str.charAt(0);
   for( i=1; i<str.length; i++ )
      if( str.charAt(i) != zeroChar )
         return false;
   return true;
}

/*----------------------------------------------------------------------------*/
/*--- hideField ...                                                        ---*/
/*----------------------------------------------------------------------------*/
function hideField(dId)
{
   $("#"+dId).hide();
}
/*----------------------------------------------------------------------------*/
/*--- showField ...                                                        ---*/
/*----------------------------------------------------------------------------*/
function showField(dId)
{
   $("#label_"+dId).show();
   $("#"+dId).show();
}


/*----------------------------------------------------------------------------*/
/*--- stateLookup ...                                                      ---*/
/*----------------------------------------------------------------------------*/
var sLookup = new Array();
sLookup["maranhao"] = "MA";sLookup["rio-grande-do-sul"] = "RS";sLookup["florida"] = "FL";sLookup["manitoba"] = "MB";sLookup["new-york"] = "NY";sLookup["arkansas"] = "AR";sLookup["nebraska"] = "NE";sLookup["sergipe"] = "SE";sLookup["ohio"] = "OH";sLookup["texas"] = "TX";sLookup["ceara"] = "CE";sLookup["missouri"] = "MO";sLookup["north-carolina"] = "NC";sLookup["santa-catarina"] = "SC";sLookup["georgia"] = "GA";sLookup["rhode-island"] = "RI";sLookup["puerto-rico"] = "PR";sLookup["ontario"] = "ON";sLookup["alaska"] = "AK";sLookup["tocantins"] = "TO";sLookup["delaware"] = "DE";sLookup["massachusetts"] = "MA";sLookup["northwest-territories"] = "NT";sLookup["rio-de-janeiro"] = "RJ";sLookup["nova-scotia"] = "NS";sLookup["california"] = "CA";sLookup["oklahoma"] = "OK";sLookup["west-virginia"] = "WV";sLookup["alabama"] = "AL";sLookup["mato-grosso"] = "MT";sLookup["louisiana"] = "LA";sLookup["goias"] = "GO";sLookup["kansas"] = "KS";sLookup["pennsylvania"] = "PA";sLookup["utah"] = "UT";sLookup["yukon"] = "YT";sLookup["minnesota"] = "MN";sLookup["parana"] = "PR";sLookup["oregon"] = "OR";sLookup["virginia"] = "VA";sLookup["paraiba"] = "PB";sLookup["washington"] = "WA";sLookup["sao-paulo"] = "SP";sLookup["alagoas"] = "AL";sLookup["iowa"] = "IA";sLookup["north-dakota"] = "ND";sLookup["south-carolina"] = "SC";sLookup["arizona"] = "AZ";sLookup["maryland"] = "MD";sLookup["piaui"] = "PI";sLookup["prince-edward-island"] = "PE";sLookup["illinois"] = "IL";sLookup["nunavut"] = "NU";sLookup["rio-grande-do-norte"] = "RN";sLookup["para"] = "PA";sLookup["tennessee"] = "TN";sLookup["pernambuco"] = "PE";sLookup["saskatchewan"] = "SK";sLookup["distrito-federal"] = "DF";sLookup["montana"] = "MT";sLookup["idaho"] = "ID";sLookup["kentucky"] = "KY";sLookup["wisconsin"] = "WI";sLookup["maine"] = "ME";sLookup["nevada"] = "NV";sLookup["new-jersey"] = "NJ";sLookup["south-dakota"] = "SD";sLookup["hawaii"] = "HI";sLookup["michigan"] = "MI";sLookup["connecticut"] = "CT";sLookup["quebec"] = "QC";sLookup["alberta"] = "AB";sLookup["colorado"] = "CO";sLookup["wyoming"] = "WY";sLookup["rondonia"] = "RO";sLookup["district-of-columbia"] = "DC";sLookup["amapa"] = "AP";sLookup["mato-grosso-do-sul"] = "MS";sLookup["bahia"] = "BA";sLookup["minas-gerais"] = "MG";sLookup["roraima"] = "RR";sLookup["newfoundland"] = "NL";sLookup["espirito-santo"] = "ES";sLookup["acre"] = "AC";sLookup["new-mexico"] = "NM";sLookup["vermont"] = "VT";sLookup["new-brunswick"] = "NB";sLookup["mississippi"] = "MS";sLookup["new-hampshire"] = "NH";sLookup["amazonas"] = "AM";sLookup["british-columbia"] = "BC";sLookup["indiana"] = "IN";
function stateLookup(str)
{
   return sLookup[str];
}


/*----------------------------------------------------------------------------*/
/*--- titleField ...                                                       ---*/
/*----------------------------------------------------------------------------*/
function titleField()
{
   $(':input[title]').each(function() {
      var $this = $(this);
      if($this.val() === '') {
         $this.val($this.attr('title'));
         $this.css("color", "silver");
      }
      $this.focus(function() {
         if($this.val() === $this.attr('title')) {
           $this.val('');
           $this.css("color", "black");
         }
      });
      $this.blur(function() {
         if($this.val() === '') {
            $this.val($this.attr('title'));
            $this.css("color", "silver");
         }
      });
   });   
}

/*----------------------------------------------------------------------------*/
/*--- checkFixEmail ...                                                    ---*/
/*----------------------------------------------------------------------------*/
function checkFixEmail()
{
   if( !emailCheck(document.frm.fixEmail.value) )
   {
      alert( "Invalid Email" );
      document.frm.fixEmail.focus();
      document.frm.fixEmail.select();
      return false;
   }
   
   if( !emailCheck(document.frm.fixConfirmEmail.value) )
   {
      alert( "Invalid Email" );
      document.frm.fixConfirmEmail.focus();
      document.frm.fixConfirmEmail.select();
      return false;
   }
   
   if( document.frm.fixEmail.value != document.frm.fixConfirmEmail.value )
   {
      alert( "Email not matching confirmation" );
      document.frm.fixEmail.focus();
      document.frm.fixEmail.select();
      return false;
   }
   $("#fixemail").colorbox.close();
   try { $("#email").html(document.frm.fixConfirmEmail.value) } catch(e) {}
   return true;
}


function popForm()
{
   try
   {
      for( var v in _PROFILE )
      {
         if( v=="state_1" && _PROFILE[v]=="" )
            continue;
         if( v.charAt(0)!="_" )
            $("#"+v).val( _PROFILE[v] );
         if( v=="date_of_birth" )
         {
            _dob = _PROFILE[v].split("/");
            $("#_mm_date_of_birth").val( _dob[0] );
            $("#_dd_date_of_birth").val( _dob[1] );
            $("#_yy_date_of_birth").val( _dob[2] );
         }
      }
   }
   catch(e)
   {
   }
}


function placeholders()
{
   try
   {
      if( navigator.appName.indexOf("Explorer")>0  && parseInt($.browser.version, 10) <=9 )
      {
         try
         {
            $('input, textarea').placeholder();
         }
         catch(e)
         {
         }
      }
   }
   catch(e)
   {
   }
}
$(document).ready(function(){
   
});

function customValidate()
{
   return true;
}


// $('#radioBtn a,#radioBtn input').on('click', function(){
//     var sel = $(this).data('title');
//     var tog = $(this).data('toggle');
//     $('#'+tog).prop('value', sel);
//     if(sel=='Temporary'){
//     	$('#hiddenTempDate').show();
//     	$('#usps_forward_stop').addClass('required');
//     	$('#usps_forward_stop').addClass('alwaysValidate');
//     } else {
//     	$('#hiddenTempDate').hide();
//     	$('#usps_forward_stop').removeClass('required');
//     	$('#usps_forward_stop').removeClass('alwaysValidate');
//     }
//     $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
//     $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
// })

// function checkMatch(id) {
//    oldAddr = ($.trim($("#address_1").val())+$.trim($("#suite_1").val())+$.trim($("#state_1").val())+$.trim($("#zip_1").val())).toLowerCase();
//    newAddr = ($.trim($("#address_2").val())+$.trim($("#suite_2").val())+$.trim($("#state_2").val())+$.trim($("#zip_2").val())).toLowerCase();
//    return ( oldAddr!=newAddr );
// }

// /*
// $(function () {
//     $('#datetimepicker1').datetimepicker();
// });
// */
// $('.glyphicon-info-sign').hover(function(){
// 	$(this).tooltip('toggle');
// });

// function isDate(txtDate)
// {
  
//   var currVal = txtDate;
//   if(currVal == '')
//     return false;
 

//   //Declare Regex  
//   var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
//   var dtArray = currVal.match(rxDatePattern); // is format OK?

//   if (dtArray == null)
//      return false;
 
//   //Checks for mm/dd/yyyy format.
//   dtMonth = dtArray[1];
//   dtDay= dtArray[3];
//   dtYear = dtArray[5];

//   if (dtMonth < 1 || dtMonth > 12)
//       return false;
//   else if (dtDay < 1 || dtDay> 31)
//       return false;
//   else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
//       return false;
//   else if (dtMonth == 2)
//   {
//      var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
//      if (dtDay> 29 || (dtDay ==29 && !isleap))
//           return false;
//   }


//   return true;
// }

// function isFutureDate(date) {
    
//     var parts = date.split('/');
// 	var date = new Date(parseInt(parts[2], 10),     // year
// 	                    parseInt(parts[0], 10)-1, // month, starts with 0
// 	                    parseInt(parts[1], 10));    // day

// 	if (date < new Date()) {
// 	    // It's in the past, including one millisecond ago
// 	    return false;
// 	} else {
// 		return true;
// 	}

// }

// function isFutureOfForwardDate(date,forwardDate) {
    
//     var stopDate = date;
//     var startDate = forwardDate;

//     var parts = startDate.split('/');
// 	var futureDate = new Date(parseInt(parts[2], 10),     // year
// 	                    parseInt(parts[0], 10)-1, // month, starts with 0
// 	                    parseInt(parts[1], 10)+10);    // day

// 	var parts = stopDate.split('/');
// 	var stopDate = new Date(parseInt(parts[2], 10),     // year
// 	                    parseInt(parts[0], 10)-1, // month, starts with 0
// 	                    parseInt(parts[1], 10));    // day

// 	if (stopDate < futureDate) {
// 	    // It's in the past, including one millisecond ago
// 	    return false;
// 	} else {
// 		return true;
// 	}

// }

// function extractNumberJunk( val )
// {
//    _v = "";
//    for( var n=0; n<val.length; n++ )
//    {
//       if( val.charAt(n)==" " )
//          continue;
//       try
//       {
//          evl = eval( val.charAt(n) );
//          _v += val.charAt(n);
//       }
//       catch(e)
//       {
//       }
//    }
//    return _v;
// }


// function isEmail(email) { 
//     var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
//     return re.test(email);
// } 

// function isPhone(phone) {
// 	msisdn = extractNumberJunk(phone);
//       if( msisdn.length!=0 )
//       {
//          if( (msisdn.length!=10 && "US"=="US") ||
//              (msisdn.length!=10 && "US"=="BR")
//          )
//          {
//             return false;
//          }
//       } else {
//       	return false;
//       }
// }

// function checkTwin(elem) {

//  	// Find twin checkbox and set the value = to current status from THIS elem
// 	var value = $(elem).prop('checked');
// 	var className = $(elem).attr('class');
// 	var partsOfClass = className.split(' ');
// 	var twinClass = partsOfClass[1];

// 	$('.'+twinClass).each(function() {
// 		$(this).prop('checked',value);
// 	});

// }

// function validateData(elem) {
// 	var flag = false;
// 	$('#'+elem+' .required').each(function() {
// 	if($(this).is(":visible") || $(this).hasClass('alwaysValidate') ){
// 		if($(this).attr('type')=='checkbox') {
// 			if($(this).prop('checked')!=true){
// 				flag=true;
// 				$(this).parent().parent().addClass('has-error');
// 				if($(this).parent().parent().children('.error-msg')) {
// 					$(this).parent().parent().children('.error-msg').remove();
// 				}
// 				if($(this).hasClass('terms')) {
// 					$(this).parent().parent().append('<div class="error-msg col-md-12"><div class="clear"></div><br><div class="alert alert-danger ">You must agree to all terms and conditions in order to use our service.</div></div>');
// 				} else {
// 					$(this).parent().parent().append('<div class="error-msg col-md-12"><div class="clear"></div><br><div class="alert alert-danger">This item is required</div></div>');
// 				}
// 			}
// 		}
	
// 		if($(this).data('type')=='date') {
// 			if($(this).data('check')=='endDate') {
// 				if(isFutureOfForwardDate($(this).val(),$('#usps_forward').val())==false) {
// 					flag = true;
// 					$(this).parent().addClass('has-error');
// 					if($(this).parent().children('.error-msg')) {
// 						$(this).parent().children('.error-msg').remove();
// 					}
// 					$(this).parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">You must provide a 2 weeks after forward date</div>');
// 				}
// 			}
// 			if(isFutureDate($(this).val())==false) {
// 				flag = true;
// 				$(this).parent().addClass('has-error');
// 				if($(this).parent().children('.error-msg')) {
// 					$(this).parent().children('.error-msg').remove();
// 				}
// 				$(this).parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">You must provide a future date</div>');
// 			}
// 			if(isDate($(this).val())==false) {
// 				flag = true;
// 				$(this).parent().addClass('has-error');
// 				if($(this).parent().children('.error-msg')) {
// 					$(this).parent().children('.error-msg').remove();
// 				}
// 				$(this).parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">Date format incorrect. Expected: mm/dd/yyyy</div>');
// 			}

// 		}
// 		if($(this).data('type')=='email') {
// 			if(isEmail($(this).val())==false) {
// 				flag = true;
// 				$(this).parent().addClass('has-error');
// 				if($(this).parent().children('.error-msg')) {
// 					$(this).parent().children('.error-msg').remove();
// 				}
// 				$(this).parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">Please enter a valid email address</div>');
// 			}
// 		}
// 		if($(this).data('type')=='phone') {
// 			if(isPhone($(this).val())==false) {
// 				flag = true;
// 				$(this).parent().addClass('has-error');
// 				if($(this).parent().children('.error-msg')) {
// 					$(this).parent().children('.error-msg').remove();
// 				}
// 				$(this).parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">Please enter a valid phone number</div>');
// 			}
// 		}
//                 //--- ADDRESS MISMATCH
// 		if($(this).data('addr')=='mismatch') {
// 			if(checkMatch($(this).attr('id'))==false) {
// 				flag = true;
// 				$(this).parent().addClass('has-error');
// 				if($(this).parent().children('.error-msg')) {
// 					$(this).parent().children('.error-msg').remove();
// 				}
// 				$(this).parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">Your old address can not match your new address</div>');
// 			}
// 		}


// 		if($(this).val()==null || $(this).val()==false) {
// 			if($(this).parent().hasClass('input-group') && $(this).parent().hasClass('super-parent')) {
// 				$(this).parent().parent().addClass('has-error');
// 				if($(this).parent().parent().children('.error-msg')) {
// 						$(this).parent().parent().children('.error-msg').remove();
// 					}
// 				if($(this).data("error")){
// 					$(this).parent().parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">'+$(this).data('error')+'</div>');
// 				} else {
// 					$(this).parent().parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">This field is required</div>');
// 				}
// 			} else {
// 				$(this).parent().addClass('has-error');
// 				if($(this).parent().children('.error-msg')) {
// 						$(this).parent().children('.error-msg').remove();
// 					}
// 				if($(this).data("error")){
// 					$(this).parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">'+$(this).data('error')+'</div>');
// 				} else {
// 					$(this).parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">This field is required</div>');
// 				}
// 			}
			
			
// 			flag = true;
// 			// return false;
// 		}
// 	}
		
// 	});

// 	if(flag!=false) {
// 		return false;
// 	}
// 	return true;
	
	
// }

// $('.nextBtn').click(function() {
// 	var parentId = $(this).parent().attr('id');

// 	if(validateData(parentId)==true){
// 	   track( parentId );
	   
// 		$(this).parent().addClass('hidden-xs');
// 		$(this).parent().addClass('visible-lg');
// 		$(this).parent().removeClass('visible-xs');
// 		$(this).parent().next('.fieldset').removeClass('hidden-xs');
// 		$(this).parent().next('.fieldset').addClass('visible-xs');
// 		$(this).parent().next('.fieldset').addClass('visible-lg');
// 		var elem = $(this).parent().next('.fieldset').attr('id');
// 		$('.mobileSteps .nav-pills li').removeClass('active');
// 		$('.mobileSteps .nav-pills li .'+elem).parent('li').addClass('active');
// 	    $('.mobileSteps .nav-pills li .'+elem).parent('li').prev('li').addClass('done');
// 		window.scrollTo(0, 0);
// 	} else {
// 		return false;
// 	}

// });

// $('.finalSubmit').click(function(e) {
// 	e.preventDefault();
// 	var parentId = $(this).parent().attr('id');

// 	if(validateData(parentId)==true){
// 	$('.removeBeforeSubmit').each(function(){
// 		$(this).remove();
// 	});
// 	$('form').submit();
// 	} else {
// 		return false;
// 	}
// });

// $('.finalSubmitDesktop').click(function(e) {
// 	e.preventDefault();
// 	flag=false;
// 	var parentId = $(this).parent().attr('id');
// 	$('.fieldset').each(function() {
// 		if(validateData($(this).attr('id'))!=true){
// 			flag=true;
// 		}
// 	});

// 	//Manually Validate New Address

	
// 	if(flag==false){
// 		$('.removeBeforeSubmit').each(function(){
// 			$(this).remove();
// 		});
// 		$('form').submit();
// 	}
	

// });

// function setDate(elem)
// {
//    objId    = elem.id.substr(4);
//    mainObj  = $("#" + objId );
//    mObj     = $("#_mm_" + objId );
//    dObj     = $("#_dd_" + objId );
//    yObj     = $("#_yy_" + objId );
   
//    m        =  mObj.val();
//    d        =  dObj.val();
//    y        =  yObj.val();
//    full     =  m + "/" + d + "/" + y;
//    if( full=="//" )
//       full = "";
//    mainObj.val( full );
// }


// $(document).ready(function() {
	
// 	$('input,select').change(function() {

// 		// revalidate field
// 	  if($(this).attr('type')=="checkbox") {
// 	  	if($(this).parent().parent().hasClass('has-error')) {
// 	  	  var value = $(this).prop('checked');
// 		  if(value!=false){
// 		  	$(this).parent().parent().removeClass('has-error');
// 		  	// $(this).parent().addClass('has-success');
// 		  }
// 		  if($(this).parent().parent().children('.error-msg')) {
// 			$(this).parent().parent().children('.error-msg').remove();
// 		  }
// 	  }
// 	  }
// 	  if($(this).parent().hasClass('has-error')) {
// 		  if($(this).val()!="" && $(this).val()!=null && $(this).val()!=false && $(this).val()!=" "){
// 		  	$(this).parent().removeClass('has-error');
// 		  	// $(this).parent().addClass('has-success');
// 		  }
// 		  if($(this).parent().children('.error-msg')) {
// 			$(this).parent().children('.error-msg').remove();
// 		  }
// 	  }

// 	  var name = $(this).attr('name');
// 	  var valueToChange = $(this).val();

// 	  $('.'+name).each(function() {
// 	  	$(this).val(valueToChange);
// 	  });
// 	});
	

// 	var i = 1;
// 	$('.fieldset').each(function(){
// 		if(i==1){
// 			var item = '<li class="active"><a href="#" class="stepToggle '+$(this).attr('id')+'" data-order="'+i+'" toggle-data="'+$(this).attr('id')+'">Step '+i+'</a></li>';
// 		} else {
// 			var item = '<li><a href="#" class="stepToggle '+$(this).attr('id')+'" data-order="'+i+'" toggle-data="'+$(this).attr('id')+'">Step '+i+'</a></li>';
// 		}
		
// 		$('.mobileSteps .nav-pills').append(item);
// 		i=i+1;
// 	});

// 	$(document).delegate(".stepToggle","click",function(e){
// 	    e.preventDefault();
// 	    var orderReq = $(this).data('order');
// 	    var orderActive = $('.mobileSteps .nav-pills li.active a').data('order');

// 	    var active = $('.mobileSteps .nav-pills li.active a').attr('toggle-data');
// 		var elem = $(this).attr('toggle-data');
		
// 		if(orderReq>orderActive) {
// 			if(validateData(active)==false){
// 				return false;
// 			} else {
// 				$('.mobileSteps .nav-pills li.active').addClass('done');
// 			}
// 		}

// 		$('.mobileSteps .nav-pills li').removeClass('active');
// 		$(this).parent('li').addClass('active');
// 		// $('.fieldset').hide();
// 		$('.fieldset').each(function() {
			
// 			$(this).removeClass('visible-xs');
// 			$(this).addClass('hidden-xs');
// 			$(this).addClass('visible-lg');
// 		});
		
// 		$('#'+elem).removeClass('hidden-xs');
// 		$('#'+elem).addClass('visible-xs');
		
// 	});
// popForm();
// });


// function popForm()
// {
//    try
//    {
//       for( var v in _PROFILE )
//       {
//          if( v.charAt(0)!="_" )
//          {
//             //console.log( v + "=" + _PROFILE[v] );
//             $("#"+v).val( _PROFILE[v] );
//          }
//       }
      
//       $("#email_confirm").val( $("#email").val() );
      
      
//       if( _PROFILE["prefix"]=="Mr." )
//          $("#male").attr(  'checked', true);
//       else
//          $("#female").attr('checked', true);

     
 



//       dt = _PROFILE["usps_forward"].split("/");
//       dt[0] = (dt[0].length==1?"0":"") + dt[0];
//       dt[1] = (dt[1].length==1?"0":"") + dt[1];
//       $("#_mm_usps_forward").val( dt[0] );
//       $("#_dd_usps_forward").val( dt[1] );
//       $("#_yy_usps_forward").val( dt[2] );


//       $("#" + _PROFILE["moving_type"].toLowerCase()).attr('checked', true);
//    }
//    catch(e)
//    {
//    }
// }




// VALIDACION BILLING




var curNoDate;

var popcount    = 0;
var leaveSite   =  true;
var user_ip     = "190.7.203.105";
var user_id     = "null";
var user_agent  = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36";
var transam     = "null";
var coverdell   =  null;
var forwarding_service = "null";


var __SPLASHZIP   = "null";
var __SPLASHSTATE = "null";
var quiz_points   = "null";
var quiz_response = "null";
var upsellYN      = "null";
var paymentTrans  =  false;



var _PAYMENT          = new Array();
var _PROFILE          = new Array();
var domainName        = "fishinglicense.org";
var restaurant_coupon = "null";



var utm_source    =  "null";
var utm_campaign  =  "null";
var utm_content   =  "null";
var utm_term      =  "null";



var poll = new Array();





$(document).ready(function()
{
   $(".datepicker").blur(function() {
      var v = $(this).val();
      var d = new Date(v);
      var p = mmddyyyy(d);
      if( p.indexOf("NaN")==-1 || v=="" )
      {
         if( v!="" )
            $(this).val( p );
         $(this).removeClass( "error" );
      }
      else
      {
         $(this).focus().val( "Invalid Date" ).addClass( "error" );
         curNoDate = $(this);
         setTimeout( "clearNoDate();", "2000" );
      }
   });
   jQuery('.datepicker').bind('keyup',function(evt){
       if( !(evt.which==35 || evt.which==36 || evt.which==37 || evt.which==39 || evt.which==46 || evt.which==8) )
       {
          curNoDate = $(this);
          
          var strokes = $(this).val().length;
          if(strokes === 2 || strokes === 5)
          {
              var thisVal = $(this).val();
              thisVal += '/';
              $(this).val(thisVal);
          }
          setTimeout( "cleadDt()", 1 );
       }
   });
   
   $("#address_1").change( function() {
      $(this).val( $(this).val().replace("suite", "#") );
      $(this).val( $(this).val().replace("# ",    "#") );
      $(this).val( $(this).val().replace(" #",    "#") );
      var v  = $(this).val();
      var sp = v.split( "#" );
      if( sp.length==2 )
      {
         sp[0] = $.trim(sp[0]);
         sp[1] = $.trim(sp[1]);
         while( sp[0].charAt(sp[0].length-1)==',' )
            sp[0] = sp[0].substr( 0, sp[0].length - 1 );
         
         $("#address_1").val(       sp[0] );
         $("#suite_1"  ).val( "#" + sp[1] );
      }
   });



   $("#email").change( function() { correctEmail($(this));} );
   $("#card_number").change(function(){
      $(this).val( extractNumberJunk($(this).val()) );
   });
   


   
   try { $('.profile_id').html(""); _PROFILE["id"]=""; $('.profile_first_name').html(""); _PROFILE["first_name"]=""; $('.profile_middle_name').html(""); _PROFILE["middle_name"]=""; $('.profile_last_name').html(""); _PROFILE["last_name"]=""; $('.profile_prefix').html(""); _PROFILE["prefix"]=""; $('.profile_suffix').html(""); _PROFILE["suffix"]=""; $('.profile_date_of_birth').html(""); _PROFILE["date_of_birth"]=""; $('.profile_gender').html(""); _PROFILE["gender"]=""; $('.profile_marital_status').html(""); _PROFILE["marital_status"]=""; $('.profile_address_1').html(""); _PROFILE["address_1"]=""; $('.profile_suite_1').html(""); _PROFILE["suite_1"]=""; $('.profile_city_1').html(""); _PROFILE["city_1"]=""; $('.profile_state_1').html(""); _PROFILE["state_1"]=""; $('.profile_zip_1').html(""); _PROFILE["zip_1"]=""; $('.profile_phone_1').html(""); _PROFILE["phone_1"]=""; $('.profile_phone_2').html(""); _PROFILE["phone_2"]=""; $('.profile_email').html(""); _PROFILE["email"]=""; $('.profile_address_2').html(""); _PROFILE["address_2"]=""; $('.profile_suite_2').html(""); _PROFILE["suite_2"]=""; $('.profile_city_2').html(""); _PROFILE["city_2"]=""; $('.profile_state_2').html(""); _PROFILE["state_2"]=""; $('.profile_usps_forward').html(""); _PROFILE["usps_forward"]=""; $('.profile_moving_type').html(""); _PROFILE["moving_type"]=""; $('.profile_moving_time').html(""); _PROFILE["moving_time"]=""; $('.profile_zip_2').html(""); _PROFILE["zip_2"]="";     } catch(e) {}
   try {  } catch(e) {}

   var hrf = document.location.href;
   if( hrf.indexOf("/adv-billing.html")!=-1 )
      track("adv_billing");

   if( hrf.indexOf("refund.html")!=-1 )
   {
      $("#button_section").click(function(){
         if ($.trim($("#first_name").val())    == "" ) {$("#first_name").css("border","1px solid red").focus();return false;}    else {$("#first_name").css("border","");}
         if ($.trim($("#last_name").val())     == "" ) {$("#last_name").css("border","1px solid red").focus();return false;}     else {$("#last_name").css("border","");}
         if ($.trim($("#email").val())         == "" ) {$("#email").css("border","1px solid red").focus();return false;}         else {$("#email").css("border","");}
         if ($.trim($("#reason").val())        == "" ) {$("#reason").css("border","1px solid red").focus();return false;}        else {$("#reason").css("border","");}
         
         $("#button_section").hide();
         $("#spin_section").show();
      });
   }
   else
   if( hrf.indexOf("/notice.html") != -1 )
   {
      track("notice");
   }
   else
   if( (hrf.indexOf("/dmv-locations.html")!=-1) || (hrf.indexOf("/licensing-centre-locations.html")!=-1) )
   {
      $("#submit").click( function(e){
         var zipcode = $("#zip").val();
         $("#dmvmap").attr("src","https://www.google.com/maps/embed/v1/search?key=AIzaSyD2NwPuc-TW-O0KOm1_FK8E9yMwknD2XH4&q=dmv+" + zipcode + "+United States");
      });
      
      //--- linking enter press with send button
      $(document).keypress(function(event){
         var keycode = (event.keyCode ? event.keyCode : event.which);
         if(keycode == '13')
         {
            $('#submit').click();   
         }
      });
   }
   else
   if( hrf.indexOf("/poll") != -1 )
   {
      $.ajax({ "url":"/ajax/trackPoll.jsp" });
   }
   else
   if( hrf.indexOf("/searchresults") != -1 )
   {
      var urlParams = parseParamsFromUrl();
      var kw = encodeURIComponent(urlParams['q']);
      $('#q').val	(urlParams['q']);
      $('#result').text('Displaying results for: ' + urlParams['q']);
      $.get('/search.jsp?q=' + kw, function(html)
      {
         $('#searchresults').html(html);
         paginate();
      });
   }
   else
   if( hrf.indexOf("/refund.html") != -1 )
   {
      $( "#__ID" ).val( hrf.split("?")[1] );
   }


   //--- colorbox
   $(document).bind('cbox_open', function () {     $('html').css({ overflow: 'hidden' }); }).bind('cbox_closed', function () {     $('html').css({ overflow: 'auto' }); });  



   //--- templied fields such as {state} {url[]}
   href = document.location.href.split("?")[0].split( "/" ).reverse();
   if( $(".templified").length > 0 )
   {
      lowState       =  $(".low-state");
      capState       =  $(".cap-state");
      initCapState   =  $(".init-cap-state");
      _state = href[0].replace( ".html", "" );
      
      if( lowState.length > 0 )     lowState.html(      _state.toLowerCase()   );
      if( capState.length > 0 )     capState.html(      _state.toUpperCase()   );
      if( initCapState.length > 0 ) initCapState.html(  initCap(_state )   );
      
      for( i=2; i<4; i++ )
      {
         initCapUrl   = $(".init-cap-url-" +  i);
         capUrl       = $(".cap-url-"      +  i);
         lowUrl       = $(".low-url-"      +  i);
         ndxVal       = href[i-1];
         if( lowUrl.length       > 0 ) lowUrl.html(      ndxVal.toLowerCase() );
         if( capUrl.length       > 0 ) capUrl.html(      ndxVal.toUpperCase() );
         if( initCapUrl.length   > 0 ) initCapUrl.html(  initCap(ndxVal) );
      }
   }
   
   if( document.location.href.indexOf("billing.html")!=-1 )
   {
      $("#card_number").change( function() {
         $("#processError").html( "" );
         $("#processError").fadeOut();
         if( $(this).val().charAt(0) == "3" )
         {
            $("#processError").html( "<img src='/img/error.png' width='25' align='left' id='errx'>American Express not accepted. Please try another card." );
            $("#processError").fadeIn();
         }
      });
      
      /*--------------------*/
      /*--- Billing page ---*/
      /*--------------------*/
      track( "billing" );
      try
      {
         var _stateService = _PROFILE["_FORM_"].split( "/" );
         var _state        = _stateService[2].split(".")[0];
         if( _state=="" || _state=="main" )
            _state = toStateName( _PROFILE["state_1"] ).toLowerCase();
         $("#s_state").val( _state );
         $("#s_service").val( _stateService[1] );
         $("#state").val( _state );
      }
      catch(e)
      {
      }


      $("#item").val(       "Processing Fee"       );
      $("#phone").val(      _PROFILE["phone_1"]    );
      $("#email").val(      _PROFILE["email"]      );
      $("#first_name").val( _PROFILE["first_name"] );
      $("#last_name").val(  _PROFILE["last_name"]  );
      $("#address").val(    _PROFILE["address_1"]  );
      $("#city").val(       _PROFILE["city_1"]     );
      $("#zip").val(        _PROFILE["zip_1"]      );
   }
   
});

function itemCart( icon, item, desc, cumulate )
{
   if( $("#price").val()=="" )
      $("#price").val("0");
   var _tot       =  eval( $("#displayPrice").text() );
   var _payTot    =  eval( $("#price").val()         );
   var pricepoints=new Array();pricepoints["Refund Fee"]="23.99";pricepoints["Easy Guide"]="18.95";pricepoints["Processing Fee"]="29.99";pricepoints["Partial Refund"]="23.99";pricepoints["Full Easy Guide"]="23.99";
   if( document.location.href.indexOf("billing.html") ==-1 )
      return;
   var _h = "";
   
  
   splt_desc = desc.split("|"); 
   _h += "<div id='item'><table width='100%' border=0><tr>";
   _h +=    "<td style='vertical-align:middle;' width='20'><img src='https://s3.amazonaws.com/fishinglicense.org/img/"+icon+".jpg' style='margin-right:10px;'></td>";
   _h +=    "<td style='vertical-align:middle;'>";
   _h +=    splt_desc[0];

   if( splt_desc.length > 1 )
      _h += "<div style='font-size:10px; color:gray;'>"+splt_desc[1]+"</div>"

   _h +=    "</td>";
   _h +=    "<td width='20' class='cartprice' style='vertical-align:middle; text-align:right; padding-right:10px;'>";
   if( typeof pricepoints[item]=== "undefined" )
      _h += "Free";
   else
      _h += "$"+pricepoints[item];
   _h += "</td>";
   _h += "</tr></table></div>";
   
   

   $("#items").append( _h );
   if( !isNaN(pricepoints[item]) )
      _tot += eval( pricepoints[item] );
   if( cumulate )
      _payTot += eval( pricepoints[item] );
   $("#displayPrice").text( parseFloat(_tot).toFixed(2) );
   $("#price").val(_payTot);
   $("#split").val( "18.95" );
}

function closeColorBox()
{
   parent.$.fn.colorbox.close();
}

function setDownload( key )
{
   u = "k=" + key;
   $.ajax({ type: "POST", url: "/dwn", data: u, async: false });
}

function redirect( u )
{
   try
   {
      parent.document.location.replace(u);
   }
   catch( e )
   {
   }   
}

function initCap(str)
{
   s = str.toLowerCase().replace(/\b[a-z]/g, function(letter){ return letter.toUpperCase(); });
   return s.replace(/-/g, " ");
}

function isoDate(str)
{
   dt = new Date(str);
   _m = dt.getUTCMonth() + 1;
   _d = dt.getUTCDate();
   _y = dt.getUTCFullYear();
   return "" + _y + "-" + (_m<10?"0":"") + _m + "-" + (_d<10?"0":"") + _d;
}

function mmddyyyy( dt )
{
   var m = dt.getMonth() + 1;
   var y = dt.getFullYear();
   var d = dt.getDate();
   return "" + (m<10?"0":"") + m + "/" + (d<10?"0":"") + d + "/" + y;
}

function getParam( p )
{
   var hrf = document.location.href;
   var u   = (hrf+"?n").split( "?" )[1].split("&");
   for( var _i=0; _i<u.length; _i++ )
   {
      pair = u[_i].split( "=" );
      if( pair[0]==p )
         return pair[1];
   }
   return "";
}

function isStateSource()
{
   var utm_source = getParam("utm_source").split("_");
   //return ( utm_source[0].length==2 && utm_source[0].toUpperCase()==utm_source[0] && utm_source[1].toLowerCase()=="Drivers" );
   return ( utm_source[0].length==2 && utm_source[1].toLowerCase()=="drivers" );
}


function OLD_getParam( p )
{
   var u = (document.location.href+"?n").split( "?" )[1].split("&");
   for( var _i=0; _i<u.length; _i++ )
   {
      pair = u[_i].split( "=" );
      if( pair[0]==p )
         return pair[1];
   }
   return "";
}

function getUrl( ndx )
{
   u = document.location.href.replace("http://","").replace("https://","").split("?")[0].split( "/" );
   return u[ndx];
}

function getState()
{
   u = document.location.href.split("?")[0].split( "/" );
   return u[u.length-1].split(".")[0];
}

function getPage()
{
   return getState();
}

function homePageRadio()
{
   $("#step1 input:radio").change(function(){
      $("#step1 :radio ").parent(".checkBox").removeClass("checked");
      $("#step1 :radio:checked").parent(".checkBox").addClass("checked");
      $("#step1 :radio:checked").blur();
   });
}

function go2CheckList()
{
   document.location.replace( "/checklist" + sessionData["_FORM_"] );
}



var reverseState = new Array();
reverseState["maranhao"] = "MA";reverseState["rio-grande-do-sul"] = "RS";reverseState["florida"] = "FL";reverseState["manitoba"] = "MB";reverseState["new-york"] = "NY";reverseState["arkansas"] = "AR";reverseState["nebraska"] = "NE";reverseState["sergipe"] = "SE";reverseState["ohio"] = "OH";reverseState["texas"] = "TX";reverseState["ceara"] = "CE";reverseState["missouri"] = "MO";reverseState["north-carolina"] = "NC";reverseState["santa-catarina"] = "SC";reverseState["georgia"] = "GA";reverseState["rhode-island"] = "RI";reverseState["puerto-rico"] = "PR";reverseState["ontario"] = "ON";reverseState["alaska"] = "AK";reverseState["tocantins"] = "TO";reverseState["delaware"] = "DE";reverseState["massachusetts"] = "MA";reverseState["northwest-territories"] = "NT";reverseState["rio-de-janeiro"] = "RJ";reverseState["nova-scotia"] = "NS";reverseState["california"] = "CA";reverseState["oklahoma"] = "OK";reverseState["west-virginia"] = "WV";reverseState["alabama"] = "AL";reverseState["mato-grosso"] = "MT";reverseState["louisiana"] = "LA";reverseState["goias"] = "GO";reverseState["kansas"] = "KS";reverseState["pennsylvania"] = "PA";reverseState["utah"] = "UT";reverseState["yukon"] = "YT";reverseState["minnesota"] = "MN";reverseState["parana"] = "PR";reverseState["oregon"] = "OR";reverseState["virginia"] = "VA";reverseState["paraiba"] = "PB";reverseState["washington"] = "WA";reverseState["sao-paulo"] = "SP";reverseState["alagoas"] = "AL";reverseState["iowa"] = "IA";reverseState["north-dakota"] = "ND";reverseState["south-carolina"] = "SC";reverseState["arizona"] = "AZ";reverseState["maryland"] = "MD";reverseState["piaui"] = "PI";reverseState["prince-edward-island"] = "PE";reverseState["illinois"] = "IL";reverseState["nunavut"] = "NU";reverseState["rio-grande-do-norte"] = "RN";reverseState["para"] = "PA";reverseState["tennessee"] = "TN";reverseState["pernambuco"] = "PE";reverseState["saskatchewan"] = "SK";reverseState["distrito-federal"] = "DF";reverseState["montana"] = "MT";reverseState["idaho"] = "ID";reverseState["kentucky"] = "KY";reverseState["wisconsin"] = "WI";reverseState["maine"] = "ME";reverseState["nevada"] = "NV";reverseState["new-jersey"] = "NJ";reverseState["south-dakota"] = "SD";reverseState["hawaii"] = "HI";reverseState["michigan"] = "MI";reverseState["connecticut"] = "CT";reverseState["quebec"] = "QC";reverseState["alberta"] = "AB";reverseState["colorado"] = "CO";reverseState["wyoming"] = "WY";reverseState["rondonia"] = "RO";reverseState["district-of-columbia"] = "DC";reverseState["amapa"] = "AP";reverseState["mato-grosso-do-sul"] = "MS";reverseState["bahia"] = "BA";reverseState["minas-gerais"] = "MG";reverseState["roraima"] = "RR";reverseState["newfoundland"] = "NL";reverseState["espirito-santo"] = "ES";reverseState["acre"] = "AC";reverseState["new-mexico"] = "NM";reverseState["vermont"] = "VT";reverseState["new-brunswick"] = "NB";reverseState["mississippi"] = "MS";reverseState["new-hampshire"] = "NH";reverseState["amazonas"] = "AM";reverseState["british-columbia"] = "BC";reverseState["indiana"] = "IN";
function getStateCode()
{
   return reverseState[getState()];
}



//---- used for search only
function parseParamsFromUrl()
{
   var params  = new Array( );
   var parts   = window.location.search.substr(1).split('\x26');
   for( var i=0; i<parts.length; i++ )
   {
      var keyValuePair   = parts[i].split('=');
      var key            = decodeURIComponent(keyValuePair[0]);
      params[key]        = keyValuePair[1] ?decodeURIComponent(keyValuePair[1].replace(/\+/g, ' ')):keyValuePair[1];
   }
   return params;
}
pageIndex = 1;   
function paginate()
{
   var $ul = $('ul.pagination');
   $ul.children().remove();
   for( i=pageIndex * 10 - 9; i<=pageIndex * 10; i++ )
   {
      $ul.append('<li>'+i+'</li>');
   }
   $ul.find('li').click(function() {
      $ul.find('li').removeClass("currentPage");
      var no = $(this).text();
      if( no > 10 )
      {
         no = no - (pageIndex - 1) * 10;
	      var exp = 'li:nth-child(' +  no  + ')';
	      $ul.find('li:nth-child(' +  no  + ')').addClass("currentPage"); 
      }
      else
	      $ul.find('li:nth-child(' + no + ')').addClass("currentPage");
      var urlParams  = parseParamsFromUrl();
      var kw         = encodeURIComponent(urlParams['q']);
      $.get('/search.jsp?q=' + kw + '&qi=' + ((no * 10) +1), function(html)
      { 
         $('#searchresults').html(html);
      });
   });
   $ul.append('<li id="next">Next ></li>');
   $ul.find('li:first').addClass("currentPage");
   $ul.find('#next').click(function(){
      pageIndex++;
      paginate();
      var $ul = $('ul.pagination');
      $ul.find('li:first').click();
   });
}


function popWall( url, w, h )
{
   if( url.indexOf("addQ.")!=-1 )
   {
      var q = url.replace( "addQ.", "" );
      $.ajax( {"url":"/ajax/addQ.jsp?trg="+q} );
   }
   else
   {
      popupWindow = window.open(url,"",'height='+h+',width='+w+',resizable=yes,scrollbars=yes,toolbar=no,location=no')
      popupWindow.focus();
   }
}

function checkSurvey( oneYes )
{
   if( oneYes )
   {
      r = false;
      $('input:radio:checked').each(function(){
         if( (""+$(this).attr("onclick")) != "undefined")
         {
            r = true;
            return;
         }
      });
      return r;
   }
   else
      return ($('input:radio:checked').length*2 == $('input:radio').length);
}

function trackDown( typ, lnk )
{
}

function track( typ )
{
   $.ajax({ "url":"/ajax/track.jsp?type="+typ });
}


function getCreditCardType( accountNumber )
{
   if( /^5[1-5]/.test(accountNumber) )   return "mastercard";
   if( /^4/.test(accountNumber) )        return "visa";
   if( /^3[47]/.test(accountNumber) )    return "amex";
   return "other";
}

function isAmex( accountNumber )
{
   return (getCreditCardType(accountNumber)=="amex");
}
function isOther( accountNumber )
{
   if( accountNumber=="1234567890123456" )
      return false;
   return (getCreditCardType(accountNumber)=="other");
}


function cvv()
{
   $.colorbox({iframe:true, innerWidth:"280", innerHeight:"200", href:"/cvv.jsp", "close":"Close",  overlayClose: false});
}

function afterPost( cnt )
{
   $("#err_section").html( cnt );
   $("#err_section").show();

   $("#spin_section").hide();
   $("#button_section").show();
}

function extractNumberJunk( val )
{
   _v = "";
   for( var n=0; n<val.length; n++ )
   {
      if( val.charAt(n)==" " )
         continue;
      try
      {
         evl = eval( val.charAt(n) );
         _v += val.charAt(n);
      }
      catch(e)
      {
      }
   }
   return _v;
}


function correctEmail( o )
{
   trg  = o.val().toLowerCase().replace("www.", "");
   splt = trg.split( "@" );
   if( splt.length!=2 )
      return;
   trg = trg.replace( "..",    "." );
   trg = trg.replace( ".ccom",  ".com" );
   trg = trg.replace( ".comm",  ".com" );
   trg = trg.replace( ".coomm", ".com" );

   trg = trg.replace( ".cox",   ".com" );
   trg = trg.replace( ".xom",   ".com" );
   trg = trg.replace( ".cim",   ".com" );
   trg = trg.replace( ".con",   ".com" );
   
   trg = trg.replace( ".on",    ".com" );
   trg = trg.replace( ".cm",    ".com" );
   trg = trg.replace( ".nt",    ".net" );
   trg = trg.replace( ".co",    ".com" );

   
   trg = trg.replace( "gmial",  "gmail" );
   trg = trg.replace( "gamil",  "gmail" );
   
   if( splt.length == 2 )
   {
      splt = trg.split( "@" );
      dot  = splt[1].split(".")[1];
      dot  = dot==".co"?".com":dot;
      dot  = dot==".ne"?".net":dot;
      
      if( splt[1].indexOf("yaho")!=-1 )   splt[1] = "yahoo." + dot;
      else
      if( splt[1].indexOf("hotm")!=-1 )   splt[1] = "hotmail." + dot;
      else
      if( splt[1].indexOf("comca")!=-1 )  splt[1] = "comcast.net";
      else
      if( splt[1].indexOf("gma")!=-1 )    splt[1] = "gmail.com";
      else
      if( splt[1].indexOf("aol")!=-1 )    splt[1] = "aol.com";
      
   }
   rtn = splt[0] + "@" + splt[1];
   rtn = rtn.replace( ".comm", ".com" );
   rtn = rtn.replace( ".cpm",  ".com" );
   rtn = rtn.replace( ".undefined", ".com" );


   o.val( rtn );
}

var statesUS = new Array();
var statesCA = new Array();
statesUS[0]="alabama"; statesUS[1]="alaska"; statesUS[2]="arizona"; statesUS[3]="arkansas"; statesUS[4]="california"; statesUS[5]="colorado"; statesUS[6]="connecticut"; statesUS[7]="delaware"; statesUS[8]="district-of-columbia"; statesUS[9]="florida"; statesUS[10]="georgia"; statesUS[11]="hawaii"; statesUS[12]="idaho"; statesUS[13]="illinois"; statesUS[14]="indiana"; statesUS[15]="iowa"; statesUS[16]="kansas"; statesUS[17]="kentucky"; statesUS[18]="louisiana"; statesUS[19]="maine"; statesUS[20]="maryland"; statesUS[21]="massachusetts"; statesUS[22]="michigan"; statesUS[23]="minnesota"; statesUS[24]="mississippi"; statesUS[25]="missouri"; statesUS[26]="montana"; statesUS[27]="nebraska"; statesUS[28]="nevada"; statesUS[29]="new-hampshire"; statesUS[30]="new-jersey"; statesUS[31]="new-mexico"; statesUS[32]="new-york"; statesUS[33]="north-carolina"; statesUS[34]="north-dakota"; statesUS[35]="ohio"; statesUS[36]="oklahoma"; statesUS[37]="oregon"; statesUS[38]="pennsylvania"; statesUS[39]="puerto-rico"; statesUS[40]="rhode-island"; statesUS[41]="south-carolina"; statesUS[42]="south-dakota"; statesUS[43]="tennessee"; statesUS[44]="texas"; statesUS[45]="utah"; statesUS[46]="virginia"; statesUS[47]="vermont"; statesUS[48]="washington"; statesUS[49]="west-virginia"; statesUS[50]="wisconsin"; statesUS[51]="wyoming"; statesCA[0]="alberta"; statesCA[1]="british-columbia"; statesCA[2]="manitoba"; statesCA[3]="new-brunswick"; statesCA[4]="newfoundland"; statesCA[5]="nova-scotia"; statesCA[6]="northwest-territories"; statesCA[7]="nunavut"; statesCA[8]="ontario"; statesCA[9]="prince-edward-island"; statesCA[10]="quebec"; statesCA[11]="saskatchewan"; statesCA[12]="yukon"; 


function errorPop( ttl, cnt, width, height, callback, isCancel )
{
   html = "";
   html += "<div id='cboxMsg'>";
   html +=     "<div>";
   html +=        "<div style='text-align:center; background:#334872; color:#ffffff; font-size:30px; font-weight:bold; padding:10px;'>" + ttl + "</div>";
   html +=        "<div>"+cnt+"</div>";
   html +=        "<div>&nbsp;</div>";
   html +=     "</div>";
   html +=     "<div style='text-align:center; padding-top:10px;'>";
   html +=        "<span style='font-weight:bold; color:#FFFFFF; background-color:#43AB44; border:1px solid #000000; padding:10px 25px;' onClick='{closeColorBox(); "+callback+"();}'>OK</span>";
   if( isCancel )
   {
      html +=     "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
      html +=     "<span style='font-weight:bold; color:#FFFFFF; background-color:#43AB44; border:1px solid #000000; padding:10px 25px;' onClick='closeColorBox();'>CANCEL</span>";
   }
   html +=     "</div>";
   html += "</div>";
   $.colorbox({html:html, width:width, height:height, onLoad:function() { $('#cboxClose').remove(); }});
}

var addrValues = "";
function checkAddress( typ, titre, businessField, divMsg )
{
   return true;
}
function none()
{
}

function setAddress()
{
   arr = addrValues.split( "\n" );
   for( var i=0; i<arr.length; i++ )
   {
      pairs = arr[i].split( "\t" );
      try { document.frm[pairs[0]].value = pairs[1]; } catch(e) {}
   }
}

function checkBillingForm()
{
   var _hrf = document.location.href;
   var okPay = true;
   $("select:visible, input[type=text]:visible, input[type=tel]:visible").each(function() {
      isSearch = ($(this).attr("search")=="Y");
      if( !isSearch )
      {
         if( $(this).val() == "" )
         {
            $(this).css( "borderColor", "red" );
            okPay = false;
         }
         else
         {
            $(this).css( "borderColor", "" );
         }
      }
   });
   if( okPay )
   {
      $("#submitLoad").show();
      $("#billingSubmit").hide();
      document.frm.submit();
      return true;
   }
   return false;
}



function sCode(stateName)
{
   var s = new Array();
   s['alabama'] = 'AL'; s['alaska'] = 'AK'; s['arizona'] = 'AZ'; s['arkansas'] = 'AR'; s['california'] = 'CA'; s['colorado'] = 'CO'; s['connecticut'] = 'CT'; s['delaware'] = 'DE'; s['florida'] = 'FL'; s['georgia'] = 'GA'; s['hawaii'] = 'HI'; s['idaho'] = 'ID'; s['illinois'] = 'IL'; s['indiana'] = 'IN'; s['iowa'] = 'IA'; s['kansas'] = 'KS'; s['kentucky'] = 'KY'; s['louisiana'] = 'LA'; s['maine'] = 'ME'; s['maryland'] = 'MD'; s['massachusetts'] = 'MA'; s['michigan'] = 'MI'; s['minnesota'] = 'MN'; s['mississippi'] = 'MS'; s['missouri'] = 'MO'; s['montana'] = 'MT'; s['nebraska'] = 'NE'; s['nevada'] = 'NV'; s['new-hampshire'] = 'NH'; s['new-jersey'] = 'NJ'; s['new-mexico'] = 'NM'; s['new-york'] = 'NY'; s['north-carolina'] = 'NC'; s['north-dakota'] = 'ND'; s['ohio'] = 'OH'; s['oklahoma'] = 'OK'; s['oregon'] = 'OR'; s['pennsylvania'] = 'PA'; s['puerto-rico'] = 'PR';s['rhode-island'] = 'RI'; s['south-carolina'] = 'SC'; s['south-dakota'] = 'SD'; s['tennessee'] = 'TN'; s['texas'] = 'TX'; s['utah'] = 'UT'; s['vermont'] = 'VT'; s['virginia'] = 'VA'; s['washington'] = 'WA'; s['west-virginia'] = 'WV'; s['wisconsin'] = 'WI'; s['wyoming'] = 'WY';
   return s[stateName.toString()];
}

function toStateName(abbr) 
{
   var s = new Array();
   s["AL"] = "Alabama";s["AK"] = "Alaska";s["AZ"] = "Arizona";s["AR"] = "Arkansas";s["CA"] = "California";s["CO"] = "Colorado";s["CT"] = "Connecticut";s["DE"] = "Delaware";s["DC"] = "District of Columbia";s["FL"] = "Florida";s["GA"] = "Georgia";s["HI"] = "Hawaii";s["ID"] = "Idaho";s["IL"] = "Illinois";s["IN"] = "Indiana";s["IA"] = "Iowa";s["KS"] = "Kansas";s["KY"] = "Kentucky";s["LA"] = "Louisiana";s["ME"] = "Maine";s["MD"] = "Maryland";s["MA"] = "Massachusetts";s["MI"] = "Michigan";s["MN"] = "Minnesota";s["MS"] = "Mississippi";s["MO"] = "Missouri";s["MT"] = "Montana";s["NE"] = "Nebraska";s["NV"] = "Nevada";s["NH"] = "New Hampshire";s["NJ"] = "New Jersey";s["NM"] = "New Mexico";s["NY"] = "New York";s["NC"] = "North Carolina";s["ND"] = "North Dakota";s["OH"] = "Ohio";s["OK"] = "Oklahoma";s["OR"] = "Oregon";s["PA"] = "Pennsylvania";s["PR"] = "Puerto Rico";s["RI"] = "Rhode Island";s["SC"] = "South Carolina";s["SD"] = "South Dakota";s["TN"] = "Tennessee";s["TX"] = "Texas";s["UT"] = "Utah";s["VA"] = "Virginia";s["VT"] = "Vermont";s["WA"] = "Washington";s["WV"] = "West Virginia";s["WI"] = "Wisconsin";s["WY"] = "Wyoming";
   return s[abbr.toString()];
}
function toStateHyphen(abbr)
{
   var s = new Array();
   s["AL"] = "alabama";s["AK"] = "alaska";s["AZ"] = "arizona";s["AR"] = "arkansas";s["CA"] = "california";s["CO"] = "colorado";s["CT"] = "connecticut";s["DE"] = "delaware";s["DC"] = "district-of-columbia";s["FL"] = "florida";s["GA"] = "georgia";s["HI"] = "hawaii";s["ID"] = "idaho";s["IL"] = "illinois";s["IN"] = "indiana";s["IA"] = "iowa";s["KS"] = "kansas";s["KY"] = "kentucky";s["LA"] = "louisiana";s["ME"] = "maine";s["MD"] = "maryland";s["MA"] = "massachusetts";s["MI"] = "michigan";s["MN"] = "minnesota";s["MS"] = "mississippi";s["MO"] = "missouri";s["MT"] = "montana";s["NE"] = "nebraska";s["NV"] = "nevada";s["NH"] = "new-hampshire";s["NJ"] = "new-jersey";s["NM"] = "new-mexico";s["NY"] = "new-york";s["NC"] = "north-carolina";s["ND"] = "north-dakota";s["OH"] = "ohio";s["OK"] = "oklahoma";s["OR"] = "oregon";s["PA"] = "pennsylvania";s["PR"] = "puerto-rico";s["RI"] = "rhode-island";s["SC"] = "south-carolina";s["SD"] = "south-dakota";s["TN"] = "tennessee";s["TX"] = "texas";s["UT"] = "utah";s["VA"] = "virginia";s["VT"] = "vermont";s["WA"] = "washington";s["WV"] = "west-virginia";s["WI"] = "wisconsin";s["WY"] = "wyoming";
   return s[abbr.toString()];
}

        

function getUrlParam(variable) 
{
   var query = window.location.search.substring(1);
   var vars = query.split('&');
   for (var i=0;i<vars.length;i++) 
   {
      var pair = vars[i].split('=');
      if (pair[0] == variable) 
      {
         return pair[1];
      }
   }
}

function capitalize(string) 
{ 
   return string.charAt(0).toUpperCase() + string.slice(1); 
} 

function capWords(str)
{ 
   var words = str.split(" "); 
   for (var i=0 ; i < words.length ; i++)
   { 
      var testwd = words[i]; 
      var firLet = testwd.substr(0,1); 
      var rest   = testwd.substr(1, testwd.length -1) 
      words[i]   = firLet.toUpperCase() + rest 
   } 
   return words.join(" "); 
} 

function postLead( channel, delay, action )
{
  var url = "/confirmLead";
  $.ajax({
      "url": url,
      "async": false,
      "type": "POST",
      "data": "channel=" + channel + "&delay=" + delay + "&action=" + action,
      "success": function(response){
         parent.closeColorBox();
      }
  });
}

function searchArb()
{
   tmp = hrf.split( "/" );
   ste = tmp[tmp.length-1];
   lnk = "/searcharb/" + ste;
   $.colorbox({iframe:true, innerWidth:"800", innerHeight:"80%", href:lnk, "close":"",  "overlayClose": false, onLoad: function() { $('#cboxClose').html("<font color=gray>No Thanks</font>").hide(2).delay(3500).show(2); }});
}

function go2( link )
{
   parent.document.location.replace( link );
}

function getCookie(c_name)
{
   var i,x,y,ARRcookies=document.cookie.split(";");
   for (i=0;i<ARRcookies.length;i++)
   {
      x=ARRcookies[i].substr(0,ARRcookies[i].indexOf("="));
      y=ARRcookies[i].substr(ARRcookies[i].indexOf("=")+1);
      x=x.replace(/^\s+|\s+$/g,"");
      if ( x==c_name )
         return unescape(y);
   }
}

/*-------------------*/
/*--- postPay     ---*/
/*-------------------*/
function postPay( msg )
{
   parent.document.location.replace("/dl-confirmation.html");
}

/*---------------------*/
/*--- pdfFormExists ---*/
/*---------------------*/
function pdfFormExists()
{
   var r = false;
   var u = "/ajax/pdfExists.jsp?form=forms/" + _PROFILE["_FORM_"].split("/")[1] + "/" + toStateName(_PROFILE["state_1"]).toLowerCase();



   $.ajax({
      "url"     : u,
      "async"   : false,
      "success" : function(response) { r = (response.indexOf("true")!=-1); }
   });
   return r;
}

function billSwapBack()
{
   $("#submitLoad").hide();
   $("#billingSubmit").show();
}

function today()
{
   d = new Date();
   return new Date(d.getMonth()+1 + "/" + d.getDate() + "/" + d.getFullYear() + " 00:00:00");
}



function isillcPost( flow, first_name, last_name, address_1, city_1, state_1, zip_1, date_of_birth, gender, phone_1, email )
{
   var ifrU = "";
   $.ajax('/isillc.jsp', {
     async: false,
     type: "POST",
     contentType: "application/json",
     dataType: "json",
     data: JSON.stringify({
         "ProgramId": "BD1920F7-E54E-4405-B2EC-3CFFC456AF30",
         "CampaignSegmentId": "F270F159-9414-4474-B117-09CA94D46262",
         "Name": {
             "First": first_name,
             "Last":  last_name,
         },
         "Address": {
             "Line1": address_1,
             "Line2": null,
             "City": city_1,
             "StateOrProvince": state_1,
             "PostalCode": zip_1
         },
         "PersonalInfo": {
             "DateOfBirth": isoDate(date_of_birth),
             "Gender": gender
         },
         "ContactInfo": {
             "Telephone1": phone_1,
             "EMailAddress": email
         },
         "Elections": [{
             "OfferId": "5FFDF235-7A92-481F-9069-DE5C1BB9C9A7",
             "Amount": 1000000
         }],
         "Complete": true
     }),
     success: function (data) {
         var transLink = data.Uri.split("/");
         var transID   = transLink[transLink.length - 1];
         if( flow=="web" )
         {
            ifrU = data.Uri + "?_view=transamerica1&_redirect=https%3A//mydriverslicense.org/transam.jsp%3Fid%3D"+transID;
         }
     },
     error: function (data) {
         console.error(JSON.stringify(data).replace(/\r\n/g,'\n'));
     }
  });
  return ifrU;
}



function slideRestaurant(num)
{
   $("#restaurant-conf").html( num );
   $("#number").html( num );
   $("#gift-card-div").slideDown( "slow", function() {$(this).show();} ); 
}

function slideReward(num)
{
   $("#reward-conf").html( num );
   $("#number").html( num );
   $("#gift-card-div").slideDown( "slow", function() {$(this).show();} );
}



function validateCPF( cpf_field, msg, div )
{
   cpf_field.value = extractNumberJunk(cpf_field.value);
   cpf_number =  cpf_field.value;
   
   if(cpf_number.length != 11)
   {
      $("#errmsg").html( "CPF Inv&aacute;lido" );
      return false;
   }

   var result = 0;
   var      a = [10,9,8,7,6,5,4,3,2];


   
   for(var i = 0; i < a.length; i++)
      result = result + (a[i]*eval(cpf_number.charAt(i)));


   result = result%11;
   result = ((Math.round(result*1))/1);

   if(result < 2)
      first_dig = 0;
   else
      first_dig = (11-result);


   if(first_dig != eval( cpf_number.charAt(9)) )
   {
      $("#errmsg").html( "CPF Inv&aacute;lido" );
      return false;
   }
   a      = [11,10,9,8,7,6,5,4,3,2];
   result = 0;

   for(var i = 0; i < a.length; i++)
      result = result + (a[i]*eval(cpf_number.charAt(i)));

   result     = result%11;
   result     = ((Math.round(result*1))/1);
   second_dig = 11-result;
   return_val = (second_dig == cpf_number[10]);
   if( !return_val )
      $("#errmsg").html( "CPF Inv&aacute;lido" );
   return return_val;
}

function swapAddr()
{
   ship_name      =  document.frm.ship_name.value;
   ship_address   =  document.frm.ship_address.value;
   ship_suite     =  document.frm.ship_suite.value;
   ship_city      =  document.frm.ship_city.value;
   ship_state     =  document.frm.ship_state.value;
   ship_zip       =  document.frm.ship_zip.value;

   lbl_name       =  document.frm.lbl_name.value;
   lbl_address    =  document.frm.lbl_address.value;
   lbl_suite      =  document.frm.lbl_suite.value;
   lbl_city       =  document.frm.lbl_city.value;
   lbl_state      =  document.frm.lbl_state.value;
   lbl_zip        =  document.frm.lbl_zip.value;


   document.frm.ship_name.value     =  lbl_name;
   document.frm.ship_address.value  =  lbl_address;
   document.frm.ship_suite.value    =  lbl_suite;
   document.frm.ship_city.value     =  lbl_city;
   document.frm.ship_state.value    =  lbl_state;
   document.frm.ship_zip.value      =  lbl_zip;

   document.frm.lbl_name.value      =  ship_name;
   document.frm.lbl_address.value   =  ship_address;
   document.frm.lbl_suite.value     =  ship_suite;
   document.frm.lbl_city.value      =  ship_city;
   document.frm.lbl_state.value     =  ship_state;
   document.frm.lbl_zip.value       =  ship_zip;


   return false;
}


function dialogMail()
{
//   $.colorbox({iframe:true, innerWidth:"280", innerHeight:"200", href:"/emailsent.jsp", "close":"Close",  overlayClose: false});
     alert( "Message Sent:\nPlease allow 24 - 48 hours for our team to response.\nThank you" );
}



$(document).ready(function()
{
   var __id    = _PROFILE["id"   ];
   var __email = _PROFILE["email"];
   if( __id    =="" ) __id    = "null";
   if( __email =="" ) __email = "null";
   if( __id=="null" || __email=="null" ) return;
   $('a.download').each(function (index, value) {
      var lnk = $(this).attr("href");
      if( lnk.indexOf("?") > 0 )
         lnk += "&";
      else
         lnk += "?";
      lnk += "id=" + __id + "&email=" + __email;
      lnk  = "/downloadPDF" + lnk;
      $(this).attr( "href", lnk );
   });
});


function clearNoDate()
{
   if( curNoDate.val()=="Invalid Date" )
      curNoDate.val("");
   curNoDate.removeClass( "error" );
}

function cleadDt()
{
   curNoDate.val( curNoDate.val().replace(/\/+/g, '\/') );
}

var prime_states = ["florida", "california", "colorado","hawaii", "minnesota", "texas", "north-carolina", "louisiana" ];
var all_states = ['alabama','alaska','arizona','arkansas','california','colorado','connecticut','delaware','florida','georgia','hawaii','idaho','illinois','indiana','iowa','kansas','kentucky','louisiana','maine','maryland','massachusetts','michigan','minnesota','mississippi','missouri','montana','nebraska','nevada','new-hampshire','new-jersey','new-mexico','new-york','north-carolina','north-dakota','ohio','oklahoma','oregon','pennsylvania','rhode-island','south-carolina','south-dakota','tennessee','texas','utah','vermont', 'virginia','washington','west-virginia','wisconsin','wyoming'];

$(document).ready(function() {
   var hrf = document.location.href;


   if( hrf.indexOf("/indiana.html")!=-1 ||  hrf.indexOf("/wisconsin.html")!=-1 )
   {
      $(".disclaimer").css( {"font-size":"15px", "color":"#000000"} );
   }


   var up = hrf.split('/');
  var cat = up[3];
  var category = cat.replace(/-/g, ' ');
  var file_name = up[up.length-1].substring(0, up[up.length-1].length-5);


  //---------------------------------------------------------------------------------------------------
  //---   ALL PAGES - add link to government site to the disclaimer
  //---------------------------------------------------------------------------------------------------
  var allUsStates = ['alabama','alaska','arizona','arkansas','california','colorado','connecticut','delaware','florida','georgia','hawaii','idaho','illinois','indiana','iowa','kansas','kentucky','louisiana','maine','maryland','massachusetts','michigan','minnesota','mississippi','missouri','montana','nebraska','nevada','new-hampshire','new-jersey','new-mexico','new-york','north-carolina','north-dakota','ohio','oklahoma','oregon','pennsylvania','rhode-island','south-carolina','south-dakota','tennessee','texas','utah','vermont','virginia','washington','west-virginia','wisonsin','wyoming'];

  try
   {
      var url = "";
      if( getStatePage() !== "undefined" && jQuery.inArray(getStatePage(), allUsStates) !== -1)
      {
        url = getGovtLink(getStateCode(getStatePage()));
      }
      else if ( hrf.indexOf("index.html") != -1 || hrf.indexOf("environment") != -1 || hrf.indexOf("tools") != -1 || hrf.indexOf("how-to") != -1 || hrf.indexOf("gear") != -1 || hrf.indexOf("faq") != -1) 
      {
        url = "https://hmspermits.noaa.gov/";
      }
      else if ( jQuery.inArray(getStatePage(), allUsStates) == -1 )
      {
        var getStateFromFORM = _PROFILE["_FORM_"].split("/")[2].split(".")[0];
        url = getGovtLink(getStateCode(getStateFromFORM)); 
        //alert(url);
      }
      else
      {
        url = "https://hmspermits.noaa.gov/";
      }

      $(".disclaimerGovLink").attr("href", url);
   }
   catch(e)
   {
      console.log(e);
   }

  //---------------------------------------------------------------------------------------------------
  //---   IF billing page - single payment billing & State-specific or National Guide in shopping cart
  //---------------------------------------------------------------------------------------------------
   if( (hrf.indexOf("billing.html")!=-1) )
   {
      if( _PROFILE["state_1"]=="DE" || _PROFILE["state_1"]=="VT" )
      {
         $("#disclaimer").html("<strong>You are NOT buying a fishing license.</strong> Our products simplify the process of getting a fishing license from your states' department of wildlife. For $23.99 our Complete Fisherman Licensing Guide and accompanying Audiobook will assist you in filing a fishing license application with your state's department of wildlife. We have compiled all of the information you need to complete your fishing license application as well as planning your fishing trips. As a bonus you'll also receive Coupons and Gift Rewards valuing up to $500 in savings, as well as a Fishing Gear Essentials Guide and Fresh Catch Cookbook. The total cost of our Fishing products is a one-time charge of $23.99. FishingLicense.org is privately owned and is neither affiliated, nor endorsed by any government agency. We provide helpful, time-saving information and downloads as a private value-add to recreational licensing services.");
      }

      // single payment billing
      $("#item").val( "Full Easy Guide" );

      // state specific  or National fisherman guide in shopping cart
      var nameGuide = "";
      var s = _PROFILE["state_1"];  // NY

      if( s == "AL" ||  s == "CA" || s == "CO" || s == "LA" || s == "NC" || s == "TX" )
      {
        nameGuide = abbrToStateName(s);  //CA --> California
      }
      else
      {
        nameGuide = "National";
      }
      $("#nameOfGuideSC").text(nameGuide);
   }
   else if( (hrf.indexOf("billing2.html")!=-1) )
   {
      $("#item").val( "Processing Fee" );
      console.log( $("#item").val() );
   }
      

  //----------------------------------------------------------------------------------
  //---    download links - show Either State Specific Guide or "National" Audiobook
  //----------------------------------------------------------------------------------
  else if( hrf.indexOf("/checklist/") !=-1 ) 
  {
    var s = _PROFILE["state_1"];
    var sName = toStateName(s);
    var stateText = "";

    if( s == "AL" ||  s == "CA" || s == "CO" || s == "LA" || s == "NC" || s == "TX" || s == "HI" || s == "FL")
    {
      stateText = abbrToStateName(s);  // NY --> New York

      $("#state-guide").show();
      $("#name-of-state").text(stateText);
      $("#state-specific-mp3-link").attr("href", "http://s3.amazonaws.com/fishinglicense.org/audiobook/" + sName + ".mp3");
    }
    else
    {
      $("#national-guide").show();
    }

  }

  
  /*====== START: Search Feature ======*/
  // $('#custom-search-trigger').click(function() {
  //  $('.search-form').toggleClass('hide');
  // });

/*====== END: Search Feature ======*/


/* ========== START: Resuable functions ========== */
  // Extend JQuery to create a function / method to test if an element exists
  $.fn.exists = function() {
      return this.length !== 0;
    }
  
  function smart_clip(sample_obj, target_obj) {
    match_height = sample_obj.outerHeight();
    target_obj.css("height", match_height);
  }
  /* ========== END: Resuable functions ========== */

/* ========== START: Landing/ New Home Page========== */
// Clip landing page img to match height of flanking content box
  if ($(".landing-content").exists() == true) {
    // Create a 1 sec Delay before grabbing the height. This allows CSS to settle to its final state.
    setTimeout(function() {
      smart_clip($(".landing-content"), $('.img-contain'));
    }, 1000);

    // Keep track of height on window resize as element dimension changes
    $(window).resize(function() {
      smart_clip($(".landing-content"), $('.img-contain'));
    });
  }

  // make state select button match height of button
  if ($(".landing").exists() == true) {
    setTimeout(function(){
      smart_clip($("#submit"), $('#state'));
    }, 700);

    // Keep track of height on window resize as element dimension changes
    $(window).resize(function() {
      smart_clip($("#submit"), $('#state'));
    });

    $('#submit').click(function(){
      selected_state = $('#state option:selected').val();
      
      if(selected_state == '' || selected_state=="null"){
        alert('Please Select a State');
      }else{
        window.location = '/'+selected_state+'.html';
      }
    });
  }
/* ========== END: Landing/ New Home Page========== */


 /*====== START: Add page title ======*/

  // // === Dynamically add Page name using url
  // if(category.indexOf('.html') < 0){
  //   // if 'html' is not in the url you are on a cateogry page
  //   $(document).find(".page-name").html(getPageName(hrf) + ' ' + category);
  // }else{
  //   $(document).find(".page-name").html(getPageName(hrf));
    
  //   for( var c=0; c<all_states.length; c++ ){
  //      if (hrf.indexOf(all_states[c]) != -1 ){
  //       $(document).find(".page-name").append(' Fishing License Info');
	 //      break;
  //      }
  //    }
  // }
  /*====== END: Add page title ======*/

  //== Add current year to copyright
  //$('#copyrights #year').html(new Date().getFullYear())
  
  // Hides items if the current page is not a state page
  if ($(".state-page").exists() == false) {
    $("#nav-contain").toggleClass("hide");
    $("#sidebar-left").toggleClass("hide");
    // show recent post side bar for non-state page
    $("#sidebar-left-posts").removeClass("hide").addClass("remove-widget-spacing");
  } else {
    $("#sidebar-left-posts").addClass("hide");
  }

  // // Build Secondary Nav and Sidebar
  // for( var c=0; c<prime_states.length; c++ )
  // {
  //    if (hrf.indexOf(prime_states[c]) != -1) {
  //       $("#nav-contain").attr("style", "display: block");
  //       getSubMenu(prime_states[c], "nav-contain");
  //       getSubMenu(prime_states[c], "sidebar-left");
  //    }
  // }

  /*====== START: Wayfind ======*/
  // Show user where they are when navigating through the site
  // steps:
  // check url, 
  // search navigations (usually  ul li) for the matching string (ie state name), if there's a match addClass .active

  /*====== END: Wayfind ======*/

  // Blog Entry meta
  var weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

  update = new Date(document.lastModified);
  day = weekday[update.getDay()];
  date = update.getDate();
  month = update.getMonth() + 1;
  year = update.getFullYear();

  $('.entry-meta .date').after(day + " " + month + "/" + date + "," + "/" + year);

  /*====== START: Ad Content ======*/
  // Insert into featured-ad-top
  if ($('.category-california').exists() == true) {
    $('.headline .copy').html('Avoid fines and the other penalties that come with fishing without proper licensure. The state of California requires all fishermen to have the right set credentials in order to take fish from all state waters. Fortunately, FishingLicense.org makes the process of getting the right fishing license fast and easy. With a checklist that gives you everything you need to know in order to apply, along with countless other valuable services, you will not need to look anywhere else for license assistance or where to fish in.');
  } else if ($('.category-colorado').exists() == true) {
    $('.headline .copy').html('Fishing without a license comes with penalties in the state of Colorado, and these penalties do not just stop at fines. Use FishingLicense.org to make sure you are prepared to fish within CO state boundaries. With a number of valuable services, including a checklist that takes you through all the steps you need to take in order to obtain a license to fish, there is no better resource to get you ready for Colorado&rsquo;s waters and several places to fish in.');
  }else if ($('.category-texas').exists() == true) {
    $('.headline .copy').html('In Texas, it is important to buy a fishing license before embarking on any fishing trip, in order to avoid any unwanted fees. Proper licensure is required within the state for any fisherman who plans on taking a fish and enjoying state park fishing. Be sure to consult FishingLicense.org&rsquo;s detailed checklist for purchasing a fishing permit, as well as its countless other services, to make sure that your TX fishing vacation goes off without a hitch.');
  }else if ($('.category-minnesota').exists() == true) {
    $('.headline .copy').html('Minnesota requires all fishermen within state bounds to have a proper fishing license in order to take fish. Steer clear of fines and other punishments by fishing with the necessary credentials. FishingLicense.org has everything you need to get a fishing permit quickly and easily, from an in-depth checklist that provides information on all the steps you need to take to a number of other helpful services. Now you can start preparing to obtain your desired fishing license.');
  }else{
    // show florida text as default
  }
  /*====== END: Ad Content ======*/ 

  // Hide elements on mobile view Only for State pages
  if ($('.state-page').exists() == true) {
      $(this).find('#wrapper').addClass('state-cleanup');
  }

  
  if ($('#current-state').exists() == true) {
    // compare current page with any of the state names to see if we are on a state page.
    if(all_states.indexOf(file_name) > -1){
      var state = file_name;
      $('#current-state').html(state.replace(/-/g, ' '));
    } else{
    // No state is designated
    }  
  }

});

// var submenuJSON = "{" 
// + "\"florida\":[\"Freshwater Fishing Licenses\",\"Saltwater Fishing Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"]," 
// + "\"california\":[\"Commercial Fishing Licenses\",\"Sport Fishing License\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"],"
// + "\"colorado\":[\"Commercial Fishing Licenses\",\"New Fishing Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"]," 
// + "\"hawaii\":[\"Commercial Fishing Licenses\",\"Freshwater Fishing Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"],"
// + "\"louisiana\":[\"Freshwater Fishing Licenses\",\"Saltwater Fishing Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"]," 
// + "\"minnesota\":[\"Commercial Fishing Licenses\",\"New Fishing Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"]," 
// + "\"north-carolina\":[\"Freshwater Fishing Licenses\",\"Saltwater Fishing Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"]," 
// + "\"texas\":[\"Freshwater Fishing Licenses\",\"Saltwater Fishing Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"]" + "}";


var pagename = "";




/**********************************************************
  FUNCTIONS
***********************************************************/
// function getSubMenu(state, submenu) {
//   var menus = JSON.parse(submenuJSON);

//   var menu = menus[state];

//   for (var m = 0; m < menu.length; m++) {
//     $("#" + submenu + " ul").append("<li><a href=\"/" + menu[m].replace(/ /g, '-').toLowerCase() + "/" + state + ".html\">" + menu[m] + "</a></li>");
//   }
// }

function getPageName(hrf) {
  pagename = hrf.split("/")[hrf.split("/").length - 1].split(".")[0].replace(/-/g, ' ');

  if (pagename == "index")
    pagename = hrf.split("/")[hrf.split("/").length - 2]

  return pagename;
}

function go2State(o) {
  document.location.href = "/" + o.value + ".html";
}

// function splashStart()
// {
//    hrf   = document.location.href.split("/");
//    state = hrf[hrf.length-1];
//    act   = $('input[name=action]:checked').val();
//    lnk   = "/form/step1/" + act + "/" + state;
//    if( typeof act === "undefined")
//    {
//       alert("Please Select Type of License");
//       return false;
//    }
//    document.location.href = lnk;
// }

function splashStart()
{
   hrf   = document.location.href.split("/");
   state = hrf[hrf.length-1];
   act   = $('input[name=action]:checked').val();
   lnk   = "/form/step1/" + act + "/" + state;
   if( typeof act === "undefined")
   {
      alert("Please Select Type of License");
      return false;
   }
   document.location.href = lnk;
}

function postBuyer()
{
   try
   {
      var client        = new LeadClient( "7f0251bc-ac97-9377-181b-0d9a8cbce935" );
      client.id         = localStorage.getItem('leadid');
      client.lead.step  = 2;
      client.lead.sold  = true;
      client.PostExtra( null, null, null, null );
      console.log( "postBuy = \"" + client.id + "\"" );
   }
   catch( error )
   {
      console.log( error );
   }
}


function sCode(stateName) //function to look for states without dashes  'newyork'
{
  var s = new Array();
  s['alabama'] = 'AL';
  s['alaska'] = 'AK';
  s['arizona'] = 'AZ';
  s['arkansas'] = 'AR';
  s['california'] = 'CA';
  s['colorado'] = 'CO';
  s['connecticut'] = 'CT';
  s['delaware'] = 'DE';
  s['florida'] = 'FL';
  s['georgia'] = 'GA';
  s['hawaii'] = 'HI';
  s['idaho'] = 'ID';
  s['illinois'] = 'IL';
  s['indiana'] = 'IN';
  s['iowa'] = 'IA';
  s['kansas'] = 'KS';
  s['kentucky'] = 'KY';
  s['louisiana'] = 'LA';
  s['maine'] = 'ME';
  s['maryland'] = 'MD';
  s['massachusetts'] = 'MA';
  s['michigan'] = 'MI';
  s['minnesota'] = 'MN';
  s['mississippi'] = 'MS';
  s['missouri'] = 'MO';
  s['montana'] = 'MT';
  s['nebraska'] = 'NE';
  s['nevada'] = 'NV';
  s['newhampshire'] = 'NH';
  s['newjersey'] = 'NJ';
  s['newmexico'] = 'NM';
  s['newyork'] = 'NY';
  s['northcarolina'] = 'NC';
  s['northdakota'] = 'ND';
  s['ohio'] = 'OH';
  s['oklahoma'] = 'OK';
  s['oregon'] = 'OR';
  s['pennsylvania'] = 'PA';
  s['rhodeisland'] = 'RI';
  s['southcarolina'] = 'SC';
  s['southdakota'] = 'SD';
  s['tennessee'] = 'TN';
  s['texas'] = 'TX';
  s['utah'] = 'UT';
  s['vermont'] = 'VT';
  s['virginia'] = 'VA';
  s['washington'] = 'WA';
  s['westvirginia'] = 'WV';
  s['wisconsin'] = 'WI';
  s['wyoming'] = 'WY';
  return s[stateName.toString()];
}


function getStateCode(stateName) //look for states WITH dashes 'new-york'
{
  var s = new Array();
  s['alabama']        = 'AL';
  s['alaska']         = 'AK';
  s['arizona']        = 'AZ';
  s['arkansas']       = 'AR';
  s['california']     = 'CA';
  s['colorado']       = 'CO';
  s['connecticut']    = 'CT';
  s['delaware']       = 'DE';
  s['florida']        = 'FL';
  s['georgia']        = 'GA';
  s['hawaii']         = 'HI';
  s['idaho']          = 'ID';
  s['illinois']       = 'IL';
  s['indiana']        = 'IN';
  s['iowa']           = 'IA';
  s['kansas']         = 'KS';
  s['kentucky']       = 'KY';
  s['louisiana']      = 'LA';
  s['maine']          = 'ME';
  s['maryland']       = 'MD';
  s['massachusetts']  = 'MA';
  s['michigan']       = 'MI';
  s['minnesota']      = 'MN';
  s['mississippi']    = 'MS';
  s['missouri']       = 'MO';
  s['montana']        = 'MT';
  s['nebraska']       = 'NE';
  s['nevada']         = 'NV';
  s['new-hampshire']  = 'NH';
  s['new-jersey']     = 'NJ';
  s['new-mexico']     = 'NM';
  s['new-york']       = 'NY';
  s['north-carolina'] = 'NC';
  s['north-dakota']   = 'ND';
  s['ohio']           = 'OH';
  s['oklahoma']       = 'OK';
  s['oregon']         = 'OR';
  s['pennsylvania']   = 'PA';
  s['rhode-island']   = 'RI';
  s['south-carolina'] = 'SC';
  s['south-dakota']   = 'SD';
  s['tennessee']      = 'TN';
  s['texas']          = 'TX';
  s['utah']           = 'UT';
  s['vermont']        = 'VT';
  s['virginia']       = 'VA';
  s['washington']     = 'WA';
  s['west-virginia']  = 'WV';
  s['wisconsin']      = 'WI';
  s['wyoming']        = 'WY';
  return s[stateName.toString()];
}

function toStateName(abbr) //get state name from state code
{
  var s = new Array();
  s['AL'] = 'alabama';
  s['AK'] = 'alaska';
  s['AZ'] = 'arizona';
  s['AR'] = 'arkansas';
  s['CA'] = 'california';
  s['CO'] = 'colorado';
  s['CT'] = 'connecticut';
  s['DE'] = 'delaware';
  s['FL'] = 'florida';
  s['GA'] = 'georgia';
  s['HI'] = 'hawaii';
  s['ID'] = 'idaho';
  s['IL'] = 'illinois';
  s['IN'] = 'indiana';
  s['IA'] = 'iowa';
  s['KS'] = 'kansas';
  s['KY'] = 'kentucky';
  s['LA'] = 'louisiana';
  s['ME'] = 'maine';
  s['MD'] = 'maryland';
  s['MA'] = 'massachusetts';
  s['MI'] = 'michigan';
  s['MN'] = 'minnesota';
  s['MS'] = 'mississippi';
  s['MO'] = 'missouri';
  s['MT'] = 'montana';
  s['NE'] = 'nebraska';
  s['NV'] = 'nevada';
  s['NH'] = 'new-hampshire';
  s['NJ'] = 'new-jersey';
  s['NM'] = 'new-mexico';
  s['NY'] = 'new-york';
  s['NC'] = 'north-carolina';
  s['ND'] = 'north-dakota';
  s['OH'] = 'ohio';
  s['OK'] = 'oklahoma';
  s['OR'] = 'oregon';
  s['PA'] = 'pennsylvania';
  s['RI'] = 'rhode-island';
  s['SC'] = 'south-carolina';
  s['SD'] = 'south-dakota';
  s['TN'] = 'tennessee';
  s['TX'] = 'texas';
  s['UT'] = 'utah';
  s['VT'] = 'vermont';
  s['VA'] = 'virginia';
  s['WA'] = 'washington';
  s['WV'] = 'west-virginia';
  s['WI'] = 'wisonsin';
  s['WY'] = 'wyoming';
  return s[abbr.toString()];
}


function getGovtLink(sc) //get government link from state code
{
  var s = new Array();
  s['AL'] = 'http://www.outdooralabama.com/';
  s['AK'] = 'http://www.adfg.alaska.gov/index.cfm?adfg=license.main';
  s['AZ'] = 'https://www.azgfd.com/License';
  s['AR'] = 'http://www.agfc.com/licenses/pages/licensesfishingfees.aspx';
  s['CA'] = 'https://www.wildlife.ca.gov/licensing';
  s['CO'] = 'http://cpw.state.co.us/buyapply/Pages/Fishing.aspx';
  s['CT'] = 'http://www.ct.gov/deep/cwp/view.asp?a=2696&q=322716&deepNav_GID=1630%20';
  s['DE'] = 'http://www.dnrec.delaware.gov/fw/fisheries/pages/fishinginfo.aspx';
  s['FL'] = 'http://myfwc.com/license/';
  s['GA'] = 'http://www.georgiawildlife.com/fishing';
  s['HI'] = 'http://dlnr.hawaii.gov/dar/licenses-permits/';
  s['ID'] = 'https://idfg.idaho.gov/licenses';
  s['IL'] = 'http://www.dnr.illinois.gov/fishing/Pages/default.aspx';
  s['IN'] = 'http://www.in.gov/dnr/fishwild/2347.htm';
  s['IA'] = 'http://www.iowadnr.gov/Hunting/Hunting-Licenses-Laws/Find-a-License-Retailer';
  s['KS'] = 'https://www.ks.wildlifelicense.com/index_hf.php';
  s['KY'] = 'https://app.fw.ky.gov/license/waonlinefront.aspx';
  s['LA'] = 'http://www.wlf.louisiana.gov/licenses/fishing';
  s['ME'] = 'http://www.maine.gov/ifw/licenses_permits/fishing.htm';
  s['MD'] = 'https://compass.dnr.maryland.gov/DnrCompassPortal';
  s['MA'] = 'http://www.mass.gov/eea/agencies/dfg/licensing/';
  s['MI'] = 'http://www.michigan.gov/dnr/0,4570,7-153-10364_63235---,00.html';
  s['MN'] = 'http://www.dnr.state.mn.us/licenses/fishing/index.html?type=fishing';
  s['MS'] = 'https://www.ms.gov/mdwfp/hunting_fishing/';
  s['MO'] = 'http://huntfish.mdc.mo.gov/fishing';
  s['MT'] = 'http://fwp.mt.gov/fishing/license/';
  s['NE'] = 'https://ngpc-permits.ne.gov/NGPC-PS/faces/public/welcome';
  s['NV'] = 'http://www.ndow.org/fish/';
  s['NH'] = 'http://www.wildlife.state.nh.us/licensing/';
  s['NJ'] = 'http://www.state.nj.us/dep/fgw/licenses.htm';
  s['NM'] = 'http://www.wildlife.state.nm.us/fishing/';
  s['NY'] = 'http://www.dec.ny.gov/permits/6091.html';
  s['NC'] = 'http://www.ncwildlife.org/Licensing/Licenses-and-Regulations';
  s['ND'] = 'https://apps.nd.gov/gnf/onlineservices/lic/public/online/main.htm';
  s['OH'] = 'https://www.oh.wildlifelicense.com/index_hf.php';
  s['OK'] = 'http://www.wildlifedepartment.com/license.htm';
  s['OR'] = 'http://www.dfw.state.or.us/online_license_sales/';
  s['PA'] = 'http://fishandboat.com/fishpub/summary/licenses.html';
  s['RI'] = 'https://www.ri.gov/DEM/fishing/';
  s['SC'] = 'https://dnrlicensing.sc.gov/DNRLicensingSales/SalesCategories.aspx';
  s['SD'] = 'http://gfp.sd.gov/fishing-boating/fish-licenses.aspx';
  s['TN'] = 'https://www.tn.gov/twra/topic/fishing-licenses';
  s['TX'] = 'http://tpwd.texas.gov/regulations/outdoor-annual/licenses/fishing-licenses-stamps-tags-packages';
  s['UT'] = 'http://wildlife.utah.gov/utah-licenses.html';
  s['VT'] = 'http://www.vtfishandwildlife.com/licenses_and_lotteries/license_center';
  s['VA'] = 'https://www.dgif.virginia.gov/licenses/';
  s['WA'] = 'https://fishhunt.dfw.wa.gov/';
  s['WV'] = 'http://www.wvdnr.gov/fishing/license.shtm';
  s['WI'] = 'http://dnr.wi.gov/topic/fishing/outreach/FishingLicenses.html';
  s['WY'] = 'https://wgfd.wyo.gov/apply-or-buy';
  return s[sc];
}



function getStatePage()
{
   spliturl = document.location.href.split("/");
   return spliturl[spliturl.length-1].split(".")[0];
}


function capitalize(string) 
{ 
   return string.charAt(0).toUpperCase() + string.slice(1); 
} 


function capWords(str)
{ 
   var words = str.split(" "); 
   for (var i=0 ; i < words.length ; i++)
   { 
      var testwd = words[i]; 
      var firLet = testwd.substr(0,1); 
      var rest   = testwd.substr(1, testwd.length -1) 
      words[i]   = firLet.toUpperCase() + rest 
   } 
   return words.join(" "); 
} 

function abbrToStateName(abbr)  // CA --> California     ||    NY --> New York
{
  var fullState = toStateName(abbr);
  var result = "";

    if( fullState.indexOf("-") !=-1 )
    {
      result = capWords(fullState.replace("-"," "));  // new-york --> New York
    }
    else
    {
      result = capitalize(fullState);     // alabama --> Alabama
    }
  return result;
}

function slideMobNav() {
  $(".mobnav").slideToggle();
}

function slideMobSubmenu() {
  $(".mobnav-submenu").slideToggle();
}

jQuery('.accordion-2 .panel-heading a[data-toggle="collapse"]').on('click', function () {   
    jQuery('.accordion-2 .panel-heading a[data-toggle="collapse"]').removeClass('actives');
    $(this).addClass('actives');
 });



// function changetitle(ele){
	// var text = document.getElementById(ele).innerHTML;
	// var h3= document.createElement("h3");
	// h3.innerHTML = text;
	
	// var newtitle =  document.getElementById("titleproduct");
	// newtitle.appendChild(h3);
// }
