$( document ).ready(function() { 
    

    pageSize = 1;
    pagesCount = $(".content").length;
    var currentPage = 1;
    
    /////////// PREPARE NAV ///////////////
    var nav = '';
    var totalPages = Math.ceil(pagesCount / pageSize);
    for (var s=0; s<totalPages; s++){
        nav += '<li class="numeros"><a href="#">'+(s+1)+'</a></li>';
    }
    $(".pag_prev").after(nav);
    $(".numeros").first().addClass("active");
    //////////////////////////////////////

    showPage = function() {
        $(".content").hide().each(function(n) {
            if (n >= pageSize * (currentPage - 1) && n < pageSize * currentPage){
				$(this).show();
				
				if(n==2){
				$(".pagination li.pag_next a").text("START");
				}
				
				if(n==3){
				$("#pagination").addClass("hidden");
				}
			}
        });
    }
    showPage();

    $(".pagination li.numeros").click(function() {
        $(".pagination li").removeClass("active");
        $(this).addClass("active");
        currentPage = parseInt($(this).text());
        showPage();
    });

    $(".pagination li.pag_prev").click(function() {
		$(".pagination li.pag_next a").css("border-bottom", "none");
		$(".pagination li.pag_prev a").css("border-bottom", "4px solid red");
        if($(this).next().is('.active')) return;
        $('.numeros.active').removeClass('active').prev().addClass('active');
        currentPage = currentPage > 1 ? (currentPage-1) : 1;
		$(".pagination li.pag_next a").text("NEXT");
        showPage();
    });

    $(".pagination li.pag_next").click(function() {
		$(".pagination li.pag_prev a").css("border-bottom", "none");
		$(".pagination li.pag_next a").css("border-bottom", "4px solid red");
        if($(this).prev().is('.active')) return;
        $('.numeros.active').removeClass('active').next().addClass('active');
        currentPage = currentPage < totalPages ? (currentPage+1) : totalPages;
		showPage();
    });
	
	
});


function modalLogin(){
	document.getElementById('id01').style.display='block';
}

function modalJoin(){
	document.getElementById('id02').style.display='block';
}