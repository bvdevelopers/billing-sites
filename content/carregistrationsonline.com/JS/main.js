var hrf = document.location.href;
$(document).ready(function(){

	//Blog alt and title tags
	if(hrf.indexOf("/blog/") != -1) {
		blog_site = window.location.hostname;
		blog_title = $("#blogtitle").text();
		the_title = blog_site + " blog: " + blog_title;
		$("#blogimg").attr("title", the_title);
		$("#blogimg").attr("alt", the_title);
	}
	 
	// check for california to change disclaimers
    if( utm_source == "CA_Drivers" || _PROFILE["state_1"] == "CA" )
    {
     $(".disclaimer").css("position","relative");
     $(".disclaimer").html('This product or service has not been approved or endorsed by any governmental agency, and this offer is not being made by an agency of the government.');
    }

    
   if( hrf.indexOf("/checklist/")!=-1 )
   {
      //--- dynamically change step icons and right info depending on what shows
      if( $('.pdfynY').length == 0 )
      {
         $("#formDiv").hide();
         $("#step1icon").attr("src","http://s3.amazonaws.com/"+domainName+"/img/step1checklist.png").show();
         $("#step3icon").attr("src","http://s3.amazonaws.com/"+domainName+"/img/step2checklist.png").show();
         $("#step4icon").attr("src","http://s3.amazonaws.com/"+domainName+"/img/step3checklist.png").show();
      }
      else
      {
         $("#step1icon").attr("src","http://s3.amazonaws.com/"+domainName+"/img/step1checklist.png").show();
         $("#step2icon").attr("src","http://s3.amazonaws.com/"+domainName+"/img/step2checklist.png").show();
         $("#step3icon").attr("src","http://s3.amazonaws.com/"+domainName+"/img/step3checklist.png").show();
         $("#step4icon").attr("src","http://s3.amazonaws.com/"+domainName+"/img/step4checklist.png").show();
      }
      
      //--- if purchased download, display confirmation
      if( paymentTrans )
         $("#dl-success").show();
      else
         $("#aside").css("margin-top","40px");
      
      // --- make all links under download boxes target _blank
      $("#checklistPar").find("a").each(function() {
         $(this).attr("target","_blank");
      });
      
      // "Click here to finish your application" DIV link open in new window
      if( $('.linksynY').length > 0 )
      {
         var a_href = $(".linksynY").closest('a[href]').attr("href");
         $('#outsideLink').click( function() {
            window.open( a_href );
            return false;
         });
      }
   }
   else
   if( (hrf.indexOf("/easyguide.html")!=-1) )
   {
      if( utm_source == "CA_Drivers" || _PROFILE["state_1"] == "CA" )
      {
         $("#ca-disc").show();
      }
   }
   else
   if( (hrf.indexOf("billing.html")!=-1) )
   {
console.log( "cart1" );
      itemCart( "processing",       "Processing Fee",  "Registration Fee",             true  );
console.log( "cart2" );
      // check for california cookie to change disclaimers
      if( utm_source == "CA_Drivers" || _PROFILE["state_1"] == "CA" )
      {
         $(".disclaimer").css("position","relative");
         $(".disclaimer").html('THIS PRODUCT OR SERVICE HAS NOT BEEN APPROVED OR ENDORSED BY ANY GOVERNMENTAL AGENCY, <br>AND THIS OFFER IS NOT BEING MADE BY AN <a href="http://dmv.ca.gov/" style="color:blue" target="_blank">AGENCY OF THE GOVERNMENT</a>.');
         $("#termsBoxCalifornia").show();
         itemCart( "checklist-thumb",  "Easy Guide",      "License Easy Guide &trade;|State specific guide containing helpful information",   false );
      }
      else
      {
         itemCart( "checklist-thumb",  "Easy Guide",      "License Easy Guide &trade;",   false );
         $("#termsBox").show();
      }
console.log( "cart3" );

      if( pdfFormExists() )
         itemCart( "form-thumb",    "Pre-filled Form", "Pre-filled Form",              false );
       
      
   }
   else
   if( hrf.indexOf("index.html")!=-1 )
   {
      var referrer = document.referrer;
      
      if( referrer.indexOf(domainName) && hrf.indexOf("t=0")==-1 )
      {
		/*Index Modal*/
        $('#myModal').modal({backdrop: 'static', keyboard: false});  
        $('#myModal').modal('show');
      }
      //--- checkbox ---
      $("#step1 input:radio").change(function(){
         $("#step1 :radio ").parent(".checkBox").removeClass("checked");
         $("#step1 :radio:checked").parent(".checkBox").addClass("checked");
         $("#step1 :radio:checked").blur();
      });
      
      //--- home page validation ---
      $("#submit").click(function(e){
         var _state  = $("#state").val();
         var _action = $('input[name=action]:checked', '#first-form').val();
         var _zip    = $.trim($('#zipcode').val());
         if( !_action )
         {
            alert( "Please Select an Action" );
            return false;
         }
         if( (_state=="") && (_zip=="") )
         {
            alert( "Please Select a State" );
            $("#state").focus();
            return false;
         }
         if( _state.length>0 )
         {
            document.location.href = "/form/step1/" + _action + "/" + _state + ".html";
            isQuit = false;
         }
         return false;
      });
   }
   else
   if( hrf.indexOf("/splash/")!=-1 || hrf.indexOf("/landing/")!=-1 )
   {
      //--- checkbox ---
      $("#step1 input:radio").change(function(){
         $("#step1 :radio ").parent(".checkBox").removeClass("checked");
         $("#step1 :radio:checked").parent(".checkBox").addClass("checked");
         $("#step1 :radio:checked").blur();
      });
      
      try
      {
         var x    = hrf.split("?")[1].split(":")[0];
         var opt1 = "new-registration";
         var opt2 = "renew-registration";
         var opt3 = "replace-registration";
         var opt4 = "change-of-address";
         var opt5 = "change-of-name";
         if( x == opt1 || x == opt2 || x == opt3 || x == opt4 || x == opt5 )
         {
            $("#"+x.toString()).parent(".checkBox").addClass("checked");
            $("#"+x.toString()).attr("checked", true);
         }
      }
      catch(e){}
      
      $("#submit").click( function(){
         _action = $('input[name=action]:checked');
         if( _action.length==0 )
         {
            alert( "Please Choose a Service" );
            return;
         }
         action = _action.val();
         _link = "/form/step1/" + action + "/" + getState() + ".html";
         document.location.href = _link;
      });
   }
   else
   if( (hrf.indexOf("/servsplash/")!=-1) || (hrf.indexOf("/servlanding/")!=-1) )
   {
      $(".splash_startTX").click(function() {
         document.location.href = "/form/step1/" + getUrl(2) + "/" + getState() + ".html";
      });
      $(".zip").hide();
      $("#continue").click(function() {
         document.location.href = "/form/step1/" + getUrl(2) + "/" + getState() + ".html";
      });
   }
   else
   if( (hrf.indexOf(".com/new-registration/")!=-1) || (hrf.indexOf(".com/renew-registration/")!=-1) || (hrf.indexOf(".com/replace-registration/")!=-1) || (hrf.indexOf(".com/change-of-name/")!=-1) || (hrf.indexOf(".com/change-of-address/")!=-1) )
   {
      $("#stateinfo-cont").click(function() {
         document.location.href = "/form/step1/" + getUrl(1) + "/" + getState() + ".html";
      });
   }
   else
   if( hrf.indexOf("/docs.html")!=-1 )
   {
      //--- checkbox ---
      $("#step1 input:radio").change(function(){
         $("#step1 :radio ").parent(".checkBox").removeClass("checked");
         $("#step1 :radio:checked").parent(".checkBox").addClass("checked");
         $("#step1 :radio:checked").blur();
      });
      //--- home page validation ---
      $("#submit").click(function(e){
         var state = $("#state").val();
         var serv = $('input[name=action]:checked','#first-form').val();
         if( !serv )
         {
            alert( "Please Select an Action" );
            return false;
         }
         if( state=="" )
         {
            alert( "Please Select a State" );
            $("#state").focus();
            return false;
         }
         if( state.length>0 )
         {
            // --- if has form, show in shopping cart
            var url = "/ajax/pdfExists.jsp?form=forms/" + serv + "/" + state;
            $.ajax({
            "url": url,
            "success": function(response){
                    if( response.indexOf("true") !=-1 )
                    {
//                       $("#form").attr("href","/PDForm/forms/" + serv + "/" + state + ".pdf");
                       $("#form").show();
                    }
                }
            });
            $("#checklist").attr("href","/checklist/" + serv + "/" + state + ".pdf");
            $("form, #seals").hide();
            $("#dlh1").text("Your Documents are Ready!");
            $("#download-par").text("Your Car Registration Easy Guide and form (if applicable) are available below for download.");
            $("#progress").attr("src", "http://s3.amazonaws.com/"+domainName+"/img/progress2.png");
            $("#show-downloads").show();
         }
      });
   }
});


