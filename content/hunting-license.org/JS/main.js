var statenames = ['alabama','alaska','arizona','arkansas','california','colorado','connecticut','delaware','florida','georgia','hawaii','idaho','illinois','indiana','iowa','kansas','kentucky','louisiana','maine','maryland','massachusetts','michigan','minnesota','mississippi','missouri','montana','nebraska','nevada','new-hampshire','new-jersey','new-mexico','new-york','north-carolina','north-dakota','ohio','oklahoma','oregon','pennsylvania','rhode-island','south-carolina','south-dakota','tennessee','texas','utah','vermont', 'virginia','washington','west-virginia','wisconsin','wyoming'];

var active_states = ["florida", "california", "colorado","hawaii", "minnesota", "texas", "north-carolina", "louisiana" ];

$(document).ready(function() {

  /*====== START: Resuable Code ======*/
  // Extend JQuery to create a function / method to test if an element exists
  $.fn.exists = function() {
  return this.length !== 0;
  }
  /*====== END: Resuable Code ======*/

  var hrf = document.location.href;
  var up = hrf.split('/');
  var cat = up[3];
  var category = cat.replace(/-/g, ' ');
  var file_name = up[up.length-1].substring(0, up[up.length-1].length-5);


/* =============================================================
  INPAGE LINKS TO FORM
==============================================================*/
try {
  $(".inpagelink").attr("href", "/form/step1/new-license/"+ getPage() +".html")
}
catch (e) {}

/* =============================================================
  HOMEPAGE SUBMIT
==============================================================*/
  if( hrf.indexOf("index.html") != -1 )
  {
      $("#submit").click(function(e){
         var _state  = $("#state").val();
         var _action = $('input[name=action]:checked','#first-form').val();
         if( !_action )
         {
            alert( "Please Select an Action" );
            return false;
         }
         if( _state=="" )
         {
            alert( "Please Select a State" );
            $("#state").focus();
            return false;
         }
         if( _state.length>0 )
         {
               document.location.href = "/form/step1/" + _action + "/" + _state + ".html";
         }
         return false;
      });
   }

 /* =============================================================
  LANDING PAGE SUBMIT
==============================================================*/
  
   // ALL STATE PAGES
   $('#promo-submit').click(function() {
      // alert(page_name);
      if($('#resident').val() != '' && $('#license').val() != '' && $('#tags').val() != ''){
        redirectToForm();
      } 
      else
      {
        selectFieldEmptyError();
      }
   });

   // RESIDENT, NON-LICENSE, TYPES OF GAME PAGES
   $('#promo-nonstate-submit').click(function() {
      if ( $('#state').val() != '' && $('#license').val() != '' && $('#tags').val() != '' )
      {
        var state = $("#state").val();
        document.location.href = "/form/step1/new-license/" + state + ".html";
      } 
      else
      {
        selectFieldEmptyError();
      }
   });

   // MOBILE VIEW OF PROMO SPACE
   $("#mob-submit").click(function() {
      if ( $("#state").val() != "" )
      {
        var state = $("#state").val();
        document.location.href = "/form/step1/new-license/" + state + ".html";
      }
      else {
        $("#state").css("border","1px solid red");
        alert("Please select a State");
      }
   });

   // MOBILE VIEW OF PROMO SPACE
   $("#mob-state-submit").click(function() {
        // alert(getPage());
        document.location.href = "/form/step1/new-license/" + getPage() + ".html";
   });


/* =============================================================
  FORM FEILD PREPOP - RESIDENT/NON-RESIDENT
==============================================================*/
if(hrf.indexOf("/resident-license.html") !=-1)
{
  $("#resident").val("yes");
  $("#license").focus();
}
else if(hrf.indexOf("/non-resident-license.html") !=-1)
{
  $("#resident").val("no");
  $("#license").focus();
}

/* =============================================================
  REDIRECT STATE LANDING PAGE TO FORM
==============================================================*/
function redirectToForm(){
  if(statenames.indexOf(getPage()) > -1){
    var state = getPage();
    document.location.href = "/form/step1/new-license/"+state+".html";
  } 
}


/* =============================================================
  COPYRIGHT - GARY
==============================================================*/
  $('#copyrights #year').html(new Date().getFullYear())
    // Hides items if the current page is not a state page
  if ($(".state-page").exists() == false) {
    $("#nav-contain").toggleClass("hide");
    $("#sidebar-left").toggleClass("hide");
    // show recent post side bar for non-state page
    $("#sidebar-left-posts").removeClass("hide").addClass("remove-widget-spacing");
  } else {
    $("#sidebar-left-posts").addClass("hide");
  }


/* =============================================================
  ADD LOGO/DEPT TEXT TO STATE LANDING PAGES
==============================================================*/
  if( statenames.indexOf(getPage()) > -1) {
    if(getDeptName(getPage()) != undefined) {
      var stateVar = getPage();
      if(getPage().indexOf("-") !=-1)
      {
        stateVar = capWords(stateVar.replace("-"," "));
      }
      else 
      {
        stateVar = capitalize(stateVar);
      }

      $('.card-title').html( "with the " + stateVar + " " + getDeptName(getPage()));
      $('.card-badge').html('<img src="//s3.amazonaws.com/hunting-license.org/images/logos/'+getPage()+'.png" alt="" align="left">');
    }
    else
    {
      $('.featured-card').addClass('generic');
    }
  }
  else
  {
    $('.featured-card').addClass('generic');
  }

/* =============================================================
  JQUERY QUIZ
==============================================================*/
if( hrf.indexOf("/practice-test") !=-1 ) 
{
    var questions = [{
      question: "Which one of the following ammunition calibers is correct for hunting small game?",
      choices: [".220 SWIFT", ".223 REM", ".22 WMR", ".300 WBY"],
      correctAnswer: 2
  }, {
      question: "What is the name of the rear end of the barrel in a firearm?",
      choices: ["Bolt", "Base", "Breech", "Chamber"],
      correctAnswer: 2
  }, {
      question: "What is the color of hunting safety gear worn in the United States?",
      choices: ["Green", "Orange", "Red", "Yellow"],
      correctAnswer: 2
  }, {
      question: "Which one of the following is banned under fair chase laws?",
      choices: ["Decoys", "Enhanced scent sprays", "Radios", "Infrared cameras"],
      correctAnswer: 2
  }, {
      question: "When on a hunt involvin multiple hunters, what is the ideal maximum number of hunters that should be in one group for safety purposes?",
      choices: ["3","4","5","6"],
      correctAnswer: 0
  }, {
      question: "How many compasses should a hunter bring on a trip?",
      choices: ["1", "2", "1 GPS compass and 2 magnetic", "3"],
      correctAnswer: 1
  }, {
      question: "Which article of clothing should be taken on a hunt?",
      choices: ["Sneakers", "Rain Suit", "Bandana", "Sunglasses"],
      correctAnswer: 1
  }, {
      question: "How far apart should multiple hunters be spaced on a hunt to establish safe fire zone?",
      choices: ["10-15 yards","15-20 yards","20-40 yards","40-50 yards"],
      correctAnswer: 2
  }, {
      question: "When taking a shot with a rifle scope mounted on top of a rifle, what should the scope’s range be set at?",
      choices: ["0", "10m", "15m", "20m"],
      correctAnswer: 0
  }, {
      question: "What is the optimal body position to fire a rifle from?",
      choices: ["Prone","Kneeling","Standing","Sitting"],
      correctAnswer: 0
  }];

  var currentQuestion = 0;
  var correctAnswers = 0;
  var quizOver = false;

  // Display the first question
  displayCurrentQuestion();
 
  // On clicking next, display the next question
  $(this).find(".nextButton").on("click", function () {
      if (!quizOver) {

          value = $("input[type='radio']:checked").val();

          if (value == undefined) {
              $(".quizMessage").text("Please select an answer").show();
          } 
          else 
          {
              $(".quizMessage").css("display","none");
              if (value == questions[currentQuestion].correctAnswer) 
              {
                  correctAnswers++;
              }

              currentQuestion++; // Since we have already displayed the first question on DOM ready
              $(".questionCount").text("Question" + currentQuestion+1 + "of" + questions.length-1);

              if (currentQuestion < questions.length) 
              {
                  displayCurrentQuestion();
              } 
              else 
              {
                  displayScore();
                  $(".nextButton").text("Continue")
                  quizOver = true;
                  $(".question, .choiceList").hide();
                  $(".questionCount").text("You've completed the Practice Test!");
                  $(".nextButton").click(function(){
                    document.location.href = "/checklistpage";
                  });   
              }
          }
      } 
      else 
      { // quiz is over and clicked the next button (which now displays 'Play Again?'
          quizOver = false;
          $(".nextButton").text("Continue");
          resetQuiz();
          displayCurrentQuestion();
          hideScore();
      }
  });
}




// This displays the current question AND the choices
function displayCurrentQuestion() {

    console.log("In display current Question");

    var numChoices = questions[currentQuestion].choices.length;
    var onq = currentQuestion+1;
    var ofq = questions.length;

    // Set the questionClass text to the current question
    $(".question").text(questions[currentQuestion].question);
    $(".questionCount").text("Question " + onq + " of " + ofq);

    // Remove all current <li> elements (if any)
    $(".quizContainer > .choiceList").find("div").remove();

    var choice;
    for (i = 0; i < numChoices; i++) {
        choice = questions[currentQuestion].choices[i];
        $(".quizContainer > .choiceList").append('<div><input type="radio" name="radio" id="radio'+i+'" value="'+i+'" class="radio"/><label for="radio'+i+'">' + choice + '</label></div>');
    }
}

function resetQuiz() {
    currentQuestion = 0;
    correctAnswers = 0;
    hideScore();
}

function displayScore() {
    $(".quizContainer > .result").text("You scored: " + correctAnswers + " out of " + questions.length).show();
    var perc = (correctAnswers/questions.length)*100;
    $(".percentage").text(perc+"%");
}

function hideScore() {
    $(".result").hide();
}


/* =============================================================
  BLOGS - GARY CODE
==============================================================*/
  // Blog Entry meta
  var weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

  update = new Date(document.lastModified);
  day = weekday[update.getDay()];
  date = update.getDate();
  month = update.getMonth() + 1;
  year = update.getFullYear();

  $('.entry-meta .date').after(day + " " + month + "/" + date + "," + "/" + year);

  /*====== START: Ad Content ======*/
  // Insert into featured-ad-top
  if ($('.category-california').exists() == true) {
    $('.headline .copy').html('Avoid fines and the other penalties that come with hunting without proper licensure. The state of California requires all hunter to have the right set credentials in order to hunt. Fortunately, Hunting-License.org makes the process of getting the right Hunting License fast and easy. With a checklist that gives you everything you need to know in order to apply, along with countless other valuable services, you will not need to look anywhere else for license assistance or where to hunt in.');
  } else if ($('.category-colorado').exists() == true) {
    $('.headline .copy').html('Hunting without a license comes with penalties in the state of Colorado, and these penalties do not just stop at fines. Use Hunting-License.org to make sure you are prepared to hunt within CO state boundaries. With a number of valuable services, including a checklist that takes you through all the steps you need to take in order to obtain a license to hunt, there is no better resource to get you ready.');
  }else if ($('.category-texas').exists() == true) {
    $('.headline .copy').html('In Texas, it is important to buy a Hunting License before embarking on any hunting trip, in order to avoid any unwanted fees. Proper licensure is required within the state for any hunter who plans on enjoying state park hunting. Be sure to consult Hunting-License.org&rsquo;s detailed checklist for purchasing a hunting permit, as well as its countless other services, to make sure that your TX hunting vacation goes off without a hitch.');
  }else if ($('.category-minnesota').exists() == true) {
    $('.headline .copy').html('Minnesota requires all hunter within state bounds to have a proper Hunting License in order to take hunt. Steer clear of fines and other punishments by hunting with the necessary credentials. Hunting-License.org has everything you need to get a hunting permit quickly and easily, from an in-depth checklist that provides information on all the steps you need to take to a number of other helpful services. Now you can start preparing to obtain your desired Hunting License.');
  }else{
    // show florida text as default
  }
  /*====== END: Ad Content ======*/ 

  // Hide elements on mobile view Only for State pages
  if ($('.state-page').exists() == true) {
      $(this).find('#wrapper').addClass('state-cleanup');
  }

   var blog_posts = ['how-weather-affects-your-aim', 'gun-safety', 'hunting-seasons'];
  // Randomly select Pdf page
  //shuffle(pdf_page_select);

 //------- Insert Blog Articles
  
  // .child-category should only exist on pages that wants to populate articles
  if($('#IgnoreForNow').exists() == true){
     //alert('trying to build blog exceprts here');
     // If the directory isn't 'article' the program assumes we are in a category page    
      createBlogExcerpts(blog_posts);
     }
  function createBlogExcerpts(sort_by){
      // loop is based on the current category defined by the url. The program will loop through all articles for that category
      for (i = 0; i < sort_by.length; i++) {
           // Create blog post html elements
           $('#posts').append('<article class="entry clearfix ' + i + '"><div class="entry-image" '+i+'><a href=/blogs/"'+sort_by[i]+'.html"></a></div><section class="entry-c " '+i+'><div class="entry-title"><h2></h2></div><ul class="entry-meta clearfix"></ul><div class="entry-content "'+i+'></div></section></article>');

           // Post Feature image
           $(".entry." + i + " .entry-image a").load("/blogs/"+sort_by[i]+".html .entry-image >* ");
           // Title
           $(".entry-title." + i + " h2").load("/blogs/"+sort_by[i]+".html .entry-title >* ");
       }
  }


});



var submenuJSON = "{" 
+ "\"florida\":[\"Freshwater Hunting Licenses\",\"Saltwater Hunting Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"]," 
+ "\"california\":[\"Commercial Hunting Licenses\",\"Sport Hunting License\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"],"
+ "\"colorado\":[\"Commercial Hunting Licenses\",\"New Hunting Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"]," 
+ "\"hawaii\":[\"Commercial Hunting Licenses\",\"Freshwater Hunting Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"],"
+ "\"louisiana\":[\"Freshwater Hunting Licenses\",\"Saltwater Hunting Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"]," 
+ "\"minnesota\":[\"Commercial Hunting Licenses\",\"New Hunting Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"]," 
+ "\"north-carolina\":[\"Freshwater Hunting Licenses\",\"Saltwater Hunting Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"]," 
+ "\"texas\":[\"Freshwater Hunting Licenses\",\"Saltwater Hunting Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"]" + "}";


var pagename = "";

function getSubMenu(state, submenu) {
  var menus = JSON.parse(submenuJSON);

  var menu = menus[state];

  for (var m = 0; m < menu.length; m++) {
    $("#" + submenu + " ul").append("<li><a href=\"/" + menu[m].replace(/ /g, '-').toLowerCase() + "/" + state + ".html\">" + menu[m] + "</a></li>");
  }
}

function getPageName(hrf) {
  pagename = hrf.split("/")[hrf.split("/").length - 1].split(".")[0].replace(/-/g, ' ');
  if (pagename == "index")
    pagename = hrf.split("/")[hrf.split("/").length - 2]
  return pagename;
}

function go2State(o) {
  document.location.href = "/" + o.value + ".html";
}

function splashStart()
{
   hrf   = document.location.href.split("/");
   state = hrf[hrf.length-1];
   act   = $('input[name=action]:checked').val();
   if( typeof act === "undefined")
   {
      alert( "Please Select a Type of License" );
      return false;
   }
   else
   {
      document.location.href = "/form/step1/" + act + "/" + state + ".html";
      // document.location.href = "/form/step1/" + act + "/" + state;
   }
}
 // this function gets the department name from the state name variable -- example:  $("#title").text(getDeptName(getPage()));
  function getDeptName(stateName) 
  {
    var dept = new Array();

    dept['texas'] = 'Parks & Wildlife'; 
    dept['california'] = 'Department of Fish and Wildlife'; 
    dept['indiana'] = 'Department of Natural Resources Law Enforcement Division'; 
    dept['kansas'] = 'Wildlife, Parks & Tourism'; 
    dept['kentucky'] = 'Department of Fish and Wildlife Resources'; 
    dept['missouri'] = 'Department of Conservation'; 
    dept['new-york'] = 'Department of Environmental Conservation'; 
    dept['ohio'] = 'Department of Natural Resources'; 
    dept['pennsylvania'] = 'Game Commission'; 
    dept['tennessee'] = 'Wildlife Resources Agency'; 
    dept['mississippi'] = 'Department of Wildlife, Fisheries & Parks'; 
    dept['nebraska'] = 'Game & Parks Commission'; 
    dept['new-mexico'] = 'Department of Game & Fish'; 
    dept['oklahoma'] = 'Department of Wildlife Conservation'; 
    dept['oregon'] = 'Department of Fish & Wildlife'; 
    dept['rhode-island'] = 'Division of Fish & Wildlife'; 
    dept['south-carolina'] = 'Department of Natural Resources'; 
    dept['north-dakota'] = 'Game and Fish Department'; 
    dept['south-dakota'] = 'Department of Game, Fish and Parks'; 
    dept['utah'] = 'Division of Wildlife Resources'; 
    dept['vermont'] = 'Fish & Wildlife Department'; 
    dept['virginia'] = 'Department of Game and Inland Fisheries'; 
    dept['washington'] = 'Department of Fish & Wildlife'; 
    dept['west-virginia'] = 'Division of Natural Resources'; 
    dept['wisconsin'] = 'Department of Natural Resources'; 
    dept['wyoming'] = 'Game and Fish Department'; 
    dept['north-carolina'] = 'Wildlife Resources Commission'; 
    dept['alabama'] = 'Department of Conservation and Natural Resources';
    dept['alaska'] = 'Department of Fish and Game';
    dept['arizona'] = 'Game and Fish Department';
    dept['arkansas'] = 'Game and Fish Commission';
    dept['colorado'] = 'Parks and Wildlife';
    dept['connecticut'] = 'Department of Energy and Environmental Protection';
    dept['delaware'] = 'Division of Fish and Wildlife';
    dept['florida'] = 'Fish and Wildlife Conservation Commission';
    dept['georgia'] = 'Department of Natural Resources';
    dept['hawaii'] = 'Division of Forestry and Wildlife';
    dept['idaho'] = 'Department of Fish and Game';
    dept['illinois'] = 'Department of Natural Resources';
    dept['iowa'] = 'Department of Natural Resources';
    dept['louisiana'] = 'Department of Wildlife and Fisheries';
    dept['maine'] = 'Department of Inland Fisheries and Wildlife';
    dept['maryland'] = 'Department of Natural Resources';
    dept['massachusetts'] = 'Department of Energy and Environmental Affairs';
    dept['michigan'] = 'Department of Natural Resources';
    dept['minnesota'] = 'Department of Natural Resources';
    dept['montana'] = 'Fish, Wildlife and Parks';
    dept['nevada'] = 'Department of Wildlife';
    dept['new-hampshire'] = 'Fish and Game Department';
    dept['new-jersey'] = 'Department of Fish and Wildlife';
    dept['north-carolina'] = 'Wildlife Resources Commission';
    dept['north-dakota'] = 'Game and Fish Department'; 

    return dept[stateName];
  }




/**************************************************
FUNCTIONS
**************************************************/
function capitalize(string) 
{ 
   return string.charAt(0).toUpperCase() + string.slice(1); 
} 

function capWords(str)
{ 
   var words = str.split(" "); 
   for (var i=0 ; i < words.length ; i++)
   { 
      var testwd = words[i]; 
      var firLet = testwd.substr(0,1); 
      var rest   = testwd.substr(1, testwd.length -1) 
      words[i]   = firLet.toUpperCase() + rest 
   } 
   return words.join(" "); 
} 

function getPage()
{
   spliturl = document.location.href.split("/");
   return spliturl[spliturl.length-1].split(".")[0];
}

function slideMobNav() {
  $(".mobnav").slideToggle();
}

function selectFieldEmptyError() {
  alert('Please select an option from each field');
  $('select').change(function () {
    $("select").each(function() {
      if ($(this).val() == ''){
          var empty_selects = $('select').filter(function() {
          return $.trim( $(this).val() ) == '';
        });
        empty_selects.css('border', '2px solid red');
      }else if ($(this).val() != ''){
        $(this).css('border', '1px solid #cccccc');
      } 
    });
  })
  .change();
}