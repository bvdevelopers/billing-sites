function customValidate()
{
   return true;
}


$('#radioBtn a,#radioBtn input').on('click', function(){
    var sel = $(this).data('title');
    var tog = $(this).data('toggle');
    $('#'+tog).prop('value', sel);
    if(sel=='Temporary'){
    	$('#hiddenTempDate').show();
    	$('#usps_forward_stop').addClass('required');
    	$('#usps_forward_stop').addClass('alwaysValidate');
    } else {
    	$('#hiddenTempDate').hide();
    	$('#usps_forward_stop').removeClass('required');
    	$('#usps_forward_stop').removeClass('alwaysValidate');
    }
    $('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
    $('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
})

function checkMatch(id) {
   oldAddr = ($.trim($("#address_1").val())+$.trim($("#suite_1").val())+$.trim($("#state_1").val())+$.trim($("#zip_1").val())).toLowerCase();
   newAddr = ($.trim($("#address_2").val())+$.trim($("#suite_2").val())+$.trim($("#state_2").val())+$.trim($("#zip_2").val())).toLowerCase();
   return ( oldAddr!=newAddr );
}

/*
$(function () {
    $('#datetimepicker1').datetimepicker();
});
*/
$('.glyphicon-info-sign').hover(function(){
	$(this).tooltip('toggle');
});

function isDate(txtDate)
{
  
  var currVal = txtDate;
  if(currVal == '')
    return false;
 

  //Declare Regex  
  var rxDatePattern = /^(\d{1,2})(\/|-)(\d{1,2})(\/|-)(\d{4})$/; 
  var dtArray = currVal.match(rxDatePattern); // is format OK?

  if (dtArray == null)
     return false;
 
  //Checks for mm/dd/yyyy format.
  dtMonth = dtArray[1];
  dtDay= dtArray[3];
  dtYear = dtArray[5];

  if (dtMonth < 1 || dtMonth > 12)
      return false;
  else if (dtDay < 1 || dtDay> 31)
      return false;
  else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
      return false;
  else if (dtMonth == 2)
  {
     var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
     if (dtDay> 29 || (dtDay ==29 && !isleap))
          return false;
  }


  return true;
}

function isFutureDate(date) {
    
    var parts = date.split('/');
	var date = new Date(parseInt(parts[2], 10),     // year
	                    parseInt(parts[0], 10)-1, // month, starts with 0
	                    parseInt(parts[1], 10));    // day

	if (date < new Date()) {
	    // It's in the past, including one millisecond ago
	    return false;
	} else {
		return true;
	}

}

function isFutureOfForwardDate(date,forwardDate) {
    
    var stopDate = date;
    var startDate = forwardDate;

    var parts = startDate.split('/');
	var futureDate = new Date(parseInt(parts[2], 10),     // year
	                    parseInt(parts[0], 10)-1, // month, starts with 0
	                    parseInt(parts[1], 10)+10);    // day

	var parts = stopDate.split('/');
	var stopDate = new Date(parseInt(parts[2], 10),     // year
	                    parseInt(parts[0], 10)-1, // month, starts with 0
	                    parseInt(parts[1], 10));    // day

	if (stopDate < futureDate) {
	    // It's in the past, including one millisecond ago
	    return false;
	} else {
		return true;
	}

}

function extractNumberJunk( val )
{
   _v = "";
   for( var n=0; n<val.length; n++ )
   {
      if( val.charAt(n)==" " )
         continue;
      try
      {
         evl = eval( val.charAt(n) );
         _v += val.charAt(n);
      }
      catch(e)
      {
      }
   }
   return _v;
}


function isEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 

function isPhone(phone) {
	msisdn = extractNumberJunk(phone);
      if( msisdn.length!=0 )
      {
         if( (msisdn.length!=10 && "US"=="US") ||
             (msisdn.length!=10 && "US"=="BR")
         )
         {
            return false;
         }
      } else {
      	return false;
      }
}

function checkTwin(elem) {

 	// Find twin checkbox and set the value = to current status from THIS elem
	var value = $(elem).prop('checked');
	var className = $(elem).attr('class');
	var partsOfClass = className.split(' ');
	var twinClass = partsOfClass[1];

	$('.'+twinClass).each(function() {
		$(this).prop('checked',value);
	});

}

function validateData(elem) {
	var flag = false;
	$('#'+elem+' .required').each(function() {
	if($(this).is(":visible") || $(this).hasClass('alwaysValidate') ){
		if($(this).attr('type')=='checkbox') {
			if($(this).prop('checked')!=true){
				flag=true;
				$(this).parent().parent().addClass('has-error');
				if($(this).parent().parent().children('.error-msg')) {
					$(this).parent().parent().children('.error-msg').remove();
				}
				if($(this).hasClass('terms')) {
					$(this).parent().parent().append('<div class="error-msg col-md-12"><div class="clear"></div><br><div class="alert alert-danger ">You must agree to all terms and conditions in order to use our service.</div></div>');
				} else {
					$(this).parent().parent().append('<div class="error-msg col-md-12"><div class="clear"></div><br><div class="alert alert-danger">This item is required</div></div>');
				}
			}
		}
	
		if($(this).data('type')=='date') {
			if($(this).data('check')=='endDate') {
				if(isFutureOfForwardDate($(this).val(),$('#usps_forward').val())==false) {
					flag = true;
					$(this).parent().addClass('has-error');
					if($(this).parent().children('.error-msg')) {
						$(this).parent().children('.error-msg').remove();
					}
					$(this).parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">You must provide a 2 weeks after forward date</div>');
				}
			}
			if(isFutureDate($(this).val())==false) {
				flag = true;
				$(this).parent().addClass('has-error');
				if($(this).parent().children('.error-msg')) {
					$(this).parent().children('.error-msg').remove();
				}
				$(this).parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">You must provide a future date</div>');
			}
			if(isDate($(this).val())==false) {
				flag = true;
				$(this).parent().addClass('has-error');
				if($(this).parent().children('.error-msg')) {
					$(this).parent().children('.error-msg').remove();
				}
				$(this).parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">Date format incorrect. Expected: mm/dd/yyyy</div>');
			}

		}
		if($(this).data('type')=='email') {
			if(isEmail($(this).val())==false) {
				flag = true;
				$(this).parent().addClass('has-error');
				if($(this).parent().children('.error-msg')) {
					$(this).parent().children('.error-msg').remove();
				}
				$(this).parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">Please enter a valid email address</div>');
			}
		}
		if($(this).data('type')=='phone') {
			if(isPhone($(this).val())==false) {
				flag = true;
				$(this).parent().addClass('has-error');
				if($(this).parent().children('.error-msg')) {
					$(this).parent().children('.error-msg').remove();
				}
				$(this).parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">Please enter a valid phone number</div>');
			}
		}
                //--- ADDRESS MISMATCH
		if($(this).data('addr')=='mismatch') {
			if(checkMatch($(this).attr('id'))==false) {
				flag = true;
				$(this).parent().addClass('has-error');
				if($(this).parent().children('.error-msg')) {
					$(this).parent().children('.error-msg').remove();
				}
				$(this).parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">Your old address can not match your new address</div>');
			}
		}


		if($(this).val()==null || $(this).val()==false) {
			if($(this).parent().hasClass('input-group') && $(this).parent().hasClass('super-parent')) {
				$(this).parent().parent().addClass('has-error');
				if($(this).parent().parent().children('.error-msg')) {
						$(this).parent().parent().children('.error-msg').remove();
					}
				if($(this).data("error")){
					$(this).parent().parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">'+$(this).data('error')+'</div>');
				} else {
					$(this).parent().parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">This field is required</div>');
				}
			} else {
				$(this).parent().addClass('has-error');
				if($(this).parent().children('.error-msg')) {
						$(this).parent().children('.error-msg').remove();
					}
				if($(this).data("error")){
					$(this).parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">'+$(this).data('error')+'</div>');
				} else {
					$(this).parent().append('<div class="clear"></div><div class="alert alert-danger error-msg date">This field is required</div>');
				}
			}
			
			
			flag = true;
			// return false;
		}
	}
		
	});

	if(flag!=false) {
		return false;
	}
	return true;
	
	
}

$('.nextBtn').click(function() {
	var parentId = $(this).parent().attr('id');

	if(validateData(parentId)==true){
	   track( parentId );
	   
		$(this).parent().addClass('hidden-xs');
		$(this).parent().addClass('visible-lg');
		$(this).parent().removeClass('visible-xs');
		$(this).parent().next('.fieldset').removeClass('hidden-xs');
		$(this).parent().next('.fieldset').addClass('visible-xs');
		$(this).parent().next('.fieldset').addClass('visible-lg');
		var elem = $(this).parent().next('.fieldset').attr('id');
		$('.mobileSteps .nav-pills li').removeClass('active');
		$('.mobileSteps .nav-pills li .'+elem).parent('li').addClass('active');
	    $('.mobileSteps .nav-pills li .'+elem).parent('li').prev('li').addClass('done');
		window.scrollTo(0, 0);
	} else {
		return false;
	}

});

$('.finalSubmit').click(function(e) {
	e.preventDefault();
	var parentId = $(this).parent().attr('id');

	if(validateData(parentId)==true){
	$('.removeBeforeSubmit').each(function(){
		$(this).remove();
	});
	$('form').submit();
	} else {
		return false;
	}
});

$('.finalSubmitDesktop').click(function(e) {
	e.preventDefault();
	flag=false;
	var parentId = $(this).parent().attr('id');
	$('.fieldset').each(function() {
		if(validateData($(this).attr('id'))!=true){
			flag=true;
		}
	});

	//Manually Validate New Address

	
	if(flag==false){
		$('.removeBeforeSubmit').each(function(){
			$(this).remove();
		});
		$('form').submit();
	}
	

});

function setDate(elem)
{
   objId    = elem.id.substr(4);
   mainObj  = $("#" + objId );
   mObj     = $("#_mm_" + objId );
   dObj     = $("#_dd_" + objId );
   yObj     = $("#_yy_" + objId );
   
   m        =  mObj.val();
   d        =  dObj.val();
   y        =  yObj.val();
   full     =  m + "/" + d + "/" + y;
   if( full=="//" )
      full = "";
   mainObj.val( full );
}


$(document).ready(function() {
	
	$('input,select').change(function() {

		// revalidate field
	  if($(this).attr('type')=="checkbox") {
	  	if($(this).parent().parent().hasClass('has-error')) {
	  	  var value = $(this).prop('checked');
		  if(value!=false){
		  	$(this).parent().parent().removeClass('has-error');
		  	// $(this).parent().addClass('has-success');
		  }
		  if($(this).parent().parent().children('.error-msg')) {
			$(this).parent().parent().children('.error-msg').remove();
		  }
	  }
	  }
	  if($(this).parent().hasClass('has-error')) {
		  if($(this).val()!="" && $(this).val()!=null && $(this).val()!=false && $(this).val()!=" "){
		  	$(this).parent().removeClass('has-error');
		  	// $(this).parent().addClass('has-success');
		  }
		  if($(this).parent().children('.error-msg')) {
			$(this).parent().children('.error-msg').remove();
		  }
	  }

	  var name = $(this).attr('name');
	  var valueToChange = $(this).val();

	  $('.'+name).each(function() {
	  	$(this).val(valueToChange);
	  });
	});
	

	var i = 1;
	$('.fieldset').each(function(){
		if(i==1){
			var item = '<li class="active"><a href="#" class="stepToggle '+$(this).attr('id')+'" data-order="'+i+'" toggle-data="'+$(this).attr('id')+'">Step '+i+'</a></li>';
		} else {
			var item = '<li><a href="#" class="stepToggle '+$(this).attr('id')+'" data-order="'+i+'" toggle-data="'+$(this).attr('id')+'">Step '+i+'</a></li>';
		}
		
		$('.mobileSteps .nav-pills').append(item);
		i=i+1;
	});

	$(document).delegate(".stepToggle","click",function(e){
	    e.preventDefault();
	    var orderReq = $(this).data('order');
	    var orderActive = $('.mobileSteps .nav-pills li.active a').data('order');

	    var active = $('.mobileSteps .nav-pills li.active a').attr('toggle-data');
		var elem = $(this).attr('toggle-data');
		
		if(orderReq>orderActive) {
			if(validateData(active)==false){
				return false;
			} else {
				$('.mobileSteps .nav-pills li.active').addClass('done');
			}
		}

		$('.mobileSteps .nav-pills li').removeClass('active');
		$(this).parent('li').addClass('active');
		// $('.fieldset').hide();
		$('.fieldset').each(function() {
			
			$(this).removeClass('visible-xs');
			$(this).addClass('hidden-xs');
			$(this).addClass('visible-lg');
		});
		
		$('#'+elem).removeClass('hidden-xs');
		$('#'+elem).addClass('visible-xs');
		
	});
popForm();
});


function popForm()
{
   try
   {
      for( var v in _PROFILE )
      {
         if( v.charAt(0)!="_" )
         {
            //console.log( v + "=" + _PROFILE[v] );
            $("#"+v).val( _PROFILE[v] );
         }
      }
      
      $("#email_confirm").val( $("#email").val() );
      
      
      if( _PROFILE["prefix"]=="Mr." )
         $("#male").attr(  'checked', true);
      else
         $("#female").attr('checked', true);

     
 



      dt = _PROFILE["usps_forward"].split("/");
      dt[0] = (dt[0].length==1?"0":"") + dt[0];
      dt[1] = (dt[1].length==1?"0":"") + dt[1];
      $("#_mm_usps_forward").val( dt[0] );
      $("#_dd_usps_forward").val( dt[1] );
      $("#_yy_usps_forward").val( dt[2] );


      $("#" + _PROFILE["moving_type"].toLowerCase()).attr('checked', true);
   }
   catch(e)
   {
   }
}


