<%@page import="java.util.*, com.util.*, com.bl.*, com.servlets.*"%>
<script src="//api.direct-market.com/client/leadclient2.js"></script>
<script>
   $(document).ready(function(){
      <%
         String  domain                =  Web.getDomainName( request );
         HashMap hm                    =  (HashMap) request.getAttribute( "data"       );
         HashMap hmProfile             =  User.getSessionProfile( request );
         hm.putAll( hmProfile );

         String  downloadId            =  (String)  request.getAttribute( "downloadId" );
         String  pageNum               =  (String)  hm.get( "_PAGENUM_"                );
         pageNum                       =  (pageNum==null)?"0":pageNum;
         String  form                  =  (String)  hm.get( "_FORM_"                   );
         String  id                    =  (String)  hm.get( "id"                       );

         if( pageNum.equals("01") )
         {
            %>setDownload( "<%=downloadId%>" );<%
         }
      %>
      try
      {
         window.client = new LeadClient("925f5711dfc5-49ad-8a94-9f99ac867cf0");
         client.check              = false;
         client.sold               = false;
         client.lead.first_name    = "<%=hm.get("first_name")%>";
         client.lead.last_name     = "<%=hm.get("last_name" )%>";
         client.lead.address1      = "<%=hm.get("address_1")%> <%=hm.get("suite_1")%>";
         client.lead.city          = "<%=hm.get("city_1")%>";;
         client.lead.state         = "<%=hm.get("state_1")%>";
         client.lead.zip_code      = "<%=hm.get("zip_1")%>";
         client.lead.phone_home    = "<%=hm.get("phone_1")%>";
         client.lead.email         = "<%=hm.get("email")%>";
         client.lead.lead_id_token = "<%=hm.get("universal_leadid")%>";
         client.lead.gender        = "<%=hm.get("gender")%>";
         client.lead.service_type  = "<%=form.split("/")[1]%>";

         var onSuccess = function (data, textStatus, jqXhr){
            localStorage.setItem( 'leadid', data.id );
         };
         var onFailure = function (jqXhr, textStatus, errorThrown) {  };
         var onAlways  = function ()                               {  };
         try {
            ga(function (tracker) {
            var gaId = tracker.get('clientId');
            client.lead.ga_id = gaId;});
         }
         catch (e) { };
         client.Post( onSuccess, onFailure, onAlways );
      }
      catch(e)
      {
      }
      parent.document.location.replace( "/billing.html" );
   });
</script>
