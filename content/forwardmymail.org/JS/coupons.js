var pagination = function () {
			var items = $(".coupon-main");
			var numItems = items.length;
			var perPage = 10;						
			items.slice(perPage).hide();
			
			$(".pagination-page").pagination({
				items: numItems,
				itemsOnPage: perPage,
				cssStyle: "light-theme",

				onPageClick: function(pageNumber) {

					var showFrom = perPage * (pageNumber - 1);
					var showTo = showFrom + perPage;

					items.hide()
						.slice(showFrom, showTo).show();
				}
			});	
}

$.ajax(
{
    url: '/fmtc.jsp',
        dataType: 'xml',		
        type:'POST',
		beforeSend: function(){
			 $("#loading").show();
		   },
		   complete: function(){
			 $("#loading").hide();
		   },		 
        success: function(xml){			 
			result = "<div class='coupons col-md-10 col-md-push-1'>";
    
			$(xml).find('item').each(function() {
						var couponMerchant = $(this).find("merchantname").text();
						var couponDescription = $(this).find("label").text();
						var couponUrl = $(this).find("affiliateurl").text();
						var couponType = $(this).find("dealtypes").text();
						var couponCode = $(this).find("couponcode").text();
						var couponImage = $(this).find("image").text();
						
						
						if(couponImage.length <= 0) {
							couponImage = "{CSS-CLOUD}/img/coupon.jpg"
						}
						
						result += "<div class='col-xs-12 col-sm-6 col-md-6 coupon-main'>"
						result += "	<a href='" + couponUrl + "' target='_blank'>"
						result += "		<div class='coupon-container row'>";
						result += "			<div class='coupon-image col-xs-12 col-sm-3 col-md-3'><img class='img-responsive' src='" + couponImage + "' /></div>"
						result += "			<div class='coupon-content col-xs-12 col-sm-9 col-md-9'>"				
						result += "				<h1 class='coupon-title'>" + couponMerchant + "</h1>";
						result += "				<div class='coupon-description'><p>" + couponDescription + "</p></div>";
						if(couponCode.length > 0) {result += "				<div class='coupon-code'><p>Coupon code: " + couponCode + "</p></div>";}
						result += "				<div class='coupon-url'><p>" + "Click here for details" + "</p></div>";
						result += "			</div>";
						result += "		</div>";

						result += "	</a>";				
						result += "	</div>";
			});
			
			result += "</div>";
			$("#result").html(result);
			pagination();
		}
	
});
