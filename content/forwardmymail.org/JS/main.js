$(document).ready(function() {
   var hrf = document.location.href;

   //--- aim track ---
   if( _AIM_!="null" )
   {
      if( (hrf.indexOf("index.html")!=-1) )
      {
         var aimLink = "//aimpixel.com/h/?hash=" + _AIM_ + "&p1=g8labs&p2=" + domainName + "&p3=visit";
         try { $.ajax({ "url":aimLink }); } catch(e) { }
      }
      if( (hrf.indexOf("/checklist/")!=-1) )
      {
         var aimLink = "//aimpixel.com/h/?hash=" + _AIM_ + "&p1=g8labs&p2=" + domainName + "&p3=click";
         try { $.ajax({ "url":aimLink }); } catch(e) { }
      }
   }
      
   if( (hrf.indexOf("billing.html")!=-1) )
   {
  
      itemCart( "checklist-thumb",  "Full Easy Guide",      "Moving Simplified Easy Guide &trade;",   true );
      $("#item").val( "Full Easy Guide" );
      
      /*--- prepop right billing column ---*/
      $("#moving_type").text( _PROFILE["moving_type"] );
      $("#usps_forward").text( _PROFILE["usps_forward"] );
      
      var radzip = "";
      radzip += "<table cellpadding=0 cellspacing=0 border=0>";
      
      radzip += "<tr>";
      radzip +=    "<th style='padding-right:10px; font-size:12px;'><input checked value='"+_PROFILE["zip_1"]+"' name='othzip' id='o001' onClick=\"{setZip(this); $('#payment-address').hide(500);}\" type='radio'> <label for='o001'>Old Address :</label> </th>";
      radzip +=    "<td style='font-size:12px;'>"+_PROFILE["address_1"]+" "+_PROFILE["city_1"]+" "+_PROFILE["state_1"]+" "+_PROFILE["zip_1"]+"</td>";
      radzip += "</tr>";

      radzip += "<tr>";
      radzip += "<th style='padding-right:10px; font-size:12px;'><input value='"+_PROFILE["zip_2"]+"' name='othzip' id='o002' onClick=\"{setZip(this); $('#payment-address').hide(500);}\" type='radio'> <label for='o002'>New Address :</label> </th>";
      radzip += "<td style='font-size:12px;'>"+_PROFILE["address_2"]+" "+_PROFILE["city_2"]+" "+_PROFILE["state_2"]+" "+_PROFILE["zip_2"]+"</td>";
      radzip += "</tr>";
     
      radzip += "<tr>";
      radzip +=    "<th style='padding-right:10px; font-size:12px;'><input name='othzip' id='o003' onClick=\"{document.frm.zip.value=document.frm.otherzip.value; $('#payment-address').show(500);}\" type='radio'> <label for='o003'>Other Address :</label> </th>";
      radzip +=    "<td style='font-size:12px;'>Specify street address and zip code</td>";
      radzip += "</tr>";

      radzip += "<tr>";
      radzip +=    "<td colspan='2' id='otheraddr' style='display:none'><input style='padding:3px; font-size:12px; width:200px' placeholder='Address'>&nbsp;<input onChange='document.frm.zip.value=document.frm.otherzip.value;' id='otherzip' name='otherzip' style='padding:3px; font-size:12px; width:70px' placeholder='Zip Code'></td>";
      radzip += "</tr>";


 
      radzip += "</table>";

      $("#pickzip").html(radzip);


      $("#card_name").val( $("#first_name").val() + " " + $("#last_name").val() );
	   

   }
   
   
   


   
$('.terms-box').click(function () {

  if ($(this).find('input:checkbox').is(":checked")) {

    $(this).find('input:checkbox').attr("checked", false);
  }
  else {
      $(this).find('input:checkbox').prop("checked", true);
  }
   
});


 $('input[type=checkbox]').click(function (e) {
     e.stopPropagation();
 });   
   
   
$('.radioWrap').click(function () {    
   var val =  $(this).find('input:radio').prop('checked')?false:true;
   $(this).find('input:radio').prop('checked', val);
});   

 $('input[type=radio]').click(function (e) {
     e.stopPropagation();
 });     

 //Populate billing fields with profile info 
  var businessName =  _PROFILE["businessName"];
  if(businessName !== undefined) {
	$(".profile_business_name").html( _PROFILE["businessName"] );
  }
  else {
	$(".profile_business_name").html( "N/A" );
  }
  
  $(".profile_residence_status").html( _PROFILE["residence_status"] );   

//Change address based on selected radio button
var oldAddress = _PROFILE["address_1"];
var oldSuite = _PROFILE["suite_1"];
var oldCity = _PROFILE["city_1"];
var oldState = _PROFILE["state_1"];
var oldZip = _PROFILE["zip_1"];

var newAddress = _PROFILE["address_2"];
var newSuite = _PROFILE["suite_2"];
var newCity = _PROFILE["city_2"];
var newState = _PROFILE["state_2"];
var newZip = _PROFILE["zip_2"];


$('input#address').val(oldAddress);
$('input#suite').val(oldSuite);
$('input#city').val(oldCity);
$('select#state').val(oldState);
$('input#zip').val(oldZip);
$('input#phone').val(_PROFILE["phone_1"]);

$("input[name='select_address']").on("change", function(){
	if ($(this).val() == "new_address") {
		$('input#address').val(newAddress);
		$('input#suite').val(newSuite);
		$('input#city').val(newCity);
		$('select#state').val(newState);
		$('input#zip').val(newZip);
	}
	else if ($(this).val() == "old_address") {
		$('input#address').val(oldAddress);
		$('input#suite').val(oldSuite);
		$('input#city').val(oldCity);
		$('select#state').val(oldState);
		$('input#zip').val(oldZip);
	}
	else if ($(this).val() == "different_address") {
		$('input#address').val("").focus();
		$('input#suite').val("");
		$('input#city').val("");
		$('select#state').val($("#state option:first").val());
		$('input#zip').val("");			
	}
});
 
});
function setZip(o)
{
   document.frm.zip.value = o.value;
}

function postUpsell( num )
{
   $("#billingSubmit").slideUp();
   $("#gift-card-div").slideDown();
   $(".rcoupon").html( num );
   $("#processError").hide();
   $("#submitLoad").hide();
}

function toggleOtherAddress(){
   
   
   
}
function postPayment( json, user )
{
   parent.document.location.replace( "/checklist/change-of-address/letters.html" );
}   
