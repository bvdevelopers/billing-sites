$(document).ready(function() {
   var hrf = document.location.href;
   
   if( (hrf.indexOf("billing")!=-1) )
   {
      itemCart( "checklist-thumb",  "Full Easy Guide",      "Moving Simplified Easy Guide &trade;",   true );
      $("#item").val( "Full Easy Guide" );
      
      /*--- prepop right billing column ---*/
      $("#moving_type").text( _PROFILE["moving_type"] );
      $("#usps_forward").text( _PROFILE["usps_forward"] );
      
      var radzip = "";
      radzip += "<table cellpadding=0 cellspacing=0 border=0><tr>";
      
      radzip +=    "<td><input checked value='"+_PROFILE["zip_1"]+"' name='othzip' id='o001' onClick=\"{setZip(this); otherzip.style.display='none';}\"} type='radio'></td>";
      radzip +=    "<td style='padding-top:5px; padding-left:2px;'><label style='margin:0px;' for='o001'>" + _PROFILE["zip_1"] + "</label></td>";
      radzip +=    "<td width='20'></td>";
      if( _PROFILE["zip_1"] != _PROFILE["zip_2"] )
      {
         radzip +=    "<td><input value='"+_PROFILE["zip_2"]+"' name='othzip' id='o002' onClick=\"{setZip(this); otherzip.style.display='none';}\"} type='radio'></td>";
         radzip +=    "<td style='padding-top:5px; padding-left:2px;'><label style='margin:0px;' for='o002'>" + _PROFILE["zip_2"] + "</label></td>";
         radzip +=    "<td width='20'></td>";
      }
      radzip +=    "<td><input name='othzip' id='o003' onClick={otherzip.style.display='block';} type='radio'></td>";
      radzip +=    "<td style='padding-top:5px; padding-left:2px;'><label style='margin:0px;' for='o003'>Other</label></td>";
      radzip +=    "<td width='5'></td>";
      radzip +=    "<td><input onChange='setZip(this);' style='display:none; width:50px;' id='otherzip' name='otherzip' type='text' maxlength='5'></td>";
      
      radzip += "</tr></table>";
      $("#pickzip").html(radzip);


      $("#card_name").val( $("#first_name").val() + " " + $("#last_name").val() );
   }
});
function setZip(o)
{
   document.frm.zip.value = o.value;
}


