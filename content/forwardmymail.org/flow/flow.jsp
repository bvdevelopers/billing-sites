<%@page import="java.util.*, com.util.*, com.bl.*"%>
<script src="//api.direct-market.com/client/leadclient2.js"></script>
<%
   HashMap hm = (HashMap) request.getAttribute( "data" );
   try
   {
      String  downloadId =  (String)  request.getAttribute( "downloadId" );
      session.setAttribute( "download", downloadId );
      session.setAttribute( "checklist", "/change-of-address/letters.html" );
      session.setMaxInactiveInterval( 900 );
      String            domain = Web.getDomainName(  request );
//    Queries.postLead( domain, "adt", ""+hm.get("id"), 60, "" );
   }
   catch( Exception e )
   {
      out.print(e);
   }
//   com.util.Web.redirect( response, com.util.Web.MOVE_PERMANENT ,"/billing.html" );
%>
<script>
var moveType = "Individual";
try
{
   moveType                  = "<%=hm.get("moving_type" )%>";   
   window.client = new LeadClient("7c55dd9f8621-4ffc-9c6a-eb3b75c7f2c4");
   client.lead.first_name    = "<%=hm.get("first_name" )%>";
   client.lead.last_name     = "<%=hm.get("last_name" )%>";
   client.lead.address1      = "<%=hm.get("address_2")%> <%=hm.get("suite_2")%>";
   client.lead.city          = "<%=hm.get("city_2")%>";;
   client.lead.state         = "<%=hm.get("state_2")%>";
   client.lead.zip_code      = "<%=hm.get("zip_2")%>";
   client.lead.gender        = "";
   client.lead.dob_month     = "";
   client.lead.dob_day       = "";
   client.lead.dob_year      = "";
   client.lead.phone_home    = "<%=hm.get("phone_1")%>";
   client.lead.email         = "<%=hm.get("email")%>";
   client.lead.lead_id_token = "<%=hm.get("universal_leadid")%>";


   var onSuccess = function (data, textStatus, jqXhr) {};
   var onFailure = function (jqXhr, textStatus, errorThrown) {};
   var onAlways = function () {};
   try {
      ga(function (tracker) {
      var gaId = tracker.get('clientId');
      client.lead.ga_id = gaId;});
   }
   catch (e) { };
   
   //LEAD CLIENT POST METHOD
   client.Post(onSuccess, onFailure, onAlways);

}
catch(e)
{
}
if(moveType == "Business") 
{
   document.location.replace("/business.html");
}
else 
{
   document.location.replace("/billing.html");
}
</script>
