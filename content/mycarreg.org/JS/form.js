$(document).ready(function()
{
   // --- phone_1 focus
   //$("#phone_1").focus( function() { $("#_msg_phone_1").removeClass("err_on").addClass("hint_msg"); $("#_msg_phone_1").html("Can be same as cell Phone");  } );
   //$("#phone_1").blur(  function() { $("#_msg_phone_1").removeClass("hint_msg").addClass("err_on"); $("#_msg_phone_1").html("");                           } );
   
   // --- blueKai
   var _h = document.location.href;
   if( _h.indexOf("/forms/step1/") != -1 )
   {
      // start step1 changes
      $("#first_name").focus();
      $("#zip_1").attr("maxlength",5);
      $("#phone_1").attr("maxlength","12");

      try
      {
         var state = initCap(document.location.href.split("/")[6].split(".")[0]);  //get state from url
         $("#formh1").html( "Get your "+ state +" Road Advisors&trade;" );
      }
      catch(e){}
      
      hrf = document.location.href.split( "/" );
      hrf = initCap( hrf[href.length-1].split(".")[0] );
      
      var map =   {  

         "id-cards"                       : ["ID Cards",                       "Licensed Driver"],
         "new-car-registration"           : ["New Car Registration",           "Licensed Driver"],
         "renew-car-registration"         : ["Renew Car Registration",         "Licensed Driver"],
         "reinstate-suspended-license"    : ["Reinstate Suspended License",    "Licensed Driver"],
         "new-driver-license"             : ["New Drivers License",            "Licensed Driver"],
         "renew-driver-license"           : ["Renew Drivers License",          "Licensed Driver"],
         "replace-driver-license"         : ["Replace Drivers License",        "Licensed Driver"],
         "change-of-address"              : ["Change Of Address",              "Licensed Driver"],
         "change-name-on-registration"    : ["Change Name on Registration",    "Licensed Driver"],
         "replace-registration"           : ["Replace Registration",           "Licensed Driver"],
         "change-address-on-registration" : ["Change Address on Registration", "Licensed Driver"],
         "change-of-name"                 : ["Change of Name",                 "Professional Driver"]
      
      };
      
      __spt = _h.split( "/" );
      __zip = $("#zip_1").val();
      __sta = $("#state_1").val();
      __act = __spt[__spt.length-2];
      
      _imSrc = "https://stags.bluekai.com/site/4159?phint=wiki%3Ddriving&phint=wiki%3Ddriverslicense&phint=local%20services%3Dgovernment%20services&phint=local%20services%3Dinsurance&phint=in%20market%3Dauto%20insurance&phint=in%20market%3DAutomotive Services&phint=intent%3D" + escape(map[__act][0]) + "&phint=driver%20class%3D" + escape(map[__act][1]) + "&phint=state%3D" + escape(__sta) + "&phint=zip%3D" + __zip;
      if( __act=="change-of-address" )
         _imSrc += "&phint=in%20market%3Dmoving";
      $('body').append( "<img height=\"1\" width=\"1\" src=\"" + _imSrc + "\">" );
   }
   else
   if( document.location.href.indexOf("/forms/coa/")!=-1 )
   {
      $("#address_1").attr("tabindex",  10);
      $("#suite_1").attr(  "tabindex",  20);
      $("#city_1").attr(   "tabindex",  30);
      $("#state_1").attr(  "tabindex",  40);
      $("#zip_1").attr(    "tabindex",  50);
      
      $("#address_2").attr("tabindex",  60);
      $("#suite_2").attr(  "tabindex",  70);
      $("#city_2").attr(   "tabindex",  80);
      $("#state_2").attr(  "tabindex",  90);
      $("#zip_2").attr(    "tabindex", 100);
      
      //------------------------------------------------------------------------
      //---- zip_2
      //------------------------------------------------------------------------
      $("#zip_2").focus( function() { $("#_msg_zip_2").removeClass("err_on").addClass("hint_msg"); $("#_msg_zip_2").html("leave blank if unknown");  } );
      $("#zip_2").blur(  function() { $("#_msg_zip_2").removeClass("hint_msg").addClass("err_on"); $("#_msg_zip_2").html("");                        } );
      
      
      //------------------------------------------------------------------------
      //---- Have you already changed your address with the U.S  Postal Service?
      //------------------------------------------------------------------------
      $("#main_27").hide();
      $("#usps").change(function(){
          if( $(this).val() == "N" )
          {
             $("#main_27").fadeIn(  'slow', function() { $(this).show();});
             if( $("#forwarding_service").val()=="Y" )
               $("#main_29").fadeIn(  'slow', function() { $(this).show();});
          }
          else
          {
             $("#main_27").fadeOut(  'slow', function() { $(this).hide();});
             $("#main_29").fadeOut(  'slow', function() { $(this).hide();});
          }
      });
      
      //--------------------------------------------------
      //---- Would you like to use our forwarding service?
      //--------------------------------------------------
      $("#main_29").hide();
      $("#forwarding_service").change(function(){
          if( $(this).val() == "Y" )
             $("#main_29").fadeIn(  'slow', function() { $(this).show();});
          else
             $("#main_29").fadeOut(  'slow', function() { $(this).hide();});
      });
      /*
      //--------------
      //---- Move Type
      //--------------
      $("#extra_22, #extra_23").hide();
      $("#moving_type").change(function(){
          if( $(this).val() == "Business" )
             $("#extra_22, #extra_23").fadeIn(  'slow', function() { $(this).show();});
          else
             $("#extra_22, #extra_23").fadeOut(  'slow', function() { $(this).hide();});
      });
      */
      //--------------
      //---- Move Time
      //--------------
      $("#extra_11").hide();
      $("#moving_time").change(function(){
          if( $(this).val() == "Temporary" )
             $("#extra_11").fadeIn(  'slow', function() { $(this).show();});
          else
             $("#extra_11").fadeOut(  'slow', function() { $(this).hide();});
      });      

      //----------------------------------------------------
      //---- Have you already relocated to your new address?
      //----------------------------------------------------
      $("#extra_1e, #extra_1f, extra_1g").hide();
      $("#extra_3").hide();
      $("#already_moved_in").change(function(){
          if( $(this).val() == "Y" )
          {
            $("#extra_1e, #extra_1f, extra_1g").fadeIn(  'slow', function() { $(this).show();});
             if( $("#know_date_moving_in").val()=="Y" )
               $("#extra_3").fadeIn(  'slow', function() { $(this).show();});
          }
          else
          {
            $("#extra_1e, #extra_1f, extra_1g").fadeOut(  'slow', function() { $(this).hide();});
            $("#extra_3").fadeOut(  'slow', function() { $(this).hide();});
          }
      });
      $("#know_date_moving_in").change(function(){
          if( $(this).val() == "Y" )
            $("#extra_3").fadeIn(  'slow', function() { $(this).show();});
          else
            $("#extra_3").fadeOut(  'hide', function() { $(this).hide();});
      });
   }
});

var _h = document.location.href;
/*if( _h.indexOf("/forms/step1/")!=-1 )
{
   function validateStep1FormFields()
   {
     
      var fullDate = $("#_dd_date_of_birth, #_mm_date_of_birth, #_yy_date_of_birth");
      var getZip = $("#zip_1").val();
      var ff = navigator.userAgent.indexOf("Firefox");

      $("#first_name").on( "focusout", function(){
        if( $("#first_name").val() == "" ) {
           m = $("#_msg_first_name");
           m.html( "First Name is required" );
           fadeErr(m);
           //m.css({"width":"140px","float":"left"});
           errs = true;
        }else{
           m = $("#_msg_first_name");
           m.html( "" );
           fadeErr(m);
           //$(this).parent().prev().children('label').removeClass("errorlabel");
           //m.css({"width":"auto","float":"none"});
           errs = false;
        }
        
      });
      $("#last_name").on( "focusout", function(){
         if( $("#last_name").val() == "" ) {
            m = $("#_msg_last_name");
            m.html( "Last Name is required" );
            fadeErr(m);
            //m.css({"width":"140px","float":"left"});
            errs = true;
         }else{
            m = $("#_msg_last_name");
            m.html( "" );
            fadeErr(m);
            //$(this).parent().prev().children('label').removeClass("errorlabel");
            //m.css({"width":"auto","float":"none"});
            errs = false;
         }
      });
      $("#address_1").on( "focusout", function(){
         if( $("#address_1").val() == "" ) {
            m = $("#_msg_address_1");
            m.html( "Address is required" );
            fadeErr(m);
            //m.css({"width":"140px","float":"left"});
            errs = true;
         }else{
            m = $("#_msg_address_1");
            m.html( "" );
            fadeErr(m);
            //$(this).parent().prev().children('label').removeClass("errorlabel");
            //m.css({"width":"auto","float":"none"});
            errs = false;
         }
      });
      $("#city_1").on( "focusout", function(){
         if( $("#city_1").val() == "" ) {
            m = $("#_msg_city_1");
            m.html( "City is required" );
            fadeErr(m);
            //m.css({"width":"140px","float":"left"});
            errs = true;
         }else{
            m = $("#_msg_city_1");
            m.html( "" );
            fadeErr(m);
            //$(this).parent().prev().children('label').removeClass("errorlabel");
            //m.css({"width":"auto","float":"none"});
            errs = false;
         }
      });
      $("#phone_1").on( "focusout", function(){
         if( $("#phone_1").val() == "" ) {
            m = $("#_msg_phone_1");
            m.html( "Primary Phone is required" );
            fadeErr(m);
            //m.css({"width":"140px","float":"left"});
            errs = true;
         }else if($("#phone_1").val().length < 10){
            m = $("#_msg_phone_1");
            m.html( "Invalid US Phone Number");
            fadeErr(m);
            //m.css({"width":"140px","float":"left"});
            errs = true;
         }else if($("#phone_1").val().length > 10 && $("#phone_1").val().indexOf("-") == -1){
            m = $("#_msg_phone_1");
            m.html( "Invalid US Phone Number");
            fadeErr(m);
            errs = true;
         }else{
            m = $("#_msg_phone_1");
            m.html( "" );
            fadeErr(m);
            //$(this).parent().prev().children('label').removeClass("errorlabel");
            //m.css({"width":"auto","float":"none"});
            errs = false;
         }
      });
      $("#gender").on( "focusout", function(){
         if( $("#gender").val() == "" ) {
            m = $("#_msg_gender");
            m.html( "Gender is required" );
            fadeErr(m);
            //m.css({"width":"140px","float":"left"});
            errs = true;
         }else{
            m = $("#_msg_gender");
            m.html( "" );
            fadeErr(m);
            //$(this).parent().prev().children('label').removeClass("errorlabel");
            //m.css({"width":"auto","float":"none"});
            errs = false;
         }
      });

      $("#state_1").on( "focusout", function(){
         if ( $("#state_1").val() =="" )
         {
            m = $("#_msg_state_1");
            m.html( "State is required" );
            fadeErr(m);
            //m.css({"width":"140px","float":"left"});
            errs = true;
         }else{
            m = $("#_msg_state_1");
            m.html( "" );
            fadeErr(m);
            //$(this).parent().prev().children('label').removeClass("errorlabel");
            //m.css({"width":"auto","float":"none"});
            errs = false;
         }
      });
      
      $("#email").on( "focusout", function(){
         var dEmail = $("#email").val();

         if( dEmail == "" ) {
            m = $("#_msg_email");
            m.html( "Email is required" );
            fadeErr(m);
            //m.css({"width":"140px","float":"left"});
            errs = true;
         }else if( dEmail.indexOf("@") == -1 || dEmail.indexOf(".") == -1 ) {
            m = $("#_msg_email");
            m.html( "Email format invalid" );
            fadeErr(m);
            //m.css({"width":"140px","float":"left"});
            errs = true;
         }else{
            m = $("#_msg_email");
            m.html( "" );
            fadeErr(m);
            //$(this).parent().prev().children('label').removeClass("errorlabel");
            //m.css({"width":"auto","float":"none"});
            errs = false;
         }
         
         
      });
      if( ff == -1 )
      {
         $("#zip_1").keypress(function(key){
            if(key.charCode < 48 || key.charCode > 57)
            {
               m = $("#_msg_zip_1");
               m.html( "Invalid US ZipCode" );
               fadeErr(m);
               //m.css({"width":"140px","float":"left"});
               errs = true;
               $("#zip_1").attr("value","");
            }else{
               m = $("#_msg_zip_1");
               m.html( "" );
               fadeErr(m);
               //$(this).parent().prev().children('label').removeClass("errorlabel");
               //m.css({"width":"auto","float":"none"});
               errs = false;
            }
         });
      }else{
         $("#zip_1").on( "blur", function(){
            var numberRegex = /^([0-9]{5})$/;
            if( numberRegex.test( $("#zip_1").attr("value") ) )
            {
               m = $("#_msg_zip_1");
               m.html( "" );
               fadeErr(m);
               //$(this).parent().prev().children('label').removeClass("errorlabel");
               //m.css({"width":"auto","float":"none"});
               errs = false;
            }else{
               m = $("#_msg_zip_1");
               m.html( "Invalid US ZipCode" );
               fadeErr(m);
               //m.css({"width":"140px","float":"left"});
               errs = true;
            }
         });   
      }
      $("#zip_1").on( "focusout", function(){
         if( $("#zip_1").val() == "") {
            m = $("#_msg_zip_1");
            m.html( "ZipCode is required" );
            fadeErr(m);
            //m.css({"width":"140px","float":"left"});
            errs = true;
         }else if($("#zip_1").val().length - 5 ){
            m = $("#_msg_zip_1");
            m.html( "Invalid US ZipCode" );
            fadeErr(m);
            //m.css({"width":"140px","float":"left"});
            $("#zip_1").focus();
            errs = true;            
         }else{
            m = $("#_msg_zip_1");
            m.html( "" );
            fadeErr(m);
            //$(this).parent().prev().children('label').removeClass("errorlabel");
            //m.css({"width":"auto","float":"none"});
            errs = false;
         }
      });
      
      fullDate.on( "focusout", function(){
         if(  $("#_dd_date_of_birth").val() == 0 || $("#_mm_date_of_birth").val() == 0 || $("#_yy_date_of_birth").val() == 0 )
         {
            m = $("#_msg_date_of_birth");
            m.html( "Birth Date is required" );
            fadeErr(m);
            //m.css({"width":"140px","float":"left"});
            errs = true;         
         }else{
            if( $("#_dd_date_of_birth").val() != 0 && $("#_mm_date_of_birth").val() != 0 && $("#_yy_date_of_birth").val() != 0 )
            {
               m = $("#_msg_date_of_birth");
               m.html( "" );
               fadeErr(m);
               //$(this).parent().prev().children('label').removeClass("errorlabel");
               //m.css({"width":"auto","float":"none"});
               errs = false;
            } 
         }
      });
      return false;
   }
   validateStep1FormFields();
}*/

function customValidate()
{
   //validateStep1FormFields();

   var _h = document.location.href;
   if( _h.indexOf("/forms/coa/")!=-1 )
   {
      if( $("#email").val() != $("#email_confirm").val() )
      {
         setErrMsg( "email_confirm", "Invalid Confirmation" )
         return false;
      }
      if( $("#forwarding_service").is(":visible") && $("#forwarding_service").val()=="Y" )
      {
         chk1 = checkAddress( 1, "Old",    null,            null );
         chk2 = checkAddress( 2, "Moving", "moving_type",   "newAddressError" );
         return (chk1 && chk2);
      }
   }
   
   return true;
}