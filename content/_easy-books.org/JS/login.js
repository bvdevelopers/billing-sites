function login()
{
   $( "#login-error" ).hide();
   var _login = $("#user"    ).val();
   var _pass  = $("#password").val();
   if( $.trim(_login)=="" || $.trim(_pass)=="" )
   {
      var _error = "Username and password required";
      if( $.trim(_login)=="" && $.trim(_pass)=="" )
      {
         _error  = "Username and password required";
         $( "#user" ).focus();
      }
      else if( $.trim(_login)=="" )
      {
         _error  = "Username required";
         $( "#user" ).focus();
      }
      else if( $.trim(_pass)=="" )
      {
         _error  = "Password required";
         $( "#password" ).focus();
      }
      $( "#login-error" ).html( _error );
      $( "#login-error" ).show();
   }
   else
   {
      $( "#login-button" ).hide();
      $( ".loader-spin" ).show();
      var link = "/login.jsp?user=" + escape(_login) + "&password=" + escape(_pass);
      $.ajax(  {  "url"          :  link,
                  "success"      :  function(response){
                     if( response.status!="OK" )
                     {
                        $( "#login-error" ).html( "Username or password incorrect" );
                        $( ".loader-spin" ).hide();
                        $( "#login-error" ).show();
                        $( "#login-button" ).show();
                        $( "#password" ).focus();
                     }
                     else
                     {
                        try { postLogin(response); } catch(e) {}
                     }
                  }
               });
   }
}

function changepass()
{
   $( "#login-error" ).hide();
   var _old_pass = $("#old_pass").val();
   var _new_pass = $("#new_pass").val();
   var _confirm  = $("#confirm" ).val();
   if( $.trim(_old_pass)=="" || $.trim(_new_pass)=="" || $.trim(_confirm)=="" )
   {
      var _error = "";
      var focus  = true;
      if( $.trim(_old_pass)=="" )
      {
         _error  += "Current Password required<br/>";
         if( focus ) $( "#old_pass" ).focus();
         focus    = false;
      }
      if( $.trim(_new_pass)=="" )
      {
         _error  += "New Password required<br/>";
         if( focus ) $( "#new_pass" ).focus();
         focus    = false;
      }
      if( $.trim(_confirm)=="" )
      {
         _error  += "Confirm Password required<br/>";
         if( focus ) $( "#confirm" ).focus();
      }
      
      _error = _error.substring(0,_error.length-5); //remove last <br/>
      $( "#login-error" ).html( _error );
      $( "#login-error" ).show();
   }
   else
   {
      $( "#login-button" ).hide();
      $( ".loader-spin" ).show();
      var link = "/password.jsp?old=" + escape(_old_pass) + "&new=" + escape(_new_pass) + "&confirm=" + escape(_confirm);
      $.ajax(  {  "url"          :  link,
                  "success"      :  function(response){
                     if( response.status!="OK" )
                     {
                        $( "#login-error" ).html( response.status );
                        $( ".loader-spin" ).hide();
                        $( "#login-error" ).show();
                        $( "#login-button" ).show();
                        if( response.status=="Invalid Current Password" ) $( "#old_pass" ).focus();
                        else $( "#new_pass" ).focus();
                     }
                     else
                     {
                        try { postChangePass(); } catch(e) {}
                     }
                  }
               });

   }
}


function forgotpass()
{
   $( "#login-error" ).hide();
   var email = $("#email").val();
   if( $.trim(email)=="" )
   {
      $( "#login-error" ).html( "Email Address required" );
      $( "#login-error" ).show();
      $( "#email" ).focus();
   }
   else
   {
      $( "#login-button" ).hide();
      $( ".loader-spin" ).show();
      var link = "/forgot_password.jsp?email=" + escape(email);
      $.ajax(  {  "url"          :  link,
                  "success"      :  function(response){
                     if( response.status!="OK" )
                     {
                        $( "#login-error" ).html( response.status );
                        $( ".loader-spin" ).hide();
                        $( "#login-error" ).show();
                        $( "#login-button" ).show();
                        $( "#email" ).focus();
                     }
                     else
                     {
                        try { postForgotPass(); } catch(e) {}
                     }
                  }
               });
   }
}


