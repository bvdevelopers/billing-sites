var statenames = ['alabama','alaska','arizona','arkansas','california','colorado','connecticut','delaware','florida','georgia','hawaii','idaho','illinois','indiana','iowa','kansas','kentucky','louisiana','maine','maryland','massachusetts','michigan','minnesota','mississippi','missouri','montana','nebraska','nevada','new-hampshire','new-jersey','new-mexico','new-york','north-carolina','north-dakota','ohio','oklahoma','oregon','pennsylvania','rhode-island','south-carolina','south-dakota','tennessee','texas','utah','vermont', 'virginia','washington','west-virginia','wisconsin','wyoming'];

var active_states = ["florida", "california", "colorado","hawaii", "minnesota", "texas", "north-carolina", "louisiana" ];


  var hrf = document.location.href;
  var up = hrf.split('/');
  var cat = up[3];
  var category = cat.replace(/-/g, ' ');
  var file_name = up[up.length-1].substring(0, up[up.length-1].length-5);

 var fladdata = [
	{
		"category":"auto",
		"subcategory": [
			{
				"drivers-license": [
					{
						"title": "replace-drivers-license",
						"description": "Get information about how to replace your Virginia driver's license. Click below for a comprehensive application assistance guide about license replacement information and more."
					},
					{
						"title": "renew-drivers-license",
						"description": "Learn how to renew your Virginia driver's license. Click below for a comprehensive application assistance guide about DMV drivers license renewal and more."
					},
					{
						"title": "reinstate-suspended-license",
						"description": "Learn how to renew your Virginia driver's license. Click below for a comprehensive application assistance guide about DMV drivers license renewal and more."
					},
					{
						"title": "reinstate-suspended-license",
						"description": "Learn how to renew your Virginia driver's license. Click below for a comprehensive application assistance guide about DMV drivers license renewal and more."
					},
					{
						"title": "new-drivers-license",
						"description": "New driver's license applicants in Virginia may need extra assistance to get the application process right. Click below for a comprehensive drivers license application assistance guide!"
					},
					{
						"title": "learners-permit",
						"description": "First-time driver looking to get your Virginia learner's permit? Click below for a comprehensive application assistance guide about how to get a learners permit and more."
					},
					{
						"title": "id-cards",
						"description": "Find out how to get your Virginia Identification card quickly and easily. Click below for a comprehensive application assistance guide about obtaining an ID card and more."
					},
					{
						"title": "change-of-name",
						"description": "If you change your name, you must update your Virginia driver's license or ID. Click below for a comprehensive application assistance guide about how to change your name on your license!"
					},
					{
						"title": "change-of-address",
						"description": "Requires that you notify the DMV of any change of address within 30 days. Click below for a comprehensive application assistance guide about how to change your address."
					}
				],
				"car-registration": [
					{
						"title": "change-of-address",
						"description": "Any change of address must be reflected on your Virginia car registration. Click below for a comprehensive application assistance guide about how to change your address."
					},
					{
						"title": "change-of-name",
						"description": "You must report a change of name on your Virginia car registration. Click below for a comprehensive application assistance guide about how to update your car registration."
					},
					{
						"title": "new-registration",
						"description": "Need a new car registration for a Virginia vehicle? Click below for a comprehensive application assistance guide about how to get your new car registration and more."
					},
					{
						"title": "renew-registration",
						"description": "Is your car registration about to expire? Click below for a comprehensive application assistance guide about how to renew your car registration and more."
					},
					{
						"title": "replace-registration",
						"description": "Learn how to get a duplicate car registration. Click below for a comprehensive application assistance guide about how to replace your car registration and more."
					}
				],
				"car-title": [
					{
						"title": "change-of-address",
						"description": "Your car title must include your current address. Click below for a comprehensive application assistance guide about how to change the address on your car title and more."
					},
					{
						"title": "change-of-name",
						"description": "Find out how to change your name on your Virginia car title. Click below for a comprehensive application assistance guide about how to update your name on your car title and more."
					},
					{
						"title": "new-title",
						"description": "Learn how to get your new car title in Virginia. Click below for a comprehensive application assistance guide about how to obtain a new car title and much more."
					},
					{
						"title": "replace-title",
						"description": "Need to replace your car title? Click below for a comprehensive application assistance guide about how to obtain a duplicate car title if it was lost, stolen or damaged."
					}
				]
			}
		]
	},
	{
		"category":"travel",
		"subcategory": [
			{
				"title": "travel-precheck",
				"description": "Find out if you qualify for TSA travel PreCheck and how to apply. Click below for a comprehensive application assistance guide about how to register for TSA PreCheck and more."
			},
			{
				"title": "airport-precheck",
				"description": "Learn about airport TSA PreCheck procedures and eligibility. Click below for a comprehensive application assistance guide covering how to sign up as efficiently as possible."
			},
			{
				"title": "global-entry",
				"description": "Wondering if you qualify for the Global Entry program? Click below for a comprehensive application assistance guide about how to gain priority status at airports worldwide."
			}
		]
	},
	{
		"category":"federal-resources",
		"subcategory": [
			{
				"title": "food-stamp-guide",
				"description": "For all the information you need to know about food stamps, click below for a comprehensive application assistance guide to assist with and simplify the SNAP registration process."
			},
			{
				"title": "medicaid-help",
				"description": "Learn whether you qualify for Medicaid and how to enroll. Click below for a comprehensive application assistance guide about how to register for Medicaid and more."
			}
		]
	},
	{
		"category":"recreation",
		"subcategory": [
			{
				"title": "fishing-license",
				"description": "Find out what kind of fishing license you need to legally fish in Virginia. Click below for a comprehensive application assistance guide about how to get your fishing license."
			},
			{
				"title": "hunting-license",
				"description": "Interested in hunting in Virginia? You will need a hunting license to do so. Click below for a comprehensive application assistance guide about how to get your hunting license."
			},
			{
				"title": "name-change",
				"description": "Learn how to officially change your name. Click below for a comprehensive application assistance guide to assist with and simplify the name changing process."
			}
		]
	}
];

$(document).ready(function() {
   

	try {
		var cat = getUrlParam("cat");
		var service = getUrlParam("service");
		if (!("undefined" === typeof cat || "undefined" === typeof service)) {
			var lnk = "/sessionvar.jsp?name=redirectchecklist&value=" + "/checklist/" + getUrl(3) + "/" + cat + "/" + service + "/" + getPage() + ".html";
			$.ajax({
				"url" : lnk
			});
		} else {
			if (!("undefined" === typeof service)) {
				var lnk = "/sessionvar.jsp?name=redirectchecklist&value=" + "/checklist/" + getUrl(3) + "/" + service + "/" + getPage() + ".html";
				$.ajax({
					"url" : lnk
				});
			}

		}
	} catch (e) {}

   
   // DYNAMIC GUIDE DOWNLOAD  
	if( hrf.indexOf("/checklist/") != -1 ) 
	{
		var cat = hrf.split("/")[5];
		var precat = hrf.split("/")[4];
		$(".lk-service-guide-drivers").attr("href",      "/downloadPDF.jsp?f=http://easy-books.org.s3.amazonaws.com/pdf/" + cat + "/" + getPage() + ".pdf");
		$(".lk-service-guide-registration").attr("href", "/downloadPDF.jsp?f=http://easy-books.org.s3.amazonaws.com/pdf/" + cat + "/" + getPage() + ".pdf");
		$(".lk-service-guide-title").attr("href",        "/downloadPDF.jsp?f=http://easy-books.org.s3.amazonaws.com/pdf/" + cat + "/" + getPage() + ".pdf");
		if( hrf.indexOf("fishing-license") != -1 )
      {
         $(".lk-service-guide").attr("href", "/downloadPDF.jsp?f=http://easy-books.org.s3.amazonaws.com/pdf/" + precat + "/" + cat + "/" + getPage() + ".pdf");
      }
      
      if( hrf.indexOf("/auto/") == -1 )
      {
         var globalcheck = hrf.replace(/https?:\/\/[^\/]+/i, "");
          $.ajax({
             "url": "/sessionvar.jsp?name=redirectchecklist&value=" + globalcheck,
             "success": function(response){
             }
          });
      }
	}

  var carreglist = ["new-registration", "renew-registration", "replace-registration", "change-of-name", "change-of-address"];
  var cartitlelist = ["new-title", "replace-title", "change-of-name", "change-of-address"];
  var driverslist = ["new-drivers-license", "renew-drivers-license", "replace-drivers-license", "change-of-name", "change-of-address", "id-cards", "reinstate-suspended-license"];


if( hrf.indexOf("/category/auto.html") != -1 ) {
   $("#type").change(function(){
        var val = $(this).val();
        if (val == "") {
          $(this).css("border", "red");
          alert("Please select a type.");
      }
      else
      {
          if (val == "drivers-license") {
            $('#service').empty();
            for (i = 0; i < driverslist.length; ++i) {
                $("#service").append('<option value="'+ driverslist[i] +'">' + capDashWords(driverslist[i]) + '</option>');
            }
          }
          else if (val == "car-registration") {
            $('#service').empty();
            for (i = 0; i < carreglist.length; ++i) {
                $("#service").append('<option value="'+ carreglist[i] +'">' + capDashWords(carreglist[i]) + '</option>');
            }
          }
          else if (val == "car-title") {
            $('#service').empty();
            for (i = 0; i < cartitlelist.length; ++i) {
                $("#service").append('<option value="'+ cartitlelist[i] +'">' + capDashWords(cartitlelist[i]) + '</option>');
            }
          }
      }
  });

  $("#catautocontinue").click(function(event){
  	  var state = $("#state").val();
      var t     = $("#type").val();
      var s     = $("#service").val();

      if( t=="" ) 
      {
        alert("Please select Type.");
        event.preventDefault();
      }
      else if( s=="" ) 
      {
        alert("Please Select Service.");
        event.preventDefault(); 
      } 
      else 
      {
          var globalcheck = "/checklist/auto/" + t + "/" + s + "/" + state + ".html";
          $.ajax({
             "url": "/sessionvar.jsp?name=redirectchecklist&value=" + globalcheck,
             "success": function(response){
              console.log(globalcheck);
                document.location.href = "/user/form/step1/auto/"+state+".html?cat=" + t + "&service=" + s;
             }
          });
      }
      
  });

}
  

/* =============================================================
  CHANGE RELATED RESOURCES LINKS
==============================================================*/



	try {
		if (hrf.indexOf("/checklist/") != -1) {
      var rrtemplate = '<div class="col-xs-12 col-sm-12 col-md-12"><div class="row list-download border-gray-top"><div class="col-xs-12 col-sm-12 col-md-7  pl-0"><div class="icon-wrapper"><img title="Download" src="{CSS-CLOUD}/img/pdf-small.jpg"></div><p class="mb-0">%s</p></div><div class="col-xs-12 col-sm-12 col-md-5 btn-orange"><button type="button" class="btn btn-primary" style="width:100%" onclick="{document.location.href=\'/downloadPDF.jsp?f={CSS-CLOUD}/pdf/auto/%s\';}">Download</button></div></div></div>';




 			var rrva = { 	
 				titles: ["Acceptable Documents for Titling a Vehicle VA","Antique & Vintage Cars in VA","Buyer Beware Buying Vehicles Online VA","Commercial Drivers Manual VA","Converting a Manufactured Home to Real Property VA","Disability Parking Application Information VA","DMV Guide for Active Duty Members VA","DMV Guide for Familiy Members and Friends of the Recently Deceased VA","Drivers Manual VA","Escort Drivers Manual VA","Experience & Knowledge the keys to safe motorcycle riding VA","Farm Vehicle FAQS VA","Hauling Permit Manual VA","Home Schooled in Car Driver Education Information Sheet VA","How to Title and License a Motor Vehicle VA","Information to Title & Register your Vehicle in VA","Medical Fitness for safe Driving Frequently Asked Questions VA","Medical Fitness for Safe Driving VA","Mopeds in VA","Motor Carrier Manual VA","Motorcycle Operator Manual VA","Motor Vehicle Registration Information Sheet VA","Moving Violations and Point Assessment VA","Obtaining a Drivers License or Identification ID Card VA","Parents in the Drivers Seat VA","Re-establishing your VA Residency Qualifying for a VA Address","Requirements to Obtain for Hire License Plates VA","Size Weight & Equipment Requirements for Trucks Trailers & Towed Vehicles VA","Special Identification Cards for State & Local Government Agencies & Organizations VA","Supplement to the Commercial Driver License Manual","Three Wheel Motorcycle Skills Test","Two Wheel Motorcycle Skill Test VA","Vehicle Safety Inspection VA Insurance Requirements","Voter Registration Application Instructions VA","Your Odometer the Key to your Cars Value VA",],
				links: ["acceptable-documents-for-titling-a-vehicle-va.pdf","antique-and-vintage-cars-in-va.pdf","buyer-beware-buying-vehicles-online-va.pdf","commercial-drivers-manual-va.pdf","converting-a-manufactured-home-to-real-property-va.pdf","disability-parking-application-information-va.pdf","dmv-guide-for-active-duty-members-va.pdf","dmv-guide-for-familiy-members-and-friends-of-the-recently-deceased-va.pdf","drivers-manual-va.pdf","escort-drivers-manual-va.pdf","experience-and-knowledge-the-keys-to-safe-motorcycle-riding-va.pdf","farm-vehicle-faqs-va.pdf","hauling-permit-manual-va.pdf","home-schooled-in-car-driver-education-information-sheet-va.pdf","how-to-title-and-license-a-motor-vehicle-va.pdf","information-to-title-and-register-your-vehicle-in-va.pdf","medical-fitness-for-safe-driving-frequently-asked-questions-va.pdf","medical-fitness-for-safe-driving-va.pdf","mopeds-in-va.pdf","motor-carrier-manual-va.pdf","motorcycle-operator-manual-va.pdf","motor-vehicle-registration-information-sheet-va.pdf","moving-violations-and-point-assessment-va.pdf","obtaining-a-drivers-license-or-identification-id-card-va.pdf","parents-in-the-drivers-seat-va.pdf","re-establishing-your-va-residency-qualifying-for-a-va-address.pdf","requirements-to-obtain-for-hire-license-plates-va.pdf","size-weight-and-equipment-requirements-for-trucks-trailers-and-towed-vehicles-va.pdf","special-identification-cards-for-state-and-local-government-agencies-and-organizations-va.pdf","supplement-to-the-commercial-driver-license-manual-va.pdf","three-wheel-motorcycle-skills-test-va.pdf","two-wheel-motorcycle-skill-test-va.pdf","vehicle-safety-inspection-va-insurance-requirements.pdf","voter-registration-application-instructions-va.pdf","your-odometer-the-key-to-your-Cars-value-va.pdf",] 
			}; 
 			var rfva = { 	
 				titles: ["Address Change Request VA","Affidavit of Vehicle Purchase Price Vehicle 5 Years old or less VA","Application for Assigned Vehicle Identification Number VA","Application for Certificate of Qualification Salespersons License VA","Application for Certificate of Title & Registration VA","Application for Certificate of Title Manufactured Home VA","Application for Certification of a Vital Record VA","Application for Refund of Sales and use Tax VA","Application for Replacement & Substitute Titles VA","Application for Self Insurance VA","Application for Transfer & Supplemental Liens VA","Application for Watercraft Title & Registration VA","Beneficiary Transaction Request VA","Blanket Hauling Permit Application VA","Boating Accident Form VA","Change Registration VIN Business Name Application","Commercial Drivers License Application VA","Commercial Drivers License Self Certification VA","Disabled Parking Plates or Placard Application VA","DMV Customer Concern Report VA","DMV Customer Medical Report VA","DMV Customer Vision Screening Report VA","DMV Medical Review Request VA","DMV Records Information Request VA","Drivers License & Identification Card Application","Electronic Birth Certification Request Born in VA","Electronic Birth Certification Request not Born in VA","Emergency Contact Application VA","Exchanging a Drivers License from a Foreign Country VA","Farm Vehicle Plate Certification","Gender Designation Change Request","Hauling Permit Addendum Additional Axle Form VA","Home Schooled in Car Driver Education Parental Authorization Application VA","Identification Card Application for Minors Under Age 15 VA","In Car Maneuvers Observation Record VA","Individual Vehicle Mileage & Fuel Report VA","Instructor License Application Commercial Vehicle VA","Instructor License Application Passenger Vehicle VA","License ID Card and Records Payment Authorization VA","Moped Certification VA","Motor Carrier Fax Cover Sheet VA","National Driver Register File Check Employer Request VA","National Driver Register File Check Individual Request VA","Parental Consent for Online Driver Education Examination VA","Permission to Use Dealers License Plates VA","Power of Attorney to Sign for Owner VA","Purchasers Statement of Tax Exemption VA","Request for Vehicle Information by a Prospective Purchaser VA","Single Trip or Superload Hauling Permit Application VA","Supplemental Drivers Licensing History for CDL Applicants VA","Transfer of Certificate of Title With Lien VA","Vehicle Price Certification Vehicle Over 5 Years VA","Vehicle Registration Application VA","Vehicle Registration Refund Application VA","Veteran Certification of Disability VA","Veteran Identification Card Application VA","Vital Record Application Arriage Divorce Death Certificate VA","Voluntary Report of a Crash VA","Zoning Compliance Certification VA","Zoning Compliance VA"],
				links: ["address-change-request-va.pdf","affidavit-of-vehicle-purchase-price-vehicle-5-years-old-or-less-va.pdf","application-for-assigned-vehicle-identification-number-va.pdf","application-for-certificate-of-qualification-salespersons-license-va.pdf","application-for-certificate-of-title-and-registration-va.pdf","application-for-certificate-of-title-manufactured-home-va.pdf","application-for-certification-of-a-vital-record-va.pdf","application-for-refund-of-sales-and-use-tax-va.pdf","application-for-replacement-and-substitute-titles-va.pdf","application-for-self-insurance-va.pdf","application-for-transfer-and-supplemental-liens-va.pdf","application-for-watercraft-title-and-registration-va.pdf","beneficiary-transaction-request-va.pdf","blanket-hauling-permit-application-va.pdf","boating-accident-form-va.pdf","change-registration-vin-business-name-application-va.pdf","commercial-drivers-license-application-va.pdf","commercial-drivers-license-self-certification-va.pdf","disabled-parking-plates-or-placard-application-va.pdf","dmv-customer-concern-report-va.pdf","dmv-customer-medical-report-va.pdf","dmv-customer-vision-screening-report-va.pdf","dmv-medical-review-request-va.pdf","dmv-records-information-request-va.pdf","drivers-license-and-identification-card-application-va.pdf","electronic-birth-certification-request-born-in-va.pdf","electronic-birth-certification-request-not-born-in-va.pdf","emergency-contact-application-va.pdf","exchanging-a-drivers-license-from-a-foreign-country-va.pdf","farm-vehicle-plate-certification-va.pdf","gender-designation-change-request-va.pdf","hauling-permit-addendum-additional-axle-form-va.pdf","home-schooled-in-car-driver-education-parental-authorization-application-va.pdf","identification-card-application-for-minors-under-age-15-va.pdf","in-car-maneuvers-observation-record-va.pdf","individual-vehicle-mileage-and-fuel-report-va.pdf","instructor-license-application-commercial-vehicle-va.pdf","instructor-license-application-passenger-vehicle-va.pdf","license-id-card-and-records-payment-authorization-va.pdf","moped-certification-va.pdf","motor-carrier-fax-cover-sheet-va.pdf","national-driver-register-file-check-employer-request-va.pdf","national-driver-register-file-check-individual-request-va.pdf","parental-consent-for-online-driver-education-examination-va.pdf","permission-to-use-dealers-license-plates-va.pdf","power-of-attorney-to-sign-for-owner-va.pdf","purchasers-statement-of-tax-exemption-va.pdf","request-for-vehicle-information-by-a-prospective-purchaser-va.pdf","single-trip-or-superload-hauling-permit-application-va.pdf","supplemental-drivers-licensing-history-for-cdl-applicants-va.pdf","transfer-of-certificate-of-title-with-lien-va.pdf","vehicle-price-certification-vehicle-over-5-years-va.pdf","vehicle-registration-application-va.pdf","vehicle-registration-refund-application-va.pdf","veteran-certification-of-disability-va.pdf","veteran-identification-card-application-va.pdf","vital-record-application-marriage-divorce-death-certificate-va.pdf","voluntary-report-of-a-crash-va.pdf","zoning-compliance-certification-va.pdf","zoning-compliance-va.pdf"] 
			};

			var rrca = {
				titles : ["Bill of Sale Vehicle or Vessel CA", "Commercial Driver Handbook CA", "Declaration Regarding Certificate of insurance for non resident driver CA", "dmv Health Questionnaire CA", "dmv Senior guide for safe driving CA", "Driver Handbook CA", "Driver License Renewal by Mail Eligibility Information CA", "Driver Medical Evaluation CA", "Id Card Renewal by Mail Eligibility Information CA", "Info on Appeals for Infractions CA", "Motorcycle Driver Handbook CA", "Parent Teen Training Driver guide CA", "Participant Certification of dui Program Enrollment or completion CA", "Permanent Trailer Identification pti Certification CA", "Recreational Veh & Trailers Handbook CA", "What to know when buying a vehicle CA"],
				links : ["bill-of-sale-vehicle-or-vessel-ca.pdf", "commercial-driver-handbook-ca.pdf", "declaration-regarding-certificate-of-insurance-for-non-resident-driver-ca.pdf", "dmv-health-questionnaire-ca.pdf", "dmv-senior-guide-for-safe-driving-ca.pdf", "driver-handbook-ca.pdf", "driver-license-renewal-by-mail-eligibility-information-ca.pdf", "driver-medical-evaluation-ca.pdf", "id-card-renewal-by-mail-eligibility-information-ca.pdf", "info-on-appeals-for-infractions-ca.pdf", "motorcycle-driver-handbook-ca.pdf", "parent-teen-training-driver-guide-ca.pdf", "participant-certification-of-dui-program-enrollment-or-completion-ca.pdf", "permanent-trailer-identification-pti-certification-ca.pdf", "recreational-veh-and-trailers-handbook-ca.pdf", "what-to-know-when-buying-a-vehicle-ca.pdf"]
			};
			var rfca = {
				titles : ["10 Year History Record Check CA", "Application for Disabled Person Placard or Plates CA", "Application for Duplicate or Paperless Title CA", "Application for Junior Permit CA", "Application for Replacement Plates Stickers or Documents CA", "Application for Title or Registration CA", "Commercial Driver License Self Certification form CA", "dmv Report of Vision Examination CA", "Driver License Record Correction Request CA", "Lien Satisfied legal Owner Title Holder Release CA", "Medical Certification & Authorization Gender Change CA", "Motor Carrier Permit Application CA", "Notice of Change of Address CA", "Notice of Transfer & Release of Liability CA", "Petition for Name Change CA", "Power of Attorney CA", "Record of Complaint form CA", "Registration Information Request for Lien sale CA", "Report of out of State Traffic Conviction by Commercial Driver CA", "Request for Cancellation of Surrender of Driver License or Id CA", "Request for Own drivlicense id or Veh Reg Record CA", "Request for Record Information CA", "Requests for National Driver Regisiter NDR Record Checks CA", "Self Referral for Reevaluation of Driving Skill CA", "Traffic Accident Report Form CA"],
				links : ["10-year-history-record-check-ca.pdf", "application-for-disabled-person-placard-or-plates-ca.pdf", "application-for-duplicate-or-paperless-title-ca.pdf", "application-for-junior-permit-ca.pdf", "application-for-replacement-plates-stickers-or-documents-ca.pdf", "application-for-title-or-registration-ca.pdf", "commercial-driver-license-self-certification-form-ca.pdf", "dmv-report-of-vision-examination-ca.pdf", "driver-license-record-correction-request-ca.pdf", "lien-satisfied-legal-owner-title-holder-release-ca.pdf", "medical-certification-and-authorization-gender-change-ca.pdf", "motor-carrier-permit-application-ca.pdf", "notice-of-change-of-address-ca.pdf", "notice-of-transfer-and-Release-of-liability-ca.pdf", "petition-for-name-change-ca.pdf", "power-of-attorney-ca.pdf", "record-of-complaint-form-ca.pdf", "registration-information-request-for-lien-sale-ca.pdf", "report-of-out-of-state-traffic-conviction-by-commercial-driver-ca.pdf", "request-for-cancellation-of-surrender-of-driver-license-or-id-ca.pdf", "request-for-own-drivlicense-id-or-veh-reg-record-ca.pdf", "request-for-record-information-ca.pdf", "requests-for-national-driver-regisiter-ndr-record-checks-ca.pdf", "self-referral-for-reevaluation-of-driving-skill-ca.pdf", "traffic-accident-report-form-ca.pdf"]
			};

			var rrpa = {
				titles : ["Commercial Drivers Manual PA", "Id Requirements for non us Citizens to Apply for a PA License Permit Id", "Id Requirements for us Citizens to Apply for a PA License Permit Id", "Mature Driver Safety Tips PA", "Motorcycle Operator Manual PA", "Motorcycle Safety Program PA", "Parents Supervised Driving Program PA", "Pennsylvania Drivers Manual", "Self Insurance Package for an Individual PA", "Special Point Examination Study Guide PA", "Talking with Older Drivers a Guide PA"],
				links : ["commercial-drivers-manual-pa.pdf", "id-requirements-for-non-us-citizens-to-apply-for-a-pa-license-permit-id.pdf", "id-requirements-for-us-citizens-to-apply-for-a-pa-license-permit-id.pdf", "mature-driver-safety-tips-pa.pdf", "motorcycle-operator-manual-pa.pdf", "motorcycle-safety-program-pa.pdf", "parents-supervised-driving-program-pa.pdf", "pennsylvania-drivers-manual.pdf", "self-insurance-package-for-an-individual-pa.pdf", "special-point-examination-study-guide-pa.pdf", "talking-with-older-drivers-a-guide-pa.pdf"]
			};

			var rfpa = {
				titles : ["20 Day Hunter Permit Application PA", "Acknowledgment of Suspension Revoc Disq Canc PA", "Application for a Vertical Motorcycle Registration Plate PA", "Application for Correction or Change of Name PA", "Application for Initial Id Card PA", "Application for non Commercial Drivers License PA", "Application for Photo Id Card PA", "Application for Seasonal Vehicle Registration PA", "Application for US Military Serice Registration Plate PA", "CDL Request for 10 year History from other states PA", "Change of Address dot PA", "Commercial Learners Permit Application PA", "Motorcycle Learners Permit Application to Amend PA", "Non Commercial Drivers License Application for Renewal PA", "Non Commercial Learners Permit Application PA", "Parent or Guardian Certification Form PA", "Parent or Guardian Consent form PA", "Person with Disability Parking Placard Application PA", "Probationary License PL Petition PA", "Report of eye Examination PA", "Request for Cancellation of Drivers License or Learners Permit PA", "Request for Driver Information PA", "Request for Gender Change on Drivers Llicense Id Card PA", "Request for Organ Donor Designation on PA Ddrivers License or Id", "Request for Vehicle Information PA", "Self Certification Form dot PA", "Self Certification of Vehicles Owned Operated PA", "Statement of non Ownership of Vehicles PA"],
				links : ["20-day-hunter-permit-application-pa.pdf", "acknowledgment-of-suspension-revoc-disq-canc-pa.pdf", "application-for-a-vertical-motorcycle-registration-plate-pa.pdf", "application-for-correction-or-change-of-name-pa.pdf", "application-for-initial-id-card-pa.pdf", "application-for-non-commercial-drivers-license-pa.pdf", "application-for-photo-id-card-pa.pdf", "application-for-seasonal-vehicle-registration-pa.pdf", "application-for-us-military-serice-registration-plate-pa.pdf", "cdl-request-for-10-year-history-from-other-states-pa.pdf", "change-of-address-dot-pa.pdf", "commercial-learners-permit-application-pa.pdf", "motorcycle-learners-permit-application-to-amend-pa.pdf", "non-commercial-drivers-license-application-for-renewal-pa.pdf", "non-commercial-learners-permit-application-pa.pdf", "parent-or-guardian-certification-form-pa.pdf", "parent-or-guardian-consent-form-pa.pdf", "person-with-disability-parking-placard-application-pa.pdf", "probationary-license-pl-petition-pa.pdf", "report-of-eye-examination-pa.pdf", "request-for-cancellation-of-drivers-license-or-learners-permit-pa.pdf", "request-for-driver-information-pa.pdf", "request-for-gender-change-on-drivers-license-id-card-pa.pdf", "request-for-organ-donor-designation-on-pa-drivers-license-or-id.pdf", "request-for-vehicle-information-pa.pdf", "self-certification-form-dot-pa.pdf", "self-certification-of-vehicles-owned-operated-pa.pdf", "statement-of-non-ownership-of-vehicles-pa.pdf"]
			};

			var rrwi = {
				titles : ["Acceptable Docs For WI Driver License or ID Application", "Cell Phones, Driving and the Law WI", "Commercial Driver Certification Tier Operation WI", "Commercial Driver License Disqualifications WI", "Driver License Withdrawals WI", "Driver Training Vehicle Record WI", "Family Care Program Enrollment WI", "Graduated Driver License Driving Log WI", "Habitual Traffic Offender Law WI", "Occupational (Restricted) Driver License Info WI", "Older Driver Workbook - Be Safe, Not Sorry WI", "Parent's Supervised Driving Program WI", "Point System Abbreviation Codes & Charge Points WI", "Road Test Terms - English & Spanish WI", "Wisconsin Commercial Drivers Manual", "Wisconsin Drivers Book", "Wisconsin Motorcyclists Handbook", "Wisconsin Motorists Handbook"],
				links : ["acceptable-docs-for-wi-driver-license-or-id-application.pdf", "cell-phones-driving-and-the-law-wiI.pdf", "commercial-driver-certification-tier-operation-wi.pdf", "commercial-driver-license-disqualifications-wi.pdf", "driver-license-withdrawals-wi.pdf", "driver-training-vehicle-record-wi.pdf", "family-care-program-enrollment-wi.pdf", "graduated-driver-license-driving-log-wiI.pdf", "habitual-traffic-offender-law-wi.pdf", "occupational-restricted-driver-license-info-wi.pdf", "older-driver-workbook-be-safe-not-sorry-wi.pdf", "parents-supervised-driving-program-wi.pdf", "point-system-abbreviation-codes-charge-points-wi.pdf", "road-test-terms-english-spanish-wi.pdf", "wisconsin-commercial-drivers-manual.pdf", "wisconsin-drivers-book.pdf", "wisconsin-motorcyclists-handbook.pdf", "wisconsin-motorists-handbook.pdf"]
			};
			var rfwi = {
				titles : ["Address Change Notice Form WI", "Certification of Vision Exam for Driver License WI", "Declaration to Physicians - Living Will WI", "Disabled Parking License Plates Application WI", "Disabled Veteran Parking License Plates Application WI", "Driver License Application Form WI", "Driver Report of Accident WI", "Duplicate Certificate of Registration Application WI", "Identification Card Application WI", "Medical Examination Report WI", "Occupational Operator License Application WI", "Power of Attorney for FInances & Property WI", "Power of Attorney for Health Care WI", "Registration Reinstatement Application WI", "Replacement or Duplicate License Plate Application WI", "Replacement Title Application WI", "Repossession Application WI", "Request to Withhold Name and Address WI", "Title-License Plate Application WI", "Title-Registration Correction Request Form WI", "Vehicle - Driver Record Information Request WI"],
				links : ["address-change-notice-form-wi.pdf", "certification-of-vision-exam-for-driver-license-wi.pdf", "declaration-to-physicians-living-will-wi.pdf", "disabled-parking-license-plates-application-wi.pdf", "disabled-veteran-parking-license-plates-application-wi.pdf", "driver-license-application-form-wi.pdf", "driver-report-of-accident-wi.pdf", "duplicate-certificate-of-registration-application-wi.pdf", "identification-card-application-wi.pdf", "medical-examination-report-wi.pdf", "occupational-operator-license-application-wi.pdf", "power-of-attorney-for-finances-property-wi.pdf", "power-of-attorney-for-health-care-wi.pdf", "registration-reinstatement-application-wi.pdf", "replacement-or-duplicate-license-plate-application-wi.pdf", "replacement-title-application-wi.pdf", "repossession-application-wi.pdf", "request-to-withhold-name-and-address-wi.pdf", "title-license-plate-application-wi.pdf", "title-registration-correction-request-form-wi.pdf", "vehicle-driver-record-information-request-wi.pdf"]
			};

			var rroh = {
				titles : ["Acceptable Proof of ID Documents List OH", "Consumer Scams Brochure OH", "Display of Stickers OH", "Fair Employment Guide OH", "Fair Housing Guide OH", "General Information on the Ohio Driver Abstract", "ID Kids For Safety OH", "Motorcycle Operator Manual OH", "Ohio Commercial Driver License Manual", "Ohio Motor Vehicle Laws", "Proof of Financial Responsibility OH", "Scrap Vehicle Processing Guide OH", "Used Car Buyer Checklist OH", "Welcome New Resident Manual OH"],
				links : ["acceptable-proof-of-id-documents-list-oh.pdf", "consumer-scams-brochure-oh.pdf", "display-of-stickers-oh.pdf", "fair-employment-guide-oh.pdf", "fair-housing-guide-oh.pdf", "general-information-on-the-ohio-driver-abstract.pdf", "id-kids-for-safety-oh.pdf", "motorcycle-operator-manual-oh.pdf", "ohio-commercial-driver-license-manual.pdf", "ohio-motor-vehicle-laws.pdf", "proof-of-financial-responsibility-oh.pdf", "scrap-vehicle-processing-guide-oh.pdf", "used-car-buyer-checklist-oh.pdf", "welcome-new-resident-manual-oh.pdf"]
			};
			var rfoh = {
				titles : ["Affidavit Conversion to Motor Home OH", "Affidavit for Registration OH", "Alcohol-Drug Reinstatement Form OH", "Application for Certificate of Title to Motor Vehicle OH", "Application for Disability Placards OH", "Application for ID Card for Hearing-Impaired OH", "Application for Out-Of-State Registration by Mail OH", "Application for Replacement VIN OH", "Commercial Driver License Self-Certification Authorization OH", "Customer Request to Cancel Vehicle Registration OH", "Declaration of Gender Change OH", "Duplicate Registration Card, Transfer, Replacement Plates OH", "Eligible Adult in Loco Parentis Affidavit OH", "Employer-Employee Request for Nat'l Driver Register File Check OH", "Export Application for Motor Vehicle OH", "Gratis Car Registration Application For Disabled Veterans OH", "Individual's Request for Nat'l Driver Register File Check OH", "Minor Consent Form OH", "Next of Kin - Emergency Contact Enrollment OH", "OBMV Record Request OH", "Odometer Disclosure Statement OH", "Organ Donor Registry Enrollment OH", "Power of Attorney for Certificate of Title OH", "Power of Attorney for Vehicle Registration OH", "Proof of Ohio Residency - Certified Statement", "Record Confidentiality Request OH", "Request for Change of Address OH", "Request for Special License Plates OH", "Unclaimed Motor Vehicle Affidavit OH", "Vision Exam for Out-Of-State Driver License Applicants OH"],
				links : ["affidavit-conversion-to-motor-home-oh.pdf", "affidavit-for-registration-oh.pdf", "alcohol-drug-reinstatement-form-oh.pdf", "application-for-certificate-of-title-to-motor-vehicle-oh.pdf", "application-for-disability-placards-oh.pdf", "application-for-id-card-for-hearing-impaired-oh.pdf", "application-for-out-of-state-registration-by-mail-oh.pdf", "application-for-replacement-vin-oh.pdf", "commercial-driver-license-self-certification-authorization-oh.pdf", "customer-request-to-cancel-vehicle-registration-oh.pdf", "declaratio-of-gender-change-oh.pdf", "duplicate-registration-card-transfer-replacement-plates-oh.pdf", "eligible-adult-in-loco-parentis-affidavit-oh.pdf", "employer-employee-request-for-natl-driver-register-file-check-oh.pdf", "export-application-for-motor-vehicle-oh.pdf", "gratis-car-registration-application-for-disabled-veterans-oh.pdf", "individuals-request-for-natl-rriver-register-file-check-oh.pdf", "minor-consent-form-oh.pdf", "next-of-kin-emergency-contact-enrollment-oh.pdf", "obmv-record-request-oh.pdf", "odometer-disclosure-statement-oh.pdf", "organ-donor-registry-enrollment-oh.pdf", "power-of-attorney-for-certificate-of-title-oh.pdf", "power-of-attorney-for-vehicle-registration-oh.pdf", "proof-of-ohio-residency-certified-statement.pdf", "record-confidentiality-request-oh.pdf", "request-for-change-of-address-oh.pdf", "request-for-special-license-plates-oh.pdf", "unclaimed-motor-vehicle-affidavit-oh.pdf", "vision-exam-for-out-of-state-driver-license-applicants-oh.pdf"]
			};

			//RR
			var rresources = {};
			var rforms = {};

			switch(getPage()) {
			case 'virginia':
				rresources = rrva;
				rforms = rfva;
				break;
			case 'california':
				rresources = rrca;
				rforms = rfca;
				break;
			case 'pennsylvania':
				rresources = rrpa;
				rforms = rfpa;
				break;
			case 'wisconsin':
				rresources = rrwi;
				rforms = rfwi;
				break;
			case 'ohio':
				rresources = rroh;
				rforms = rfoh;
				break;
			default:
				rresources = rrvirginia;
				rforms = rfvirginia;
			}

			for (var i = 0; i < rresources.titles.length; i++) {
				$('#related-resources').append(sprintf(rrtemplate, rresources.titles[i], getPage() + '/related-resources/' + rresources.links[i]));
			};

			for (var i = 0; i < rforms.titles.length; i++) {
				$('#related-forms').append(sprintf(rrtemplate, rforms.titles[i], getPage() + '/related-forms/' + rforms.links[i]));
			};
		}
	} 
	catch (e) {	}





   /*====== START:Read More Button ======*/
	$('a.read-more').click(function(event){ /* find all a.read_more elements and bind the following code to them */
        event.preventDefault(); /* prevent the a from changing the url */
        
        $('.description-product').css('line-height',1.5);
        
        $('.more-text').show(); /* show the .more_text span */
		$('.read-more').hide();
    });
   /*====== END:Read More Button ======*/

  /*====== START: Resuable Code ======*/
  // Extend JQuery to create a function / method to test if an element exists
  $.fn.exists = function() {
  return this.length !== 0;
  }
  /*====== END: Resuable Code ======*/


/* =============================================================
  INPAGE LINKS TO FORM
==============================================================*/
try {
  $(".inpagelink").attr("href", "/user/form/step1/new-license/"+ getPage() +".html")
  $(".inpagelinkauto").attr("href", "/user/form/step1/"+cat+"/"+ getPage() +".html")

}
catch (e) {}

/* =============================================================
  INPAGE SRC COVERS IMG
==============================================================*/
/*try {
	////s3.amazonaws.com/easy-books.org/images/covers/auto/car-registration/virginia/renew-registration.png
	var str = '//s3.amazonaws.com/easy-books.org/images/covers/13/14/15/16.png';
	
	var res = str.replace("13", up[3]).replace("14", up[4]).replace("15", getPage()).replace("16", up[5]);
  	$(".coverimg").attr("src", res);
  	
  	var res = str.replace("13", up[4]).replace("14", up[5]).replace("15", getPage()).replace("16", up[6]);
  	$(".coverimgcl").attr("src", res);
}
catch (e) {}*/

/* =============================================================
  INPAGE SRC COVERS IMG
==============================================================*/
try {
	var str = '//s3.amazonaws.com/easy-books.org/img/category/subcategory/service/statex.png';
	var devstr = '//s3.amazonaws.com/easy-books.org/img/category/subcategory/service/statex.png';
	var localstr = '//s3.amazonaws.com/v2easy-books.org/img/category/subcategory/service/statex.png';
	
	/*var res = localstr.replace("category", up[3]).replace("subcategory", up[4]).replace("service", up[5]).replace("statex", getPage());
  	$(".coverimg").attr("src", res);*/
  	
  	var res = devstr.replace("category", up[4]).replace("subcategory", up[5]).replace("service", up[6]).replace("statex", getPage());
  	$(".coverimgcl").attr("src", res);
}
catch (e) {}

/* =============================================================
  FORM V2 IMG COVER
==============================================================*/

try {
	var url = window.location.href;
	var querys = url.split("?")[1].split("&");
	
	var category =   window.location.pathname.split("/")[3];
	var subcategory = getUrlParam("cat");                              //querys[0].split("=")[1];
	var state =  getPage();                                            //window.location.pathname.split("/").pop().split(".")[0];

	if (category == "auto") {
		
		var locallnk = '//s3.amazonaws.com/v2easy-books.org/img/category/subcategory/service/statex.png';
		var devlnk   = '//s3.amazonaws.com/easy-books.org/img/category/subcategory/service/statex.png';
		var lnk      = '//s3.amazonaws.com/easy-books.org/img/category/subcategory/service/statex.png';

		var service = getUrlParam("service");    //querys[1].split("=")[1];
		var src = devlnk.replace("category", category).replace("subcategory", subcategory).replace("service", service).replace("statex", state);
	}
	else {
		
		var locallnk = '//s3.amazonaws.com/v2easy-books.org/img/category/subcategory/statex.png';
		var devlnk   = '//s3.amazonaws.com/easy-books.org/img/category/subcategory/statex.png';
		var lnk      = '//s3.amazonaws.com/easy-books.org/img/category/subcategory/statex.png';
		
      var service = getUrlParam("service");    //querys[1].split("=")[1];
      var src = devlnk.replace("category", category).replace("subcategory", service).replace("statex", service);
	}
	
	$(".coverimage").attr("src", src);
}
catch (e) {}


/* =============================================================
DESCRIPTION FORM V2
==============================================================*/
try {
	if (hrf.indexOf("/form/step1") != -1) {
		var url = window.location.href;
		var querys = url.split("?")[1].split("&");

		var category = window.location.pathname.split("/")[3];
		var subcategory = querys[0].split("=")[1];
		var state = window.location.pathname.split("/").pop().split(".")[0];
		var service = "";

		if (category == "auto") {
			service = querys[1].split('=')[1];
			strTitle = service.replace(/-/g, ' ');

		} else {
			service = subcategory;
			strTitle = service.replace(/-/g, ' ');
		}

		for (var i = 0; i < fladdata.length; i++) {
			if (fladdata[i]["category"] == category) {
				//alert(data[i]["subcategory"][j][subcategory][x]['description']);
				for (var j = 0; j < fladdata[i]["subcategory"].length; j++) {

					if (category == 'auto') {
						for (var x = 0; x < fladdata[i]["subcategory"][j][subcategory].length; x++) {
							if (fladdata[i]["subcategory"][j][subcategory][x]['title'] == service) {
								//alert(data[i]["subcategory"][j][subcategory][x]['description']);
								document.querySelector('.title-service-guide').innerHTML = strTitle + " " + " " + state;
								var description = document.createElement('div');
                        var cap_state = capWordsWithDash(state);//state.charAt(0).toUpperCase() + state.slice(1);
								description.innerHTML = fladdata[i]["subcategory"][j][subcategory][x]['description'].replace("Virginia", cap_state);
								var p = document.querySelector('.services-description');
								p.prepend(description);

							}

						}
					} else {
						if (fladdata[i]["subcategory"][j]['title'] == service) {
							//alert(data[i]["subcategory"][j]['description']);
							document.querySelector('.title-service-guide').innerHTML = strTitle + " " + " " + state;
							var description = document.createElement('div');
                     var cap_state = capWordsWithDash(state);//state.charAt(0).toUpperCase() + state.slice(1);
							description.innerHTML = fladdata[i]["subcategory"][j]['description'].replace("Virginia", cap_state);
							var p = document.querySelector('.services-description');
							p.prepend(description);
						}
					}
				}
			}
		}
	}
}
catch (e) {}


/* =============================================================
  HOMEPAGE SUBMIT
==============================================================*/
  if( hrf.indexOf("index.html") != -1 )
  {
      $("#submit").click(function(e){
         var _state  = $("#state").val();
         var _action = $('input[name=action]:checked','#first-form').val();
         if( !_action )
         {
            alert( "Please Select an Action" );
            return false;
         }
         if( _state=="" )
         {
            alert( "Please Select a State" );
            $("#state").focus();
            return false;
         }
         if( _state.length>0 )
         {
               document.location.href = "/user/form/step1/" + _action + "/" + _state + ".html";
         }
         return false;
      });
   }

 /* =============================================================
  LANDING PAGE SUBMIT
==============================================================*/
  
   // ALL STATE PAGES
   $('#promo-submit').click(function() {
      // alert(page_name);
      if($('#resident').val() != '' && $('#license').val() != '' && $('#tags').val() != ''){
        redirectToForm();
      } 
      else
      {
        selectFieldEmptyError();
      }
   });

   // RESIDENT, NON-LICENSE, TYPES OF GAME PAGES
   $('#promo-nonstate-submit').click(function() {
      if ( $('#state').val() != '' && $('#license').val() != '' && $('#tags').val() != '' )
      {
        var state = $("#state").val();
        document.location.href = "/user/form/step1/new-license/" + state + ".html";
      } 
      else
      {
        selectFieldEmptyError();
      }
   });

   // MOBILE VIEW OF PROMO SPACE
   $("#mob-submit").click(function() {
      if ( $("#state").val() != "" )
      {
        var state = $("#state").val();
        document.location.href = "/user/form/step1/new-license/" + state + ".html";
      }
      else {
        $("#state").css("border","1px solid red");
        alert("Please select a State");
      }
   });

   // MOBILE VIEW OF PROMO SPACE
   $("#mob-state-submit").click(function() {
        // alert(getPage());
        document.location.href = "/user/form/step1/new-license/" + getPage() + ".html";
   });


/* =============================================================
  REDIRECT STATE LANDING PAGE TO FORM
==============================================================*/
function redirectToForm(){
  if(statenames.indexOf(getPage()) > -1){
    var state = getPage();
    document.location.href = "/user/form/step1/new-license/"+state+".html";
  } 
}


/* =============================================================
  COPYRIGHT - GARY
==============================================================*/
  $('#copyrights #year').html(new Date().getFullYear())
    // Hides items if the current page is not a state page
  if ($(".state-page").exists() == false) {
    $("#nav-contain").toggleClass("hide");
    $("#sidebar-left").toggleClass("hide");
    // show recent post side bar for non-state page
    $("#sidebar-left-posts").removeClass("hide").addClass("remove-widget-spacing");
  } else {
    $("#sidebar-left-posts").addClass("hide");
  }


/* =============================================================
  ADD LOGO/DEPT TEXT TO STATE LANDING PAGES
==============================================================*/
  if( statenames.indexOf(getPage()) > -1) {
    if(getDeptName(getPage()) != undefined) {
      var stateVar = getPage();
      if(getPage().indexOf("-") !=-1)
      {
        stateVar = capWords(stateVar.replace("-"," "));
      }
      else 
      {
        stateVar = capitalize(stateVar);
      }

      $('.card-title').html( "with the " + stateVar + " " + getDeptName(getPage()));
      $('.card-badge').html('<img src="//s3.amazonaws.com/hunting-license.org/images/logos/'+getPage()+'.png" alt="" align="left">');
    }
    else
    {
      $('.featured-card').addClass('generic');
    }
  }
  else
  {
    $('.featured-card').addClass('generic');
  }



/* =============================================================
  BLOGS - GARY CODE
==============================================================*/
  // Blog Entry meta
  var weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

  update = new Date(document.lastModified);
  day = weekday[update.getDay()];
  date = update.getDate();
  month = update.getMonth() + 1;
  year = update.getFullYear();

  $('.entry-meta .date').after(day + " " + month + "/" + date + "," + "/" + year);

  /*====== START: Ad Content ======*/
  // Insert into featured-ad-top
  if ($('.category-california').exists() == true) {
    $('.headline .copy').html('Avoid fines and the other penalties that come with hunting without proper licensure. The state of California requires all hunter to have the right set credentials in order to hunt. Fortunately, Hunting-License.org makes the process of getting the right Hunting License fast and easy. With a checklist that gives you everything you need to know in order to apply, along with countless other valuable services, you will not need to look anywhere else for license assistance or where to hunt in.');
  } else if ($('.category-colorado').exists() == true) {
    $('.headline .copy').html('Hunting without a license comes with penalties in the state of Colorado, and these penalties do not just stop at fines. Use Hunting-License.org to make sure you are prepared to hunt within CO state boundaries. With a number of valuable services, including a checklist that takes you through all the steps you need to take in order to obtain a license to hunt, there is no better resource to get you ready.');
  }else if ($('.category-texas').exists() == true) {
    $('.headline .copy').html('In Texas, it is important to buy a Hunting License before embarking on any hunting trip, in order to avoid any unwanted fees. Proper licensure is required within the state for any hunter who plans on enjoying state park hunting. Be sure to consult Hunting-License.org&rsquo;s detailed checklist for purchasing a hunting permit, as well as its countless other services, to make sure that your TX hunting vacation goes off without a hitch.');
  }else if ($('.category-minnesota').exists() == true) {
    $('.headline .copy').html('Minnesota requires all hunter within state bounds to have a proper Hunting License in order to take hunt. Steer clear of fines and other punishments by hunting with the necessary credentials. Hunting-License.org has everything you need to get a hunting permit quickly and easily, from an in-depth checklist that provides information on all the steps you need to take to a number of other helpful services. Now you can start preparing to obtain your desired Hunting License.');
  }else{
    // show florida text as default
  }
  /*====== END: Ad Content ======*/ 

  // Hide elements on mobile view Only for State pages
  if ($('.state-page').exists() == true) {
      $(this).find('#wrapper').addClass('state-cleanup');
  }

   var blog_posts = ['how-weather-affects-your-aim', 'gun-safety', 'hunting-seasons'];
  // Randomly select Pdf page
  //shuffle(pdf_page_select);

 //------- Insert Blog Articles
  
  // .child-category should only exist on pages that wants to populate articles
  if($('#IgnoreForNow').exists() == true){
     //alert('trying to build blog exceprts here');
     // If the directory isn't 'article' the program assumes we are in a category page    
      createBlogExcerpts(blog_posts);
     }
  function createBlogExcerpts(sort_by){
      // loop is based on the current category defined by the url. The program will loop through all articles for that category
      for (i = 0; i < sort_by.length; i++) {
           // Create blog post html elements
           $('#posts').append('<article class="entry clearfix ' + i + '"><div class="entry-image" '+i+'><a href=/blogs/"'+sort_by[i]+'.html"></a></div><section class="entry-c " '+i+'><div class="entry-title"><h2></h2></div><ul class="entry-meta clearfix"></ul><div class="entry-content "'+i+'></div></section></article>');

           // Post Feature image
           $(".entry." + i + " .entry-image a").load("/blogs/"+sort_by[i]+".html .entry-image >* ");
           // Title
           $(".entry-title." + i + " h2").load("/blogs/"+sort_by[i]+".html .entry-title >* ");
       }
  }


});



var submenuJSON = "{" 
+ "\"florida\":[\"Freshwater Hunting Licenses\",\"Saltwater Hunting Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"]," 
+ "\"california\":[\"Commercial Hunting Licenses\",\"Sport Hunting License\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"],"
+ "\"colorado\":[\"Commercial Hunting Licenses\",\"New Hunting Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"]," 
+ "\"hawaii\":[\"Commercial Hunting Licenses\",\"Freshwater Hunting Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"],"
+ "\"louisiana\":[\"Freshwater Hunting Licenses\",\"Saltwater Hunting Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"]," 
+ "\"minnesota\":[\"Commercial Hunting Licenses\",\"New Hunting Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"]," 
+ "\"north-carolina\":[\"Freshwater Hunting Licenses\",\"Saltwater Hunting Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"]," 
+ "\"texas\":[\"Freshwater Hunting Licenses\",\"Saltwater Hunting Licenses\",\"Education\",\"Accommodations\",\"Organizations\",\"FAQs\"]" + "}";


var pagename = "";

function getSubMenu(state, submenu) {
  var menus = JSON.parse(submenuJSON);

  var menu = menus[state];

  for (var m = 0; m < menu.length; m++) {
    $("#" + submenu + " ul").append("<li><a href=\"/" + menu[m].replace(/ /g, '-').toLowerCase() + "/" + state + ".html\">" + menu[m] + "</a></li>");
  }
}

function getPageName(hrf) {
  pagename = hrf.split("/")[hrf.split("/").length - 1].split(".")[0].replace(/-/g, ' ');
  if (pagename == "index")
    pagename = hrf.split("/")[hrf.split("/").length - 2]
  return pagename;
}

function go2State(o) {
  document.location.href = "/" + o.value + ".html";
}

function splashStart()
{
   hrf   = document.location.href.split("/");
   state = hrf[hrf.length-1];
   act   = $('input[name=action]:checked').val();
   if( typeof act === "undefined")
   {
      alert( "Please Select a Type of License" );
      return false;
   }
   else
   {
      document.location.href = "/user/form/step1/" + act + "/" + state + ".html";
      // document.location.href = "/user/form/step1/" + act + "/" + state;
   }
}
 // this function gets the department name from the state name variable -- example:  $("#title").text(getDeptName(getPage()));
  function getDeptName(stateName) 
  {
    var dept = new Array();

    dept['texas'] = 'Parks & Wildlife'; 
    dept['california'] = 'Department of Fish and Wildlife'; 
    dept['indiana'] = 'Department of Natural Resources Law Enforcement Division'; 
    dept['kansas'] = 'Wildlife, Parks & Tourism'; 
    dept['kentucky'] = 'Department of Fish and Wildlife Resources'; 
    dept['missouri'] = 'Department of Conservation'; 
    dept['new-york'] = 'Department of Environmental Conservation'; 
    dept['ohio'] = 'Department of Natural Resources'; 
    dept['pennsylvania'] = 'Game Commission'; 
    dept['tennessee'] = 'Wildlife Resources Agency'; 
    dept['mississippi'] = 'Department of Wildlife, Fisheries & Parks'; 
    dept['nebraska'] = 'Game & Parks Commission'; 
    dept['new-mexico'] = 'Department of Game & Fish'; 
    dept['oklahoma'] = 'Department of Wildlife Conservation'; 
    dept['oregon'] = 'Department of Fish & Wildlife'; 
    dept['rhode-island'] = 'Division of Fish & Wildlife'; 
    dept['south-carolina'] = 'Department of Natural Resources'; 
    dept['north-dakota'] = 'Game and Fish Department'; 
    dept['south-dakota'] = 'Department of Game, Fish and Parks'; 
    dept['utah'] = 'Division of Wildlife Resources'; 
    dept['vermont'] = 'Fish & Wildlife Department'; 
    dept['virginia'] = 'Department of Game and Inland Fisheries'; 
    dept['washington'] = 'Department of Fish & Wildlife'; 
    dept['west-virginia'] = 'Division of Natural Resources'; 
    dept['wisconsin'] = 'Department of Natural Resources'; 
    dept['wyoming'] = 'Game and Fish Department'; 
    dept['north-carolina'] = 'Wildlife Resources Commission'; 
    dept['alabama'] = 'Department of Conservation and Natural Resources';
    dept['alaska'] = 'Department of Fish and Game';
    dept['arizona'] = 'Game and Fish Department';
    dept['arkansas'] = 'Game and Fish Commission';
    dept['colorado'] = 'Parks and Wildlife';
    dept['connecticut'] = 'Department of Energy and Environmental Protection';
    dept['delaware'] = 'Division of Fish and Wildlife';
    dept['florida'] = 'Fish and Wildlife Conservation Commission';
    dept['georgia'] = 'Department of Natural Resources';
    dept['hawaii'] = 'Division of Forestry and Wildlife';
    dept['idaho'] = 'Department of Fish and Game';
    dept['illinois'] = 'Department of Natural Resources';
    dept['iowa'] = 'Department of Natural Resources';
    dept['louisiana'] = 'Department of Wildlife and Fisheries';
    dept['maine'] = 'Department of Inland Fisheries and Wildlife';
    dept['maryland'] = 'Department of Natural Resources';
    dept['massachusetts'] = 'Department of Energy and Environmental Affairs';
    dept['michigan'] = 'Department of Natural Resources';
    dept['minnesota'] = 'Department of Natural Resources';
    dept['montana'] = 'Fish, Wildlife and Parks';
    dept['nevada'] = 'Department of Wildlife';
    dept['new-hampshire'] = 'Fish and Game Department';
    dept['new-jersey'] = 'Department of Fish and Wildlife';
    dept['north-carolina'] = 'Wildlife Resources Commission';
    dept['north-dakota'] = 'Game and Fish Department'; 

    return dept[stateName];
  }




/**************************************************
FUNCTIONS
**************************************************/
function capitalize(string) 
{ 
   return string.charAt(0).toUpperCase() + string.slice(1); 
} 

function capWords(str)
{ 
   var words = str.split(" "); 
   for (var i=0 ; i < words.length ; i++)
   { 
      var testwd = words[i]; 
      var firLet = testwd.substr(0,1); 
      var rest   = testwd.substr(1, testwd.length -1) 
      words[i]   = firLet.toUpperCase() + rest 
   } 
   return words.join(" "); 
} 

function capDashWords(str)
{ 
   var words = str.split("-"); 
   for (var i=0 ; i < words.length ; i++)
   { 
      var testwd = words[i]; 
      var firLet = testwd.substr(0,1); 
      var rest   = testwd.substr(1, testwd.length -1) 
      words[i]   = firLet.toUpperCase() + rest 
   } 
   return words.join(" "); 
} 

function getPage()
{
   spliturl = document.location.href.split("/");
   return spliturl[spliturl.length-1].split(".")[0];
}

function slideMobNav() {
  $(".mobnav").slideToggle();
}

function selectFieldEmptyError() {
  alert('Please select an option from each field');
  $('select').change(function () {
    $("select").each(function() {
      if ($(this).val() == ''){
          var empty_selects = $('select').filter(function() {
          return $.trim( $(this).val() ) == '';
        });
        empty_selects.css('border', '2px solid red');
      }else if ($(this).val() != ''){
        $(this).css('border', '1px solid #cccccc');
      } 
    });
  })
  .change();
}

function getCover(category, service, state){
   var result = "";
   if(service == "generic"){
      result = "img/generic-cover.jpg"
   }
   else{
      if(category == "drivers-license" || category == "car-registration" || category == "car-title" ){
         //"img/auto/drivers-license/renew-drivers-license/california.png"
         result = "img/auto/"+category+"/"+service+"/"+state+".png";
      }else{
         //"img/travel/global-entry/global-entry.png"
         result = "img/"+category+"/"+service+".png"
      }
   }
   return "{CSS-CLOUD}/"+result;
}

function profileDownloadHistory(){
   
   /*var dLinks = ["http://s3.easy-books.org/easy-books.org/pdf/auto/wisconsin/related-resources/acceptable-docs-for-wi-driver-license-or-id-application.pdf", "http://easy-books.org.s3.amazonaws.com/pdf/drivers-license/wisconsin.pdf", "http://s3.easy-books.org/easy-books.org/pdf/auto/wisconsin/related-forms/certification-of-vision-exam-for-driver-license-wi.pdf", "http://easy-books.org.s3.amazonaws.com/pdf/car-title/virginia.pdf", "http://easy-books.org.s3.amazonaws.com/pdf/car-title/virginia.pdf", "http://easy-books.org.s3.amazonaws.com/pdf/car-registration/pennsylvania.pdf", "http://easy-books.org.s3.amazonaws.com/pdf/drivers-license/california.pdf", "http://s3.easy-books.org/easy-books.org/pdf/travel/global-entry.pdf", "http://s3.easy-books.org/easy-books.org/pdf/travel/travel-precheck.pdf", "http://s3.easy-books.org/easy-books.org/pdf/travel/airport-precheck.pdf", "http://easy-books.org.s3.amazonaws.com/pdf/recreation/fishing-license/virginia.pdf", "http://s3.easy-books.org/easy-books.org/pdf/recreation/hunting-license.pdf", "http://s3.easy-books.org/easy-books.org/pdf/recreation/name-change.pdf", "http://s3.easy-books.org/easy-books.org/pdf/federal-resources/medicaid-help.pdf", "http://s3.easy-books.org/easy-books.org/pdf/federal-resources/food-stamp.pdf"];

   var dNames = ["auto wisconsin related-resources acceptable-docs-for-wi-driver-license-or-id-application.pdf", "drivers-license wisconsin.pdf", "auto wisconsin related-forms certification-of-vision-exam-for-driver-license-wi.pdf", "car-title virginia.pdf", "car-title virginia.pdf", "car-registration pennsylvania.pdf", "drivers-license california.pdf", "travel global-entry.pdf", "travel travel-precheck.pdf", "travel airport-precheck.pdf", "recreation fishing-license virginia.pdf", "recreation hunting-license.pdf", "recreation name-change.pdf", "federal-resources medicaid-help.pdf", "federal-resources food-stamp.pdf"];

   var dStamps = ["06/29/2017@10:49:28", "06/29/2017@10:49:19", "06/29/2017@10:49:06", "06/29/2017@10:47:21", "06/29/2017@10:46:59", "06/29/2017@10:46:30", "06/29/2017@10:46:13", "06/29/2017@10:45:43", "06/29/2017@10:45:21", "06/29/2017@10:45:14", "06/29/2017@10:44:20", "06/29/2017@10:44:13", "06/29/2017@10:44:08", "06/29/2017@10:43:48", "06/29/2017@10:43:24"];

   var dChecks = ["http://s3.easy-books.org/easy-books.org/pdf/auto/wisconsin/related-resources/acceptable-docs-for-wi-driver-license-or-id-application.pdf", "/checklist/auto/drivers-license/reinstate-suspended-license/wisconsin.html", "http://s3.easy-books.org/easy-books.org/pdf/auto/wisconsin/related-forms/certification-of-vision-exam-for-driver-license-wi.pdf", "/checklist/auto/car-title/replace-title/virginia.html", "/checklist/auto/car-title/new-title/virginia.html", "/checklist/auto/car-registration/new-registration/pennsylvania.html", "/checklist/auto/drivers-license/new-drivers-license/california.html", "/checklist/travel/global-entry/ohio.html", "/checklist/travel/travel-precheck/virginia.html", "/checklist/travel/airport-precheck/california.html", "/checklist/recreation/fishing-license/virginia.html", "/checklist/recreation/hunting-license/ohio.html", "/checklist/recreation/name-change/pennsylvania.html", "/checklist/federal-resources/medicaid-help/pennsylvania.html", "/checklist/federal-resources/food-stamp-guide/ohio.html"];
   */
   var sample     = $( "#download-history-sample" ).html();
   var html       = "";
   try{
      for( var i=0; i<dLinks.length; i++ )
      {
         if(i==0 && dLinks.length>12){
            $(".view-all").show();
         }
         var tmp        = dNames[i].replace(".pdf","").split(" ");
         var category   = tmp[0];
         var state      = tmp[1];
         var service    = "change-of-name";
         if(dChecks[i] != "" && dChecks[i] != null){
            var tmp2    = dChecks[i].replace("checklist/auto/","checklist/").split("/");
            service     = tmp2[3];
            if( dChecks[i].indexOf("/related-resources/") != -1 || dChecks[i].indexOf("/related-forms/") != -1  )
            {
               service = "generic";
            }
         }
         var new_sample = sample;
             new_sample = new_sample.replace("download_date", dStamps[i].substring(0,10));
             new_sample = new_sample.replace("#download_cover", getCover(category, service, state));
             new_sample = new_sample.replace("download_link_pdf", dLinks[i].replace("http:","")+"&newredirectchecklist="+dChecks[i]);
         if(i<12){
            html       += new_sample;
         }else{
            break;
         }
      }
   }
   catch (e) {}
   if(html == ""){
      html = "<p>None.</p>";
   }
   return html;
}

function profileDownloadHistoryLoadAll(old_sample){
   var sample     = old_sample;
   var html       = "";
   try{
      for( var i=12; i<dLinks.length; i++ )
      {
         var tmp        = dNames[i].replace(".pdf","").split(" ");
         var category   = tmp[0];
         var state      = tmp[1];
         var service    = "change-of-name";
         if(dChecks[i] != ""){
            var tmp2    = dChecks[i].replace("checklist/auto/","checklist/").split("/");
            service     = tmp2[3];
         }
         var new_sample = sample;
             new_sample = new_sample.replace("download_date", dStamps[i].substring(0,10));
             new_sample = new_sample.replace("#download_cover", getCover(category, service, state));
             new_sample = new_sample.replace("download_link_pdf", dLinks[i].replace("http:","")+"&newredirectchecklist="+dChecks[i]);
             html      += new_sample;
      }
   }
   catch (e) {}
   return html;
}

function profileAccountDetails(){
   $( "#profile-first-name" ).html(login_name);
}

/***********************************************************************
   GET HREF, state/page, category - GLOBAL
************************************************************************/

var hrf = document.location.href;
var getThePageName = getPage();

try{
	var category = hrf.split(".org")[1].split("/")[1];
}
catch(e) {}

/***********************************************************************
   DOC READY
************************************************************************/

$(document).ready(function() {


/******************************************
   dynamic menu active
*******************************************/
try 
{
	if(hrf.indexOf("index") !=-1) 
	{
		$("#menu-index").addClass("active");
	}
	$("#menu-" + category).addClass("active");
}
catch(e) {}

/******************************************
   set date to today
*******************************************/
try 
{
	var d = new Date();
	var strDate = (d.getMonth()+1) + " / " + d.getDate() + " / " + d.getFullYear() ;
	$(".dynamicdate").text(strDate);
}
catch(e) {}

/******************************************
   dynamic category
*******************************************/
try 
{
	$(".showcategory").text(category.toUpperCase());
	$(".linktocatpage").attr("href", "/category/" + category + ".html");
}
catch(e) {}


/******************************************
   click continue to go to form
*******************************************/
$(".gotoform").click(function() {
	document.location.href = "/user/form/step1/" + category + "/" + getThePageName + ".html";
	
});

/******************************************
   dynamic guide title from URL
*******************************************/
/*try 
{
	var getService = hrf.split("/")[5];
	
	if(getService.endsWith(".html")){
		getService = hrf.split("/")[4];
	}
	
	//console.log(getService);
	// remove dashes and capitalize all words
	var service = capWordsWithDash(getService);
	// capitalize state names and remove dash if one from URL
	var state = "";
	if(getThePageName.indexOf("-") !=-1)
	{
		state = capWordsWithDash(getThePageName);
	}
	else
	{
		state = capitalize(getThePageName);
	}
	//alert(service + " in " + state);
	$(".titleofguide").text(service + " in " + state);
}
catch(e) {}*/

try 
{
	var getCat = hrf.split("/")[3];
	var getSubCategory = hrf.split("/")[4];
	var getService = hrf.split("/")[5];
	
	if(getService.endsWith(".html")){
		getService = hrf.split("/")[4];
		getSubCategory = '';
	}
	
	// remove dashes and capitalize all words
	var service = capWordsWithDash(getService);
	var subcategory = capWordsWithDash(getSubCategory);
	
	// capitalize state names and remove dash if one from URL
	var state = "";
	if(getThePageName.indexOf("-") !=-1)
	{
		state = capWordsWithDash(getThePageName);
	}
	else
	{
		state = capitalize(getThePageName);
	}
	
	if (getCat == 'auto')
	{
		if(service == 'Id Cards' || service == 'Learners Permit' ){
			if(service == 'Id Cards')
				service = 'ID Cards';
			$(".titleofguide").text("Get A " + state + " " + service);
		}
		else
			if(service == 'Change Of Name' || service == 'Change Of Address')
				$(".titleofguide").text(service + " for " + state + " " + subcategory);
			else
				$(".titleofguide").text(service + " in " + state);
	}
	else
	{
		if (getCat == 'recreation' && service == "Name Change")
			$(".titleofguide").text("Official Name Change Guide");
		else
		{
			if(service == "Medicaid Help")
				$(".titleofguide").text("Medicaid Benefits Guide");
			else{
				if(service == "Food Stamp Guide")
					$(".titleofguide").text("Food Stamp Benefits Guide");
				else
					if(service == "Global Entry")
						$(".titleofguide").text("Global Entry for International Travel");
					else
						$(".titleofguide").text(service + " Guide");
			}
		}
	}
	
	//$(".titleofguide").text(service + " in " + state);
	//$(".titleofguide").text(service + " for " + state + " " + subcategory);
}
catch(e) {}

/******************************************
   checkilst page - hide/show dynamic links
*******************************************/
if( hrf.indexOf("/checklist/") !=-1 )
{

}


});  //------------------------------------end of doc ready




/***********************************************************************
   FUNCTIONS
************************************************************************/
/* =============================================================
  Returns a string produced according to the formatting string format.
==============================================================*/
var sprintf = function(str) {
  var args = arguments,
    flag = true,
    i = 1;

  str = str.replace(/%s/g, function() {
    var arg = args[i++];

    if (typeof arg === 'undefined') {
      flag = false;
      return '';
    }
    return arg;
  });
  return flag ? str : '';
};

function getUrlParam(variable) 
{
   var query = window.location.search.substring(1);
   var vars = query.split('&');
   for (var i=0;i<vars.length;i++) 
   {
      var pair = vars[i].split('=');
      if (pair[0] == variable) 
      {
         return pair[1];
      }
   }
}


function sCode(stateName) {
   var s = new Array();
   s['alabama'] = 'AL'; s['alaska'] = 'AK'; s['arizona'] = 'AZ'; s['arkansas'] = 'AR'; s['california'] = 'CA'; s['colorado'] = 'CO'; s['connecticut'] = 'CT'; s['delaware'] = 'DE'; s['florida'] = 'FL'; s['georgia'] = 'GA'; s['hawaii'] = 'HI'; s['idaho'] = 'ID'; s['illinois'] = 'IL'; s['indiana'] = 'IN'; s['iowa'] = 'IA'; s['kansas'] = 'KS'; s['kentucky'] = 'KY'; s['louisiana'] = 'LA'; s['maine'] = 'ME'; s['maryland'] = 'MD'; s['massachusetts'] = 'MA'; s['michigan'] = 'MI'; s['minnesota'] = 'MN'; s['mississippi'] = 'MS'; s['missouri'] = 'MO'; s['montana'] = 'MT'; s['nebraska'] = 'NE'; s['nevada'] = 'NV'; s['new-hampshire'] = 'NH'; s['new-jersey'] = 'NJ'; s['new-mexico'] = 'NM'; s['new-york'] = 'NY'; s['north-carolina'] = 'NC'; s['north-dakota'] = 'ND'; s['ohio'] = 'OH'; s['oklahoma'] = 'OK'; s['oregon'] = 'OR'; s['pennsylvania'] = 'PA'; s['rhode-island'] = 'RI'; s['south-carolina'] = 'SC'; s['south-dakota'] = 'SD'; s['tennessee'] = 'TN'; s['texas'] = 'TX'; s['utah'] = 'UT'; s['vermont'] = 'VT'; s['virginia'] = 'VA'; s['washington'] = 'WA'; s['west-virginia'] = 'WV'; s['wisconsin'] = 'WI'; s['wyoming'] = 'WY';
   return s[stateName.toString()];
}

function toStateName(abbr) {
   var s = new Array();
   s['AL'] = 'alabama'; s['AK'] = 'alaska'; s['AZ'] = 'arizona'; s['AR'] = 'arkansas'; s['CA'] = 'california'; s['CO'] = 'colorado'; s['CT'] = 'connecticut'; s['DE'] = 'delaware'; s['FL'] = 'florida'; s['GA'] = 'georgia'; s['HI'] = 'hawaii'; s['ID'] = 'idaho'; s['IL'] = 'illinois'; s['IN'] = 'indiana'; s['IA'] = 'iowa'; s['KS'] = 'kansas'; s['KY'] = 'kentucky'; s['LA'] = 'louisiana'; s['ME'] = 'maine'; s['MD'] = 'maryland'; s['MA'] = 'massachusetts'; s['MI'] = 'michigan'; s['MN'] = 'minnesota'; s['MS'] = 'mississippi'; s['MO'] = 'missouri'; s['MT'] = 'montana'; s['NE'] = 'nebraska'; s['NV'] = 'nevada'; s['NH'] = 'new-hampshire'; s['NJ'] = 'new-jersey'; s['NM'] = 'new-mexico'; s['NY'] = 'new-york'; s['NC'] = 'north-carolina'; s['ND'] = 'north-dakota'; s['OH'] = 'ohio'; s['OK'] = 'oklahoma'; s['OR'] = 'oregon'; s['PA'] = 'pennsylvania'; s['RI'] = 'rhode-island'; s['SC'] = 'south-carolina'; s['SD'] = 'south-dakota'; s['TN'] = 'tennessee'; s['TX'] = 'texas'; s['UT'] = 'utah'; s['VT'] = 'vermont'; s['VA'] = 'virginia'; s['WA'] = 'washington'; s['WV'] = 'west-virginia'; s['WI'] = 'wisonsin'; s['WY'] = 'wyoming'; 
   return s[abbr.toString()];
}

/*----   capitalize first letter of multiple words (state names)   ----*/
function capWordsWithDash(str){ 
   var words = str.split("-"); 
   for (var i=0 ; i < words.length ; i++)
   { 
      var testwd = words[i]; 
      var firLet = testwd.substr(0,1); 
      var rest = testwd.substr(1, testwd.length -1) 
      words[i] = firLet.toUpperCase() + rest 
   } 
   return words.join(" "); 
}


/*----   capitalize word   ----*/
function capitalize(string) 
{ 
   return string.charAt(0).toUpperCase() + string.slice(1); 
} 

function getPage()
{
   var spliturl = document.location.href.split("/");
   return spliturl[spliturl.length-1].split(".")[0];
}


// Carousel time

$(document).ready(function(){


	if (hrf.indexOf("/billing.html") != -1) {

                //$("#item").val(       "Full Easy Guide"       );

		$("#billingForm").validate({
			onfocusout : false,
			onkeyup : false,
			rules : {
				first_name : {
					required : true
				},
				last_name : {
					required : true
				},
				state : {
					required : true
				},
				zip : {
					required : true,
					minlength : 5
				},
				card_number : {
					required : true,
					minlength : 16,
				},
				month : {
					required : true
				},
				year : {
					required : true
				},
				security_code : {
					required : true,
					minlength : 3
				}
			},
			messages : {
				first_name : {
					required : "First Name is Required"
				},
				last_name : {
					required : "Last Name is Required"
				},
				state : {
					required : "Required"
				},
				zip : {
					required : "Required",
					minlength : "Invalid ZIP"
				},
				card_number : {
					required : "Credit Card number is Required",
					minlength : "Invalid Credit Card number"
				},
				month : {
					required : "Required"
				},
				year : {
					required : "Required"
				},
				security_code : {
					required : "Security Code is Required",
					minlength : "Invalid Security Code"
				}
			},
			submitHandler : function(form) {
				$("#billingForm label.error").hide();
				$("#card_number-error").html("");

				/*var valid = $.payment.validateCardNumber($('#card_number').val());
				if (!valid) {
					$("#card_number-error").html("Invalid Credit Card number");
					$("#card_number-error").show();
					$("#card_number").focus();
					return false;
				}*/

				$("#billingForm #billing-error").html("");
				$("#billingSubmit").css({
					"display" : "none"
				});
				$("#submitLoad").css({
					"display" : "block"
				});
				form.submit();
			}
		});
	}


	if( $('.owl-carousel').length )         // use this if you are using class to check
	{
	    var owl = $('.owl-carousel');
		owl.owlCarousel({
	        loop: true,
	        margin: 10,
	        responsiveClass: true,
	        responsive: {
	           0: {
					items: 1,
					nav: true
	            },
				
				768: {
					items: 2,
					nav: true
	            },
				
				
				900: {
	                items: 4,
	                nav: false
	            },
	            
				1290: {
					items: 4,
	                nav: true,
	                loop: false,
	                margin: 20
	            }
	        }
	    })
	}
});



$(document).ready(function(){
   $(".session").click( function() {
      var lnk = $(this).attr("href");
      var hrf = window.location.pathname;
      $.ajax({
         "url": "/sessionvar.jsp?name=redirectchecklist&value=" + hrf,
         "success": function(response){
            document.location.href = lnk;
         }
      });
      event.preventDefault();
   });
});


