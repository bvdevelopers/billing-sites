<%@page import="java.util.*, com.util.*, com.bl.*, com.servlets.*"%>
<%
   HashMap hm = (HashMap) session.getAttribute( "returnuser" );
   String trid              =  (String) hm.get("transaction_id"    );
   String first_name        =  (String) hm.get("first_name"        );
   String last_name         =  (String) hm.get("last_name"         );
   String email             =  (String) hm.get("email"             );
   String phone_1           =  (String) hm.get("phone_1"           );
   
   String address_1         =  (String) hm.get("address_1"         );
   String suite_1           =  (String) hm.get("suite_1"           );
   String city_1            =  (String) hm.get("city_1"            );
   String state_1           =  (String) hm.get("state_1"           );
   String zip_1             =  (String) hm.get("zip_1"             );
   
   String address_2         =  (String) hm.get("address_2"         );
   String suite_2           =  (String) hm.get("suite_2"           );
   String city_2            =  (String) hm.get("city_2"            );
   String state_2           =  (String) hm.get("state_2"           );
   String zip_2             =  (String) hm.get("zip_2"             );
   
   String moving_type       =  (String) hm.get("moving_type"       );
   String residence_status  =  (String) hm.get("residence_status"  );
   String moving_time       =  (String) hm.get("moving_time"       );
   String usps_forward      =  (String) hm.get("usps_forward"      );
   String usps_forward_stop =  (String) hm.get("usps_forward_stop" );
   
   String sql = "insert into addresschange (repost,transaction_id,timestamp,first_name,last_name,email,phone_1,address_1,suite_1,city_1,state_1,zip_1,address_2,suite_2,city_2,state_2,zip_2,moving_type,residence_status,usps_forward,usps_forward_stop,moving_time) values ( 'Y', "+trid+",sysdate(),";
   sql += "\"" + first_name         + "\",";
   sql += "\"" + last_name          + "\",";
   sql += "\"" + email              + "\",";
   sql += "\"" + phone_1            + "\",";
   
   sql += "\"" + address_1          + "\",";
   sql += "\"" + suite_1            + "\",";
   sql += "\"" + city_1             + "\",";
   sql += "\"" + state_1            + "\",";
   sql += "\"" + zip_1              + "\",";
   
   sql += "\"" + address_2          + "\",";
   sql += "\"" + suite_2            + "\",";
   sql += "\"" + city_2             + "\",";
   sql += "\"" + state_2            + "\",";
   sql += "\"" + zip_2              + "\",";
   
   sql += "\"" + moving_type        + "\",";
   sql += "\"" + residence_status   + "\",";
   sql += "\"" + usps_forward       + "\",";
   sql += "\"" + usps_forward_stop  + "\",";
   sql += "\"" + moving_time        + "\")";
   com.util.DB.executeSQL( sql );
%>
<script>parent.document.location.replace( "/checklist/change-of-address/main.html" );</script>
