<body>
   <script src="/jquery.js"></script>
   <script>
      /*----------------------------------------------------------------------*/
      /*--- setup ...                                                      ---*/
      /*----------------------------------------------------------------------*/
      $(document).ready(function()
      {
         $("#email").change( function() {
            correctEmail( $(this) );
         });
      });
      
      /*----------------------------------------------------------------------*/
      /*--- correctEmail ...                                               ---*/
      /*----------------------------------------------------------------------*/
      function correctEmail( o )
      {
         trg  = o.val().toLowerCase().replace("www.", "");
         splt = trg.split( "@" );
         if( splt.length!=2 )
            return;
         trg = trg.replace( "..",    "." );
         trg = trg.replace( ".ccom",  ".com" );
         trg = trg.replace( ".comm",  ".com" );
         trg = trg.replace( ".coomm", ".com" );
      
         trg = trg.replace( ".cox",   ".com" );
         trg = trg.replace( ".xom",   ".com" );
         trg = trg.replace( ".cim",   ".com" );
         trg = trg.replace( ".con",   ".com" );
      
         trg = trg.replace( ".on",    ".com" );
         trg = trg.replace( ".cm",    ".com" );
         trg = trg.replace( ".nt",    ".net" );
         trg = trg.replace( ".co",    ".com" );
      
      
         trg = trg.replace( "gmial",  "gmail" );
         trg = trg.replace( "gamil",  "gmail" );
      
         if( splt.length == 2 )
         {
            splt = trg.split( "@" );
            dot  = splt[1].split(".")[1];
            dot  = dot==".co"?".com":dot;
            dot  = dot==".ne"?".net":dot;
      
            if( splt[1].indexOf("yaho")!=-1 )   splt[1] = "yahoo." + dot;
            else
            if( splt[1].indexOf("hotm")!=-1 )   splt[1] = "hotmail." + dot;
            else
            if( splt[1].indexOf("comca")!=-1 )  splt[1] = "comcast.net";
            else
            if( splt[1].indexOf("gma")!=-1 )    splt[1] = "gmail.com";
            else
            if( splt[1].indexOf("aol")!=-1 )    splt[1] = "aol.com";
      
         }
         rtn = splt[0] + "@" + splt[1];
         rtn = rtn.replace( ".comm", ".com" );
         rtn = rtn.replace( ".cpm",  ".com" );
         rtn = rtn.replace( ".undefined", ".com" );
      
      
         o.val( rtn );
      }
   </script>
   
   
   
   
   
   <input type="text" id="email" placeholder="type an invalid email the press tab" style="width:300px;">
</body>

