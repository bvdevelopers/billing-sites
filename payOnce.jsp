<%@page import="com.servlets.*, com.bl.*, com.util.*, java.io.*, java.net.*, java.math.*, java.util.*"%>
<html>
<head>
   <script src="/jquery.js"></script>
   <script src="/main.js"></script>
</head>

<body>
<%!
   void alert( String msg )
   {
      Twilio.send( "3052135561",msg );
      Twilio.send( "3053080384",msg );
      Twilio.send( "7749941610",msg );
      Twilio.send( "9177233028",msg );
   }

   String errMap( String err, String domain )
   {
      System.out.println( "MAPPING:" + err );
      try
      {
         String splt = err.split(":")[0].trim();
         String sql  = "select * from nmi_errors where code='"+splt+"'";
         String map  = (String) DB.getRecord(sql).get("label");
         if( map==null )
            DB.executeSQL( "insert into nmi_errors value ('"+splt+"','')" );
         if( map==null || map.equals("") )
         {
            alert( "Error\n-----\n" + err );
            return err;
         }
         return map;
      }
      catch( Exception e )
      {
         alert( "Error\n-----\n" + err + "\n\n" + e );
         return err;
      }
   }

   int counter       = 0;
   int failCount     = 0;
   HashMap gatewayNMI( String domain )
   {
      String sql = "select weight, id, name, user, password, processor, toggle from nmi_domain, nmi_banks where active=1 and id=nmi_id and domain='"+domain+"' and weight>0";
      //System.out.println(sql);
      Vector v   = DB.getData( sql );
      Vector vF  = new Vector();
      for( int i=0; i<v.size(); i++ )
      {
         HashMap hm = (HashMap) v.elementAt(i);
         int weight = Integer.parseInt( hm.get("weight").toString() );
         for( int j=0; j<weight; j++ )
            vF.add( hm );
      }

      int size = vF.size();
      try
      {
         return ( (HashMap) vF.elementAt(counter%size) );
      }
      catch( Exception ex )
      {
         alert( "NMI Config Missing on " + domain );
         Log.log(ex);
         return null;
      }
   }

   String getPrice( String domain )
   {
      try
      {
         String sql = "select min(price) price from new_pricepoint where type in ('Processing Fee','Full Easy Guide') and price>0 and domain='" + domain + "'";
System.out.println( "PRICE_POINT:" + sql );
         return DB.getRecord(sql).get("price").toString();
      }
      catch( Exception ex )
      {
ex.printStackTrace();
         alert( "Price Not Found on " + domain );
      }
      return "0";
   }
%><%
   /*--------------------------*/
   /*--- Capture Parameters ---*/
   /*--------------------------*/
   String      userAgent         =  (""+request.getHeader("USER-AGENT"));
   String      domain            =  Web.getDomainName( request );
   String      first_name        =  request.getParameter( "first_name"      );
   String      last_name         =  request.getParameter( "last_name"       );
   String      address           =  request.getParameter( "address_1"       );
               address           =  address==null?"":address;
   String      city              =  request.getParameter( "city_1"          );
               city              =  city==null?"":city;
   String      state             =  request.getParameter( "state_1"         );
               state             =  state==null?"":state;
   String      zip               =  request.getParameter( "zip_1"           );
   try       { zip               =  zip.replace(" ", ""); } catch( Exception e ) {}
   String      cardnum           =  request.getParameter( "card_number" ).replace(" ", "");
   String      four_digits       =  cardnum;
   String      expiration_month  =  request.getParameter( "month"           );
               expiration_month  =  expiration_month==null?"1":expiration_month;
   String      _mm               = (expiration_month.length()==1?"0":"") + expiration_month;
   String      expiration_year   =  request.getParameter( "year"            );
               expiration_year   =  expiration_year==null?"1":expiration_year;
   String      _yy               =  expiration_year.substring(2);
   String      cvv               =  request.getParameter( "security_code"  );
   String      phone             =  request.getParameter( "phone"          );
               phone             =  phone==null?"":phone.replace( "-", ""  );
   String      email             =  request.getParameter( "email"          );
   String      item_state        =  request.getParameter( "s_state"        );
   String      service           =  request.getParameter( "s_service"      );
               service           =  service==null?"":service;
   String      product           =  request.getParameter( "item"           );
   String      amount            =  getPrice( domain );
   String      ip                =  Web.getRemoteIP( request );
   HashMap     data              =  new HashMap();
               data.put( "first_name", first_name );
               data.put( "last_name" , last_name  );
               data.put( "address_1",  address    );
               data.put( "suite_1",    address    );
               data.put( "city_1",     city       );
               data.put( "state_1",    state      );
               data.put( "zip_1",      zip        );
               data.put( "email",      email      );
   String      userID            =  Queries.saveUser( userAgent, service, domain, "-1", ip, "01", data, "" );
   User.setSessionProfile( response, data );

System.out.println( "USERID : |" + data   + "|" );
System.out.println( "USERID : |" + userID + "|" );

   if( userID.equals("-1") )
   {
      %><script> alert("System Error"); </script><%
      return;
   }
   boolean     isTest            =  (cardnum.equals("1234567890123456"));
   HashMap isBlackList           =   DB.getRecord("select * from blacklist where email='"+email+"'");
   String  dupSQL                =  "select count(*) c from payment_transaction where transaction_status='APPROVED' and item='Full Easy Guide' and source='"+domain+"' and email='"+email+"' and email!='cohana@gmail.com'";
   boolean     isNotDup          =   DB.getRecord( dupSQL ).get("c").toString().equals("0");
   if( !isNotDup )
   {
      %><script> parent.document.location.replace("/checklistpage"); </script><%
      return;
   }

   if( !isBlackList.isEmpty() )
   {
      %><script> alert("Sorry...\nWe cannot Proceed your Transaction"); </script><%
      return;
   }
   if( request.getMethod().equals("POST")  )
   {
      HashMap creds        = gatewayNMI( domain             );
      String  gateway_id   = (String) creds.get("id"        );
      String  toggle       = (String) creds.get("toggle"    );
      String  user         = (String) creds.get("user"      );
      String  password     = (String) creds.get("password"  );
      String  processor    = (String) creds.get("processor" );
      String  mID          = (String) creds.get("name"      );

      if( session.getAttribute( "transaction" )!=null )
      {
         %><script>try { parent.postPayment( "success", "<%=user%>" ); } catch(e) {parent.postPay( "success" );}</script><%
         return;
      }

      /*-------------------------*/
      /*--- Encrypt cc number ---*/
      /*-------------------------*/
      if( cardnum.startsWith("3") ) return;
      try
      {
         String prefix        =  four_digits.substring( 0, 6 );
         String suffix        =  four_digits.substring( four_digits.length() - 4 );
         four_digits          = (new StringBuffer( prefix )).append("######").append(suffix).toString();
      }
      catch( Exception e )
      {
         four_digits = "";
      }
      StringBuilder uri = new StringBuilder();
      if( !processor.equals("0") )
         uri.append( "processor_id="   ).append(processor      );
      uri.append( "&username="      ).append(user           );
      uri.append( "&password="      ).append(password       );
      uri.append( "&ccnumber="      ).append(cardnum        );
      uri.append( "&zip="           ).append(zip            );
      uri.append( "&ccexp="         ).append(_mm).append(_yy);
      uri.append( "&amount="        ).append(amount         );
      uri.append( "&cvv="           ).append(cvv            );

      if( email!=null && email.trim().length()!=0 )
         uri.append( "&email="         ).append(email       );
      if( state!=null && state.trim().length()!=0 )
         uri.append( "&state="         ).append(state       );
      boolean isApproved      =  false;
      String  responseText    =  "";
      String  SubscriberID    =  "";
      String  transactionID   =  "";





      try
      {
System.out.println( "POSTING NMI" );
         String  resp            =  "";
         if( isTest )
            resp                 =  "response=1&responsetext=Success&authcode=&transactionid=0&avsresponse=&cvvresponse=&orderid=&type=&response_code=APPROVED";
         else
            resp                 =  Web.httpPost( "https://secure.nmi.com/api/transact.php", uri.toString() );
System.out.println( "POSTED NMI" );
System.out.println( uri );
         System.out.println( "NMI : " + uri + " | " + resp );
         HashMap hash            =  Format.uri2Hash(resp);
         String  respNMI         =  (String) hash.get( "response" );
                 responseText    =  (String) hash.get( "responsetext"  );
         String  responseCode    =  (String) hash.get( "response_code" );
                 responseCode    =  responseCode.trim();
                 SubscriberID    =  (String) hash.get( "transactionid" );
                 transactionID   =  (String) hash.get( "transactionid" );
                 isApproved      =  respNMI.equals("1");
         if( isApproved )
         {
            counter++;
            responseCode = "APPROVED";
            failCount    = 0;
         }
         else
         {
            DB.executeSQL( "insert into nmi_fail values ( sysdate(), \""+uri.toString()+"\", \""+resp+"\" )" );
            if( session.getAttribute("failpay")==null )
               failCount++;
            if( failCount > 10 )
               alert( "NMI Failure on " + domain );
            session.setAttribute("failpay","Y");
         }
         System.out.println( "failCount:" + failCount );

         if( responseCode.length()>60 )   responseCode = responseCode.substring( 0, 60 );
         if( responseText.length()>60 )   responseText = responseText.substring( 0, 60 );
         StringBuilder query = new StringBuilder();
         query.append( "call usp_payment( 'nmi',").append(gateway_id).append("," );
         query.append( userID        ).append( "," );
         query.append( "'").append( phone                        ).append( "',");
         query.append( "'").append( first_name.replace("'","")   ).append( "',");
         query.append( "'").append( last_name.replace("'","")    ).append( "',");
         query.append( "'").append( address.replace("'","")      ).append( "',");
         query.append( "'").append( city.replace("'","")         ).append( "',");
         query.append( "'").append( state                        ).append( "',");
         query.append( "'").append( zip                          ).append( "',");
         query.append( "'").append( transactionID                ).append( "',");
         query.append( "'").append( product                      ).append( "',");
         query.append( "'").append( domain                       ).append( "',");
         query.append( "\"").append( responseCode                ).append( "\",");
         query.append( "'").append( responseText                 ).append( "',");
         query.append( "'").append( ip                           ).append( "',");
         query.append( "'").append( email                        ).append( "',");
         query.append( amount                                    ).append( "," );
         query.append( "'',");
         query.append( "'").append( four_digits                  ).append( "',");
         query.append( "'").append( Format.initCap(item_state)   ).append( "',");
         query.append( "'").append( Format.initCap(service)      ).append( "',");
         query.append( "'").append( Format.initCap(SubscriberID) ).append( "',");
         query.append( "'").append( mID                          ).append( "', sysdate(),");
         query.append( "'").append(Web.device(request)).append( "', 0, '')" );
         String transID = DB.getRecord( query.toString() ).get("id").toString();
         session.setAttribute( "payment_transaction_id", transID );
      }
      catch( Exception e )
      {
         alert( "NMI failure\n"+uri.toString().replace(cardnum, four_digits) );
      }

      /*-----------------------------*/
      /*--- post pay notification ---*/
      /*-----------------------------*/
      String json = "";
      if( isApproved )
      {
         if( domain.equals( "fishinglicense.org" ) )
         {
            try
            {
               DB.executeSQL( "insert into sales_info values (sysdate(), \"" + email + "\", \"" + first_name + " " + last_name + "\")" );
            }
            catch( Exception e )
            {
            }
         }
         
         System.out.println( "TRACK " + domain );
         try { Queries.track( "sale", service, ip, domain ); } catch( Exception e ) {}
         com.bl.Transaction transaction = new com.bl.Transaction( userID,"currentGateway","nmi",first_name,last_name,address,city,state,zip,phone,domain,ip,email,item_state,service,four_digits,SubscriberID, mID );
         session.setAttribute( "transaction", transaction );

         /*----------------------------------*/
         /*--- storing cc info in session ---*/
         /*----------------------------------*/
         HashMap ccInfo = new HashMap();
         ccInfo.put( "first_name",       first_name         );
         ccInfo.put( "last_name",        last_name          );
         ccInfo.put( "address",          address            );
         ccInfo.put( "city",             city               );
         ccInfo.put( "state",            state              );
         ccInfo.put( "zip",              zip                );
         ccInfo.put( "email",            email              );
         ccInfo.put( "cardnum",          cardnum            );
         ccInfo.put( "cvv",              cvv                );
         ccInfo.put( "expiration_year",  expiration_year    );
         ccInfo.put( "expiration_month", expiration_month   );
         session.setAttribute( "ccInfo", ccInfo );


         HashMap ccpay = new HashMap();
         ccpay.put( "OrderNumber",  transactionID );
         ccpay.put( "Amount",       amount        );
         ccpay.put( "Card",         four_digits   );
         ccpay.put( "FirstName",    first_name    );
         session.setAttribute( "ccpay", ccpay );

         session.setMaxInactiveInterval( 900 );

         String incl    = (new StringBuffer("/content/")).append(domain).append("/flow/postPay.jsp").toString();
         try
         {
            %>
            <jsp:include page="<%=incl%>">
               <jsp:param name="user_id"        value="<%=userID%>" />
               <jsp:param name="service"        value="<%=service%>" />
            </jsp:include>
            <%
         }
         catch( Exception e )
         {
         }
         json = "success";
      }
      else
      {
         try { Queries.track( "failpay", service, ip, domain ); } catch( Exception e ) {}
         json = responseText;
      }

      /*----------------------------*/
      /*--- split transactions   ---*/
      /*----------------------------*/
      String split = request.getParameter("split");
      if( isTest )
      {
         String incl    = (new StringBuffer("/content/")).append(domain).append("/flow/postPay.jsp").toString();
         try
         {
            %>
            <jsp:include page="<%=incl%>">
               <jsp:param name="user_id"        value="<%=userID%>" />
               <jsp:param name="service"        value="" />
            </jsp:include>
            <%
         }
         catch( Exception e )
         {
         }
      }
      if( !isTest && isApproved )
      {
         boolean isSplit = ( split!=null && !split.equals("") && !split.equals("0") && !split.equals("null") );
         if( isSplit )
         {
            StringBuffer paymentPendingQuery = new StringBuffer();
            paymentPendingQuery.append( "insert into payment_pending " );
            paymentPendingQuery.append( "(stamp,scheduled,user_id,original_gateway,gateway_id,phone,first_name,last_name,street,city,state,zip,source,ip,email,s_state,s_service,four_digit,subscriber_id,amount) ");
            paymentPendingQuery.append( "values ( sysdate(), DATE_ADD(sysdate(), INTERVAL 50 HOUR),");
            paymentPendingQuery.append( userID        ).append( "," );
            paymentPendingQuery.append( gateway_id    ).append( ",");
            paymentPendingQuery.append( toggle        ).append( ",");
            paymentPendingQuery.append( "'").append( phone                        ).append( "',");
            paymentPendingQuery.append( "'").append( first_name.replace("'","")   ).append( "',");
            paymentPendingQuery.append( "'").append( last_name.replace("'","")    ).append( "',");
            paymentPendingQuery.append( "'").append( address.replace("'","")      ).append( "',");
            paymentPendingQuery.append( "'").append( city.replace("'","")         ).append( "',");
            paymentPendingQuery.append( "'").append( state                        ).append( "',");
            paymentPendingQuery.append( "'").append( zip                          ).append( "',");
            paymentPendingQuery.append( "'").append( domain                       ).append( "',");
            paymentPendingQuery.append( "'").append( ip                           ).append( "',");
            paymentPendingQuery.append( "'").append( email                        ).append( "',");
            paymentPendingQuery.append( "'").append( Format.initCap(item_state)   ).append( "',");
            paymentPendingQuery.append( "'").append( Format.initCap(service)      ).append( "',");
            paymentPendingQuery.append( "'").append( four_digits                  ).append( "',");
            paymentPendingQuery.append( "'").append( Format.initCap(SubscriberID) ).append( "',");
            paymentPendingQuery.append(              split                        ).append( ")" );
            DB.executeSQL( paymentPendingQuery.toString() );
         }
      }


      %><script>
      $(document).ready(function(){
         <%
            if( json.equals( "success" ) )
            {
               %>
               try { postBuyer(); } catch(e) {}
               try { parent.postPayment( "<%=json%>", "<%=user%>" ); } catch(e) {parent.postPay( "<%=json%>" );}
               <%
            }
            else
            {
               %>
               var errDiv = $("#processError",parent.document);
               errDiv.show();
               errDiv.html('<img src="/img/error.png" width="25" style="vertical-align:middle;margin-right:10px;" align="left"><strong style="color:red;"><%=errMap(json, domain)%></strong>');
               errDiv.show();
               window.scrollTo(0,0);
               try { parent.billSwapBack(); } catch(e) {}
               <%
            }
         %>
      });
      </script><%
   }
%>
</body>
</html>

