<%@ page isErrorPage="true" import="java.util.*, com.util.*, com.servlets.*"%>
<%

   String ip = request.getRemoteAddr().toString();
   if( ip.equals( "127.0.0.1" ) )
   {
      response.setContentType( "text/plain" );
      out.print( Log.printStackTrace( (Exception) exception ) );
   }
   else
   {
      response.setContentType( "text/html" );
      String log = "";
      log += "<div style='font-family:verdana, arial; font-size:12px;'>";
      log += "<b>" + (new Date()).toString() + "</b><p>";
      log += Log.printStackTrace( (Exception) exception );

      log += "<hr><font color='red'>";
      log += "" + request.getHeader("USER-AGENT") + "<br>" + request.getRemoteAddr();
      log += "</font>";

      log += "<hr><font color='blue'>" + User.getSessionProfile(request) + "</font>";
      log += "</div>";
      log = log.replace( "\n", "<br>" );

      Log.log( (Exception) exception );


      Mail.send( request, "cohana@gmail.com", "Exception on " + Web.getDomainName(request), log, null, 0, null );


      %><jsp:forward page="/500.html" /><%



   }
%>
