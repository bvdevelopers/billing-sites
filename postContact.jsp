<%@page import="com.util.*"%>
<%
   String domain     =  com.util.Web.getDomainName( request );
   String name       =  request.getParameter("name");
   String email      =  request.getParameter("email");
   String content    =  request.getParameter("content");
   if( name==null || email==null || content==null )
   {
      GoogleApps.send( domain, domain, "cohana@gmail.com", "Bogus Email", "" + name + "<br>" + email + "<br>" + content );
   }
   else
   {
      GoogleApps.send( domain, name,   "info@"+domain,     "Contact Form", content.replace("\n", "<br>"), email );
   }
%>
<script>
   parent.dialogMail();
</script>
