<%@page import="com.util.*, java.util.*, com.html.*, com.bl.*" %>
<body>
   <%
      String min  = request.getParameter("min");
             min  = min==null?"1":min;
      String sql  = "SELECT count(*) count, source from payment_transaction";
             sql += " where stamp > NOW() - INTERVAL " + min + " MINUTE";
             sql += " and item in ('Easy Guide', 'Full Easy Guide')";
             sql += " and transaction_status='APPROVED'";
             sql += " group by source order by 1 desc";
      Vector v    = DB.getData( sql );
   %>
   <style>
      *, input { font-family : verdana; font-size:10px !important; }
      table { border-collapse : collapse; }
      th, th {
         text-align:left;
      }
      th { color:#FFFFFF; background:#000000; }
      input {
         border     : 0px solid red;
         width      : 30px;
         padding    : 0px;
         text-align : right;
         background : transparent;
      }
      lbl, input {
         font-family : verdana !important;
         font-size   : 12px !important;
      }
   </style>
   <form>
   <table><tr>
      <td class='lbl'>Last</td>
      <td><input onChange="document.frm.submit();" name="min" type="tel" value="<%=min%>"></td>
      <td class='lbl'>Minute Sales</td>
   </tr></table>
   </form>
   <table border="1" class="tab">
   <tr>
      <th class="tab">Domain</th>
      <th class="tab">Sales</th>
   </tr>
   <% int sum  = 0;
      for( int i=0; i<v.size(); i++ )
      {
         HashMap hm = (HashMap) v.elementAt(i);
         %><tr class="tab">
           <td class="tab"><%=hm.get("source")%></td>
           <td class="tab num"><%=hm.get("count")%></td>
         </tr><%
         sum += Integer.parseInt( hm.get("count").toString() );
      }
   %>
   <tr>
      <th class="tab">Total</th>
      <th class="tab num"><%=sum%></th>
   </tr>
   </table>
</body>

