<%@page import="com.util.*, com.bl.*, java.util.*, com.google.gson.*, com.google.gson.reflect.TypeToken.*" %><%
   response.setContentType("application/json");
   com.servlets.ContentInclude.noCache( response );
   
   if( request.getMethod().equals("POST") )
   {
      String          db   = request.getParameter( "db" );
      String        year   = request.getParameter( "year"  );
      String        make   = request.getParameter( "make"  );
      String       model   = request.getParameter( "model" );
      Vector           v   = Queries.getCars( db, year, make, model );
      LinkedHashMap  cardata  = new LinkedHashMap();
      HashMap        exists   = new HashMap();
      for( int i=0; i<v.size(); i++ )
      {
         HashMap hm = (HashMap) v.elementAt( i );
         if( make==null && model==null )
         {
            if( exists.get( hm.get("make") ) == null )
            {
               cardata.put( hm.get("make_value"),  hm.get("make") );
               exists.put( hm.get("make"), "" );
            }
         }
         else
         if( make!=null && model==null )
         {
            if( exists.get( hm.get("model") ) == null )
            {
               cardata.put( hm.get("model_value"), hm.get("model") );
               exists.put( hm.get("model"), "" );
            }
         }
         else
         if( make!=null && model!=null )
         {
            if( exists.get( hm.get("trim") ) == null )
            {
               cardata.put( hm.get("trim_value"),  hm.get("trim") );
               exists.put( hm.get("trim"), "" );
            }
         }
      }
      String json = (new com.google.gson.Gson()).toJson(cardata);
      out.println(json);
   }
%>
