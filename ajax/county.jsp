<%@page import="com.util.*, com.bl.*, java.util.*" %>[<%!
   HashMap cache = new HashMap();
%><%
   response.setContentType("application/json");
   
   String qs      = request.getQueryString();
   String content = (String) cache.get( qs );
   com.servlets.ContentInclude.noCache( response );
   if( content==null )
   {
      String       state   = request.getParameter( "state" );
      content = "";
      if( state.length()>2 )
      {
         try { state = Static.STATE_REVERSE_LOOKUP.get( state ).toString().toUpperCase(); } catch( Exception e ) {}
      }
      String       query   = "select distinct replace( lower(county), ' ', '-' ) county from zip where state='"+state+"' order by county";
      Vector           v   = DB.getData( query );
      for( int i=0; i<v.size(); i++ )
      {
         if( i>0 )
            content += ",";
         content += "{\"county\":\"" + ((HashMap) v.elementAt(i)).get("county") + "\"}";
      }
      if( content.length() > 0 )
         cache.put( qs, content );
   }
   out.print( content );
%>]
