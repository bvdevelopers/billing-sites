<%@page import="com.util.*, com.bl.*, java.util.*, com.google.gson.*, com.google.gson.reflect.TypeToken.*" %><%
//   if( request.getMethod().equals("POST") )
   {
      com.servlets.ContentInclude.noCache( response );
      response.setContentType("application/json");
      String  zip = request.getParameter( "zip" );
      try
      {
         int     nZip   = Integer.parseInt( zip   );
         HashMap hm     = Queries.lookupZIP( zip  );
         String  county = (String) hm.get("county");
         hm.put( "county_name", county );
         hm.put( "county",      county.replace(" ", "-").toLowerCase() );

         String state =  "" + Static.STATE_LOOKUP.get((""+hm.get("state")).toLowerCase());
         hm.put( "state_name", state );
         String json = (new com.google.gson.Gson()).toJson(hm);
         session.setAttribute( "zipcode",   hm );
System.out.println( "SETUP splashzip" );
         session.setAttribute( "splashzip",   zip   );
         session.setAttribute( "splashstate", state );


         out.println(json);
      }
      catch( Exception e )
      {
         e.printStackTrace();
      }
   }
%>
