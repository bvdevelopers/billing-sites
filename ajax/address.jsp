<%@page import="java.net.*, java.io.*, java.util.*, com.util.*, com.google.gson.*, com.google.gson.reflect.TypeToken.*" %><%!
   public static String webGet( String url ) throws Exception
   {
      URLConnection   connection =  new URL(url).openConnection();
      connection.setRequestProperty( "Accept", "text/xml" );
      InputStream             is =  connection.getInputStream();
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      byte[]                   b = new byte[1024];
      int                     nr = 0;
      while( (nr=is.read(b)) != -1 )
         baos.write( b, 0, nr );
      is.close();
      return new String(baos.toByteArray());
   }
%><%
   response.setContentType("application/json");
   if( true )
   {
      System.out.println( "ADDRESS : " + Web.getDomainName(request) + " : " + request.getQueryString() );
      return;
   }
   try
   {
      String street     =  request.getParameter("street");
             street     =  street==null?"":street;
      String secondary  =  request.getParameter("suite");
             secondary  =  secondary==null?"":secondary;
      String city       =  request.getParameter("city");
             city       =  city==null?"":city;
      String state      =  request.getParameter("state");
             state      =  state==null?"":state;
      String zipcode    =  request.getParameter("zip");
             zipcode    =  zipcode==null?"":zipcode;
      
      com.servlets.ContentInclude.noCache( response );
      
      StringBuffer u = new StringBuffer();
      u.append( "https://api.smartystreets.com/street-address" );
      u.append( "?auth-id=82ba2310-4b39-4c64-b049-cd0e664a8128" );
      u.append( "&auth-token=" ).append( URLEncoder.encode("EGl5L4kHM4Di8Fc7qugV1X9QPAx0L02XoIAQZtiKt4g6rBOzDdd+FobJyjCtVemWReakXqpBl1wjZ6LqqXSGzg==", "UTF-8") );
      u.append( "&street="     ).append( URLEncoder.encode( street,      "UTF-8") );
      u.append( "&secondary="  ).append( URLEncoder.encode( secondary,   "UTF-8") );
      u.append( "&city="       ).append( URLEncoder.encode( request.getParameter("city"),     "UTF-8") );
      u.append( "&state="      ).append( URLEncoder.encode( request.getParameter("state"),    "UTF-8") );
      u.append( "&zipcode="    ).append( URLEncoder.encode( request.getParameter("zip"),      "UTF-8") );
      
      String   xml         =  webGet( u.toString() );
      while( !xml.startsWith("<") ) xml = xml.substring(1);
      XML      xResp       =  new XML( xml ); 
      
      String _type               =  xResp.parse( "/candidates/candidate/metadata/rdi"         );
      String _number             =  xResp.parse( "/candidates/candidate/components/primary_number"      );
      String _street             =  xResp.parse( "/candidates/candidate/components/street_name"         );
      String _suffix             =  xResp.parse( "/candidates/candidate/components/street_suffix"       );
      String _city               =  xResp.parse( "/candidates/candidate/components/city_name"           );
      String _default_city_name  =  xResp.parse( "/candidates/candidate/components/default_city_name"   );
      String _state              =  xResp.parse( "/candidates/candidate/components/state_abbreviation"  );
      String _zip                =  xResp.parse( "/candidates/candidate/components/zipcode"             );
      String _zipplus            =  xResp.parse( "/candidates/candidate/components/plus4_code"          );
      
      HashMap  suggestion  =  new HashMap();
      suggestion.put( "type",    _type     );
      suggestion.put( "number",  _number   );
      suggestion.put( "street",  _street   );
      suggestion.put( "suffix",  _suffix   );
      if( _default_city_name.equals("") )
         suggestion.put( "city",    _city     );
      else
         suggestion.put( "city",    _default_city_name     );
      suggestion.put( "state",   _state    );
      suggestion.put( "zip",     _zip      );
      suggestion.put( "zipplus", _zipplus  );
      
      String  json = (new com.google.gson.Gson()).toJson(suggestion);
      out.println(json);
   }
   catch( Exception e )
   {
      out.print( "{\"zip\":\"\",\"zipplus\":\"\",\"street\":\"\",\"state\":\"\",\"number\":\"\",\"type\":\"\",\"suffix\":\"\",\"city\":\"\"}" );
   }
%>
