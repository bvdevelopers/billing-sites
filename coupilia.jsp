<!doctype html>
<%@page import="java.util.*, com.util.*, org.dom4j.DocumentHelper, org.dom4j.*, org.dom4j.DocumentException, org.dom4j.io.SAXReader"%><%!
   static Vector vXML      =  null;
   static int    perPage   =  10;
%>
<body onContextMenu="return true;" style="padding:0px; margin:0px;">
   <style>
      *{font-family:arial} a:link, a:visited, a:active, a:hover {text-decoration:none; color:navy;} a:hover {color:red;}
      div { text-overflow:ellipsis; }
   </style>
   <%
      if( request.getParameter("reset")!=null )
         vXML = null;
      if( vXML==null )
      {
         HashMap   tokens       =  DB.getHashData( "select * from coupilia", "domain", "token" ); 
         String    domain       =  Web.getDomainName(request);
         String    token        =  (String) tokens.get( domain );
         String    link         =  "http://www.coupilia.com/feeds/coupons_v2.asp?token=" + token + "&recordset=all";
if( request.getParameter("DEBUG")!=null )
{
   out.print( link );
   return;
}
         String    cnt          =  new String( Web.webGet( link ) );

         vXML                   =  new Vector();
         Document  root         =  DocumentHelper.parseText( cnt );
         ArrayList alMerchant   =  (ArrayList) root.selectNodes( "/coupons/item/merchant"   );
         ArrayList alOffer      =  (ArrayList) root.selectNodes( "/coupons/item/offer"      );
         ArrayList alUrl        =  (ArrayList) root.selectNodes( "/coupons/item/url"        );
         ArrayList alLogo       =  (ArrayList) root.selectNodes( "/coupons/item/logo"       );
         ArrayList alEnddate    =  (ArrayList) root.selectNodes( "/coupons/item/enddate"    );
         ArrayList alMerchantid =  (ArrayList) root.selectNodes( "/coupons/item/merchantid" );
         
         for( int i=0; i<alMerchant.size(); i++ )
         {
            String merchant   =  ((Element) alMerchant.get(i)   ).getText();
            String offer      =  ((Element) alOffer.get(i)      ).getText();
            String url        =  ((Element) alUrl.get(i)        ).getText();
            String logo       =  ((Element) alLogo.get(i)       ).getText();
            String enddate    =  ((Element) alEnddate.get(i)    ).getText();
            String merchantid =  ((Element) alMerchantid.get(i) ).getText();
            String enddateISO =  Format.dateISO( enddate );
            String today      =  Format.todayISO().split("@")[0];
            if( today.compareTo(enddateISO)<0 )
            {
               HashMap rec = new HashMap();
               rec.put( "merchant",    ((Element) alMerchant.get(i)   ).getText() );
               rec.put( "offer",       ((Element) alOffer.get(i)      ).getText() );
               rec.put( "url",         ((Element) alUrl.get(i)        ).getText() );
               rec.put( "logo",        ((Element) alLogo.get(i)       ).getText() );
               rec.put( "enddate",     ((Element) alEnddate.get(i)    ).getText() );
               rec.put( "merchantid",  ((Element) alMerchantid.get(i) ).getText() );
               rec.put( "enddateISO",  Format.dateISO( enddate )                  );
               vXML.addElement( rec );
            }
         }
      }
      String    p = request.getParameter("p");
      int      pg = 1;
      int     nPg = (int) (vXML.size() / perPage);
      try    { pg = Integer.parseInt(p); } catch( Exception e ) {};
      int   start = (pg-1)*perPage;
      int   end   = start + perPage;
      end = Math.min( vXML.size(), end );
      
      for( int i=start; i<end; i++ )
      {
         HashMap hm = (HashMap) vXML.elementAt(i);

         out.print( "<div style='width:400px; height:100px; padding:10px; float:left; overflow:hidden;'>" );
         out.print(    "<div style='background:#FFFFFF; height:60px; border:1px solid #000000; padding:10px; overflow:hidden; text-overflow:ellipsis; '>" );
         out.print(       "<a target='_blank' href='"+hm.get("url")+"'><img src='http://coupilia.s3.amazonaws.com/"+hm.get("merchantid")+".png' align='left' style='margin-right:10px'></a>" );
         out.print(       "<div style='font-size:18px; font-weight:bold;'><a target='_blank' href='"+hm.get("url")+"'>"+hm.get("merchant")+"</a></div>" );
         out.print(       "<div style='overflow:hidden; text-overflow:ellipsis; font-size:12px;'><a target='_blank' href='"+hm.get("url")+"'>"+hm.get("offer")+"</a></div>" );
         out.print(       "<div><a style='font-size:12px;' target='_blank' href='"+hm.get("url")+"'>Click here for details</a></div>" );
         out.print(    "</div>" );
         out.print( "</div>" );
      }
   %>

   <div style='text-align:center; clear:both;'>
   <%
      if( pg>1 )
         out.print( "<a title='Previous Page' href='?p="+(pg-1)+"'>&lt;&lt;&lt;</a>&nbsp;&nbsp;&nbsp;&nbsp;" );
   %>
   Page : 
   <select style="padding:0px 5px 0px 5px;" onChange="document.location.replace('?p='+this.value)">
      <%
         for( int i=1; i<=nPg; i++ )
         {
            %><option <%=pg==i?"selected":""%> value="<%=i%>"><%=i%></option><%
         }
      %>
   </select>
   <%
      if( pg<nPg )
         out.print( "&nbsp;&nbsp;&nbsp;&nbsp;<a title='Next Page' href='?p="+(pg+1)+"'>&gt;&gt;&gt;</a>" );
   %>
   </div>
   </body>
</html>
