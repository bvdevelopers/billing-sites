<%@page import="java.util.*, com.util.*"%><%
try
{
   int    count   = Integer.parseInt( request.getParameter("count") );
   String type    = request.getParameter( "type" );
          type    = type==null?"drivers":type;
   String service = request.getParameter( "service" );
          service = service==null?"":service;
          service = service.equals("")?"new-drivers-license":service;
   String state = "";
   String ref   = request.getParameter( "ref" );
   String sql   = "select t.state, w.link, weight from site_toggler_weight w, site_toggler t where type='"+type+"' and t.id=statesite_id and t.link='"+ref+"'";
   Vector v       = DB.getData( sql );
   Vector lnk     = new Vector();
   for( int i=0; i<v.size(); i++ )
   {
      HashMap  hm   = (HashMap) v.elementAt(i);
      String link   = hm.get("link"  ).toString();
            state   = Format.getStateName(hm.get("state" ).toString());
      int  weight   = Integer.parseInt( hm.get("weight").toString() );
      for( int j=0; j<weight; j++ )
         lnk.addElement( link );
   }
   String      link  = "http://" + lnk.elementAt( count % lnk.size() ) + "/index.html";
               link += "?service=" + service + "&state=" + state + "&";
   Enumeration enm   = request.getParameterNames();
   String      utms  = "";
   while( enm.hasMoreElements() )
   {
      String ky = enm.nextElement().toString();
      String vl = request.getParameter(ky);
             ky = ky.toLowerCase();
      if( ky.startsWith("utm_") )
         utms += "&" + ky + "=" + vl;
   }
   if( service.equals("") )
      utms = utms.substring(1);
   link += utms;


   if( request.getParameter("DEBUG")!=null )
     out.print( link );
   else
      Web.redirect( response, 302, link );
}
catch( Exception e )
{
   String referer = request.getParameter( "referer" );
   DB.executeSQL( "insert into site_toggler_fail values(sysdate(),\"" + Format.dbSafe(referer) + "\")" );
// DB.executeSQL( "insert into site_toggler_fail values(sysdate(),\"" + Format.dbSafe(request.getQueryString().toString()) + "\")" );
   Web.redirect( response, 302, "/" );
}
%>

