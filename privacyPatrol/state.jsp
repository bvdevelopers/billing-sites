<%@page import="java.util.*, com.util.*"%>
<select name="state">
<option value=""></option>
<%
   String v = request.getParameter( "v" );
   for( int i=0; i<Static.STATE_CODES.length; i++ )
   {
      %><option <%=v.equals(Static.STATE_CODES[i])?"selected":""%> value="<%=Static.STATE_CODES[i]%>"><%=Static.STATE_NAMES[i]%></option><%
   }
%>
</select>
