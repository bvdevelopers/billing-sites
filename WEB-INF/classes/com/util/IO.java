package com.util;

import java.util.concurrent.*;
import java.util.*;
import java.io.*;

public class IO
{
   /*-------------------------------------------------------------------------*/
   /*--- for static ascii files ...                                        ---*/
   /*-------------------------------------------------------------------------*/
   public static ConcurrentHashMap cacheASCIIFiles = new ConcurrentHashMap();
   
   /*-------------------------------------------------------------------------*/
   /*--- saveFile ...                                                      ---*/
   /*-------------------------------------------------------------------------*/
   public static void saveFile( String content, String fileName ) throws FileNotFoundException, IOException
   {
      FileOutputStream fop = new FileOutputStream(fileName);
      fop.write( content.getBytes() );
      fop.close();
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- copyFile ...                                                      ---*/
   /*-------------------------------------------------------------------------*/
   public static void copyFile( String src, String trg ) throws FileNotFoundException, IOException
   {
      try
      {
         File          sourceFile = new File( src );
         BufferedInputStream  bis = new BufferedInputStream(new FileInputStream(sourceFile), 4096);
         File          targetFile = new File( trg );
         BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(targetFile), 4096);
         int theChar;
         while( (theChar = bis.read()) != -1 )
            bos.write(theChar);
         bos.flush();
         bos.close();
         bis.close();
      }
      catch( FileNotFoundException fnfe )
      {
         Log.log( fnfe );
         throw fnfe;
      }
      catch( IOException ioe )
      {
         Log.log(ioe);
         throw ioe;
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- loadBinaryFile ...                                                ---*/
   /*-------------------------------------------------------------------------*/
   public static byte[] loadBinaryFile( String fileName ) throws FileNotFoundException, IOException
   {
      String                 fName =  fileName.replace( "\\", "/" ).replaceAll( "//", "/" );
      ByteArrayOutputStream   baos = new ByteArrayOutputStream();
      Log.log( Log.INFO, "try to read " + fName );
      FileInputStream fip = new FileInputStream(fName);
      byte            b[] = new byte[512];
      int             nr  = 0;
      while( (nr=fip.read(b)) != -1 )
         baos.write( b, 0, nr );
      fip.close();
      return baos.toByteArray();
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- loadASCIIFile ...                                                 ---*/
   /*-------------------------------------------------------------------------*/
   public static String loadASCIIFile( String fileName ) throws FileNotFoundException, IOException
   {
      return new String(loadBinaryFile(fileName), "ISO-8859-1" );
   }
   public static String loadASCIIFile( String fileName, boolean cache ) throws FileNotFoundException, IOException
   {
      if( !cache )
         return loadASCIIFile( fileName );
      String cnt = (String) cacheASCIIFiles.get( fileName );
      if( cnt==null )
      {
         cnt = loadASCIIFile( fileName );
         cacheASCIIFiles.put( fileName, cnt );
      }
      return cnt;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- dir2UL ...                                                        ---*/
   /*--- look into a directory tree and shows as UL set                    ---*/
   /*-------------------------------------------------------------------------*/
   public static String dir2UL( String basePath, String root, String extension[], String excludes[] )
   {
      String  rt         = "";
      
      
      String parentDir = basePath + "/" + root;
      Log.log( Log.INFO, "dir2UL => " + parentDir );
      String ls[] = (new File(parentDir)).list();
      rt += "<ul>";
      boolean folderEmpty = true;
      for( int i=0; i<ls.length; i++ )
      {


         boolean isExcluded = false;
         for( int kk=0; kk<excludes.length; kk++ )
         {
            if( ls[i].contains(excludes[kk]) )
            {
               isExcluded = true;
               break;
            }
         }
         if( isExcluded )
            continue;
      
      



         String filePath = root + "/" + ls[i];
         if( (new File( parentDir + "/" + ls[i] )).isDirectory() )
         {
            rt += "<li class=\"ext_folder\">";
            rt += "<a href=\"#\">" + ls[i] + "</a>";
            try
            {
               rt += dir2UL( basePath, filePath, extension, excludes );
            }
            catch( Exception e )
            {
            }
            rt += "</li>";
            folderEmpty = false;
         }
         else
         {
            for( int k=0; k<extension.length; k++ )
            {
               if( ls[i].endsWith("."+extension[k]) )
               {
                  String  stateFileName = ls[i].replace("."+extension[k], "");
                  boolean invalidState  = (Static.STATE_REVERSE_LOOKUP.get( stateFileName )==null);
                  rt += "<li class=\"ext_" + extension[k] + "\">";
                  rt += "<a class=\"" + (invalidState?"error":"") + " file\" href=\"javascript:dir2UL('" + filePath.replace("//", "/") + "')\">" + ls[i] + "</a>";
                  rt += "</li>";
                  folderEmpty = false;
               }
            }
         }
      }
      if( folderEmpty )
      {
         rt += "<li style='color:red'>no files ...</li>";
      }
      rt += "</ul>";
      return rt;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- getWindowsShortCut ...                                            ---*/
   /*-------------------------------------------------------------------------*/
   public static String getWindowsShortCut( String fName ) throws Exception
   {
      String local       = null; 
      //String shareName   = null; 

      BufferedInputStream  in = new BufferedInputStream( new FileInputStream(fName) );
      int                  ch = -1;
      int                  offset;
      int                  offLocal; 
      int                  offPath; 
      
      // skip header 
      for( int i=0; i<=76; i++ )
         ch = in.read(); 
      offset = ch;
      if( ch < 0 ) return "";
         
      // skip to offset of local path 
      for( int i=0; i<=offset; i++ )
         ch = in.read(); 
      
      for( int i=0; i<=20; i++ )
         ch = in.read();
      
      offPath = ch;
      if (ch < 0) return "";
      
      // skip to offset of path 
      for( int i=0; i<8; i++ )
      {
         ch = in.read();
         offPath--;
      }
      
      offLocal = ch; 
      if( ch < 0 ) return "";
   
   
      // get local 
      for( int i=0; i<offLocal; i++ )
      {
         ch = in.read(); 
         offPath--; 
      } 
      
      byte  loc[] = new byte[256];
      int   index = 0;
      loc[index++] = (byte)ch;
      while( (ch = in.read()) != 0 )
      {
         loc[index++] = (byte)ch; 
         offPath--; 
      }
      local = new String(loc);
      local = local.trim();
      
      // getpath 
      for( int i=0; i<offPath; i++ )
      {
         ch = in.read();
      }
      
      loc = new byte[256];
      index = 0;
      loc[index++] = (byte)ch;
      while( (ch = in.read()) != 0 )
         loc[index++] = (byte) ch;
      
      // skip 0xh 
      ch = in.read(); 
      loc = new byte[256]; 
      index = 0; 
      loc[index++] = (byte)ch; 
      while( (ch = in.read()) != 0 )
         loc[index++] = (byte) ch;
      return local;
   }
}
