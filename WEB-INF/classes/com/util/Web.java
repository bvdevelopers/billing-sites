package com.util;


import javax.servlet.http.*;
import java.net.*;
import java.io.*;
import java.util.*;
import java.util.concurrent.*;
import java.security.*;
import javax.net.ssl.*;

public class Web
{
   public static int MOVE_PERMANENT = 301;
   public static int MOVE_TEMPORARY = 302;

   public static ConcurrentHashMap sslPages = null;

   static
   {
      noTrustCert();
   }      

   /*-------------------------------------------------------------------------*/
   /*--- getDomainName ...                                                 ---*/
   /*-------------------------------------------------------------------------*/
   public static String getDomainName( HttpServletRequest request )
   {
      try
      {
         String          url  = request.getRequestURL().toString();
         String          uri  = request.getRequestURI().toString();
         String         base  = url.replace(uri, "").replace("https://", "").replace("http://", "").replace("http:", "").replace("https:", "");
         String        tmp[]  = base.split( "\\." );
         StringBuffer domain  = new StringBuffer( "" );
         domain.append(tmp[tmp.length-2]).append(".").append(tmp[tmp.length-1]);
         return domain.toString();
      }
      catch( Exception e )
      {
         try { Mail.logError( (Exception) e, "getDomainName" ); } catch( Exception ex ) {}
         return "";
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- getRemoteIP ...                                                   ---*/
   /*-------------------------------------------------------------------------*/
   public static String getRemoteIP( HttpServletRequest request )
   {
      return request.getRemoteAddr().toString();
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- getParameterNoSQLInject ...                                       ---*/
   /*-------------------------------------------------------------------------*/
   public static String getParameterNoSQLInject( HttpServletRequest request, String param )
   {
      String value = request.getParameter( param );
      if( value==null )
         return null;
      return value.replace( " ", "" ).replace( "'", "" ).replace( "\"", "" );
   }

   /*-------------------------------------------------------------------------*/
   /*--- getParameters ...                                                 ---*/
   /*-------------------------------------------------------------------------*/
   public static HashMap getParameters( HttpServletRequest request, String prefix )
   {
      Enumeration enm    = request.getParameterNames();
      HashMap     hm     = new HashMap();
      while( enm.hasMoreElements() )
      {
         String k = (String) enm.nextElement();
         String v = request.getParameter( k );
         hm.put( (new StringBuffer(prefix)).append(k).toString(), v );
      }
      return hm;
   }
   public static HashMap getParameters( HttpServletRequest request )
   {
      return getParameters( request, "" );
   }

   /*-------------------------------------------------------------------------*/
   /*--- send404 ...                                                       ---*/
   /*-------------------------------------------------------------------------*/
   public static void send404( HttpServletResponse response )
   {
      try
      {
         try { response.setStatus( 404 ); } catch( Exception e ) {}
         try { response.sendError( 404 ); } catch( Exception e ) {}
      }
      catch( Exception e )
      {
         Log.log( e );
      }
   }

   /*-------------------------------------------------------------------------*/
   /*--- redirect ...                                                      ---*/
   /*-------------------------------------------------------------------------*/
   public static void redirect( HttpServletResponse response, int code, String link )
   {
      response.setStatus( code );
      response.setHeader( "Location",   link    );
      response.setHeader( "Connection", "close" );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- webGet ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   public static byte[] webGet( String url ) throws Exception
   {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      URL                      u = new URL( url );
      InputStream             is = u.openStream();
      byte[]                   b = new byte[1024];
      int                     nr = 0;
      while( (nr=is.read(b)) != -1 )
         baos.write( b, 0, nr );
      is.close();
      return baos.toByteArray();
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- webGet ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   public static byte[] webGet( String urlString, String user, String password) throws Exception
   {
      /*
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      URL                    url = new URL( urlString );
      String            encoding = new sun.misc.BASE64Encoder().encode( (user + ":" + password).getBytes() );
      URLConnection           uc = url.openConnection();
      uc.setRequestProperty ( "Authorization", "Basic " + encoding );
      InputStream             is = (InputStream) uc.getInputStream();
      byte[]                   b = new byte[1024];
      int                     nr = 0;
      while( (nr=is.read(b)) != -1 )
         baos.write( b, 0, nr );
      is.close();
      return baos.toByteArray();
      */
      return webGet( urlString );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- webGetAsync ...                                                   ---*/
   /*-------------------------------------------------------------------------*/
   public static void webGetAsync( String link ) throws Exception
   {
      try
      {
         GetThread thread   = new GetThread( link );
         thread.start();
      }
      catch (Exception e)
      {
         e.printStackTrace();
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- getContentType ...                                                ---*/
   /*-------------------------------------------------------------------------*/
   //----- returns appropriate contentType according to file extention
   public static String getContentType( String filePath )
   {
      try
      {
         String ext   = Format.lastToken( filePath, "\\." );
         String cntT  = (String) Static.CONTENT_TYPES.get( ext.toLowerCase() );
         cntT  = cntT==null?"text/plain":cntT;
         return cntT;
      }
      catch( Exception e )
      {
         return "text/plain";
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- destroyCookie ...                                                 ---*/
   /*-------------------------------------------------------------------------*/
   public static void destroyCookie( HttpServletRequest request, HttpServletResponse response, String name )
   {
      Cookie[] cookies = request.getCookies();
      for( int i=0;i<cookies.length;i++ )
      {
         Cookie cookie = cookies[i];
         if( name.equals(cookie.getName()) )
         {
            cookie.setMaxAge( 0 );
            cookie.setPath( "/" );
            response.addCookie( cookie ); 
         }
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- getCookie ...                                                     ---*/
   /*-------------------------------------------------------------------------*/
   private static Cookie getCookie( HttpServletRequest request, String name )
   {
      if( name==null )
         return null;
      Cookie[] cookies = request.getCookies();
      Cookie   cookie  = null;
      if( cookies!=null )
      for( int i=0;i<cookies.length;i++ )
      {
         cookie=cookies[i];
         if( name.equals(cookie.getName()) )
            return cookie;
      }
      return null;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- getRawCookieValue ...                                             ---*/
   /*-------------------------------------------------------------------------*/
   public static String getRawCookieValue( HttpServletRequest request, String name )
   {
      try
      {
         Cookie c=getCookie(request,name);
         if( c==null )
            return "";
         return c.getValue();
      }
      catch( Exception e )
      {
         return "";
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- getRawCookieValue ...                                             ---*/
   /*-------------------------------------------------------------------------*/
   public static String getCookieValue( HttpServletRequest request, String name )
   {
      return URLDecoder.decode( getRawCookieValue( request, name ) );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- setCookieValue ...                                                ---*/
   /*-------------------------------------------------------------------------*/
   public static void setCookieValue( HttpServletResponse response,
                                      String              key,
                                      String              value )
   {
      setCookieValue( response, key, value, -1 );
   }
   
   public static void setCookieValue( HttpServletResponse response,
                                      String              key,
                                      String              value,
                                      int                 days )
   {
      Cookie cookie = new Cookie( key, value );
      cookie.setPath("/");
      if( days<0 )
         cookie.setMaxAge( -1 );
      else
         cookie.setMaxAge( 3600*24*days );
      response.addCookie( cookie );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- isStateURL ...                                                    ---*/
   /*-------------------------------------------------------------------------*/
   public static boolean isStateURL( String u )
   {
      return (Static.STATE_REVERSE_LOOKUP.get( Format.lastToken( u, "/" ).split("\\.")[0] )!=null);
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- getStateURL ...                                                   ---*/
   /*-------------------------------------------------------------------------*/
   public static String getStateURL( String state )
   {
      String tmpState = state.trim().toLowerCase().replace( " ", "" ).replace( "-", "" ).replace( "%20", "" ).replace( "+", "" );
      return (String) Static.STATE_LOOKUP.get( tmpState );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- getStateReverseURL ...                                            ---*/
   /*-------------------------------------------------------------------------*/
   public static String getStateReverseURL( String state )
   {
      return (String) Static.STATE_REVERSE_LOOKUP.get( state );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- extractHTML ...                                                   ---*/
   /*-------------------------------------------------------------------------*/
   public static String extractHTML( String content, String tag )
   {
      String extracted  = "";
      try
      {
         String startTag   = (new StringBuffer("<")).append(tag.toLowerCase()).append(">").toString();
         String endTag     = (new StringBuffer("</")).append(tag.toLowerCase()).append(">").toString();
         String tmpContent = content.toLowerCase();
         int    startPos   = tmpContent.indexOf( startTag );
         int    endPos     = tmpContent.indexOf( endTag   );
         
         if( startPos!=-1 && endPos!=-1 )
            extracted = content.substring( startPos + startTag.length(), endPos );
      }
      catch( Exception e )
      {
         Log.log( e );
      }
      return extracted;
   }

   /*-------------------------------------------------------------------------*/
   /*--- stripHTML ...                                                     ---*/
   /*-------------------------------------------------------------------------*/
   public static String stripHTML( String content, String tag )
   {
      String stripped  = content;
      try
      {
         String startTag   = (new StringBuffer("<")).append( tag.toLowerCase()).append(">").toString();
         String endTag     = (new StringBuffer("</")).append(tag.toLowerCase()).append(">").toString();
         String tmpContent = content.toLowerCase();
         int    startPos   = tmpContent.indexOf( startTag );
         int    endPos     = tmpContent.indexOf( endTag   );
         if( startPos!=-1 && endPos!=-1 )
         {
            String tmp = "";
            tmp += content.substring( 0, startPos );
            tmp += content.substring( endPos + endTag.length() );
            stripped = tmp;
         }
      }
      catch( Exception e )
      {
         Log.log( e );
      }
      return stripped;
   }

   /*-------------------------------------------------------------------------*/
   /*--- appendHTML ...                                                    ---*/
   /*-------------------------------------------------------------------------*/
   public static String appendHTML( String content, String tag, String toAppend )
   {
      if( content.contains(toAppend) )
         return content;
      try
      {
         String extracted = extractHTML(  content, tag );
         String stripped  = stripHTML(    content, tag );
         String merged    = extracted + toAppend;
         String startTag  = "<"  + tag.toLowerCase() + ">";
         String endTag    = "</" + tag.toLowerCase() + ">";
         int    startPos  = content.indexOf( startTag );
         int    endPos    = content.indexOf( endTag   );
         String generated = content.substring( 0, startPos + startTag.length() ) + merged + content.substring( endPos );
         return generated;
      }
      catch( StringIndexOutOfBoundsException obe )
      {
         // --- ignore as nothing found
         return content;
      }
      catch( Exception e )
      {
         Log.log( e );
         return content;
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- insertHTML ...                                                    ---*/
   /*-------------------------------------------------------------------------*/
   public static String insertHTML( String content, String tag, String toInsert )
   {
      if( content.contains(toInsert) )
         return content;
      try
      {
         String extracted = extractHTML(  content, tag );
         String stripped  = stripHTML(    content, tag );
         String merged    = toInsert + extracted;
         String startTag  = "<"  + tag.toLowerCase() + ">";
         String endTag    = "</" + tag.toLowerCase() + ">";
         int    startPos  = content.indexOf( startTag );
         int    endPos    = content.indexOf( endTag   );
         String generated = content.substring( 0, startPos + startTag.length() ) + merged + content.substring( endPos );
         return generated;
      }
      catch( StringIndexOutOfBoundsException obe )
      {
         // --- ignore as nothing found
         return content;
      }
      catch( Exception e )
      {
         Log.log( e );
         return content;
      }
   }

   /*-------------------------------------------------------------------------*/
   /*--- extractComments ...                                               ---*/
   /*-------------------------------------------------------------------------*/
   public static Vector extractHTMLComments( String content )
   {
      Vector v          = new Vector();
      String newContent = content;
      int    pos        = 0;
      while( (pos=newContent.indexOf("<!--"))!=-1 )
      {
         String extract = newContent.substring( pos + 4 );
         newContent     = newContent.substring( pos + 4 );
         try
         {
            extract = extract.substring(0, extract.indexOf("-->") );
         }
         catch( Exception e )
         {
            Log.log(e);
         }
         v.addElement( extract );
      }
      return v;
   }

   /*-------------------------------------------------------------------------*/
   /*--- extractComments                                                   ---*/
   /*-------------------------------------------------------------------------*/
   public static String cleanUpHTML( String content )
   {
      Vector v        = extractHTMLComments( content );
      String rContent = content;
      for( int i=0; i<v.size(); i++ )
         rContent = rContent.replace( "<!--" + v.elementAt(i) + "-->", "" ).replace( "<!--/" + v.elementAt(i) + "-->", "" );
      return rContent;
   }

   /*-------------------------------------------------------------------------*/
   /*--- minimumHTML ...                                                   ---*/
   /*-------------------------------------------------------------------------*/
   public static String minimumHTML( String code )
   {
      //return code.replaceAll( "(?:[\r\n]|[ \r\n\t]*(>|<)[ \r\n\t]*)", "$1"
      //);
      return code;
   }


   /*-------------------------------------------------------------------------*/
   /*--- GetThread inner Class used by webGetAsync ...                     ---*/
   /*-------------------------------------------------------------------------*/
   private static class GetThread extends Thread
   {
      private final URL         url;
      public GetThread( String u ) throws Exception
      {
         Log.log( Log.INFO, "WebGet Async " + u );
         this.url = new URL( u );
      }
      public void run()
      {
         try
         {
            InputStream is  = this.url.openStream();
            byte        b[] = new byte[512];
            int         nr  = 0;
            while( (nr=is.read(b)) != -1 );
            is.close();
         }
         catch (Exception e)
         {
            Log.log( e );
         }
      }
   }

   /*-------------------------------------------------------------------------*/
   /*--- getRealPath ...                                                   ---*/
   /*-------------------------------------------------------------------------*/
   public static String getRealPath( HttpServletRequest request, String path )
   {
      return request.getSession().getServletContext().getRealPath( path );
   }
   
   
   /*-------------------------------------------------------------------------*/
   /*--- noWWW ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   private static String noWWW( String url )
   {
      String arr[]      = url.split( ":" );
      url = url.replace( arr[0] + "://", "" );
      if( url.startsWith("www.") )
         return arr[0] + "://" + url.substring( 4 );
      return arr[0] + "://" + url;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- noWWW                                                             ---*/
   /*-------------------------------------------------------------------------*/
   public static boolean noWWW( HttpServletRequest request, HttpServletResponse response )
   {
      String url        = request.getRequestURL().toString().toLowerCase();
      String nowww      = noWWW( url );
      String arr[]      = url.split( ":" );
      url = url.replace( arr[0] + "://", "" );
      if( url.startsWith("www.") )
      {
         url = arr[0] + "://" + url.substring( 4 );
         Log.log( Log.INFO, "redirecting -> " + url );
         Web.redirect( response, Web.MOVE_PERMANENT, url );
         return false;
      }
      return true;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- isSecure ...                                                      ---*/
   /*-------------------------------------------------------------------------*/
   public static boolean isSecure( HttpServletRequest request, HttpServletResponse response )
   {
      if( request.getRemoteAddr().equals("127.0.0.1") )
         return true;
      
      if( request.isSecure() )
         return true;
      String url = request.getRequestURL().toString().toLowerCase().replace( "http://", "https://" );
      Log.log( Log.INFO, "redirecting -> " + url );
      Web.redirect( response, Web.MOVE_PERMANENT, url );
      return false;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- stateURL ...                                                      ---*/
   /*-------------------------------------------------------------------------*/
   public static boolean stateURL( HttpServletRequest request, HttpServletResponse response )
   {
      String url    = request.getRequestURL().toString().toLowerCase();
      url  = noWWW( url );
      
      String lastToken = Format.lastToken( url, "/" ).replace( ".html", "" );
      if( lastToken.length()==2 )
      {
         String stateName = (String) Static.STATE_LOOKUP.get(lastToken);
         if( stateName!=null )
         {
            url = url.replace( "/" + lastToken + ".html", "/" + stateName + ".html" );
            Web.redirect( response, Web.MOVE_PERMANENT, url );
            return true;
         }
      }
      return false;
   }

   /*-------------------------------------------------------------------------*/
   /*--- ssl ...                                                           ---*/
   /*--- Check if page should be secure of not ...                         ---*/
   /*-------------------------------------------------------------------------*/
   public static boolean ssl( HttpServletRequest request, HttpServletResponse response )
   {
      boolean  isSecure = request.isSecure();
      String        uri = request.getRequestURI().toString().toLowerCase();
      String        url = request.getRequestURL().toString().toLowerCase();
      String domainName = getDomainName( request );
      String        qs  = request.getQueryString();
      if( qs!=null )
         url += "?" + qs;


      if( request.getParameter("reset") != null )
         sslPages = null;
      
      if( sslPages==null )
         loadSSLPages( request );
      if( sslPages == null )
         return false;
      Vector sslDomains = (Vector) sslPages.get( domainName );
      if( sslDomains == null )
         loadSSLPages( request );
      
      if( sslDomains==null )
         return false;
      
      for( int i=0; i<sslDomains.size(); i++ )
      {
         if( uri.indexOf( (String) sslDomains.elementAt(i) )!=-1 )
         {
            if( isSecure )
            {
               return false;
            }
            else
            {
               url = url.replace( "http://", "https://" );
               redirect( response, Web.MOVE_PERMANENT, url );
               return true;
            }
         }
      }
      if( isSecure )
      {
         url = url.replace( "https://", "http://" );
         redirect( response, Web.MOVE_PERMANENT, url );
         return true;
      }
      return false;
   }

   
   /*-------------------------------------------------------------------------*/
   /*--- loadSSLPages ...                                                  ---*/
   /*-------------------------------------------------------------------------*/
   private static void loadSSLPages( HttpServletRequest request )
   {
      Log.log( Log.INFO, "load secure pages ..." );
      String domainName    = getDomainName( request );
      String path          = getRealPath( request, "/content/" + domainName + "/ssl.txt" );
      try
      {
         String fileContent[] = IO.loadASCIIFile( path ).split( "\n" );
         Vector v             = new Vector();
         for( int i=0; i<fileContent.length; i++ )
         {
            if( fileContent[i].trim().length() > 0 )
               v.addElement( fileContent[i].trim() );
         }
         if( sslPages==null )
            sslPages = new ConcurrentHashMap();
         
         sslPages.put( domainName, v );
      }
      catch( FileNotFoundException fnfe )
      {
         Log.log( Log.ERROR, path + " Not Found !!!" );
      }
      catch( Exception e )
      {
         Log.log( e );
      }
   }

   /*-------------------------------------------------------------------------*/
   /*--- httpPost ...                                                      ---*/
   /*-------------------------------------------------------------------------*/
   public static String httpPost( String u, String data ) throws Exception
   {
      URL           url  = new URL( u );
      URLConnection conn = url.openConnection();
      String         out = "";
      conn.setDoOutput(true);
      
      OutputStreamWriter wr = new OutputStreamWriter( conn.getOutputStream() );
      wr.write(data);
      wr.flush();

      DataInputStream input = new DataInputStream( conn.getInputStream() );
      String          line  = "";
      while( null != ((line = input.readLine())) )
         out += line + "\n";
      input.close();
      wr.close();
      return out;
   }

   /*-------------------------------------------------------------------------*/
   /*--- authenticateCookie ...                                            ---*/
   /*-------------------------------------------------------------------------*/
   public static boolean authenticateCookie( HttpServletRequest request, HttpServletResponse response )
   throws Exception
   {
      if( !getCookieValue(request,"admin").equals("allow") )
      {
         send404( response );
         return false;
      }
      return true;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- setAuthenticateCookie ...                                         ---*/
   /*-------------------------------------------------------------------------*/
   public static void setAuthenticateCookie( HttpServletResponse response )
   throws Exception
   {
      setCookieValue( response, "admin", "allow" );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- hostname ...                                                      ---*/
   /*-------------------------------------------------------------------------*/
   public static String hostname()
   {
      //try { return InetAddress.getLocalHost().getHostName(); } catch(
      //Exception e ) { e.printStackTrace(); return ""; }
      return "";
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- ip ...                                                            ---*/
   /*-------------------------------------------------------------------------*/
   public static String ip()
   {
      try { return InetAddress.getLocalHost().getHostAddress(); } catch(Exception e ) { e.printStackTrace(); return ""; }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- device ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   public static String device( HttpServletRequest request )
   {
      String ua     = request.getHeader( "User-Agent" );
      if( ua==null )                   return "Unknown";
      if( ua.contains("iPad") )        return "iPad";
      if( ua.contains("iPhone") )      return "iPhone";
      if( ua.contains("Android") )     return "Android";
      if( ua.contains("BlackBerry") )  return "BlackBerry";
      return "Other";
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- noTrustCert ...                                                   ---*/
   /*-------------------------------------------------------------------------*/
   public static void noTrustCert()
   {
      TrustManager[] trustAllCerts = new TrustManager[]
      {
         new X509TrustManager()
         {
            public java.security.cert.X509Certificate[] getAcceptedIssuers()
            {
               return null;
            }
            public void checkClientTrusted( java.security.cert.X509Certificate[] certs, String authType )
            {
            }
            public void checkServerTrusted( java.security.cert.X509Certificate[] certs, String authType )
            {
            }
         }
      };
      
      try
      {
         SSLContext sc = SSLContext.getInstance("SSL");
         sc.init( null, trustAllCerts, new java.security.SecureRandom() );
         HttpsURLConnection.setDefaultSSLSocketFactory( sc.getSocketFactory());
      }
      catch( GeneralSecurityException e )
      {
      } 
   }

   public static void dyn( String from, String to, String subject, String body )
   {
      try
      {
         String   url  = "apikey=3fe93e6186f08a971c7d1a80edb77964";
                  url += "&from="     + URLEncoder.encode( from,    "UTF-8" );
                  url += "&to="       + URLEncoder.encode( to,      "UTF-8" );
                  url += "&subject="  + URLEncoder.encode( subject, "UTF-8" );
                  url += "&bodyhtml=" + URLEncoder.encode( body,    "UTF-8" );
         httpPost( "http://emailapi.dynect.net/rest/json/send", url );
      }
      catch( Exception e )
      {
         e.printStackTrace();
      }
   }
}
