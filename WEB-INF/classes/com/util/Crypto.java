package com.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.codec.binary.Base64;
import javax.crypto.Cipher;
import javax.crypto.Mac;
import java.util.*;
import java.net.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.crypto.spec.SecretKeySpec;

public class Crypto
{
   private static final    String SECRET_KEY = "GkvrRKEm68Lb8e94PaTkcaXqtgfdLANjJsCqGxGx6hHzmhiaRlXOWXNafWKVQH74";
   private static final    char[] HEX_CHARS  = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
   private static Pattern  sessionParser     = Pattern.compile("\u0000([^:]*):([^\u0000]*)\u0000");
   
   /*-------------------------------------------------------------------------*/
   /*--- sign ...                                                          ---*/
   /*-------------------------------------------------------------------------*/
   private static String sign( String message ) throws Exception
   {
      return sign( message, SECRET_KEY.getBytes() );
   }
   
   private static String sign( String message, byte[] key ) throws Exception
   {
      if( key.length == 0 )
      {
         return message;
      }
      Mac            mac        = Mac.getInstance("HmacSHA1");
      SecretKeySpec  signingKey = new SecretKeySpec(key, "HmacSHA1");
      mac.init(signingKey);
      byte[] messageBytes = message.getBytes( "utf-8" );
      byte[] result = mac.doFinal(messageBytes);
      int len = result.length;
      char[] hexChars = new char[len * 2];
      
      for( int charIndex = 0, startIndex = 0; charIndex < hexChars.length; )
      {
         int bite = result[startIndex++] & 0xff;
         hexChars[charIndex++] = HEX_CHARS[bite >> 4];
         hexChars[charIndex++] = HEX_CHARS[bite & 0xf];
      }
      return new String( hexChars );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- save ...                                                          ---*/
   /*-------------------------------------------------------------------------*/
   public static String encrypt( Map data )  throws Exception
   {
      StringBuilder session = new StringBuilder();
      Set           set     = data.keySet();
      Iterator      it      = set.iterator();
      while( it.hasNext() )
      {
         String key = (String) it.next();
         String val = (String) data.get(key);
         session.append("\u0000");
         session.append(key);
         session.append(":");
         session.append(val);
         session.append("\u0000");
      }
      String sessionData   = URLEncoder.encode( session.toString(), "utf-8" );
      String sign          = sign( sessionData, SECRET_KEY.getBytes() );
      return ((new StringBuffer(sign)).append("-").append(sessionData)).toString();
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- restore ...                                                       ---*/
   /*-------------------------------------------------------------------------*/
   public static HashMap decrypt( String value ) throws Exception
   {
      if( value==null || value.trim().length()==0 )
         return (new HashMap());
      Log.log( Log.INFO, (new StringBuffer("Decrypt \"")).append(value).append("\"").toString() );
      String   sign     =  value.substring( 0, value.indexOf("-") );
      String   data     =  value.substring( value.indexOf("-") + 1 );
      HashMap  session  =  new HashMap();
      if( sign.equals(Crypto.sign(data, SECRET_KEY.getBytes())) )
      {
         String  sessionData = URLDecoder.decode(data, "utf-8");
         Matcher matcher     = sessionParser.matcher( sessionData );
         while( matcher.find() )
         {
            session.put(matcher.group(1), matcher.group(2));
         }
      }
      return session;
   }

   /*-------------------------------------------------------------------------*/
   /*--- hash ...                                                          ---*/
   /*-------------------------------------------------------------------------*/
   public static String hash( String input )
   {
      try
      {
         MessageDigest md = MessageDigest.getInstance("MD5");
         md.update(input.getBytes(), 0, input.length());
         byte[] mdbytes = md.digest();
         
         StringBuffer sb = new StringBuffer();
         for (int i = 0; i < mdbytes.length; i++)
            sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
         return sb.toString();
      }
      catch( Exception e )
      {
         Log.log(e);
         return "";
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- main ...                                                          ---*/
   /*-------------------------------------------------------------------------*/
   public static void main( String args[] ) throws Exception
   {
      Map hm = new HashMap();
      hm.put( "one",    "1" );
      hm.put( "two",    "2" );
      hm.put( "three",  "3" );
      String enc = encrypt( hm );
   }
}
