package com.util;

import java.io.*;
import java.security.*;
import java.util.*;
import javax.mail.*;
import javax.activation.*;
import javax.mail.internet.*;

public class GoogleApps
{
   final private static String COMMON_USER         =  "info";
   final private static String COMMON_PASSWORD     =  "W@d1y@426!!";

   /*-------------------------------------------------------------------------*/
   /*--- static stuff ...                                                  ---*/
   /*-------------------------------------------------------------------------*/
   private static Properties props = new Properties();
   static
   {
      Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
      props.put("mail.smtp.host",                     "smtp.gmail.com"                    );
      props.put("mail.smtp.auth",                     "true"                              );
      props.put("mail.smtp.port",                     "465"                               );
      props.put("mail.smtp.socketFactory.port",       "465"                               );
      props.put("mail.smtp.socketFactory.class",      "javax.net.ssl.SSLSocketFactory"    );
      props.put("mail.smtp.socketFactory.fallback",   "false"                             );
   }

   /*-------------------------------------------------------------------------*/
   /*--- fields ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   private String   login      = "";
   private String   from       = "";
   private String   to         = "";
   private String   subject    = "";
   private String   body       = "";
   private String   reply      = "";

   /*-------------------------------------------------------------------------*/
   /*--- Constructor ...                                                   ---*/
   /*-------------------------------------------------------------------------*/
   private GoogleApps( String domain, String from, String to, String subject, String body, String reply )
   {
      this.login     =  COMMON_USER + "@" + domain;
      this.from      =  from + " <" + login + ">";
      this.to        =  to;
      this.subject   =  subject;
      this.body      =  body;
      this.reply     =  reply;
   }

   /*-------------------------------------------------------------------------*/
   /*--- send ...                                                          ---*/
   /*-------------------------------------------------------------------------*/
   private void send() throws Exception
   {
      Authenticator auth = new Authenticator()
                               {
                                 public PasswordAuthentication getPasswordAuthentication()
                                 {
                                    return new PasswordAuthentication( login, COMMON_PASSWORD );
                                 }
                               };

      Session     session  =  Session.getInstance( props, auth );
      MimeMessage msg      =  new MimeMessage( session );
      msg.setFrom( new InternetAddress(from) );
      msg.setRecipient( Message.RecipientType.TO,  new InternetAddress( to    )   );
      msg.setReplyTo(   new javax.mail.Address[] { new InternetAddress( reply ) } );
      msg.setSubject( subject );
      msg.setHeader(  "X-Mailer",           "Microsoft Office Outlook 11"  );
      msg.setHeader(  "X-Accept-Language",  "en"                           );
      msg.setHeader(  "From",                from                          );
      msg.setHeader(  "content-type",        "text/html"                   );
      msg.setContent( body,                  "text/html"                   );
      Transport.send( msg );
   }

   /*-------------------------------------------------------------------------*/
   /*--- send ...                                                          ---*/
   /*-------------------------------------------------------------------------*/
   public static void send( String domain, String from, String to, String subject, String body, String reply ) throws Exception
   {
      (new GoogleApps( domain, from, to, subject, body, reply )).send();
   }
   public static void send( String domain, String from, String to, String subject, String body ) throws Exception
   {
      (new GoogleApps( domain, from, to, subject, body, "info@"+domain )).send();
   }

   /*-------------------------------------------------------------------------*/
   /*--- main ...                                                          ---*/
   /*-------------------------------------------------------------------------*/
   public static void main( String args[] ) throws Exception
   {
      send( "fishinglicense.org", "David", "cohana@gmail.com", "AAAA", "BBBB" );
   }
}

