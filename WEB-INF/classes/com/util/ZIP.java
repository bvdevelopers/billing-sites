package com.util;

import java.io.*;
import java.util.zip.*;

/*----------------------------------------------------------------------------*/
/*--- Sorry no comments of that code ...                                   ---*/
/*----------------------------------------------------------------------------*/
public class ZIP
{
   private static void zipDir( String root, String dir2zip, ZipOutputStream zos )
   throws IOException
   {
      File       zipDir = new File(dir2zip);
      String[]  dirList = zipDir.list();
      byte[] readBuffer = new byte[2156];
      int       bytesIn = 0;
      for( int i=0; i<dirList.length; i++ )
      {
         if( dirList[i].endsWith(".pdf") )
            continue;
         File              f = new File( zipDir, dirList[i] );
         String     filePath = f.getPath();
         String filePathName = filePath.replace( (new File(root)).toString(), "" );
         if( filePathName.startsWith(File.separator.toString()) )
            filePathName = filePathName.substring(1);
         if( f.isDirectory() )
         {
            zipDir( root, filePath, zos );
            zos.putNextEntry( new ZipEntry( filePathName + "/" ) );
            continue;
         }
         FileInputStream fis = new FileInputStream(f);
         ZipEntry    anEntry = new ZipEntry(filePathName);
         zos.putNextEntry( anEntry );
         while((bytesIn = fis.read(readBuffer)) != -1)
         {
            zos.write(readBuffer, 0, bytesIn);
         }
         fis.close();
      }
   }
   
   public static byte[] zip( String dir2zip ) throws IOException
   {
      ByteArrayOutputStream baos = new ByteArrayOutputStream();
      ZipOutputStream       zos  = new  ZipOutputStream( baos );
      zipDir( dir2zip, dir2zip, zos );
      zos.close();
      baos.close();
      return baos.toByteArray();
   }
   
   public static void unzip( String zipFile, String targetDir ) throws IOException
   {
      int                  buffer = 2048;
      BufferedOutputStream dest   = null;
      FileInputStream      fis    = new FileInputStream( zipFile );
      ZipInputStream       zis    = new ZipInputStream( new BufferedInputStream(fis) );
      ZipEntry             entry;
      
      while( (entry=zis.getNextEntry()) != null )
      {
         String targetFile = targetDir + "/" + entry.getName();
         File   tmpDir     = new File( targetFile );
         tmpDir.mkdirs();
         int count;
         byte data[] = new byte[buffer];
         tmpDir.delete();
         
         if( entry.isDirectory() )
         {
            tmpDir.mkdirs();
         }
         else
         {
            FileOutputStream fos = new FileOutputStream( targetFile );
            dest = new BufferedOutputStream( fos, buffer );
            while( (count = zis.read(data, 0, buffer)) != -1)
            {
               dest.write(data, 0, count);
            }
            dest.flush();
            dest.close();
         }
      }
      zis.close();
   }
}
