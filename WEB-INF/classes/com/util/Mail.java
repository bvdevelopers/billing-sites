package com.util;

import java.io.*;
import java.security.*;
import java.util.*;
import javax.mail.*;
import javax.activation.*;
import javax.mail.internet.*;
import javax.servlet.http.*;

public class Mail
{
   final private static String CHUCKY_EMAIL        =  "cohana@gmail.com";
   final private static String DEFAULT_USER        =  "chuck@issuebasedmedia.com";
   final private static String COMMON_USER         =  "info";
   final private static String COMMON_PASSWORD     =  "G3nER@@llAdyn!!";
   final private static String DUMPFOLDER          =  "/tmp/";
   final private static String LOCAL_MAIL_SERVER   =  "localhost";
   
   /*-------------------------------------------------------------------------*/
   /*--- static stuff ...                                                  ---*/
   /*-------------------------------------------------------------------------*/
   public static HashMap mapping = new HashMap();
   
   private static Properties props = new Properties();
   static
   {
      Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
      props.put("mail.smtp.host",                     "smtp.gmail.com"                    );
      props.put("mail.smtp.auth",                     "true"                              );
      props.put("mail.smtp.port",                     "465"                               );
      props.put("mail.smtp.socketFactory.port",       "465"                               );
      props.put("mail.smtp.socketFactory.class",      "javax.net.ssl.SSLSocketFactory"    );
      props.put("mail.smtp.socketFactory.fallback",   "false"                             );
      
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- fields ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   String   login      = "";
   String   from       = "";
   String   to         = "";
   String   subject    = "";
   String   body       = "";
   String   attach[][] = null;
   long     delay      = 0;
   String   id         = "";
   
   /*-------------------------------------------------------------------------*/
   /*--- Constructor ...                                                   ---*/
   /*-------------------------------------------------------------------------*/
   private Mail( String login, String from, String to, String subject, String body, String attach[][], long delay, String id )
   {
      this.login     =  login;
      this.from      =  from;
      this.to        =  to;
      this.subject   =  subject;
      this.body      =  body;
      this.attach    =  attach;
      this.delay     =  delay;
      this.id        =  id;
   }

   /*-------------------------------------------------------------------------*/
   /*--- logError ...                                                      ---*/
   /*-------------------------------------------------------------------------*/
   public static void logError( Exception e, String extra )
   {
      try
      {
         Log.log( Log.INFO, "email Exception" ) ;
         send( DEFAULT_USER, DEFAULT_USER, CHUCKY_EMAIL, e.toString(), "<hr>" + extra + "<hr>" + Log.printStackTrace(e).replace("\n", "<br>"), null, 0, null );
      }
      catch( Exception exp )
      {
         Log.log(exp);
      }
   }
   public static void logError( Exception e ) throws Exception
   {
      logError( e, null );
   }

   
   /*-------------------------------------------------------------------------*/
   /*--- send ...                                                          ---*/
   /*-------------------------------------------------------------------------*/
   public static void send( String login, String from, String to, String subject, String body, String attach[][], long delay, String id )
   {
      Mail        mail     = new Mail( login, from, to, subject, body, attach, delay, id );
      threadSend  thread   = new threadSend( mail );
      thread.start();
   }
   public static void send( HttpServletRequest request, String to, String subject, String body, String attach[][], long delay, String id )
   {
      String _login = getEmailAddress(request);
      String _from  = getFullEmailAddress(request);
      send( _login, _from, to, subject, body, attach, delay, id );
   }
   public void send()
   {
      try
      {
         Authenticator auth = new Authenticator()
                                  {
                                    public PasswordAuthentication getPasswordAuthentication()
                                    {
                                       return new PasswordAuthentication( login, COMMON_PASSWORD );
                                    }
                                  };
         
         Session     session  =  Session.getInstance( props, auth );
         MimeMessage msg      =  new MimeMessage( session );
         msg.setFrom( new InternetAddress(from) );
         msg.setRecipient( Message.RecipientType.TO, new InternetAddress( to ) );
         msg.setSubject( subject );
         msg.setHeader( "X-Mailer",           "Microsoft Office Outlook 11" );
         msg.setHeader( "X-Accept-Language",  "en"                          );
         msg.setHeader( "From",                from                         );
         msg.setHeader( "content-type",        "text/html"                  );
         
         Log.log( Log.INFO, "Sending \""+ subject +"\" to \"" + to + "\"" );
         String content = "";
         if( id!=null && id.length()>0 )
            content += "<img border=1 src=\"http://" + login.replace(COMMON_USER+"@", "") + "/banner/" + id + ".jpg\"><br>";
         content += body;
         if( attach==null || attach.length==0 )
         {
            msg.setContent( content, "text/html" );
         }
         else
         {
            Multipart mp = new MimeMultipart();
            
            //---- adding body of email
            MimeBodyPart mbp1 = new MimeBodyPart();
            mbp1.setContent(content, "text/html");
            mp.addBodyPart(mbp1);
            
            for( int i=0; i<attach.length; i++ )
            {
               try
               {
                  // create the second message part
                  MimeBodyPart mbp2 = new MimeBodyPart();
                  
                  // attach the file to the message
                  FileDataSource fds = new FileDataSource( attach[i][0] );
                  mbp2.setDataHandler(new DataHandler(fds));
                  Log.log( Log.INFO, "Attach File \"" + fds.getName() + "\" to \"" + to + "\"" );
                  try
                  {
                     mbp2.setFileName( attach[i][1] );
                  }
                  catch( Exception e )
                  {
                     mbp2.setFileName( fds.getName() );
                  }
                  mp.addBodyPart(mbp2);
               }
               catch( Exception e )
               {
                  e.printStackTrace();
               }
            }
            // add the Multipart to the message
            msg.setContent(mp);
         }
         Transport.send(msg);
         Log.log( Log.INFO, "Sent \""+ subject +"\" to \"" + to + "\"" );
         
         //--- dumpint temporary files ---
         if( attach!=null )
         {
            for( int i=0; i<attach.length; i++ )
            {
               if( attach[i][0].startsWith( DUMPFOLDER ) )
               {
                  (new File(attach[i][0])).delete();
               }
            }
         }
      }
      catch( Exception e )
      {
         Log.log( Log.EXCEPTION, "Error Sendinf From " + from );
         e.printStackTrace();
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- getEmailAddress ...                                               ---*/
   /*-------------------------------------------------------------------------*/
   private static String getEmailAddress( HttpServletRequest request )
   {
      return COMMON_USER + "@" + Web.getDomainName( request );
   }

   /*-------------------------------------------------------------------------*/
   /*--- getFullEmailAddress ...                                           ---*/
   /*-------------------------------------------------------------------------*/
   private static String getFullEmailAddress( HttpServletRequest request )
   {
      Log.log( Log.INFO, "Generating Email Address" );
      String domain  =  Web.getDomainName( request );
      String map     = (String) mapping.get( domain );
      String email   = "";
      if( map == null )
      {
         mapping = ExcelMap.load( Web.getRealPath( request, "/content/extra/domain_map.xls" ) );
         map     = (String) mapping.get( domain );
      }
      if( map!=null )
         email += map;
      email += " <" + COMMON_USER + "@" + domain + ">";
      Log.log( Log.INFO, "Generated Email : \"" + email + "\"" );
      return email;
   }

   /*-------------------------------------------------------------------------*/
   /*--- toString ...                                                      ---*/
   /*-------------------------------------------------------------------------*/
   public String toString()
   {
      return "Sending \"" + subject + "\" from \"" + from + "\" to \"" + to + "\"";
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- sendMail ...                                                      ---*/
   /*-------------------------------------------------------------------------*/
   public static void sendMail( String from, String to, String subject, String body )
   {
      try
      {
         Properties props   = System.getProperties();
         props.put( "mail.smtp.host", LOCAL_MAIL_SERVER );
         Session   sessio = Session.getDefaultInstance( props, null );
         MimeMessage  msg = new MimeMessage(sessio);
         msg.setFrom( new InternetAddress(from) );
         msg.setRecipients( Message.RecipientType.TO, InternetAddress.parse(to, false) );
         msg.setSubject( subject );
         msg.setText( body );
         msg.setHeader("content-type", "text/html");
         Transport.send(msg);
      }
      catch( Exception e )
      {
         logError( e, "from:\"" + from + "\" to:\"" + to + "\" subject:\"" + subject + "\"" );
      }
   }
}



/*----------------------------------*/
/*--- GetThread inner Class ...  ---*/
/*----------------------------------*/
class threadSend extends Thread
{
   private Mail mail;
   
   public threadSend( Mail mail )
   {
      this.mail = mail;
   }
   
   public void run()
   {
      try
      {
         if( mail.delay > 0 )
         {
            Log.log( Log.INFO, "Delay Email \"" + mail.to + "\" " + mail.delay + " seconds @" + (new Date()) );
            try { this.sleep( mail.delay*1000 ); } catch( InterruptedException e ) { Log.log(e); }
            Log.log( Log.INFO, "Delay Email \"" + mail.to + "\" " + " Complete @" + (new Date()) );
         }
         this.mail.send();
      }
      catch (Exception e)
      {
      }
   }
}

