package com.util;

import org.dom4j.DocumentHelper;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

public class XML
{
   Document root;
   
   /*-------------------------------------------------------------------------*/
   /*--- XML ...                                                           ---*/
   /*-------------------------------------------------------------------------*/
   public XML( String content )
   {
      try
      {
         root = DocumentHelper.parseText(content);
      }
      catch( Exception e )
      {
         Log.log( Log.EXCEPTION, content );
         Log.log( e );
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- parse ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public String parse( String xPath )
   {
      try
      {
         return root.selectSingleNode( xPath ).getText();
      }
      catch( Exception e )
      {
         Log.log( e );
         return "";
      }
   }
}
