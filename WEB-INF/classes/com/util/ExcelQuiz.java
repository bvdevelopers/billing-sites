package com.util;

import java.io.*;
import java.net.*;
import java.awt.Color;
import java.util.*;
import jxl.format.*;
import jxl.*;

public class ExcelQuiz
{
   public Vector questions = null;
   
   /*-------------------------------------------------------------------------*/
   /*--- ExcelQuiz ...                                                     ---*/
   /*-------------------------------------------------------------------------*/
   private ExcelQuiz()
   {
      questions = new Vector();
   }

   /*-------------------------------------------------------------------------*/
   /*--- toHTML ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   public String _toHTML()
   {
      String html = "";
      Collections.shuffle( questions );
      
      html += "";

      html += "<div id=\"quiz_content\">";
      html +=     "<div id=\"quiz_wrapper\">";
      html +=        "<div id=\"quiz_steps\">";
      
      for( int i=0; i<questions.size(); i++ )
      {
         Question q = (Question) questions.elementAt(i);
         
         html += "<fieldset id=\"b_" + i + "\" class=\"quiz_step\">";
         html += "<div class=\"quiz_quest\">";
         html += "" + (i+1) + " / " + questions.size() + " : ";
         html += q.question;
         html += "</div>";
         for( int j=0; j<q.options.size(); j++ )
         {
            html += "<p class=\"quiz\"><button score=\"" + q.points.elementAt(j) + "\" id=\"b_"+i+"_"+j+"\" class=\"quiz_opt\">" + q.options.elementAt(j) + "</button></p>";
         }
         html += "</fieldset>";
      }
      html +="</div></div></div>";
      
      
      return html;
   }

   public String toHTML()
   {
      String html = "";
      Collections.shuffle( questions );
      
      html += "<ul id=\"status\">";
      for( int i=0; i<questions.size(); i++ )
         html += "<li" + (i==0?" class=\"active\"":"") + "></li>";
      html += "</ul>";

      html += "<div class=\"items\">";
      
      for( int i=0; i<questions.size(); i++ )
      {
         Question q = (Question) questions.elementAt(i);
         
         html += "<div class=\"page\">";
         html += "<div class=\"quiz_quest\">" + q.question + "</div>";
         for( int j=0; j<q.options.size(); j++ )
         {
            html += "<p class=\"quest\">";
            html += "<button rank=\"" + i + "\" score=\"" + q.points.elementAt(j) + "\" type=\"button\" class=\"next quiz_opt\">";
            html += q.options.elementAt(j);
            html += "</button>";
            html += "</p>";
         }
         html += "</div>";
      }
      html += "<div class=\"page\">";
      html += "<div id=\"score\"></div>";
      html += "<div>";
      html +="</div>";
      html = Format.noSmartQuot( html );
      return html;
   }

   /*-------------------------------------------------------------------------*/
   /*--- loadQuiz ...                                                      ---*/
   /*-------------------------------------------------------------------------*/
   public static ExcelQuiz loadQuiz( String fileName ) throws Exception
   {
      Vector              v   =  new Vector();
      FileInputStream   fip   =  new FileInputStream( fileName );
      Workbook     workbook   =  Workbook.getWorkbook( fip );
      Sheet           sheet   =  workbook.getSheets()[0];
      ExcelQuiz        quiz   =  new ExcelQuiz();
      for( int i=0; i<sheet.getRows(); i++ )
      {
         Cell[] cells  = sheet.getRow(i);
         if( cells.length > 0 )
         {
            String questionId    = cells[0].getContents().trim().toUpperCase();
            String questionLabel = cells[1].getContents().trim();
            if( questionId.length()>0 && questionId.charAt(0)=='Q' )
            {
               Question question = new Question( questionLabel );
               int n = i+1;
               for( i=n; i<sheet.getRows(); i++ )
               {
                  cells = sheet.getRow(i);
                  String optionId    = cells[0].getContents().trim().toUpperCase();
                  String optionLabel = cells[1].getContents().trim();
                  String optionScore = cells[2].getContents().trim();
                  
                  if( optionId.length()==0 || optionId.charAt(0)!='A' )
                     break;
                  question.options.addElement( optionLabel );
                  question.points.addElement(  optionScore );
               }
               quiz.questions.addElement( question );
            }
         }
      }
      fip.close();
      return quiz;
   }
}

class Question
{
   String question;
   Vector options;
   Vector points;
   
   Question( String question )
   {
      this.question = question;
      this.options  = new Vector();
      this.points   = new Vector();
   }
   
}
