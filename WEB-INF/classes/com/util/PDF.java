package com.util;

import com.lowagie.text.Document;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.*;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.*;
import java.util.*;
import java.io.*;
import com.servlets.*;
import com.lowagie.text.html.simpleparser.HTMLWorker;

public class PDF
{
   /*-------------------------------------------------------------------------*/
   /*--- getReader ...                                                     ---*/
   /*-------------------------------------------------------------------------*/
   synchronized private static PdfReader getReader( String template ) throws Exception
   {
      byte[] templateContent = Cache.getBinary( template );
      if( templateContent==null )
      {
         templateContent = IO.loadBinaryFile( template );
         Cache.setBinary( template, templateContent );
      }
      return ( new PdfReader(templateContent) );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- populateForm ...                                                  ---*/
   /*-------------------------------------------------------------------------*/
   public static void populateForm( String template, HashMap data, OutputStream out )
   {
      //--- populate extra data ---
      try
      {
         String extra_data[] = ((String) data.get("extra_data")).split( "\n" );
         for( int i=0; i<extra_data.length; i++ )
         {
            String pairs[] = extra_data[i].split( "\t" );
            if( pairs.length>=2 )
               data.put( pairs[0], pairs[1] );
         }
      }
      catch( Exception e )
      {
      }
      
      
      
      Log.log( Log.INFO, "PDF.populateForm : " + template );
      try
      {
         PdfReader           reader = getReader( template );
         PdfStamper         stamper = new PdfStamper( reader, out );
         AcroFields            form = stamper.getAcroFields();
         HashMap                 hm = form.getFields();
         Set                    set = hm.keySet();
         Iterator                it = set.iterator();
         while( it.hasNext() )
         {
            try
            {
               String k = (String) it.next();
               String v = (String) data.get(k.split("\\[")[0]);
               if( v!=null )
               {
                  if( k.contains("[") )
                  {
                     Log.log( Log.INFO, "parsing special field" );
                     try
                     {
                        int    ndx = Integer.parseInt( k.split("\\[")[1].replace("]", "") );
                        String val = v.split( "[-/ \\.]" )[ndx-1];
                        form.setField( k, val );
                     }
                     catch( Exception e )
                     {
                        Log.log( Log.ERROR, "WTF did u do with the PDF \"" + template + "\" on \"" + k + "\"" );
                        Log.log( e );
                     }
                  }
                  else
                  {
                     form.setField( k, v );
                  }
               }
            }
            catch( Exception e )
            {
               Log.log( e );
            }
         }
         stamper.close();
      }
      catch( Exception e )
      {
         Log.log(e);
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- getFields ...                                                     ---*/
   /*-------------------------------------------------------------------------*/
   public static Vector getFields( String template )
   {
      Log.log( Log.INFO, "PDF.getFields : " + template );
      Vector v = new Vector();
      try
      {
         ByteArrayOutputStream baos = new ByteArrayOutputStream();
         PdfReader           reader = getReader( template );
         PdfStamper         stamper = new PdfStamper( reader, baos );
         AcroFields            form = stamper.getAcroFields();
         HashMap                 hm = form.getFields();
         Set                    set = hm.keySet();
         Iterator                it = set.iterator();
         while( it.hasNext() )
            v.addElement( it.next() );
         stamper.close();
      }
      catch( Exception e )
      {
         Log.log(e);
      }
      return v;
   }

   /*-------------------------------------------------------------------------*/
   /*--- getTypes ...                                                      ---*/
   /*-------------------------------------------------------------------------*/
   public static HashMap getFieldsType( String template )
   {
      HashMap fTypes = new HashMap();
      fTypes.put( "" +  AcroFields.FIELD_TYPE_PUSHBUTTON   , "BUTTON"      );
      fTypes.put( "" +  AcroFields.FIELD_TYPE_CHECKBOX     , "CHECKBOX"    );
      fTypes.put( "" +  AcroFields.FIELD_TYPE_RADIOBUTTON  , "RADIO"       );
      fTypes.put( "" +  AcroFields.FIELD_TYPE_TEXT         , "TEXT"        );
      fTypes.put( "" +  AcroFields.FIELD_TYPE_LIST         , "LIST"        );
      fTypes.put( "" +  AcroFields.FIELD_TYPE_COMBO        , "COMBO"       );
      fTypes.put( "" +  AcroFields.FIELD_TYPE_SIGNATURE    , "SIGNATURE"   );

      Log.log( Log.INFO, "PDF.getFieldsType : " + template );
      HashMap hmTypes = new HashMap();
      try
      {
         ByteArrayOutputStream baos = new ByteArrayOutputStream();
         PdfReader           reader = getReader( template );
         PdfStamper         stamper = new PdfStamper( reader, baos );
         AcroFields            form = stamper.getAcroFields();
         HashMap                 hm = form.getFields();
         Set                    set = hm.keySet();
         Iterator                it = set.iterator();
         while( it.hasNext() )
         {
            String fieldName = (String) it.next();
            String fieldType = "" + form.getFieldType(fieldName);
            String typeName  = "" + fTypes.get(""+fieldType);
            String appears[] = form.getAppearanceStates(fieldName);
            if( typeName.equals("RADIO") )
            {
               String tmp = "";
               for( int i=0; i<appears.length; i++ )
               {
                  if( appears[i].equals("Off") )
                     continue;
                  tmp += "|" + appears[i];
               }
               try { tmp = tmp.substring(1); } catch( Exception e ) {}
               typeName += " [ " + tmp + " ]";
            }
            hmTypes.put( fieldName, typeName );
         }
         stamper.close();
      }
      catch( Exception e )
      {
         Log.log(e);
      }
      return hmTypes;
   }

   /*-------------------------------------------------------------------------*/
   /*--- checkFields ...                                                   ---*/
   /*-------------------------------------------------------------------------*/
   public static Vector checkFields( String template, HashMap dictionary )
   {
      Vector v = getFields( template );
      Vector o = new Vector();
      for( int i=0; i<v.size(); i++ )
      {
         String  value  = (String) v.elementAt(i);
         HashMap record = new HashMap();
         
         String  val    = (String) dictionary.get(value);
         if( val == null )
            record.put( value, Boolean.FALSE  );
         else
            record.put( value, Boolean.TRUE );
         o.addElement( record );
      }
      return o;
   }

   /*-------------------------------------------------------------------------*/
   /*--- renameFields ...                                                  ---*/
   /*-------------------------------------------------------------------------*/
   public static void renameFields( String template, HashMap fieldMapping )
   {
      Log.log( Log.INFO, "Renaming Fields for " + template );
      FileOutputStream out = null;
      String       outFile = template + ".tmp.pdf";
      try
      {
         out = new FileOutputStream( outFile );
         PdfReader           reader = getReader( template );
         PdfStamper         stamper = new PdfStamper( reader, out );
         AcroFields            form = stamper.getAcroFields();
         HashMap                 hm = form.getFields();
         Set                    set = hm.keySet();
         Iterator                it = set.iterator();
         Vector           allFields = new Vector();
         while( it.hasNext() )
            allFields.addElement( it.next() );
         for( int i=0; i<allFields.size(); i++ )
         {
            try
            {
               String fieldName = (String) allFields.elementAt(i);
               String newName   = (String) fieldMapping.get(fieldName);
               if( (newName!=null) && (newName.trim().length()!=0) )
                  form.renameField( fieldName, newName );
            }
            catch( Exception e )
            {
               Log.log(e);
            }
         }
         stamper.close();
         
         //--- remove original file, rename generated file with original name
         File tmpFile = new File(template);
         tmpFile.delete();
         (new File(outFile)).renameTo( tmpFile );
      }
      catch( Exception e )
      {
         Log.log(e);
      }
      finally
      {
         try { out.close(); } catch( Exception e ) {}
      }
   }

   /*-------------------------------------------------------------------------*/
   /*--- html2PDF ...                                                      ---*/
   /*-------------------------------------------------------------------------*/
   public static byte[] html2PDF( String input ) throws Exception
   {
      /*
      Document              document   =  new Document();
      ByteArrayOutputStream baos       =  new ByteArrayOutputStream();
      PdfWriter             pdfWriter  =  PdfWriter.getInstance( document, baos );
      document.open();
      
      // document.addAuthor("Author of the Doc");
      // document.addCreator("Creator of the Doc");
      // document.addSubject("Subject of the Doc");
      // document.addCreationDate();
      // document.addTitle("This is the title");
      
      HTMLWorker  htmlWorker  =  new HTMLWorker(document);
      if( input.startsWith("http://") || input.startsWith("https://") )
         htmlWorker.parse( new String(webGet(input)) );
      else
         htmlWorker.parse( new StringReader(loadASCIIFile(input)) );
      document.close();
      byte[] b = baos.toByteArray();
      baos.close();
      return b;
      */

      return (new byte[1]);
   }
}
