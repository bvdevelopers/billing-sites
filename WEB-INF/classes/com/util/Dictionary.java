package com.util;

import java.io.*;
import java.net.*;
import java.util.*;
import jxl.*;

public class Dictionary
{
   public  static HashMap cacheDictionary = new HashMap();
   
   /*-------------------------------------------------------------------------*/
   /*--- dictionary ...                                                    ---*/
   /*-------------------------------------------------------------------------*/
   public static HashMap dictionary( String fileName )
   {
      return dictionary( fileName, false );
   }
   
   public static HashMap dictionary( String fileName, boolean nameOnly )
   {
      LinkedHashMap hm =  new LinkedHashMap();
      Log.log( Log.INFO, "Load dictionary from " + fileName );
      try
      {
         FileInputStream   fip         =  new FileInputStream( fileName );
         Workbook          workbook    =  Workbook.getWorkbook( fip );
         Sheet             sheet[]     =  workbook.getSheets();
         Sheet             mainSheet   =  sheet[0];
         
         for( int i=1; i<mainSheet.getRows(); i++ )
         {
            Cell[] cells = mainSheet.getRow(i);
            if( cells.length==1 || cells[1].getContents().trim().length()==0 )
            {
               String tmpCnt = cells[0].getContents().trim();
               hm.put( tmpCnt, tmpCnt );
            }
            else
            {
               String k          = "";
               String fieldName  = cells[1].getContents().trim();
               if( nameOnly )
               {
                  k = fieldName;
               }
               else
               {
                  k += "|"; try { k += cells[0].getContents(); } catch( Exception e ) {}
                  k += "|"; try { k += fieldName;              } catch( Exception e ) {}
                  k += "|"; try { k += cells[2].getContents(); } catch( Exception e ) {}
                  try {           k = k.substring(1);          } catch( Exception e ) {}
               }
               try
               {
                  hm.put( k, cells[1].getContents().trim() );
               }
               catch( Exception e )
               {
                  // ignore... it means that the row is empty or not valid
               }
            }
         }
         fip.close();
         Log.log( Log.INFO, hm.toString() );
      }
      catch( Exception exp )
      {
         Log.log( exp );
      }
      return hm;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- checkDictionary ...                                               ---*/
   /*-------------------------------------------------------------------------*/
   public static boolean checkDictionary( String fileName, String type, String name, String size )
   {
      StringBuffer    logTxt = null;
      StringBuffer       key = new StringBuffer();
      key.append( type ).append("|").append(name).append("|");
      if( !size.equals("") )
         key.append(size);
      
      logTxt = (new StringBuffer("Check if \"")).append(key).append("\" in dictionary");
      Log.log( Log.INFO, logTxt.toString() );
      String dicValue = (String) cacheDictionary.get( key.toString() );
      dicValue = dicValue==null?"":dicValue.trim();
      boolean isValid = (dicValue.length()>0);
      
      if( !isValid )
      {
         logTxt = (new StringBuffer(key)).append(" NOT FOUND in Dictionary");
         Log.log( Log.INFO, logTxt.toString() );
         cacheDictionary = dictionary( fileName );
         Log.log( Log.INFO, "reloading Dictionary" );
         dicValue = (String) cacheDictionary.get( key.toString() );
         dicValue = dicValue==null?"":dicValue.trim();
         isValid         = isValid = (dicValue.length()>0);
      }
      return isValid;
   }

   /*-------------------------------------------------------------------------*/
   /*--- htmlSelectDictionary ...                                          ---*/
   /*-------------------------------------------------------------------------*/
   public static String htmlSelectDictionary( String fileName )
   {
      StringBuffer html = new StringBuffer();
      if( cacheDictionary.isEmpty() )
         cacheDictionary = dictionary( fileName );
      Iterator it = cacheDictionary.keySet().iterator();
      html.append( "<select size=\"2\" style=\"height:450px\" id='dictionary'>" );
      boolean optGroup = false;
      while( it.hasNext() )
      {
         String k  = (String) it.next().toString();
         String v  = (String) cacheDictionary.get(k);
         String kv = (new StringBuffer(k)).append(v).toString().trim();
         if( kv.length() > 0 )
         {
            if( v.equals(k) )
            {
               if( optGroup )
                  html.append( "</optgroup>" );
               html.append( "<optgroup label=\"") .append(k).append("\">");
            }
            else
            {
               html.append( "<option value=\"").append(v).append("\">").append(v).append("</option>");
            }
            optGroup = true;
         }
      }
      html.append( "</optgroup>" );
      html.append( "</select>" );
      return html.toString();
   }
   
   public static void reset()
   {
      cacheDictionary = new HashMap();
   }
}
