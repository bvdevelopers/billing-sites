package com.util;

import java.io.*;
import java.util.*;
import jxl.format.*;
import jxl.*;

public class ExcelMap
{
   /*-------------------------------------------------------------------------*/
   /*--- loadSubSheet ...                                                  ---*/
   /*-------------------------------------------------------------------------*/
   public static HashMap load( String filePath )
   {
      Log.log( Log.INFO, "Load ExcelMap from " + filePath );
      HashMap hm =  new HashMap();
      try
      {
         FileInputStream   fip      =  new FileInputStream( filePath );
         Workbook          workbook =  Workbook.getWorkbook( fip );
         Sheet             sheet    =  workbook.getSheets()[0];
         for( int i=0; i<sheet.getRows(); i++ )
         {
            try
            {
               Cell[] cells  = sheet.getRow(i);
               hm.put( cells[0].getContents(), cells[1].getContents() );
            }
            catch( Exception e )
            {
               //--- ignore
            }
         }
         fip.close();
      }
      catch( Exception e )
      {
         Log.log(e);
      }
      return hm;
   }
}
