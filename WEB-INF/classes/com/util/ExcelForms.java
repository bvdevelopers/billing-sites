package com.util;

import java.io.*;
import java.net.*;
import java.awt.Color;
import java.util.*;
import jxl.format.*;
import java.util.concurrent.*;
import jxl.*;

public class ExcelForms
{
   private static ConcurrentHashMap cacheLookup = new ConcurrentHashMap();


   private static int    CELLPADDING = 0;
   private static int    CELLSPACING = 0;
   private static String WIDTH       = "";
   
   /*-------------------------------------------------------------------------*/
   /*--- getColor ...                                                      ---*/
   /*-------------------------------------------------------------------------*/
   private static String getColor( jxl.format.RGB rgb )
   {
      return getColor( rgb.getRed(), rgb.getGreen(), rgb.getBlue() );
   }
   private static String getColor( int r, int g, int b )
   {
      Color c = new Color( r, g, b );
      String rgbCode = Integer.toHexString( c.getRGB() & 0x00ffffff );
      if( rgbCode.length()== 5 ) rgbCode = "0" + rgbCode;
      return Format.leftPad( rgbCode, "0", 6 );
   }
   
   /*------------------------------------------------------------------------*/
   /*--- isInSpan ...                                                     ---*/
   /*------------------------------------------------------------------------*/
   private static boolean isInSpan( Sheet sheet, Cell c )
   {
      Range[] r = sheet.getMergedCells();
      for( int i=0; i<r.length; i++ )
      {
         Cell tl = r[i].getTopLeft();
         Cell br = r[i].getBottomRight();
         if( c.getRow()==tl.getRow() && c.getColumn()==tl.getColumn() )
            return false;
         if(   (c.getRow()>=tl.getRow())  && (c.getColumn()>=tl.getColumn())
               &&
               (c.getRow()<=br.getRow()) && (c.getColumn()<=br.getColumn()) )
            return true;
      }
      return false;
   }
   
   /*------------------------------------------------------------------------*/
   /*--- getSpan ...                                                      ---*/
   /*------------------------------------------------------------------------*/
   private static int getSpan( char dir, Sheet sheet, Cell c )
   {
      Range[] r = sheet.getMergedCells();
      for( int i=0; i<r.length; i++ )
      {
         Cell tl = r[i].getTopLeft();
         Cell br = r[i].getBottomRight();
         if( tl.getRow()==c.getRow() && tl.getColumn()==c.getColumn() )
         {
            if( dir == 'r' )
               return br.getRow() - tl.getRow() + 1;
            if( dir == 'c' )
               return br.getColumn() - tl.getColumn() + 1;
         }
      }
      return 1;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- loadSubSheet ...                                                  ---*/
   /*-------------------------------------------------------------------------*/
   private static String loadSubSheet( String dictionary, String baseFolder, String folder, Sheet sheet[], String sheetName, boolean debug )
   {
      String arr[]         = sheetName.split( "\\." );
      Sheet  newSheets[]   = sheet;
      String newSheetName  = sheetName;
      Log.log( Log.INFO, "loading sub-sheet \"" + folder + "\"->\"" + sheetName + "\"" );
      String fileName      =  folder + "/" + arr[0] + ".xls";
      if( sheetName.startsWith("/") )
      {
         Log.log( Log.INFO, "Looking in general shared xls " + folder );
         fileName = folder + "/../../extra/forms/" + arr[0] + ".xls";
      }
      try
      {
         if( arr.length==2 )
         {
            Log.log( Log.INFO, "sub-sheet File Name \"" + fileName + "\"" );
            FileInputStream   fip         =  new FileInputStream( fileName );
            WorkbookSettings  ws          = new WorkbookSettings();
            ws.setEncoding("ISO-8859-1");
            Workbook          workbook    =  Workbook.getWorkbook( fip, ws );
            newSheets      =  workbook.getSheets();
            newSheetName   =  arr[1];
            fip.close();
         }
         return loadSheet( dictionary, baseFolder, folder, newSheets, newSheetName, debug );
      }
      catch( FileNotFoundException e )
      {
         Log.log( Log.ERROR, "\"" + fileName + "\" FILE NOT FOUND" );
         return "<span style='color:red'><b>Cannot Load " + sheetName + "</b></span>";
      }
      catch( jxl.read.biff.BiffException  be )
      {
         Log.log( Log.ERROR, "\"" + fileName + "\" INVALID EXCEL FORMAT" );
         return "<span style='color:red'><b>Invalid Format " + folder + " / " + sheetName + "</b></span>";
      }
      catch( java.io.IOException ioe )
      {
         Log.log( ioe );
         return "<span style='color:red'><b>IO Error " + folder + " / " + sheetName + "</b></span>";
      }
      catch( Exception e )
      {
         Log.log( e );
         return "<span style='color:red'><b>Unexpected ERROR " + folder + " / " + sheetName + "</b></span>";
      }
      
   }

   /*-------------------------------------------------------------------------*/
   /*--- loadSheet ...                                                     ---*/
   /*-------------------------------------------------------------------------*/
   private static String loadSheet( String dictionary, String baseFolder, String folder, Sheet sheet[], String sheetName, boolean debug ) throws Exception
   {
      Log.log( Log.INFO, "Loading Sheet : \"" + sheetName + "\"" );
      Sheet theSheet = null;
      String    code = "";
      for( int i=0; i<sheet.length; i++ )
      {
         if( sheet[i].getName().equals(sheetName) )
         {
            theSheet = sheet[i];
            break;
         }
      }
      code += "<table " +WIDTH+ " cellpadding='"+CELLPADDING+"' cellspacing='"+CELLSPACING+"' border=" + (debug?1:0) + ">";
      for( int i=0; i<theSheet.getRows(); i++ )
      {
         Cell[] cells  = theSheet.getRow(i);
         code += "<tr id=\"" + sheetName + "_" + (i+1) + "\">";
         for( int j=0; j<cells.length; j++ )
         {
            //--- Handling spanning
            if( isInSpan(theSheet, cells[j]) )
               continue;
            String spanCode = "";
            int    span     = 0;
            span = getSpan( 'r', theSheet, cells[j] );
            if( span > 1 ) spanCode += " rowspan=" + span + " ";
            span = getSpan( 'c', theSheet, cells[j] );
            if( span > 1 ) spanCode += " colspan=" + span + " ";
            
            if( cells[j].getContents().trim().equals("$vr") )
            {
               code += "<td class='form' " + spanCode + " style='font-size:1px; background:url(/img/vr.png); height:100%; padding:0px 15px 0px 15px;'>";
               code += "&nbsp;";
               code += "</td>";


            }
            else
            {
               code += "<td id=\""+sheetName+"_" + (i+1) + ((char)(j+'a'))+"\" class='form cellOK' nowrap=\"nowrap\" style=\"" + cellStyle(cells[j]) + "\" " + spanCode + ">";
               code += cellWidjet( dictionary, baseFolder, folder, sheet, cells[j], debug );
               code += "" + (debug?"&nbsp;":"") + "</td>";
            }
         }
         code += "</tr>";
      }
      code += "</table>";
      return code;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- cellWidjet ...                                                    ---*/
   /*-------------------------------------------------------------------------*/
   private static String cellWidjet(   String dictionary,
                                       String baseFolder,
                                       String folder,
                                       Sheet  sheet[],
                                       Cell   cell,
                                       boolean debug )
   {
      String cellContentData = cell.getContents().trim();
      String cellComment = "";
      
      try
      {
         cellComment = cell.getCellFeatures().getComment().trim().split( "[\r\n]" )[0];
      }
      catch( Exception e )
      {
         cellComment = "";
      }
      
      String placeholder = "";
      if( !cellComment.equals("") )
         placeholder = " placeholder=\"" + cellComment + "\" ";
      
      boolean isFreeFromDictionary = false;;
      for( int i=0; i<sheet.length; i++ )
      {
         if( sheet[i].getName().equals("FREE") )
         {
            isFreeFromDictionary = true;
         }
      }
      String returnValue = "";
      String SplitCellData[] = cellContentData.split( "\n" );
      for( int n=0; n<SplitCellData.length; n++ )
      {
         String cellContent = SplitCellData[n];
         if( cellContent.equals("\n") || cellContent.equals("\r") )
            continue;
         if( cellContent.toUpperCase().startsWith( "#" ) )
         {
            try
            {
               String tmpCode = "";
               String style   = "";
               try { style = cell.getCellFeatures().getComment().trim().replace("\r","").replace("\n"," "); } catch( Exception e ) {}
               tmpCode += "<div style=\"" + style + "\">";
               tmpCode += loadSubSheet( dictionary, baseFolder, folder, sheet, cellContent.substring(1), debug );
               tmpCode += "</div>";
               returnValue += tmpCode;
            }
            catch( Exception e )
            {
               Log.log( e );
               returnValue += "<div style='color:red'><i>" + cellContent + "</i></div>";
            }
         }
         else
         if( cellContent.toUpperCase().startsWith( "&W" ) )
         {
            returnValue += "<div style='width:" + cellContent.substring(2) + "px;'>&nbsp;</div>";
         }
         else
         if( cellContent.toUpperCase().startsWith( "&H" ) )
         {
            returnValue += "<div style='font-size:1px; height:" + cellContent.substring(2) + "px;'>&nbsp;</div>";
         }
         else
         if( cellContent.startsWith( "$" ) )
         {
            String field[] = (cellContent.substring(1)+(": : : : : : : : : : : ")).split( ":" );
            if( field[0].equals("hr") || field[0].equals("br") )
            {
               String r = "";
               r += "" + (debug?"&nbsp;":"") + "</td></tr></table>";
               r += "<div style='height:5px; font-size:1px;'>&nbsp;</div>";
               r += "<table cellpadding=0 cellspacing=0 " + (field[0].equals("hr")?"background='/img/hr.png'":"") + " border=0 height=2 width='100%'><tr><td style='font-size:1px; height:1px; padding:0px;'>&nbsp;</td></tr></table>";
               r += "<div style='height:5px; font-size:1px;'>&nbsp;</div>";
               r += "<table " + WIDTH + " cellpadding='"+CELLPADDING+"' cellspacing='"+CELLSPACING+"' border=" + (debug?1:0) + "><tr><td class='form'>";
               returnValue += r;
            }
            else
            if( field[0].equals("link") )
            {
               returnValue += "<label id=\"label_"+cellComment+"\">";
               //---- link, label
               field = cellContent.substring(6).split( ":" );
               if( field[0].contains("(") )
                  field[0] = "javascript:" + field[0];
               returnValue += "<a onClick={isQuit=false;} href=\""+field[0]+"\">" + field[1] + "</a>";
               returnValue += "</label>";
            }
            else
            if( field[0].equals("h1") || field[0].equals("h2") || field[0].equals("h3") )
            {
               returnValue += "<" + field[0] + ">" + field[1] + "</" + field[0] + ">";
            }
            else
            if( field[0].equals("msg") )
            {
               returnValue += "<label style=\"cursor:pointer;\" for=\"" + field[1] + "\" class=\"err_msg\" id=\"_msg_" + field[1] + "\">&nbsp;</label>";
            }
            else
            if( field[0].equals("txt") )
            {
               //---- name, maxlength, width, require
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], field[2] ) )
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               else
                  returnValue += "<input " + placeholder + " style=\"width:" + field[3] + "px;\" field='TXT' maxlength='" + field[2] + "' id='" + field[1] + "' name='" + field[1] + "' req=\""+field[4].toUpperCase()+"\" type=\"text\">";
            }
            else
            if( field[0].equals("number") )
            {
               //---- name, maxlength, width, require
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], field[2] ) )
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               else
                  returnValue += "<input " + placeholder + " style=\"width:" + field[3] + "px;\" field='NUMBER' maxlength='" + field[2] + "' id='" + field[1] + "' name='" + field[1] + "' req=\"" + field[4].toUpperCase() + "\" type=\"text\">";
            }
            else
            if( field[0].equals("pagenum") )
            {
               //---- pagenumber
               returnValue += "<input value=\""+field[1]+"\" name='_PAGENUM_' type=\"hidden\">";
            }
            else
            if( field[0].equals("hidden") )
            {
               //---- name, value
               returnValue += "<input value=\""+field[2].trim()+"\" name=\""+field[1]+"\" id=\""+field[1]+"\" type=\"hidden\">";
            }
            else
            if( field[0].equals("zip") )
            {
               //---- name, width, require, left-label, left-label-width (don't ask don't tell)
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], "" ) )
               {
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               }
               else
               {
                  if( field[4].trim().length() > 0 )
                     returnValue += "<table align=\"right\" cellpadding=0 cellspacing=0><tr><td style=\"" + cellStyle(cell) + "\" align=right " + (field[5].trim().length()>0?("width="+field[5]):"") + "><label class='lbls' id=\"label_"+field[1]+"\" for='"+field[1]+"'>" + field[4] + ":&nbsp;</label></td><td>";
                  returnValue += "<input " + placeholder + " style=\"width:" + field[2] + "px;\" field='ZIP' maxlength='10' id='" + field[1] + "' name='" + field[1] + "' req=\"" + field[3].toUpperCase() + "\" type=\"text\">";
                  if( field[4].trim().length() > 0 )
                     returnValue += "</td></tr></table>";
               }
            }
            else
            if( field[0].equals("email") )
            {
               //---- name, maxlength, width, require
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], field[2] ) )
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               else
                  returnValue += "<input " + placeholder + " style=\"width:" + field[3] + "px;\" field='EMAIL' maxlength='" + field[2] + "' id='" + field[1] + "'  name='" + field[1] + "' req=\"" + field[4].toUpperCase() + "\" type=\"text\">";
            }
            else
            if( field[0].equals("date") )
            {
               //---- name, past (Y/N), require
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], "" ) )
               {
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               }
               else
               {
                  int nYrs = 0; try { nYrs=Integer.parseInt(cellComment);} catch( Exception e ) {}
                  returnValue += dateObject( field[1],"DATE",field[3].toUpperCase(), field[2].equalsIgnoreCase("Y"), field[4].trim(), nYrs, field[5].trim(),field[6].trim() );
               }
            }
            else
            if( field[0].equals("picker") )
            {
               //---- name, width, require
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], "" ) )
               {
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               }
               else
               {
                  returnValue += "<input class='picker' " + placeholder + " style=\"width:" + field[2] + "px;\" field='TXT' maxlength='10' id='" + field[1] + "' name='" + field[1] + "' req=\""+field[3].toUpperCase()+"\" type=\"text\">";
               }
            }
            else
            if( field[0].equals("dob") )
            {
               //---- name, width, require
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], "" ) )
               {
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               }
               else
               {
                  int nYrs = 0; try { nYrs=Integer.parseInt(cellComment);} catch( Exception e ) {}
                  returnValue += dateObject( field[1], "DOB", field[3].toUpperCase(), true, field[4].trim(),nYrs ,field[5].trim(), field[6].trim() );
               }
            }
            else
            if( field[0].equals("height") )
            {
               //---- name, required
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], "" ) )
               {
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               }
               else
               {
                  String tmp = "";
                  String req = field[2].toUpperCase();
                  tmp += "<select onChange=\"setHeight('"+field[1]+"');\" id=\"_feet_" + field[1] + "\"><option value=\"\" style='color:gray'>Feet</option>";
                  for( int i=1; i<=7; i++ )
                     tmp += "<option value=\"" + i + "\">"+i+"</option>";
                  tmp += "</select>";
                  tmp += "&nbsp;";
                  tmp += "<select onChange=\"setHeight('"+field[1]+"');\" id=\"_inch_" + field[1] + "\">";
                  tmp += "<option  value=\"\" style='color:gray'>Inches</option>";
                  for( int i=0; i<12; i++ )
                     tmp += "<option value=\"" + i + "\">" + i + "</option>";
                  tmp += "</select>";
                  tmp += "<input field=\"HEIGHT\" req=\"" + req + "\" type=\"hidden\" name=\"" + field[1] + "\" id=\"" + field[1] + "\">";
                  returnValue += tmp;
               }
            }
            else
            if( field[0].equals("monthyear") )
            {
               //---- name, require, ahead
               String ahead = field[3].toUpperCase();
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], "" ) )
               {
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               }
               else
               {
                  int currentYear = Format.currentYear();
                  String tmp = "";
                  String req = field[2].toUpperCase();
                  tmp += "<select onChange=\"setMonthYear(this);\" id=\"_mm_" + field[1] + "\"><option value=\"\" style='color:gray'>Month</option>";
                  for( int i=1; i<=12; i++ )
                  {
                     String _mm = (i<10?"0":"")+i;
                     tmp += "<option value=\"" + _mm + "\">";
                     tmp += Static.SHORT_MONTHS[i];
                     tmp += "</option>";
                  }
                  tmp += "</select>";
                  tmp += "&nbsp;-&nbsp;";
                  tmp += "<select onChange=\"setMonthYear(this);\" id=\"_yy_" + field[1] + "\">";
                  tmp += "<option  value=\"\" style='color:gray'>Year</option>";
                  if( ahead.equals("Y") )
                  {
                     for( int i=currentYear; i<currentYear+5; i++ )
                        tmp += "<option value=\"" + i + "\">" + i + "</option>";
                  }
                  else
                  {
                     for( int i=currentYear; i>currentYear-40; i-- )
                        tmp += "<option value=\"" + i + "\">" + i + "</option>";
                  }
                  tmp += "</select>";
                  tmp += "<input field=\"MONTH_YEAR\" req=\"" + req + "\" type=\"hidden\" name=\"" + field[1] + "\" id=\"" + field[1] + "\">";
                  returnValue += tmp;
               }
            }
            else
            if( field[0].equals("phone") )
            {
               //---- name, width, require
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], "" ) )
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               else
               returnValue += "<input style=\"width:" + field[2] + "px;\" field='PHONE' " + placeholder + " maxlength='12' id='" + field[1] + "' name='" + field[1] + "' req=\"" + field[3].toUpperCase() + "\" type=\"text\">";
            }
            else
            if( field[0].equals("sex") )
            {
               //---- name, require, width
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], "" ) )
               {
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               }
               else
               {
                  String style   = "";
                  String tmpCode = "";
                  try { style = "width:" + Integer.parseInt(field[3]) + "px"; } catch( Exception e ) {}
                  tmpCode += "<select style=\"" + style + "\" req=\"" + field[2].toUpperCase() + "\" field='SEX' name='" + field[1] + "' id='" + field[1] + "'>";
                  if( field[4].equals("PT") )
                  {
                     tmpCode += "<option value=\"\">--Selecionar--</option>";
                     tmpCode += "<option value=\"M\">Masculino</option>";
                     tmpCode += "<option value=\"F\">Feminino</option>";
                  }
                  else
                  {
                     tmpCode += "<option value=\"\">--Select--</option>";
                     tmpCode += "<option value=\"M\">Male</option>";
                     tmpCode += "<option value=\"F\">Female</option>";
                  }
                  tmpCode += "</select>";
                  returnValue += tmpCode;
               }
            }
            else
            if( field[0].equals("yesno") )
            {
               //---- name, require, width
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], "" ) )
               {
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               }
               else
               {
                  if( field[3].equals("radio") )
                  {
                     String tmpCode = "";
                     tmpCode += "<table cellpadding=0 cellspacing=0><tr>";
                     tmpCode += "<td><input field='RADIO' req='"+field[2].toUpperCase()+"' name='"+field[1]+"' id='"+field[1]+"Y' value='Y' type=radio></td><td><label for='"+field[1]+"Y'>&nbsp;Yes</label></td>";
                     tmpCode += "<td width=30></td>";
                     tmpCode += "<td><input field='RADIO' req='"+field[2].toUpperCase()+"' name='"+field[1]+"' id='"+field[1]+"N' value='N' type=radio></td><td><label for='"+field[1]+"N'>&nbsp;No</label></td>";
                     tmpCode += "</tr></table>";
                     returnValue += tmpCode;
                  }
                  else
                  {
                     String style   = "";
                     String tmpCode = "";
                     try { style = "width:" + Integer.parseInt(field[3]) + "px"; } catch( Exception e ) {}
                     tmpCode += "<select style=\"" + style + "\" req=\"" + field[2].toUpperCase() + "\" field='YESNO' name='" + field[1] + "' id='" + field[1] + "'>";
                     tmpCode += "<option value=\"\">--Select--</option>";
                     tmpCode += "<option value=\"Y\">Yes</option>";
                     tmpCode += "<option value=\"N\">No</option>";
                     tmpCode += "</select>";
                     returnValue += tmpCode;
                  }
               }
            }
            else
            if( field[0].equals("state") )
            {
               //---- name, width, require, (Y)code || (N)label, country
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], "" ) )
               {
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               }
               else
               {
                  String tmpCode = "";
                  String width   = "";
                  field[4] = field[4].trim();
                  field[5] = field[5].trim();
                  field[4] = field[4].equals("")?"Y": field[4].toUpperCase();
                  field[5] = field[5].equals("")?"US":field[5].toUpperCase();
                  if( field[2].trim().length() > 0 )
                     width = "style=\"width:" + field[2] + "px\"";
                  
                  tmpCode += "<select req=\"" + field[3].toUpperCase() + "\" " + width + " field='STATE' id='" + field[1] + "' name='" + field[1] + "'>";
                  tmpCode += "<option></option>";
                  
                  if( field[4].equals("N") )
                     tmpCode += optionStates( field[5], true  );
                  else
                     tmpCode += optionStates( field[5], false );
                  
                  tmpCode += "</select>";
                  returnValue += tmpCode;
               }
            }
            else
            if( field[0].equals("radio") )
            {
               //---- name, label, value, require
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], "" ) )
               {
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               }
               else
               {
                  String tmpCode = "";
                  String lblId   = field[1] + cell.getColumn() + "" + cell.getRow();
                  tmpCode += "<div class='radio-"+cellComment+"'>";
                  tmpCode += "<table cellpadding='0' cellspacing='0'><tr>";
                  tmpCode += "<td><input class='"+field[1]+"' field='RADIO' req='"+field[4].toUpperCase()+"' name='"+field[1]+"' id='"+lblId+"' value='"+field[2]+"' type=radio></td>";
                  tmpCode += "<td><label for='"+lblId+"'>" + field[2] + "</label></td>";
                  tmpCode += "</tr></table>";
                  tmpCode += "</div>";
                  returnValue += tmpCode;
               }
            }
            else
            if( field[0].equals("select") )
            {
               //---- name, cols, require, tab_name
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], "" ) )
               {
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               }
               else
               {
                  String   tmpCode  = "";
                  int      cols     = 0;
                  try { cols = Integer.parseInt(field[2]); } catch( Exception e ) {}
                  
                  if( field[0].equals("radio") )
                  {
                     tmpCode += "<table cellpadding=0 cellspacing=0 border=" + (debug?1:0) + "><tr>";
                  }
                  else
                  {
                     String style = "";
                     if( !field[2].equals("") && !field[2].equals("0") )
                        style = "style=\"width:" + field[2] + "px;\" ";
                     tmpCode += "<select " + style + " req=" + field[3].toUpperCase() + " field='SELECT' id='" + field[1] + "' name=\"" + field[1] + "\"><option value=\"\">--Select--</option>";
                  }
                  
                  //--- GET THE SHEET ---
                  Sheet theSheet = null;
                  if( field[4].contains(".") )
                  {
                     theSheet = loadSelectSheet( baseFolder, folder, field[4] );
                  }
                  else
                  {
                     for( int i=0; i<sheet.length; i++ )
                     {
                        if( sheet[i].getName().equals(field[4]) )
                        {
                           theSheet = sheet[i];
                           break;
                        }
                     }
                  }
                  //--- RENDER THE SHEET ---
                  if( theSheet!=null )
                  {
                     for( int i=0; i<theSheet.getRows(); i++ )
                     {
                        Cell[] cells  = theSheet.getRow(i);
                        if( field[0].equals("radio") && (cols!=0) && (i%cols==0) && i>0 )
                           tmpCode += "</tr><tr>";
                        
                        String ky = cells[0].getContents();
                        String vl = cells[0].getContents();
                        try {  vl = cells[1].getContents(); } catch( ArrayIndexOutOfBoundsException e ) {}
                        if( field[0].equals("radio") )
                        {
                           tmpCode += "<td style='padding:0px;' valign=middle>";
                           tmpCode += "<div class=\"radio-input\">";
                           tmpCode += "<input req=\"" + field[3].toUpperCase() + "\" field='RADIO' id=\"" + field[1] + i + "\" name=\"" + field[1] + "\" value=\"" + vl + "\" type=\"radio\">";
                           tmpCode += "<label for=\"" + field[1] + i + "\">" + vl + "</label>";
                           tmpCode += "</div>";
                           tmpCode += "</td>";
                           tmpCode += "<td width=20>" + (debug?"&nbsp;":"") + "</td>";
                        }
                        else
                        {
                           tmpCode += "<option value=\"" + ky.trim() + "\">" + vl.trim() + "</option>";
                        }
                     }
                  }
                  if( field[0].equals("radio") )
                     tmpCode += "</tr></table>";
                  else
                     tmpCode += "</select>";
                  returnValue += tmpCode;
               }
            }
            else
            if( field[0].equals("checkbox") )
            {
               //---- name, checked
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], "" ) )
               {
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               }
               else
               {
                  boolean checked = false;
                  try { checked = (""+field[3]).toUpperCase().equals("Y"); } catch( Exception e ) {};
                  
                  String tmpCode = "";
                  tmpCode += "<input "+(checked?"checked=\"true\"":"")+" field='CHECKBOX' id=\"" + field[1] + "\" type=\"checkbox\" name=\"" + field[1] + "\">" + (debug?"&nbsp;":"");
                  tmpCode += "<label for=\"" + field[1] + "\">" + field[2] + "</label>" + (debug?"&nbsp;":"");
                  returnValue += tmpCode;
               }
            }
            else
            if( field[0].equals("button") )
            {
               /*
               //----- alt, txt, reflect
               String txt = field[2];
               try { txt = URLEncoder.encode(field[2], "UTF-8"); } catch( Exception e ) {}
               
               boolean  reflect  = false;
               try { reflect = field[3].toUpperCase().startsWith("Y"); } catch( Exception e ) {}
               
               String            align[]  = { "left", "left", "center", "right" };
               jxl.format.CellFormat fmt  = cell.getCellFormat();
               int      size        = 12;
               String   fg          = null;
               String   bg          = "3477ac";
               boolean  italic      = false;
               String   textAlign   = "";
               try { textAlign   = align[ fmt.getAlignment().getValue() ]; }                  catch( Exception e ) {}
               try { size        = fmt.getFont().getPointSize(); }                            catch( Exception e ) {}
               try { fg          = getColor( fmt.getFont().getColour().getDefaultRGB() ); }   catch( Exception e ) {}
               try { bg          = getColor( fmt.getBackgroundColour().getDefaultRGB() ); }   catch( Exception e ) {}
               try { italic      = fmt.getFont().isItalic();                      }           catch( Exception e ) {}
               size = size*3;
               String imgSrc = "/button.png?gloss=Y&rounding=2&noshad=Y&background="+bg+"&color="+fg+"&sz="+size+"&tx="+txt+ (italic?"&it=":"") + (reflect?"&reflect=y":"");
               */

               jxl.format.CellFormat fmt  = cell.getCellFormat();
               String            align[]  = { "left", "left", "center", "right" };
               String imgSrc = "/img/continue_button.png";
               if( field[1].contains("http") )
                  imgSrc = field[1].replace("https", "https:").replace("http","http:");
               String   textAlign   = "";
               try { textAlign   = align[ fmt.getAlignment().getValue() ]; }                  catch( Exception e ) {}
               
               
               //returnValue += imgSrc + "<br>";
               returnValue += "<div style=\"text-align:" + textAlign + ";\">";
               returnValue += "<div id=\"submit\"><img style='cursor:pointer;' onContextMenu=\"return false;\" onClick=\"postForm();\" alt=\"" + field[1] + "\" src=\"" + imgSrc + "\"></div>";
               returnValue += "<div id=\"spin\"style=\"display:none\"><img src=\"/img/spinner.gif\"></div>";
               returnValue += "</div>";
            }
            else
            if( field[0].equals("section") )
            {
               //---- width:\ncontent
               String tmpCode = "";
               tmpCode += "<table cellpadding=0 cellspacing=0 border=" + (debug?1:0) + " width=\"" + field[1] + "\"><tr><td style='text-align:justify;'>";
               tmpCode += cellContent.substring( 7 + field[1].length() + 3 );
               tmpCode += "" + (debug?"&nbsp;":"") + "</td></tr></table>";
               returnValue += tmpCode;
            }
            else
            if( field[0].equals("img") )
            {
               //---- src
               returnValue += "<img src=\"/img" + cellContent.substring(5) + "\">";
            }
            else
            if( field[0].equals("county") )
            {
               //---- name, state_field, required
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], "" ) )
               {
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               }
               else
               {
                  returnValue += "<select class='county' name=\"" + field[1] + "\" id=\"" + field[1] + "\" req=\"" + field[3].toUpperCase() + "\" state=\"" + field[2] + "\"></select>";
               }
            }
            else
            if( field[0].equals("caryear") )
            {
               //---- name, require, car_make_field, defaultWidth
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], "" ) )
               {
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               }
               else
               {
                  String tmp = field[1]; if( field.length>=4 ) tmp = field[3];
                  returnValue += "<select style=\"width:"+field[4]+"px; \" defaultWidth=\""+field[4]+"\" name=\"" + field[1] + "\" id=\"" + field[1] + "\" req=\"" + field[2].toUpperCase() + "\" car_year=\"" + tmp + "\"></select>";
               }
            }
            else
            if( field[0].equals("carmake") )
            {
               //---- name, require, car_model_field, defaultWidth
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], "" ) )
               {
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               }
               else
               {
                  String tmp = field[1]; if( field.length>=4 ) tmp = field[3];
                  returnValue += "<select style=\"width:"+field[4]+"px; \" defaultWidth=\""+field[4]+"\" name=\"" + field[1] + "\" id=\"" + field[1] + "\" req=\"" + field[2].toUpperCase() + "\" car_make=\"" + tmp + "\"></select>";
                  returnValue += "<input type=\"hidden\" name=\"" + field[1] + "_lbl_\" id=\"" + field[1] + "_lbl_\">";
               }
            }
            else
            if( field[0].equals("carmodel") )
            {
               //---- name, require, car_model_field, defaultWidth
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], "" ) )
               {
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               }
               else
               {
                  String tmp = field[1]; if( field.length>=4 ) tmp = field[3];
                  returnValue += "<select style=\"width:"+field[4]+"px; \" defaultWidth=\""+field[4]+"\" name=\"" + field[1] + "\" id=\"" + field[1] + "\" req=\"" + field[2].toUpperCase() + "\" car_model=\"" + tmp + "\"></select>";
                  returnValue += "<input type=\"hidden\" name=\"" + field[1] + "_lbl_\" id=\"" + field[1] + "_lbl_\">";
               }
            }
            else
            if( field[0].equals("cartrim") )
            {
               //---- name, require, car_model_field, defaultWidth
               if( !isFreeFromDictionary && !Dictionary.checkDictionary(dictionary, field[0], field[1], "" ) )
               {
                  returnValue += "<span style='color:red'><b>" + cellContent + "</b></span>";
               }
               else
               {
                  String tmp = field[1]; if( field.length>=4 ) tmp = field[3];
                  returnValue += "<select style=\"width:"+field[4]+"px; \" defaultWidth=\""+field[4]+"\" name=\"" + field[1] + "\" id=\"" + field[1] + "\" req=\"" + field[2].toUpperCase() + "\" car_trim=\"" + tmp + "\"></select>";
                  returnValue += "<input type=\"hidden\" name=\"" + field[1] + "_lbl_\" id=\"" + field[1] + "_lbl_\">";
               }
            }
            returnValue += "";
         }
         else
         {
            String content = "";
            if( cellComment!=null && !cellComment.trim().equals("") && !cellComment.trim().equals("null") )
               content += "<label class='lbls' id=\"label_"+cellComment+"\" for=\"" + cellComment + "\">";
            content += cellContent;
            if( cellComment!=null && !cellComment.trim().equals("") && !cellComment.trim().equals("null") )
               content += "</label>";
            returnValue += content + "&nbsp;";
         }
      }
      return returnValue;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- cellStyle ...                                                     ---*/
   /*-------------------------------------------------------------------------*/
   private static String cellStyle( Cell cell )
   {
      jxl.format.CellFormat fmt = cell.getCellFormat();
      
      // --- h-align
      String align[]   = { "left", "left", "center", "right" };
      String textAlign = "";
      try {  textAlign  = align[ fmt.getAlignment().getValue() ]; }                         catch( Exception e ) {}
      
      // --- v-align
      String valign[]  = { "top",  "middle", "bottom" };
      String vertAlign = "";
      try { vertAlign  = valign[ fmt.getVerticalAlignment().getValue() ]; }                catch( Exception e ) {}
      
      // --- font style (underline, italic, bold)
      int        weight = 400;
      int         under = 0;
      boolean    italic = false;
      try { under      = fmt.getFont().getUnderlineStyle().getValue();  }                  catch( Exception e ) {}
      try { weight     = fmt.getFont().getBoldWeight();                 }                  catch( Exception e ) {}
      try { italic     = fmt.getFont().isItalic();                      }                  catch( Exception e ) {}
      
      // --- font-size
      int pointSize = 12;
      try { pointSize  = fmt.getFont().getPointSize(); }                                   catch( Exception e ) {}
      
      // --- color
      String    fgColor = null;
      try { fgColor     = "#"+getColor( fmt.getFont().getColour().getDefaultRGB() ); }      catch( Exception e ) {}
      
      String style = "";
      style += "text-align:"        + textAlign                      + ";";
      style += "vertical-align:"    + vertAlign                      + ";";
      if( fgColor!=null )
         style += "color:"             + fgColor                        + ";";
      style += "text-decoration:"   + (under==0?"none":"underline")  + ";";
      style += "font-weight:"       + (weight==400?"normal":"bold")  + ";";
      style += "font-style:"        + (italic?"italic":"normal")     + ";";
      style += "font-size:"         + pointSize                      + "px;";
      
      return style;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- states ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   private static String optionStates( String country, boolean isCode )
   {
      String code      = "";
      String s_codes[] = null;
      String s_names[] = null;
      if( country.equals("US") )
      {
         s_codes = Static.STATE_CODES;
         s_names = Static.STATE_NAMES;
      }
      else
      if( country.equals("BR") )
      {
         s_codes = Static.BR_STATE_CODES;
         s_names = Static.BR_STATE_NAMES;
      }
      else
      {
         s_codes = Static.CA_STATE_CODES;
         s_names = Static.CA_STATE_NAMES;
      }
      
      for( int i=0; i<s_codes.length; i++ )
      {
if( s_codes[i].equals("NY") ) continue;
         if( isCode )
            code += "<option value=\"" + s_codes[i] + "\">" + s_names[i] + "</option>";
         else
            code += "<option value=\"" + s_codes[i] + "\">" + s_codes[i] + "</option>";
      }
      return code;
   }

   /*-------------------------------------------------------------------------*/
   /*--- loadExcel ...                                                     ---*/
   /*-------------------------------------------------------------------------*/
   public static String xl2htm( String dictionary, String baseFolder, String folder, String fileName, boolean debug ) throws Exception
   {
      if( fileName==null || fileName.trim().length()==0 )
         return "";
      String            htmFile     =  fileName.split("\\.")[0] + ".htm";
      FileInputStream   fip         =  new FileInputStream( fileName );
      WorkbookSettings  ws          =  new WorkbookSettings();
      ws.setEncoding("ISO-8859-1");
      Workbook          workbook    =  Workbook.getWorkbook( fip, ws );
      Sheet             sheet[]     =  workbook.getSheets();
      Sheet             mainSheet   =  sheet[0];
      String            content     =  loadSheet( dictionary, baseFolder, folder, sheet, mainSheet.getName(), debug );
      String            html        =  content;
      String            holder      =  "&nbsp;";
      
      html = html.replace( "{State}", "<span class=\"templified init-cap-state\">" + holder + "</span>" );
      html = html.replace( "{state}", "<span class=\"templified low-state\">"      + holder + "</span>" );
      html = html.replace( "{STATE}", "<span class=\"templified cap-state\">"      + holder + "</span>" );
      for( int i=0; i<6; i++ )
      {
         html = html.replace( "{Url["+i+"]}", "<span class=\"templified init-cap-url-" + i + "\">" + holder + "</span>" );
         html = html.replace( "{url["+i+"]}", "<span class=\"templified low-url-"      + i + "\">" + holder + "</span>" );
         html = html.replace( "{URL["+i+"]}", "<span class=\"templified cap-url-"      + i + "\">" + holder + "</span>" );
      }
      
      fip.close();
      html += "<div style='display:none'><div id='validate_content' style='tetx-align:left; padding:10px; background:#fff;'></div></div>";
      return html;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- loadExtra ...                                                     ---*/
   /*--- loads excel lookup data in 4 level hash                           ---*/
   /*--- filename{tabname{state{field=value,...},...},...},...}            ---*/
   /*-------------------------------------------------------------------------*/
   public static ConcurrentHashMap loadExtra( String path )
   {
      ConcurrentHashMap hmExtra =  (ConcurrentHashMap) cacheLookup.get( path );
      if( hmExtra==null )
      {
         Log.log( Log.INFO, "load " + path );
         hmExtra =  new ConcurrentHashMap();
         String  ls[]    =  (new File(path)).list();
         for( int i=0; i<ls.length; i++ )
         {
            try
            {
               if( !ls[i].endsWith(".xls") )
                  continue;
               HashMap           hmSheets    =  new HashMap();
               FileInputStream   fip         =  new FileInputStream( path + File.separator + ls[i] );
               WorkbookSettings  ws          = new WorkbookSettings();
               ws.setEncoding("ISO-8859-1");
               Workbook          workbook    =  Workbook.getWorkbook( fip, ws );
               Sheet             sheets[]    =  workbook.getSheets();
               for( int n=0; n<sheets.length; n++ )
               {
                  String   sheetName = sheets[n].getName();
                  
                  //---- load first row from tab for key indexing
                  Vector keys       = new Vector();
                  keys.addElement( "" );     // first cell is obviously the state
                  Cell[] cellsKeys  = sheets[n].getRow(0);
                  for( int c=1; c<cellsKeys.length; c++ )
                     keys.addElement( cellsKeys[c].getContents() );
                  
                  HashMap  hmState = new HashMap();
                  for( int j=1; j<sheets[n].getRows(); j++ )
                  {
                     Cell[]   cells       = sheets[n].getRow(j);
                     String   stateCode   = cells[0].getContents().toUpperCase();
                     HashMap  hmData      =  new HashMap();
                     for( int c=1; c<cells.length; c++ )
                     {
                        hmData.put( keys.elementAt(c), cells[c].getContents() );
                     }
                     hmState.put( stateCode, hmData );
                  }
                  hmSheets.put( sheetName, hmState );
               }
               fip.close();
               hmExtra.put( ls[i].replace(".xls", ""), hmSheets );
            }
            catch( Exception e )
            {
               Log.log( e );
            }
         }
         cacheLookup.put(  path, hmExtra);
         Log.log( Log.INFO, "loaded " + hmExtra );
      }
      return hmExtra;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- loadSelectSheet ...                                               ---*/
   /*--- used for select & radio fields                                    ---*/
   /*-------------------------------------------------------------------------*/
   private static Sheet loadSelectSheet( String baseFolder, String folder, String sheetName )
   {
      Log.log( Log.INFO, "loadSelectSheet : \"" + baseFolder + "\", \"" + folder + "\", \"" +  sheetName + "\"" );
      Sheet sh =  null;
      try
      {
         String     splt[] =  sheetName.split( "\\." );
         String   fileName =  splt[0];
         String  excelPath =  baseFolder + "/extra/forms/include/" + fileName + ".xls";
         Log.log( Log.INFO, "loadSelectSheet->FileName : \"" + excelPath + "\"" );
         String    tabName    =  splt[1];
         FileInputStream   fip      =  new FileInputStream( excelPath );
         WorkbookSettings  ws       = new WorkbookSettings();
         ws.setEncoding("ISO-8859-1");
         Workbook          workbook =  Workbook.getWorkbook( fip, ws );
         Sheet             sheets[] =  workbook.getSheets();
         for( int i=0; i<sheets.length; i++ )
         {
            if( sheets[i].getName().equalsIgnoreCase(splt[1]) )
            {
               sh = sheets[i];
               break;
            }
         }
         fip.close();
      }
      catch( Exception e )
      {
         Log.log( Log.ERROR, "-----------------------" );
         Log.log( Log.ERROR, "--- loadSelectSheet ---" );
         Log.log( Log.ERROR, "-----------------------" );
         Log.log(e);
      }
      return sh;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- dateObject ...                                                    ---*/
   /*-------------------------------------------------------------------------*/
   private static String dateObject( String fieldName, String type, String req, boolean past, String pStartYear, int nYears )
   {
      return dateObject( fieldName, type, req, past, pStartYear, nYears, null, null );
   }
   private static String dateObject( String fieldName, String type, String req, boolean past, String pStartYear, int nYears, String countryCode, String lang )
   {
      int    EN = 0;
      int    PT = 1;
      String MONTHS[][] = {
                           { "", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" },
                           { "", "Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez" }
                          };
      
      HashMap hLang        =   new HashMap();
      hLang.put( "ENday",   "day"   );
      hLang.put( "ENmonth", "month" );
      hLang.put( "ENyear",  "year"  );
      
      hLang.put( "PTday",   "dia"   );
      hLang.put( "PTmonth", "mes"   );
      hLang.put( "PTyear",  "ano"   );
      
      hLang.put( "ESday",   "dia"   );
      hLang.put( "ESmonth", "mes"   );
      hLang.put( "ESyear",  "ano"   );
      
      String _countryCode =   "US";
      String _lang        =   "EN";
      
      if( countryCode!=null && !countryCode.equals("") ) _countryCode   =  countryCode;
      if( lang!=null        && !lang.equals("")        )        _lang   =  lang;
      
      int curLang = 0;
      if( _lang.equals("EN") ) curLang = 0;
      if( _lang.equals("PT") ) curLang = 1;
      
      String code = "";
      if( _countryCode.equals("US") || _countryCode.equals("CA") )
      {
         code += "<select onChange=\"setDate(this);\" id=\"_mm_" + fieldName + "\">";
         code += "<option  style=\"color:gray\" value=\"\">"+hLang.get(_lang+"month")+"</option>";
         for( int i=1; i<=12; i++ )
            code += "<option value=\"" + i + "\">" + MONTHS[curLang][i] + "</option>";
         code += "</select>";
         
         code += "/";
         
         code += "<select onChange=\"setDate(this);\" id=\"_dd_" + fieldName + "\">";
         code += "<option style=\"color:gray\" value=\"\">"+hLang.get(_lang+"day")+"</option>";
         for( int i=1; i<=31; i++ )
            code += "<option value=\"" + i + "\">" + (i<10?"0":"") + i + "</option>";
         code += "</select>";
         
         code += "/";
      }
      else
      {
         code += "<select onChange=\"setDate(this);\" id=\"_dd_" + fieldName + "\">";
         code += "<option style=\"color:gray\" value=\"\">"+hLang.get(_lang+"day")+"</option>";
         for( int i=1; i<=31; i++ )
            code += "<option value=\"" + i + "\">" + (i<10?"0":"") + i + "</option>";
         code += "</select>";
         
         code += "/";
         
         code += "<select onChange=\"setDate(this);\" id=\"_mm_" + fieldName + "\">";
         code += "<option  style=\"color:gray\" value=\"\">"+hLang.get(_lang+"month")+"</option>";
         for( int i=1; i<=12; i++ )
            code += "<option value=\"" + i + "\">" + MONTHS[curLang][i] + "</option>";

         code += "</select>";
         
         code += "/";
      }
      
      code += "<select onChange=\"setDate(this);\" id=\"_yy_" + fieldName + "\">";
      code += "<option  style=\"color:gray\" value=\"\">"+hLang.get(_lang+"year")+"</option>";
      //--- might reconsider for DATE if it starts from past or not
      if( type.equals("DOB") || type.equals("DATE") )
      {
         int  endYear   = 0;
         int  startYear = 0;
         if( past )
         {
            endYear   = Format.currentYear() + 1;
            startYear = endYear - 100;
            //if( type.equals("DOB") )
            //   endYear = endYear - 1;
            try
            {
               startYear = Integer.parseInt(pStartYear);
            }
            catch( Exception e )
            {
            }
         }
         else
         {
            startYear = Format.currentYear();
            endYear   = startYear + 20;
         }
         int cntYears = 0;         
         for( int i=startYear; i<endYear; i++ )
         {
            code += "<option value=\"" + i + "\">" + (i<10?"0":"") + i + "</option>";
            cntYears++;
            if( nYears==cntYears && nYears!=0 )
               break;
         }
      }
      code += "</select>";
      code += "<input field=\"" + type + "\" req=\"" + req + "\" type=\"hidden\" name=\"" + fieldName + "\" id=\"" + fieldName + "\" value=\"\">";
      
      return code;
   }
   
}




