package com.util;

import java.util.HashMap;
import java.util.Properties;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class SendGridEmailer
{
   private static String SMTP_HOST_NAME = "smtp.sendgrid.com";//"smtp.sendgrid.net";
   private String SMTP_AUTH_USER = "drivers@321";
   private String SMTP_AUTH_PWD = "dmv@321";
   private String PORT = "587";
   private String textFormat = "";
   private static HashMap CREDENTIAL= new HashMap();
   static
   {
      CREDENTIAL.put ("drivers-licenses.org",    "drivers@321|dmv@321");
      CREDENTIAL.put ("dmvdriverslicenses.org",  "dmvdrivers@321|dmv@@321");
      CREDENTIAL.put ("mydriverslicense.org",    "drivers@321|dmv@321");
      CREDENTIAL.put ("absenteeballots.com",     "absent@ballot|ballat@321");
      CREDENTIAL.put ("socialsecuritycards.org", "social@321|s0ci@l");
      CREDENTIAL.put ("registertovote.org",      "register@321|r3g1s@321");
      CREDENTIAL.put ("car-registration.org",    "carreg@321|c@rr3g@321");
      CREDENTIAL.put ("car-title.org",           "cartitle@321|c@rt1tl3@321");
   }

   public SendGridEmailer()
   {
   }
   public void setCredential( String domain )
   {

      if(CREDENTIAL.containsKey (domain) )
      {
         String cred[]=CREDENTIAL.get(domain).toString ().split ("[|]");
         SMTP_AUTH_USER=cred[0];
         SMTP_AUTH_PWD= cred[1];
      }

   }
   public void setUserName( String username )
   {
      this.SMTP_AUTH_USER = username;
   }
   public void setPassWord( String password )
   {
      this.SMTP_AUTH_PWD = password;
   }
   private Properties initializeProp()
   {
      Properties props = new Properties ();
      props.put ("mail.transport.protocol", "smtp");
      props.put ("mail.smtp.host", SMTP_HOST_NAME);
      props.put ("mail.smtp.port", PORT);
      props.put ("mail.smtp.auth", "true");
      return props;
   }
   public void SendMail( String from, String to, String subject, String template )
   {

      try
      {
         Authenticator auth = new SMTPAuthenticator ();
         Session mailSession = Session.getDefaultInstance (initializeProp (), auth);
         // uncomment for debugging infos to stdout
         // mailSession.setDebug(true);
         Transport transport = mailSession.getTransport ("smtp");
         MimeMessage message = new MimeMessage (mailSession);
         Multipart multipart = new MimeMultipart ("alternative");
         BodyPart part1 = new MimeBodyPart ();
         part1.setText (textFormat);
         BodyPart part2 = new MimeBodyPart ();
         part2.setContent (template, "text/html");
         multipart.addBodyPart (part1);
         multipart.addBodyPart (part2);
         message.addHeader ("X-SMTPAPI", "{\"filters\":{\"clicktrack\":{\"settings\":{\"enable\":1}}}}");
         message.setContent (multipart);
         message.setFrom (new InternetAddress (from));
         message.setSubject (subject);
         message.addRecipient (Message.RecipientType.TO, new InternetAddress (to));
         transport.connect ();
         transport.sendMessage (message, message.getRecipients (Message.RecipientType.TO));
         transport.close ();

      }
      catch( MessagingException ex )
      {
         ex.printStackTrace ();

      }
      
   }
   class SMTPAuthenticator extends Authenticator
   {
      public PasswordAuthentication getPasswordAuthentication()
      {
         return new PasswordAuthentication (SMTP_AUTH_USER, SMTP_AUTH_PWD);
      }
   }
}

