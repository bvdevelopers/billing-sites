package com.util;

import java.util.*;
import java.util.concurrent.*;

public class Static
{
   public  static String            MONTHS[]              = { "", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
   public  static String            SHORT_MONTHS[]        = { "", "Jan",     "Feb",      "Mar",   "Apr",   "May", "Jun",  "Jul",  "Aug",    "Sep",       "Oct",     "Nov",      "Dec"      };
   public  static ConcurrentHashMap STATE_LOOKUP          = new ConcurrentHashMap();
   public  static ConcurrentHashMap STATE_REVERSE_LOOKUP  = new ConcurrentHashMap();
   public  static ConcurrentHashMap STATES                = new ConcurrentHashMap();
   public  static ConcurrentHashMap CONTENT_TYPES         = new ConcurrentHashMap();
   
   public  static String            STATE_CODES[]         = { "AL", "AK", "AZ", "AR", "CA", "CO", "CT", "DE", "DC", "FL", "GA", "HI", "ID", "IL", "IN", "IA", "KS", "KY", "LA", "ME", "MD", "MA", "MI", "MN", "MS", "MO", "MT", "NE", "NV", "NH", "NJ", "NM", "NY", "NC", "ND", "OH", "OK", "OR", "PA", "PR", "RI", "SC", "SD", "TN", "TX", "UT", "VA", "VT", "WA", "WV", "WI", "WY" };
   public  static String            STATE_NAMES[]         = { "Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "District of Columbia", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Puerto Rico", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Virginia", "Vermont", "Washington", "West Virginia", "Wisconsin", "Wyoming" };
   
   public  static String            CA_STATE_CODES[]      = { "AB", "BC", "MB", "NB", "NL", "NS", "NT", "NU", "ON", "PE", "QC", "SK", "YT" };
   public  static String            CA_STATE_NAMES[]      = { "Alberta", "British Columbia", "Manitoba", "New Brunswick", "Newfoundland", "Nova Scotia", "Northwest Territories", "Nunavut", "Ontario", "Prince Edward Island", "Quebec", "Saskatchewan", "Yukon" };
   
   public  static String            BR_STATE_CODES[]      = { "AC", "AL", "AP", "AM", "BA", "CE", "GO", "ES", "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", "RJ", "RN", "RS", "RO", "RR", "SP", "SC", "SE", "TO", "DF" };
   public  static String            BR_STATE_NAMES[]      = { "Acre", "Alagoas", "Amapa", "Amazonas", "Bahia", "Ceara", "Goias", "Espirito Santo", "Maranhao", "Mato Grosso", "Mato Grosso do Sul", "Minas Gerais", "Para", "Paraiba", "Parana", "Pernambuco", "Piaui", "Rio de Janeiro", "Rio Grande do Norte", "Rio Grande do Sul", "Rondonia", "Roraima", "Sao Paulo", "Santa Catarina", "Sergipe", "Tocantins", "Distrito Federal" };
   
   static
   {

      for( int i=0; i<BR_STATE_CODES.length; i++ )
      {
         String stateName = BR_STATE_NAMES[i].toLowerCase();
         String stateCode = BR_STATE_CODES[i].toLowerCase();
         STATE_LOOKUP.put( stateName.replace(" ", ""), stateName.replace(" ", "-") );
         STATE_LOOKUP.put(               stateCode,                   stateName.replace(" ", "-") );
         STATE_LOOKUP.put( stateCode.toUpperCase(),                   stateName.replace(" ", "-") );
         STATE_REVERSE_LOOKUP.put(  stateName.replace(" ", "-"), stateCode                   );

         STATES.put(  stateName.replace(" ", "-"), BR_STATE_CODES[i] );
         STATES.put(  stateCode,                   BR_STATE_CODES[i] );
      }
      for( int i=0; i<CA_STATE_CODES.length; i++ )
      {
         String stateName = CA_STATE_NAMES[i].toLowerCase();
         String stateCode = CA_STATE_CODES[i].toLowerCase();
         STATE_LOOKUP.put( stateName.replace(" ", ""), stateName.replace(" ", "-") );
         STATE_LOOKUP.put(               stateCode,                   stateName.replace(" ", "-") );
         STATE_LOOKUP.put( stateCode.toUpperCase(),                   stateName.replace(" ", "-") );
         STATE_REVERSE_LOOKUP.put(  stateName.replace(" ", "-"), stateCode                   );

         STATES.put(  stateName.replace(" ", "-"), CA_STATE_CODES[i] );
         STATES.put(  stateCode,                   CA_STATE_CODES[i] );
      }
      for( int i=0; i<STATE_CODES.length; i++ )
      {
         String stateName = STATE_NAMES[i].toLowerCase();
         String stateCode = STATE_CODES[i].toLowerCase();
         STATE_LOOKUP.put( stateName.replace(" ", ""), stateName.replace(" ", "-") );
         STATE_LOOKUP.put(               stateCode,                   stateName.replace(" ", "-") );
         STATE_LOOKUP.put( stateCode.toUpperCase(),                   stateName.replace(" ", "-") );
         STATE_REVERSE_LOOKUP.put(  stateName.replace(" ", "-"), stateCode                   );
         
         STATES.put(  stateName.replace(" ", "-"), STATE_CODES[i] );
         STATES.put(  stateCode,                   STATE_CODES[i] );
      }
      
      
      
      
      CONTENT_TYPES.put( "txt",  "text/plain"        );
      CONTENT_TYPES.put( "htm",  "text/html"         );
      CONTENT_TYPES.put( "xml",  "text/xml"          );
      CONTENT_TYPES.put( "html", "text/html"         );
      CONTENT_TYPES.put( "pdf",  "application/pdf"   );
      CONTENT_TYPES.put( "jpg",  "image/jpg"                     );
      CONTENT_TYPES.put( "gif",  "image/gif"                     );
      CONTENT_TYPES.put( "png",  "image/png"                     );
      CONTENT_TYPES.put( "flv",  "video/x-flv"                   );
      CONTENT_TYPES.put( "swf",  "application/x-shockwave-flash" );
      CONTENT_TYPES.put( "ico",  "image/x-icon"                  );
      
      CONTENT_TYPES.put(" eot",   "application/vnd.ms-fontobject" );
      CONTENT_TYPES.put(" otf",   "application/font-sfnt"         );
      CONTENT_TYPES.put(" ttf",   "application/font-sfnt"         );
      CONTENT_TYPES.put(" woff",  "application/font-woff"         );
   }




}

