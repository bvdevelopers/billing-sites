package com.util;

import java.net.*; 
import java.util.concurrent.*;

public class Cypher
{
   private static String            NUM      = "0123456789_";
   private static String            MAP      = "PLMHTRDXZAW";
   private static ConcurrentHashMap MAPS     = new ConcurrentHashMap();
   private static ConcurrentHashMap REVERSE  = new ConcurrentHashMap();
   
   static
   {
      for( int i=0; i<NUM.length(); i++ )
      {
         MAPS.put(      (""+NUM.charAt(i)), (""+MAP.charAt(i)) );
         REVERSE.put(   (""+MAP.charAt(i)), (""+NUM.charAt(i)) );
      }
   }
   
   public static String encode( String text )
   {
      StringBuffer sb = new StringBuffer( "" );
      for( int i=0; i<text.length(); i++ )
      {
         String c = ""+text.charAt(i);
         String m = (String) MAPS.get(c);
         if( m!=null )
            sb.append( m );
      }
      return sb.toString();
   }
   
   public static String decode( String text )
   {
      StringBuffer sb = new StringBuffer( "" );
      for( int i=0; i<text.length(); i++ )
      {
         String c = ""+text.charAt(i);
         String m = (String) REVERSE.get(c);
         if( m!=null )
            sb.append( m );
      }
      return sb.toString();
   }
}

