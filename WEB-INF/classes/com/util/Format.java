package com.util;

import java.util.regex.*;
import java.util.Calendar;
import java.text.*;
import java.util.*;
import java.math.*;
import java.util.zip.*;

public class Format
{
   private static final char[] HEX_CHARS  = "0123456789abcdef".toCharArray();

   /*-------------------------------------------------------------------------*/
   /*--- leftPad ...                                                       ---*/
   /*-------------------------------------------------------------------------*/
   public static String leftPad( String txt, String padChar, int length )
   {
      String      trim = txt.trim();
      StringBuffer pad = new StringBuffer( "" );
      int            l = trim.length();
      if( l >= length )
         return trim;
      
      for( int i=l; i<length; i++ )
         pad.append( padChar );
      return (new StringBuffer(pad)).append(trim).toString();
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- lastToken ...                                                     ---*/
   /*-------------------------------------------------------------------------*/
   public static String lastToken( String txt, String token )
   {
      String nToken = token;
      if( nToken.equals("\\") )
         nToken = "\\\\";
      try
      {
         String tmp[] = txt.split( nToken );
         return tmp[tmp.length-1];
      }
      catch( Exception e )
      {
         return txt;
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- extractLastToken ...                                              ---*/
   /*-------------------------------------------------------------------------*/
   public static String extractLastToken( String txt, String token )
   {
      try
      {
         return (new StringBuffer( txt.substring( 0, txt.lastIndexOf(token) ) )).append(token).toString();
      }
      catch( Exception e )
      {
         return txt;
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- replaceToken ...                                                  ---*/
   /*--- if ndx is negative means minus lastToken                          ---*/
   /*--- e.g. : -1 means last token, -2 previous before last etc ...       ---*/
   /*-------------------------------------------------------------------------*/
   public static String replaceToken( String txt, String token, int ndx, String val )
   {
      int      pos     = ndx;
      String   split[] = txt.split( token );
      StringBuffer rtn = new StringBuffer();
      
      if( pos < 0 )
         pos = split.length + pos;
      
      for( int i=0; i<split.length; i++ )
      {
         if( i>0 )
            rtn.append(token);
         if( i==pos )
            rtn.append(val);
         else
            rtn.append(split[i]);
      }
      return rtn.toString();
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- currentYear ...                                                   ---*/
   /*-------------------------------------------------------------------------*/
   public static int currentYear()
   {
      return Calendar.getInstance().get(Calendar.YEAR);
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- today ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public static String today( String theFormat )
   {
      SimpleDateFormat fmt = new SimpleDateFormat(theFormat);
      return fmt.format( new Date() );
   }
   
   public static String today()
   {
      return today( "MM/dd/yyyy" );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- age ...                                                           ---*/
   /*-------------------------------------------------------------------------*/
   public static int age( String dt )
   {
      try
      {
         int bY = Integer.parseInt( dt.split("/")[2] );
         int tY = currentYear();
         return tY - bY;
      }
      catch( Exception e )
      {
         return 0;
      }
   }
   
   
   /*-------------------------------------------------------------------------*/
   /*--- todayISO ...                                                      ---*/
   /*-------------------------------------------------------------------------*/
   public static String todayISO()
   {
      return today( "yyyy-MM-dd@hh-mm-ss" );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- dateISO ...                                                       ---*/
   /*--- convert mm/dd/yyyy to yyyy-MM-dd                                  ---*/
   /*-------------------------------------------------------------------------*/
   public static String dateISO( String dt )
   {
      String tmp[] = dt.split( "/" );
      StringBuffer sb = new StringBuffer();
      sb.append(tmp[2]).append("-");
      if( tmp[0].length()==1 ) sb.append( "0" );
      sb.append(tmp[0]).append("-");
      if( tmp[1].length()==1 ) sb.append( "0" );
      sb.append(tmp[1]);
      return sb.toString();
   // return (new StringBuffer("")).append(tmp[2]).append("-").append(tmp[0]).append("-").append(tmp[1]).toString();
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- reverseDateISO ...                                                ---*/
   /*--- convert yyyy-MM-dd to mm/dd/yyyy                                  ---*/
   /*-------------------------------------------------------------------------*/
   public static String reverseDateISO( String dt )
   {
      String tmp[] = dt.split( "-" );
      return (new StringBuffer("")).append(tmp[1]).append("-").append(tmp[2]).append("-").append(tmp[0]).toString();
   }


   /*-------------------------------------------------------------------------*/
   /*--- dateAddISO ...                                                    ---*/
   /*--- add Days to a date (ISO format                                    ---*/
   /*-------------------------------------------------------------------------*/
   public static String dateAddISO( String dt, int nDays )
   {
      try
      {
         SimpleDateFormat  sdf   = new SimpleDateFormat( "yyyy-MM-dd" );
         Calendar          c     = Calendar.getInstance();
         c.setTime( sdf.parse(dt) );
         c.add( Calendar.DATE, nDays ); 
         return sdf.format(c.getTime());
      }
      catch( Exception e )
      {
         return dt;
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- now ...                                                           ---*/
   /*-------------------------------------------------------------------------*/
   public static String now()
   {
      return today( "hh:mm:ss" );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- currentHour ...                                                   ---*/
   /*-------------------------------------------------------------------------*/
   public static int currentHour()
   {
      SimpleDateFormat fmt = new SimpleDateFormat("kk");
      int              h   = 0;
      try  { h = Integer.parseInt( fmt.format( new Date() ) ); } catch( Exception e ) {}
      h = (h==24)?0:h;
      return h;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- dbSafe ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   public static String dbSafe( String v )
   {
      return v.replace( "\"", "\\\"" );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- dbQuote ...                                                       ---*/
   /*-------------------------------------------------------------------------*/
   public static String dbQuote( String v )
   {
      return (new StringBuffer("")).append("\"").append(v.replace( "\"", "\\\"" )).append("\"").toString();
   }

   /*-------------------------------------------------------------------------*/
   /*--- dbMySQLDate ...                                                   ---*/
   /*-------------------------------------------------------------------------*/
   public static String dbMySQLDate( String v )
   {
      try
      {
         return (new StringBuffer( "date(\"" )).append(dateISO(v)).append("\")").toString();
      }
      catch( Exception e )
      {
         return "date(null)";
      }
   }

   /*-------------------------------------------------------------------------*/
   /*--- HashMapGet ...                                                    ---*/
   /*-------------------------------------------------------------------------*/
   public static String HashMapGet( HashMap hs, String k, String defaultVal )
   {
      try
      {
         String v = (String) hs.get(k);
         v = v==null?defaultVal:v;
         return v.trim();
      }
      catch( Exception e )
      {
         return defaultVal.trim();
      }
   }
   public static String HashMapGet( HashMap hs, String k )
   {
      return HashMapGet( hs, k, "" );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- extractMarks ...                                                  ---*/
   /*-------------------------------------------------------------------------*/
   public static Vector extractMarks( String cnt, String start, String end )
   {
      ArrayList      tokens = new ArrayList();
      StringTokenizer   stk = new StringTokenizer( cnt, (new StringBuffer(start)).append(end).toString(), true );
      Vector              v = new Vector();
      while( stk.hasMoreTokens() )
         tokens.add( stk.nextToken() );
      for( int i=0; i<tokens.size(); i++ )
      {
         String prev = "";
         String valu = "";
         String next = "";
         
         try { prev = (String) tokens.get( i-1 ); } catch( Exception e ) {}
         try { valu = (String) tokens.get( i   ); } catch( Exception e ) {}
         try { next = (String) tokens.get( i+1 ); } catch( Exception e ) {}
         
         if( prev.equals(start) && next.equals(end) )
            v.addElement( valu );
      }
      return v;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- initCap ...                                                       ---*/
   /*-------------------------------------------------------------------------*/
   public static String initCap( String word )
   {
      try
      {
         String     tmp[]   = word.split( "[\\ \\_-]" );
         StringBuffer txt   = new StringBuffer();
         for( int i=0; i<tmp.length; i++ )
         {
            StringBuffer sb = new StringBuffer( tmp[i] );
            sb.setCharAt( 0, Character.toUpperCase(sb.charAt(0)) );
            txt.append( sb );
            if( i<tmp.length-1 )
               txt.append( " " );
         }
         return txt.toString();
      }
      catch( Exception e )
      {
         return word;
      }
   }

   /*-------------------------------------------------------------------------*/
   /*--- hex2String ...                                                    ---*/
   /*-------------------------------------------------------------------------*/
   public static String hex2String( String s )
   {
      String       newS = s;
      StringBuffer r    = new StringBuffer();
      while( newS.length()>0 )
      {
         int i = Integer.parseInt( newS.substring(0,2), 16 );
         r.append( ((char)i) );
         newS = newS.substring(2);
      }
      return r.toString();
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- string2Hex ...                                                    ---*/
   /*-------------------------------------------------------------------------*/
   public static String string2Hex( String s )
   {
      byte[] buf        =  s.getBytes();
      char[] chars      = new char[2 * buf.length];
      for (int i = 0; i < buf.length; ++i)
      {
         chars[2 * i]      = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
         chars[2 * i + 1]  = HEX_CHARS[buf[i] & 0x0F];
      }
      return new String(chars);
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- noSmartQuot ...                                                   ---*/
   /*-------------------------------------------------------------------------*/
   public static String noSmartQuot( String s )
   {
      /*
      String newS = s;
      
      for( int i=0; i<bad.length; i++ )
         newS = newS.replace( bad[i], good[i] );
      
      return newS;
      */
      return s;
   }

   /*-------------------------------------------------------------------------*/
   /*--- format ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   public static String format( double d, int nd )
   {
      if( (new Double(d)).isNaN() )
         return "";
      StringBuffer fm = new StringBuffer( "###,###,###,##0" );
      if( nd > 0 )
         fm.append( "." );
      for( int i=0; i<nd; i++ )
         fm.append( "0" );
      NumberFormat formatter = new DecimalFormat( fm.toString() );
      return formatter.format(d);
   }
   public static String format( double d )
   {
      return format( d, 2 );
   }
   public static String format( String d )
   {
      try
      {
         return format( Double.parseDouble(d) );
      }
      catch( Exception e )
      {
         return d;
      }
   }

   public static String purifyText( String txt )
   {
      /*
      StringBuffer cnt = new StringBuffer();
      for( int i=0; i<txt.length(); i++ )
      {
         char n = txt.charAt(i);
         if( n==13 || (n>=32 && n<=126) )
         {
            cnt.append( n );
         }
      }
      return cnt.toString();
      */
      return txt;
   }

   public static String getStateName( String stateCode )
   {
      if( stateCode==null )
         return "";
      String stateName = (String) Static.STATE_LOOKUP.get( stateCode );
      return (stateName==null?"":stateName);
   }
   public static String getStateCode( String stateName )
   {
      if( stateName==null )
         return "";
      String stateCode = (String) Static.STATE_REVERSE_LOOKUP.get( stateName );
      return (stateCode==null?"":stateCode);
   }


   /*-------------------------------------------------------------------------*/
   /*--- jsHash ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   public static String jsHash( HashMap hm, String arrName )
   {
      StringBuffer   js = new StringBuffer();
      js.append( "var " ) .append( arrName ).append( "=new Array();" );
      try
      {
         Iterator it = hm.keySet().iterator();
         while( it.hasNext() )
         {
            String k = (String) it.next();
            String v = (String) hm.get( k );
            js.append( arrName ).append( "[\"" ).append(k).append("\"]=\"").append(v).append("\";");
         }
      }
      catch( Exception e )
      {
      }
      return js.toString();
   }

   /*-------------------------------------------------------------------------*/
   /*--- uri2Hash ...                                                      ---*/
   /*-------------------------------------------------------------------------*/
   public static HashMap uri2Hash( String data )
   {
      String  arr[] = data.split( "&" );
      HashMap hm    = new HashMap();
      for( int i=0; i<arr.length; i++ )
      {
         String rec[] = arr[i].split("=");
         if( rec.length>=2 )
            hm.put( rec[0], rec[1] );
      }
      return hm;
   }

   
   /*-------------------------------------------------------------------------*/
   /*--- tsv2Hash ...                                                      ---*/
   /*-------------------------------------------------------------------------*/
   public static HashMap tsv2Hash( String data )
   {
      String  arr[] = data.split( "\n" );
      HashMap hm    = new HashMap();
      for( int i=0; i<arr.length; i++ )
      {
         String rec[] = arr[i].replace("\r", "").split("\t");
         if( rec.length>=2 )
            hm.put( rec[0], rec[1] );
      }
      return hm;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- Hash2TSV ...                                                      ---*/
   /*-------------------------------------------------------------------------*/
   public static String Hash2TSV( HashMap hm )
   {
      Iterator       it = hm.keySet().iterator();
      StringBuffer   sb = new StringBuffer();
      while( it.hasNext() )
      {
         String k = (String) it.next();
         String v = (String) hm.get( k );
         sb.append("\n").append(k).append("\t").append(v);
      }
      String r = sb.toString();
      try { r = r.substring(1); } catch( Exception e ) {}
      return r;
   }


   public static String get()
   {
      return "got it";
   }



   /*-------------------------------------------------------------------------*/
   /*--- createPassword ...                                                ---*/
   /*-------------------------------------------------------------------------*/
   public static String createPassword()
   {
      String rand = "" + System.currentTimeMillis();
             rand = rand.substring( rand.length() - 4 );
      int  number = (int)System.currentTimeMillis()%50;
           number = (number < 0 ? -number : number);
      return com.util.Static.STATE_NAMES[number].replace(" ","").toLowerCase() + rand;
   }
}
