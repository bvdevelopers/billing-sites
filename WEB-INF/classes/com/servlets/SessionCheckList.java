package com.servlets;

import com.util.*;
import com.bl.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class SessionCheckList extends HttpServlet
{
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   {
      response.setContentType( "text/html" );
      try
      {
         String          domain  =  Web.getDomainName( request );
         StringBuffer    link    =  new StringBuffer( "/content/" );
         link.append( domain ).append( "/flow/checklist.jsp" );
         request.getRequestDispatcher( link.toString() ).include( request, response );
      }
      catch( Exception e )
      {
      }
   }
}

