package com.servlets;

import com.util.*;
import com.bl.*;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class PHP extends HttpServlet
{
   private static HashMap services = null;

   static
   {
      services = new HashMap();
      services.put( "new",          "new-drivers-license"      );
      services.put( "address",      "change-of-address"        );
      services.put( "renew",        "renew-drivers-license"    );
      services.put( "name",         "change-of-name"           );
      services.put( "replace",      "replace-drivers-license"  );
      services.put( "learners",     "learners-permit"          );
   }

   public void doGet( HttpServletRequest request, HttpServletResponse response )
   throws ServletException, IOException
   {
      String      qs     =  request.getQueryString();
      qs                 =  qs==null?"":("&"+qs);
      try
      {
         PrintWriter out    =  response.getWriter();
         String      uri    =  request.getRequestURI().substring(1).replace(".php","").toLowerCase();
         String      stName =  Format.getStateCode(uri);
         if( stName==null || stName.equals("null") )
         {
            String      s[]   =  uri.split( "-" );
            String  service   =  (String) services.get( s[0] );
            String    state   =  uri.substring( 1+s[0].length() );
            service           =  service==null?s[0]:service;
            String     form   =  ("/index.html?" + service + ":" + state + qs).replace("?&","?");
            Web.redirect( response, Web.MOVE_PERMANENT, form );
            out.print( form );
         }
         else
         {
            String     form   =  ("/index.html?:" + uri + qs).replace("?&","?");
            Web.redirect( response, Web.MOVE_PERMANENT, form );
         }
      }
      catch( Exception e )
      {
         String     form   =  ("/index.html" + (qs.equals("")?"":"?") + qs).replace("?&","?");
         Web.redirect( response, Web.MOVE_PERMANENT, form );
      }
   }
}

//http://dev.dmvdriverslicenses.org/new-florida.php?utm_source=FL_Drivers&utm_medium=Banner&utm_campaign=FL_Site
//http://dev.dmvdriverslicenses.org/florida.php
