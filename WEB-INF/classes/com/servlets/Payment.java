package com.servlets;

import java.io.*;
import java.util.*;
import com.util.*;
import javax.servlet.http.*;
import javax.servlet.*;

public class Payment
{
   public  static final String PAYMENT_STRUCT[] = {   "OrderNumber",
                                                      "Status",
                                                      "Amount",
                                                      "Product",
                                                      "ItemState",
                                                      "Card",
                                                      "FirstName" };

   /*-------------------------------------------------------------------------*/
   /*--- getSessionPayment ...                                             ---*/
   /*-------------------------------------------------------------------------*/
   public static HashMap getSessionPayment( HttpServletRequest request )
   {
      return (HashMap) request.getSession().getAttribute( "ccpay" );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- jsHash ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   public static String jsHash( HttpServletRequest request )
   {
      HashMap paymentSession = getSessionPayment( request );
      if( paymentSession==null )
         return "";
      
      StringBuffer    jsCode = new StringBuffer();
      for( int i=0; i<PAYMENT_STRUCT.length; i++ )
      {
         try
         {
            String v = (String)  paymentSession.get( PAYMENT_STRUCT[i] );
            v = v==null?"":v;
            jsCode.append("$('.").append("payment_").append(PAYMENT_STRUCT[i]).append("').html(\"").append(v).append("\"); " );
            jsCode.append("_PAYMENT[\"").append(PAYMENT_STRUCT[i]).append("\"]=\"").append(v).append("\"; ");
         }
         catch( Exception e )
         {
         }
      }
      return jsCode.toString();
   }
}

