package com.servlets;

import java.io.*;
import java.util.*;
import com.util.*;
import com.bl.*;
import javax.servlet.http.*;
import javax.servlet.*;

public class PostPoll extends HttpServlet
{
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   throws ServletException, IOException
   {
      try
      {
         String         domain   =  Web.getDomainName( request );
         String         id       =  request.getParameter( "id"     );
         String         instr    =  request.getParameter( "instr"  );
         StringBuffer   sb       =  new StringBuffer();
         sb.append("/content/").append(domain).append("/flow/postPoll.jsp?id=").append(id).append("&instr=").append(instr);
         request.getRequestDispatcher( sb.toString() ).forward( request, response );
      }
      catch( Exception e )
      {
         Log.log( e );
      }
   }
}
