package com.servlets;

import com.util.*;
import com.bl.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class StaticDownload extends HttpServlet
{
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   {
      try
      {
         String         doc      =  request.getParameter( "doc"      ).toLowerCase();
         String         state    =  request.getParameter( "state"    ).toLowerCase().replace(" ","-");
         String         doctype  =  request.getParameter( "doctype"  ).toLowerCase();
         StringBuffer   path     =  new StringBuffer();
         //state = (String) Static.STATE_LOOKUP.get(Static.STATES.get(state));
         path.append( "/downloads/" ).append( doc ).append( "/" ).append( state ).append( "." ).append( doctype );
         
         response.setContentType( (String) Static.CONTENT_TYPES.get(doctype) );
         request.getRequestDispatcher( path.toString() ).forward( request, response );
      }
      catch( Exception e )
      {
         Web.send404( response );
      }
   }
}
