package com.servlets;

import java.io.*;
import java.net.*;
import java.util.*;
import java.net.URLDecoder;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import org.jsoup.Jsoup;
import org.jsoup.nodes.*;
import org.jsoup.select.*;

public class PageProxy extends HttpServlet
{
    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String type = request.getParameter("type");
        type = (type==null)?"text/html":type;
        response.setContentType( type + "; charset=utf-8" );
        PrintWriter out = response.getWriter();
        try
        {
            String serviceUrl = URLDecoder.decode(request.getParameter("serviceUrl"), "UTF-8");
            if (!serviceUrl.contains("http"))
                serviceUrl = request.getScheme() + "://" + serviceUrl;
            URL url = new URL(serviceUrl);
            URLConnection connection = url.openConnection();
            BufferedInputStream buffer = new BufferedInputStream(connection.getInputStream());

            StringBuilder builder = new StringBuilder();
            int byteRead;
            while ((byteRead = buffer.read()) != -1)
                builder.append((char) byteRead);
            buffer.close();

            org.jsoup.nodes.Document document = Jsoup.parse(builder.toString());

            Elements elms  = null;
            Iterator it    = null;

            elms  = document.select("[href]");
            it    = elms.iterator();
            while( it.hasNext() )
            {
                Element element = (Element) it.next();
                if (!element.attr("href").contains("http"))
                  element.attr("href", serviceUrl + element.attr("href"));
            }
            
            elms  = document.select("[src]");
            it    = elms.iterator();
            while( it.hasNext() )
            {
                Element element = (Element) it.next();
                if (!element.attr("src").contains("http"))
                  element.attr("src", serviceUrl + element.attr("src"));
            }
            out.println(document.html());
        }
        catch (Exception ex)
        {
        }
        finally
        {
            out.close();
        }
    }
   
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }
}
