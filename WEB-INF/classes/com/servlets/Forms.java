package com.servlets;

import com.bl.*;
import com.util.*;
import java.util.*;
import java.util.concurrent.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Forms extends HttpServlet
{
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response ) throws IOException
   {
      String   domain     = Web.getDomainName(request);
      String   uri        = request.getRequestURI().toString().toLowerCase();
      String   splitURI[] = uri.split( "/" );
      String   service    = splitURI[splitURI.length-2];
      
      Traffic.trackSource( request );
      
      request.getSession().setAttribute( "service", service );
      request.getSession().setMaxInactiveInterval( 900 );
      
      response.setContentType( Web.getContentType(uri) );
      ContentInclude.noCache( response );
      
      String url        =  request.getRequestURL().toString().toLowerCase();
      String content    =  Cache.get( url );
      
      if( content==null )
      {
         //---- url validation, if state make sure it's properly formated, checks it must be secure, makes sure it's not www.
         if( Web.stateURL( request, response ) )
            return;
         if( !Web.noWWW( request, response ) )
            return;
         if( Web.ssl( request, response ) )
            return;
         //--- content is on form folder
         boolean  debug    =  request.getParameter( "DEBUG" )!=null;
         
         synchronized(this)
         {
            //--- in case content was loaded while this thread was waiting, no need to reload
            content = Cache.get( url );
            if( content==null )
            {
               //--- ----------------------------------------- ---
               //--- look for excel file, 4 levels of fallback ---
               //--- ----------------------------------------- ---
               String realExcelPath = "";
               String templatePath  = "";
               //--- level 1 : look for excel file with same name as url
               if( realExcelPath.length() == 0 )
               {
                  String excelFile = uri.replace( ".html", ".xls" );
                  Log.log( Log.INFO, "Try 1 : open " + excelFile );
                  realExcelPath = Web.getRealPath( request, "/content/" + domain + "/" + excelFile );
                  if( !(new File(realExcelPath)).exists() )
                     realExcelPath = "";
                  templatePath = excelFile;
               }
               //--- level 2 : look for excel file named #state#
               if( realExcelPath.length() == 0 )
               {
                  String excelFile = Format.replaceToken( uri, "/", -1, "#state#.xls" ).replace( ".html", ".xls" );
                  Log.log( Log.INFO, "Try 2 : open " + excelFile );
                  realExcelPath = Web.getRealPath( request, "/content/" + domain + "/" + excelFile );
                  if( !(new File(realExcelPath)).exists() )
                     realExcelPath = "";
                  templatePath = excelFile;
               }
               //--- level 3 : look for excel file with same name as url in __GENERIC__ folder
               if( realExcelPath.length() == 0 )
               {
                  String excelFile = Format.replaceToken( uri, "/", -2, "__GENERIC__" ).replace( ".html", ".xls" );
                  Log.log( Log.INFO, "Try 3 : open " + excelFile );
                  realExcelPath = Web.getRealPath( request, "/content/" + domain + "/" + excelFile );
                  if( !(new File(realExcelPath)).exists() )
                     realExcelPath = "";
                  templatePath = excelFile;
               }
               //--- level 4 : look for excel file named #state# in __GENERIC__ folder
               if( realExcelPath.length() == 0 )
               {
                  String excelFile = Format.replaceToken( uri, "/", -2, "__GENERIC__" );
                  if( splitURI.length==5 )
                     excelFile = Format.replaceToken( excelFile, "/", -1, "#state#.xls" );
                  else
                     excelFile = Format.replaceToken( excelFile, "/", -1, "#county#.xls" );
                  Log.log( Log.INFO, "Try 4 : open " + excelFile );
                  realExcelPath = Web.getRealPath( request, "/content/" + domain + "/" + excelFile );
                  if( !(new File(realExcelPath)).exists() )
                     realExcelPath = "";
                  templatePath = excelFile;
               }
               Log.log( Log.INFO, "Loading form " + realExcelPath );
               
               content = Cache.get( realExcelPath );
               if( content==null )
               {
                  try
                  {
                     String     dictionary   =  Web.getRealPath(    request, "/content/extra/dictionary.xls"                 );
                     String     baseFolder   =  Web.getRealPath(    request, "/content/"                                     );
                     String     formFolder   =  Web.getRealPath(    request, "/content/" + domain + "/forms"                 );
                     String       formCode   =  ExcelForms.xl2htm(  dictionary, baseFolder, formFolder, realExcelPath, debug );
                     StringBuffer formHTML   =  new StringBuffer();
                     String   templateFile   =  realExcelPath.replace("forms", "template/forms").replace(".xls", ".html");
                     boolean templateExist   =  (new File(templateFile)).exists();
                     if( !templateExist )
                     {
                        String tmp = Format.extractLastToken( request.getRequestURI(), "/" ) + "/#state#.html";
                        templateFile   =  Web.getRealPath( request, "/content/" + domain + "/template/" + tmp );
                        templateExist   =  (new File(templateFile)).exists();
                        if( templateExist )
                           templatePath   =  tmp;
                     }
                     templatePath = templatePath.replace( ".xls", ".html" );
                     if( !templateExist )
                        formHTML.append( "<!--$/forms/__GENERIC__.html$-->" );
                     else
                        formHTML.append( "<!--$" + templatePath + "$-->" );
                     formHTML.append( "<form name=\"frm\" id=\"frm\" target=\"buffer\" action=\"/postForm\" method=\"POST\">" );
                     formHTML.append( formCode );
                     formHTML.append( "<input type=\"hidden\" id=\"_FORM_\" name='_FORM_'>" );
                     formHTML.append( "<input type=\"hidden\" name='_CAR_DB_' value='").append(Queries.currentCarTable()).append("'>" );
                     formHTML.append( "</form>" );
                     if( !templateExist )
                        formHTML.append( "<!--/$/forms/__GENERIC__.html$-->" );
                     else
                        formHTML.append( "<!--/$" + templatePath + "$-->" );
                     
                     content = formHTML.toString();
                     content = Content.templify( request, content );
                     
                     boolean isSecure = request.isSecure(); 
		     //CHUCKY
                     content = Web.appendHTML( content, "head", ContentInclude.includesCoreJS(isSecure)    );
                     content = Web.appendHTML( content, "head", ContentInclude.includesJS(isSecure)        );



                     content = Web.appendHTML( content, "head", ContentInclude.includesCSS(isSecure)       );
                     content = Web.appendHTML( content, "body", "<script src=\"/form.js\"></script>"  );
                     content = Web.appendHTML( content, "body", "<iframe width='100%' height='200' style=\"display:" + (debug?"block":"none") + ";\" id=\"buffer\" name=\"buffer\"></iframe><p>"  );
                     content = Web.appendHTML( content, "body", "<img class=\"imgform\" src=\"/img/form.png\">"  );
                     
                     if( !request.getRequestURL().toString().contains("://dev.") )
                        Cache.set( realExcelPath,  content );
                  }
                  catch( Exception e )
                  {
                     Log.log( e );
                  }
               }
               if( !request.getRequestURL().toString().contains("://dev.") )
                  Cache.set( url, content );
            }
         }
      }
      PrintWriter  out  = response.getWriter();
      String   postURL  = request.getRequestURI().toString().replace( "/forms/", "/postForm/" ).replace( ".html", "" );
      if( content==null )
         Web.send404( response );
      else
         out.print( content.replace("/postForm", postURL) );
   }
}

