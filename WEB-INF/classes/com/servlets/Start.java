package com.servlets;

import java.io.*;
import com.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Start extends HttpServlet
{
   public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException
   {
      Web.redirect( response, Web.MOVE_PERMANENT, "/start.html?utm_source=Advisors&utm_medium=Direct&utm_campaign=Advisors" );
   }
   
   public void doPost( HttpServletRequest request, HttpServletResponse response )
   {
   }
}
