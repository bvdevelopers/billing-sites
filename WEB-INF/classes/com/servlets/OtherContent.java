package com.servlets;

import com.util.*;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class OtherContent extends HttpServlet
{
   public void doGet( HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException
   {
      String      uri   = request.getRequestURI().toString().toLowerCase().trim();
      String   domain   = Web.getDomainName( request );
      String fileName   = null;
      if( uri.endsWith(".pdf") )
      {
         fileName   = Web.getRealPath( request, "/content/" + domain + "/pdf" + uri );
      }
      else
      {
         fileName   = Web.getRealPath( request, "/content/" + domain + "/html" + uri );
      }
      byte        b[]   = Cache.getBinary( fileName );
      if( b==null )
      {
         try
         {
            b = IO.loadBinaryFile(fileName);
            Cache.setBinary( fileName, b );
         }
         catch( FileNotFoundException fnfe )
         {
            Web.send404( response );
         }
      }
      
      if( uri.endsWith(".pdf") )
      {
         String output = request.getParameter( "type" );
         output = output==null?"":output;
         if( !output.equals("open") )
            response.setHeader( "content-disposition","attachment; filename=" + uri.substring(1).replace("/", "_") );
      }

      response.setContentType( Web.getContentType(fileName) );
      if( b!=null )
      {
         response.getOutputStream().write( b );
      }
      else
      {
         try
         {
            Web.send404( response );
         }
         catch( IllegalStateException e )
         {
            Log.log( Log.ERROR, "IllegalStateException : " + domain + "/" + uri );
         }
      }
      response.getOutputStream().flush();
   }

   public void doPost( HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException
   {
      return;
   }

}

