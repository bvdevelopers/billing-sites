package com.servlets;

import java.awt.image.*;
import com.util.*;
import java.util.*;
import java.util.concurrent.*;

public class Cache
{
   private static ConcurrentHashMap cacheContentPage = new ConcurrentHashMap();
   
   public static String get( String ky )
   {
      return (String) cacheContentPage.get( ky );
   }
   
   public static void set( String ky, String val )
   {
      if( val!=null )
         cacheContentPage.put( ky, val );
   }
   
   public static Object getObject( String ky )
   {
      return cacheContentPage.get( ky );
   }
   
   public static void setObject( String ky, Object val )
   {
      cacheContentPage.put( ky, val );
   }
   
   public static byte[] getBinary( String ky )
   {
      return (byte[]) cacheContentPage.get( ky );
   }
   
   public static void setBinary( String ky, byte[] val )
   {
      try { cacheContentPage.put( ky, val ); } catch( Exception e ) {}
   }
   
   public static BufferedImage getBufferedImage( String ky )
   {
      return (BufferedImage) cacheContentPage.get( ky );
   }
   
   public static void setBufferedImage( String ky, BufferedImage val )
   {
      cacheContentPage.put( ky, val );
   }
   
   public static void clear( String ky )
   {
      cacheContentPage.remove( ky );
   }
   
   public static String toHTML()
   {
      Iterator it = cacheContentPage.keySet().iterator();
      String   r  = "";
      int      n  = 0;
      while( it.hasNext() )
      {
         String next = (String) it.next();
         r+= "<input id=\"c" + n + "\" type=\"checkbox\" name=\"" + next + "\">";
         r += "<label for=\"c" + n + "\">" + next + "</label>";
         r += "<br>";
         n++;
      }
      return r;
   }

   public static void reset( String domain )
   {
      Iterator it = null;
      Vector   v  = null;
      

      it = cacheContentPage.keySet().iterator();
      v  = new Vector();
      while( it.hasNext() )
      {
         String k = (String) it.next();
         try { if( k.contains(domain) ) v.addElement( it.next() ); } catch( Exception e ) {}
      }
      try { for( int i=0; i<v.size(); i++ ) cacheContentPage.remove( v.elementAt(i) ); } catch( Exception e ) {}


      it = IO.cacheASCIIFiles.keySet().iterator();
      v  = new Vector();
      while( it.hasNext() )
      {
         String k = (String) it.next();
         try { if( k.contains(domain) ) v.addElement( it.next() ); } catch( Exception e ) {}
      }
      try { for( int i=0; i<v.size(); i++ ) IO.cacheASCIIFiles.remove( v.elementAt(i) ); } catch( Exception e ) {}
   }

   public static String reset()
   {
      Iterator it = cacheContentPage.keySet().iterator();
      String  cnt = "";
      while( it.hasNext() )
         cnt += it.next() + "<hr>";
      cacheContentPage   = new ConcurrentHashMap();
      IO.cacheASCIIFiles = new ConcurrentHashMap();
      return cnt;
   }
}
