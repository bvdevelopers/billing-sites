package com.servlets;

import com.util.*;
import com.bl.*;
import java.util.*;
import jxl.*;
import java.util.concurrent.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.regex.*;

public class HasOffer extends HttpServlet
{
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response ) throws IOException
   {
      PrintWriter   out = response.getWriter();
      String      uri[] = request.getRequestURI().toString().replace( "/hasoffer/", "" ).split("/");
      String         qs = request.getQueryString().toString();
      String     leadId = uri[0];
      String       land = uri[1];
      String       link = "/splash/" + land + "?" + qs + "&utm_source=" + leadId;
      //out.print( link );
      Web.redirect( response, Web.MOVE_PERMANENT, link );
   }
}
