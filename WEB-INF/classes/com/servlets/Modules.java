package com.servlets;

import com.util.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Modules extends HttpServlet
{
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   {
      response.setContentType( "text/html" );
      String ur =  request.getRequestURL().toString();
      if( ur.startsWith("http://www.") )
      {
         ur = ur.replace( "http://www.", "http://" );
         Web.redirect( response, Web.MOVE_PERMANENT, ur );
      }
      
      String qs    =  request.getQueryString();
      String link =  ur.replace("/modules/", "/dynamic/") + ".jsp?" + qs;
            link  +=  "&__IP__=" + request.getRemoteAddr();
      try { link  +=  "&__UA__=" + URLEncoder.encode( ""+request.getHeader( "User-Agent" ), "UTF-8"); } catch( Exception e ) {}
      try
      {
         Log.log( Log.INFO, "redirect to " + link );
         String         cnt   =  new String( Web.webGet(link) );
         PrintWriter    out   =  response.getWriter();
         String    template   =  getTemplate( request, ur );
         if( template!=null )
         {
            cnt = template.replace( "<!--$content$-->", cnt );
            cnt = Content.templify( request, cnt );
            boolean isSecure = request.isSecure(); 
            cnt = Web.appendHTML( cnt, "body", ContentInclude.includesCoreJS(isSecure)    );
            cnt = Web.appendHTML( cnt, "body", ContentInclude.includesJS(isSecure)        );
            cnt = Web.appendHTML( cnt, "head", ContentInclude.includesCSS(isSecure)       );
            cnt = Web.appendHTML( cnt, "body", "<img class=\"imgmain\" src=\"/img/main.png\">"  );
         }
         out.print( cnt );
      }
      catch( FileNotFoundException fnf )
      {
         Web.send404( response );
         Log.log( Log.ERROR, "PAGE NOT FOUND " + link );
      }
      catch( Exception e )
      {
         Log.log(e);
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- doPost ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   public void doPost( HttpServletRequest request, HttpServletResponse response )
   {
      doGet( request, response );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- getTemplate ...                                                   ---*/
   /*-------------------------------------------------------------------------*/
   private static String getTemplate( HttpServletRequest request, String u )
   {
      String cnt = Cache.get( u );
      if( cnt==null )
      {
         Log.log( Log.INFO, "get Cached Module Template " + u );
         String page    = Format.lastToken( u, "/" );
         String domain  = Web.getDomainName( request );
         String path    = Web.getRealPath( request, "/content/" + domain + "/modules/" + page + ".html" );
         try
         {
            cnt = IO.loadASCIIFile( path );
            if( !request.getRequestURL().toString().contains("://dev.") )
               Cache.set( u, cnt );
         }
         catch( Exception  e )
         {
            //ignore log is already handled
         }
      }
      return cnt;
   }
}

