package com.servlets;

import com.util.*;
import com.bl.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Login extends HttpServlet
{
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   {
      String            id    =  request.getParameter( "id"       );
      String         email    =  request.getParameter( "email"    );
      String       service    =  request.getParameter( "service"  );
      String         state    =  request.getParameter( "state"    );
      String       landing    =  request.getParameter( "landing"  );
      
      String       pairs[]    =  Cypher.decode(id).split( "_" );
      String        userId    =  pairs[0];
      String    downloadId    =  pairs[1];
      String            qs    =  "" + request.getQueryString();
      
      if( Queries.checkList(downloadId, userId, email) )
      {
         Web.setCookieValue( response, "download", id );
         String  q    = "select * from user where id=" + userId + " union select * from user_lead where id=" + userId;
         HashMap hm   = DB.getRecord( q );
         User.setSessionProfile( response, hm );
         HashMap extra_data = Format.tsv2Hash( (String) hm.get( "extra_data" ) );
         request.getSession().setAttribute( "data",      extra_data               );
         request.getSession().setAttribute( "checklist", extra_data.get("_FORM_") );
         Web.redirect( response, Web.MOVE_PERMANENT, landing + "&" + qs );
      }
      else
      {
         Web.send404( response );
      }
   }
}
