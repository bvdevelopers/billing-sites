package com.servlets;

import java.io.*;
import java.util.*;
import com.util.*;
import com.bl.*;
import javax.servlet.http.*;
import javax.servlet.*;

public class PostForm extends HttpServlet
{
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doPost( HttpServletRequest request, HttpServletResponse response )
   throws ServletException, IOException
   {
      request.getSession().setMaxInactiveInterval( 900 );
      response.setContentType( "text/html" );
      String domain =  Web.getDomainName( request );
      
      //--- get return User
      HashMap  returnUser   =  (HashMap) request.getSession().getAttribute("returnuser");
      if( returnUser!=null )
      {
         returnUser.putAll( Web.getParameters(request) );
         request.getSession().setAttribute( "returnuser", returnUser );
         String u = "/content/" + domain + "/flow/returnUser.jsp";
         Web.redirect( response, Web.MOVE_PERMANENT, u );
         return;
      }
      
      //--- get current profile and current id and ip address
      HashMap     profile   =  User.getSessionProfile( request );
      String      id        =  (String) profile.get( "id" );
                  id        =  (id==null || id.length()==0)?"-1":id.trim();
      boolean     wasEmpty  =  id.equals("-1");


      //--- capture all parameters
      HashMap           data           =  Web.getParameters( request );
      HashMap           extraHashData  =  null;

      String first_name    =  (String) data.get( "first_name"  ); try { if(first_name!=null)    data.put( "first_name",    Format.initCap(first_name)    ); } catch( Exception e ) {}
      String middle_name   =  (String) data.get( "middle_name" ); try { if(middle_name!=null)   data.put( "middle_name",   Format.initCap(middle_name)   ); } catch( Exception e ) {}
      String last_name     =  (String) data.get( "last_name"   ); try { if(last_name!=null)     data.put( "last_name",     Format.initCap(last_name)     ); } catch( Exception e ) {}



      if( !id.equals("-1") )
         extraHashData  =  mergeHashData( id, data );
      else
         extraHashData  =  data;



      String            extraData      =  extra2String( extraHashData   );
      String            ip             =  request.getRemoteAddr().toString();
      String            pageNum        =  (String) data.get("_PAGENUM_");
                        pageNum        =  "C" + (pageNum==null?"01":pageNum);
      String            userAgent      =  (""+request.getHeader("USER-AGENT"));
      String            service        =  (String) request.getSession().getAttribute( "service" );
                        service        =  service==null?"":service;
      


      id = Queries.saveUser( userAgent, service, domain, id, ip, pageNum, data, extraData );


      data.put( "id", id );
      Log.log( Log.INFO, "form id : " + id );
      //--- set download cookie
      String  form                  =  (String) data.get(    "_FORM_" );
              form                  =  request.getParameter( "_FORM_" );
      try
      {
         request.getSession().setAttribute( "checklist", form );
      }
      catch( Exception e )
      {
      }

      String  downloadId            =  Downloads.addForm( domain, form, id       );
      request.setAttribute( "downloadId", downloadId );

      //--- synchronize profile with posted data
      profile.putAll( data );

      //--- set profile in cookie
      User.setSessionProfile( response, profile );

      //--- Passing the data
      extraHashData.put( "id", id );
      data.putAll(extraHashData);
      request.setAttribute( "data", data );

      //---- include flow
      StringBuffer flowCode = new StringBuffer("/content/");
      flowCode.append( domain );
      flowCode.append( "/flow/flow.jsp" );

      //---- include tracking
      StringBuffer trackingCode = new StringBuffer("/content/");
      trackingCode.append( domain );
      trackingCode.append( "/flow/tracking.jsp" );

      PrintWriter out = response.getWriter();
      out.print( "<html>" );
      out.print(     "<head>" );
      out.print(        "<script src=\"/jquery.js\"></script>" );
      out.print(        "<script src=\"/main.js\"></script>" );
      out.print(        "<script>leaveSite=false;</script>" );
      out.print(     "</head>" );
      out.print(     "<body><script>$(document).ready(function(){ try { parent.btnSwap(); } catch(e) {} });</script>" );

      int    _id = 0;
      try { _id=Integer.parseInt(id); } catch( Exception e ) {}
      String active = "" + data.get("active");
      
      try
      {
         request.getSession().setAttribute( "data", data );
      }
      catch( Exception e )
      {
      }
      if( active.equals("Y") )
      {
         //---- Google Analitics
         String googleContent = Cache.get( domain );
         if( googleContent==null )
         {
            googleContent = IO.loadASCIIFile( Web.getRealPath(request, "content/"+domain+"/template/google.html" ) );
            Cache.set( domain, googleContent );
         }
         out.println( googleContent );
         request.getRequestDispatcher( trackingCode.toString() ).include( request, response );
      }
      request.getRequestDispatcher( flowCode.toString()     ).include( request, response );
      out.print(     "</body>" );
      out.print( "</html>" );
   }

   /*-------------------------------------------------------------------------*/
   /*--- doPost ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   throws ServletException, IOException
   {
      Web.send404( response );
      //doPost( request, response );
   }

   /*-------------------------------------------------------------------------*/
   /*--- extra2String ...                                                  ---*/
   /*-------------------------------------------------------------------------*/
   private static String extra2String( HashMap finalData )
   {
      Iterator     it         = finalData.keySet().iterator();
      StringBuffer extaValues = new StringBuffer( "" );
      boolean      firstTime  = true;
      while( it.hasNext() )
      {
         String k = (String) it.next();
         String v = (String) finalData.get( k );
         if( !firstTime )
            extaValues.append( "\n" );
         extaValues.append( k ).append( "\t" ).append( v );
         firstTime = false;
      }
      return extaValues.toString();
   }

   /*-------------------------------------------------------------------------*/
   /*--- mergeHashData ...                                                 ---*/
   /*-------------------------------------------------------------------------*/
   private static HashMap mergeHashData( String id, HashMap data )
   {
      HashMap finalData = new HashMap();
      try
      {
         HashMap        record = Queries.getUserExtra( id );
         String    currentData = (String) record.get("extra_data");
         currentData = currentData==null?"":currentData.trim();
         String   arrCurrent[] = currentData.split( "\n" );
         for( int i=0; i<arrCurrent.length; i++ )
         {
            String pair[] = arrCurrent[i].split( "\t" );
            try
            {
               finalData.put( pair[0], pair[1] );
            }
            catch( Exception e )
            {
               // just ignore it
            }
         }
         finalData.put( "active", ""+record.get("active") );
         //finalData.putAll( User.stripHash(data) );
         finalData.putAll( data );
      }
      catch( Exception e )
      {
         Log.log(e);
      }
      finalData.put( "id", id );
      return finalData;
   }
}
