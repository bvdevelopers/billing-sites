package com.servlets;

import com.util.*;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Logo extends HttpServlet
{
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   {
      ContentInclude.noCache( response );
      
      String         protocol  =  request.isSecure()?"https://":"http://";
      String         domain    =  Web.getDomainName( request );
      boolean        isDev     =  request.getRequestURL().toString().contains("://dev.");
      String         devPrefix =  isDev?"dev":"";
      StringBuffer   link      =  new StringBuffer( protocol );
      link.append( "s3.amazonaws.com/" ).append( (CR.is(domain)?"n":"") + domain ).append("/img/logo.png");
      Web.redirect( response, Web.MOVE_PERMANENT, link.toString() );
   }
}
