package com.servlets;

import java.io.*;
import java.util.*;
import com.util.*;
import com.bl.*;
import javax.servlet.http.*;
import javax.servlet.*;

public class ReturnUser extends HttpServlet
{
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   throws ServletException, IOException
   {
      String  id    = Format.lastToken( request.getRequestURI().toString(), "/" );
      String  email = request.getParameter( "e" ).replace(" ", "").toLowerCase();
      HashMap data  = DB.getRecord("select * from addresschange coa, return_user ru where ru.id='"+id+"' and coa.email='"+email+"' and status='sent' and coa.id=coa_id");
      if( data==null || data.isEmpty() )
      {
         Web.redirect( response, Web.MOVE_PERMANENT, "/index.html" );
      }
      else
      {
         request.getSession().setAttribute( "returnuser", data );
         request.getSession().setMaxInactiveInterval( 900 );
         Web.redirect( response, Web.MOVE_PERMANENT, "/forms/step1/"+id+"/main.html" );
      }
   }
}
