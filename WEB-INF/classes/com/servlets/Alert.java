package com.servlets;

import java.io.*;
import java.util.*;
import com.util.*;
import com.google.*;
import com.bl.*;
import javax.servlet.http.*;
import javax.servlet.*;

public class Alert extends HttpServlet
{
   private static GetThread thread = null;
   public static  Date      stamp  = null;
   public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException
   {
      PrintWriter out = response.getWriter();
      out.print( stamp );
      try
      {
         if( thread==null )
         {
            thread = new GetThread();
            thread.start();
         }
      }
      catch( Exception e )
      {
         Log.log(e);
      }
   }
   
   private static class GetThread extends Thread
   {
      public GetThread() { }
      public void run()
      {
         try
         {
            while( true )
            {
               String alert = Queries.alertLeads().trim();
               stamp = new Date();
               if( alert.length() > 0 )
               {
                  Google.sendText( "3052135561", alert );
               }
               sleep(15*60*1000);
            }
         }
         catch (Exception e)
         {
            Log.log( e );
         }
      }
   }
}
