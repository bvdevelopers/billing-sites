package com.servlets;

import com.util.*;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class DEBUG extends HttpServlet
{
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   {
      response.setContentType( "text/html" );
      String ur =  request.getRequestURL().toString();
      if( ur.startsWith("http://www.") )
      {
         ur = ur.replace( "http://www.", "http://" );
         Web.redirect( response, Web.MOVE_PERMANENT, ur );
      }
      
      String qs    =  request.getQueryString();
      String link =  ur.replace("/modules/", "/dynamic/") + ".jsp?" + qs;
      try
      {
         String         cnt   =  new String( Web.webGet(link) );
         PrintWriter    out   =  response.getWriter();
         String    template   =  getTemplate( request, ur );
         if( template!=null )
         {
            cnt = template.replace( "<!--$content$-->", cnt );
            cnt = Content.templify( request, cnt );
         }
         out.print( cnt );
      }
      catch( FileNotFoundException fnf )
      {
         Web.send404( response );
         Log.log( Log.ERROR, "PAGE NOT FOUND " + link );
      }
      catch( Exception e )
      {
         Log.log(e);
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- doPost ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   public void doPost( HttpServletRequest request, HttpServletResponse response )
   {
      try
      {
         response.sendError( 403 );
      }
      catch( IOException e )
      {
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- getTemplate ...                                                   ---*/
   /*-------------------------------------------------------------------------*/
   private static String getTemplate( HttpServletRequest request, String u )
   {
      String cnt = Cache.get( u );
      if( cnt==null )
      {
         Log.log( Log.INFO, "get Cached Module Template " + u );
         String page    = Format.lastToken( u, "/" );
         String domain  = Web.getDomainName( request );
         String path    = Web.getRealPath( request, "/content/" + domain + "/modules/" + page + ".html" );
         try
         {
            cnt = IO.loadASCIIFile( path );
            if( !request.getRequestURL().toString().contains("://dev.") )
               Cache.set( u, cnt );
         }
         catch( Exception  e )
         {
            //ignore log is already handled
         }
      }
      return cnt;
   }
}

