package com.servlets;

import com.util.*;
import com.bl.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Download extends HttpServlet
{
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   {
      Log.log( Log.INFO, "Generate PDF" );
      response.setContentType( "application/pdf" );
      try
      {
         String cookieVal = Web.getCookieValue( request, "download" );
         if( cookieVal.equals("") )
         {
            cookieVal = (String) request.getSession().getAttribute( "download" );
            cookieVal = cookieVal==null?"":cookieVal;
         }
         if( cookieVal.equals("") )
         {
            String p = request.getParameter( "p" ) + ".pdf";
            try
            {
               request.getRequestDispatcher( p ).forward( request, response );
            }
            catch( IllegalStateException e )
            {
               Log.log( Log.ERROR, "IllegalStateException : " + p );
            }
            return;
         }
         Log.log( Log.INFO, (new StringBuffer("Cypher Cookie : \"").append(cookieVal).append("\"")).toString() );
         String  arr[]        = Cypher.decode(cookieVal).split( "_" );
         HashMap data         = Downloads.getData( arr[0], arr[1] );
         String  domain       = (String) data.get( "domain"    );
         String  defaultPath  = (String) data.get( "form_path" );
                 defaultPath  = defaultPath==null?"":defaultPath;
                 defaultPath  = defaultPath.replace(".html", ".pdf");
         String  path         = request.getParameter( "p" );

         path = (path==null)?defaultPath:("/forms/"+path+".pdf");



         String  form         = (new StringBuffer("/content/")).append(domain).append("/pdf/").append(path).toString();
         String  formName     = (new StringBuffer(domain)).append(path).toString();
         form = Web.getRealPath( request, form );
         response.setHeader( "content-disposition", (new StringBuffer("attachment; filename=")).append(formName).toString());
         StringBuffer loxText = (new StringBuffer( "Populate " )).append( form ).append( " WITH " ).append( data.toString() );
         Log.log( Log.INFO, loxText.toString() );
         com.util.PDF.populateForm( form, data, response.getOutputStream() );
      }
      catch( Exception e )
      {
         Log.log(e);
         try { throw e; } catch( Exception e2 ) {}
      }
   }
}
