package com.servlets;

import java.io.*;
import java.util.*;
import com.util.*;
import javax.servlet.http.*;
import javax.servlet.*;

public class Logout extends HttpServlet
{
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   throws ServletException, IOException
   {
      User.logout( request, response );
   }
   
   public void doPost( HttpServletRequest request, HttpServletResponse response )
   throws ServletException, IOException
   {
      doGet( request, response );
   }
}
