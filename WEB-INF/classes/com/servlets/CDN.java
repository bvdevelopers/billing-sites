package com.servlets;

import com.util.*;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.zip.*;

public class CDN extends HttpServlet
{
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   {
      ContentInclude.noCache( response );
      String        url = request.getRequestURL().toString().toLowerCase();
      String   fileName = request.getRequestURI().toString();
      
      if(      fileName.endsWith(".css") )
         response.setContentType( "text/css" );
      else if( fileName.endsWith(".js") )
         response.setContentType( "text/javascript" );
      else if( fileName.endsWith(".woff") || fileName.endsWith(".woff2") )
         response.setContentType( "application/x-font-woff" );
      else if( fileName.endsWith(".ttf") )
         response.setContentType( "application/font-sfnt" );
      else
      {
         Web.send404( response );
         return;
      }
      String     domain =  Web.getDomainName(request);
      String       path =  Web.getRealPath( request, "/" ) + "content" + File.separator + domain + fileName;
      String cnt = Cache.get( path );

      try
      {
         synchronized(this)
         {
            if( !Web.noWWW( request, response ) )
               return;
            if( cnt==null )
            {
               try
               {
                  cnt = IO.loadASCIIFile( path );
                  cnt = cnt.replace( "{domain}", domain );
                  Cache.set( path, cnt );

               }
               catch( Exception fnf )
               {
                  Web.send404( response );
               }
            }
         }

         //response.setHeader("Content-Encoding", "gzip");
         PrintWriter  out  =  response.getWriter();
         /*
         OutputStream outA = response.getOutputStream();
         PrintWriter out = new PrintWriter(new GZIPOutputStream(outA), false);
         response.setHeader("Content-Encoding", "gzip");
         */
         out.print(cnt);
      }
      catch( FileNotFoundException e )
      {
         e.printStackTrace();
         Log.log( Log.INFO, path + " NOT EXISTS" );
      }
      catch( IOException e )
      {
         Log.log( e );
      }
   }
}
