package com.servlets;

import com.util.*;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class Favicon extends HttpServlet
{
   public void doGet( HttpServletRequest request, HttpServletResponse response ) throws IOException, ServletException
   {
      String   domain   = Web.getDomainName( request );
      String fileName   = Web.getRealPath( request, "/content/" + domain + "/favicon.ico" );
      
      byte        b[]   = Cache.getBinary( fileName );
      if( b==null )
      {
         try
         {
            b = IO.loadBinaryFile(fileName);
            if( !request.getRequestURL().toString().contains("://dev.") )
               Cache.setBinary( fileName, b );
         }
         catch( FileNotFoundException fnfe )
         {
            Web.send404( response );
         }
      }
      
      response.setContentType( Web.getContentType(fileName) );
      if( b!=null )
         response.getOutputStream().write( b );
      else
         Web.send404( response );
      response.getOutputStream().flush();
   }
}

