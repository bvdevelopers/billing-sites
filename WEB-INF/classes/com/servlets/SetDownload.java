package com.servlets;

import com.util.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class SetDownload extends HttpServlet
{
   // calls via ajax takes download id as parameters and puts in cookie for futur downloads
   // don't ask y I do it that way, was a hack due to a jsp bug
   // mapped as /dwn so it wont attact attemtion
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   {
   }
   
   public void doPost( HttpServletRequest request, HttpServletResponse response )
   {
      try
      {
         String k = URLEncoder.encode( request.getParameter( "k" ), "UTF-8" );
         Log.log( Log.INFO, "set download ID : " + k );
         Web.setCookieValue( response, "download", k, -1 );
      }
      catch( Exception e )
      {
         Log.log(e);
      }
   }
}
