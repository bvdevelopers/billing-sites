package com.servlets;

import java.io.*;
import java.util.*;
import com.util.*;
import com.bl.*;
import javax.servlet.http.*;
import javax.servlet.*;

public class FixEmail extends HttpServlet
{
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doPost( HttpServletRequest request, HttpServletResponse response )
   throws ServletException, IOException
   {
      Log.log( Log.INFO, "Fixing Email" );
      HashMap     profile   =  User.getSessionProfile( request );
      String      id        =  (String) profile.get( "id" );
                  id        =  (id==null || id.length()==0)?"-1":id.trim();
      String      email     =  request.getParameter( "fixEmail" );
      Queries.updateEmail( id, email );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   throws ServletException, IOException
   {
      Web.send404( response );
   }
}
