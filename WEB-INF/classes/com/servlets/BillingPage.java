package com.servlets;

import java.io.*;
import java.util.*;
import com.util.*;
import com.bl.*;
import javax.servlet.http.*;
import javax.servlet.*;

public class BillingPage extends HttpServlet
{
   private static String cnt = null;
   
   public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException
   {
      PrintWriter  out  = response.getWriter();
      if( cnt==null )
      {
         StringBuffer html = new StringBuffer();
         html.append(  "<!--$fulltemplate.html$-->" );
         html.append(  "<style>td {padding-bottom:7px;}</style>");
         html.append(  "<div id='buySS'>");
         html.append(     "<h1>Billing Information</h1>");
         html.append(     "<hr id='hr-dotted' />");
         html.append(     "<h3>Please complete the form below to process your payment.</h3>");
         html.append(     "<br />");
         
         html.append(     "<div id='floatLeft' style='width:63%;'>");
         html.append(        "<div class='prodGrey' style='background:#eee;'>");
         html.append(           "<strong>Enter your billing information -- SECURE <img src='{CSS-CLOUD}/img/lock.png' style='vertical-align:middle;'></strong>");
         html.append(        "</div>");
         html.append(        "<div class='prodGrey' id='processError' style='background: #fff; border:2px solid red; margin-top:10px;display:none;'></div>");
         html.append(        "<br />");
         html.append(        "<form name='frm' onSubmit='return frmPay();' action='/new_paymentGateway.jsp' method='post' target='buffer' id='billingForm'>");
         html.append(           "<table cellspacing='3' cellpadding='3'>");
         html.append(              "<tr><td style='width:160px;'><label for='cc_first_name'>First Name:</label></td><td colspan='3'><input type='text' placeholder='First Name on Credit Card' name='cc_first_name' id='cc_first_name' size='50' onBlur='this.value = capitalize(this.value)' /></td></tr>");
         html.append(              "<tr><td><label for='cc_last_name'>Last Name:</label></td><td colspan='3'><input type='text' placeholder='Last Name on Credit Card' name='cc_last_name' id='cc_last_name' size='50' onBlur='this.value = capitalize(this.value)' /></td></tr>");
         html.append(              "<tr><td><label for='cc_address'>Address:</label></td><td colspan='3'><input placeholder='Billing Street Address' type='text' name='cc_address' id='cc_address' size='50' /></td></tr>");
         html.append(              "<tr><td><label for='cc_city'>City:</label></td><td colspan='3'><input placeholder='Billing City' type='text' name='cc_city' id='cc_city' size='50' /></td></tr>");
         html.append(              "<tr><td><label for='cc_state'>State:</label></td><td style='width:135px;'><select name='cc_state' id='cc_state'><!--state_option.html--></select></td><td style='text-align:right; width:200px;'>&nbsp;<label for='cc_zip'>&nbsp;Zip Code:</label>&nbsp;<input placeholder='Billing Zip Code' type='text' name='cc_zip' id='cc_zip' size='10' maxlength='5' /></td></tr>");
         html.append(              "<tr><td><label for='cc_card_number'>Credit Card Number:</label></td><td colspan='2'><input type='text' placeholder='Credit Card Number' name='cc_card_number' id='cc_card_number' size='25' /><img src='{CSS-CLOUD}/img/credit-cards.png' style='vertical-align:middle;'></td></tr>");
         html.append(              "<tr>");
         html.append(                 "<td><label for='cc_expires'>Expiration Date:</label></td>");
         html.append(                 "<td colspan='2'>");
         html.append(                    "<select name='month' id='cc_month'>");
         html.append(                       "<option value=''>  --month--  </option>");
         html.append(                       "<option value = '1'>January</option>");
         html.append(                       "<option value = '2'>February</option>");
         html.append(                       "<option value = '3'>March</option>");
         html.append(                       "<option value = '4'>April</option>");
         html.append(                       "<option value = '5'>May</option>");
         html.append(                       "<option value = '6'>June</option>");
         html.append(                       "<option value = '7'>July</option>");
         html.append(                       "<option value = '8'>August</option>");
         html.append(                       "<option value = '9'>September</option>");
         html.append(                       "<option value = '10'>October</option>");
         html.append(                       "<option value = '11'>November</option>");
         html.append(                       "<option value = '12'>December</option>");
         html.append(                    "</select>");
         html.append(                    "<select name='year' id='cc_year'>");
         html.append(                       "<option value=''>  --year--  </option>");
         int y = Format.currentYear();
         for( int i=0; i<10; i++ )
            html.append(                       "<option value = '"+(y+i)+"'>"+(y+i)+"</option>");
         html.append(                    "</select>");
         html.append(                 "</td>");
         html.append(              "</tr>");
         html.append(              "<tr><td><label for='cc_security_code'>Security Code:</label>&nbsp;<a href='javascript:cvv()'><img src='{CSS-CLOUD}/img/tooltip.png' id='securitycode' style='border:none;'></a></td><td colspan='2'><input type='text' name='cc_security_code' id='cc_security_code' size='4' maxlength='4' />&nbsp;&nbsp;<img src='{CSS-CLOUD}/img/last3.jpg' style='vertical-align:middle;'></td></tr>");
         html.append(              "<tr><td height='10'></td></tr>");
         html.append(           "</table>");
         html.append(           "<div id='termsBox'>");
         html.append(              "<label for='agree_tc'>By clicking 'continue' and using the {domain} Drivers-Licenses.org website, you agree to follow and be bound by these <a gref='/tc.html' target='_blank'>Terms and Conditions</a> and consent to our <a href='/privacy.html' target='_blank'>Privacy Policy</a>. This site is privately owned and is neither operated by, nor affiliated with, any government agency. We provide helpful, time-saving information and downloads as a private value-add to motor vehicle services.</label>");
         html.append(           "</div>");
         html.append(           "<input type='xhidden' name='price'    id='price'     value=''>");
         html.append(           "<input type='xhidden' name='phone'    id='phone'     value=''>");
         html.append(           "<input type='xhidden' name='email'    id='email'     value=''>");
         html.append(           "<input type='xhidden' name='item'     id='item'      value=''>");
         html.append(           "<input type='xhidden' name='service'  id='service'   value=''>");
         html.append(           "<input type='xhidden' name='state'    id='state'     value=''>");
         
         html.append(           "<br /><input type='image' src='{CSS-CLOUD}/img/pt-continue1.png' class='center' id='billingSubmit' style='cursor:pointer;border:none;'><span id='submitLoad' style='display:none;'><img src='{CSS-CLOUD}/img/spinner.gif'></span>");
         html.append(        "</form>");
         html.append(        "<br />");
         html.append(     "</div>");
         
         html.append(     "<div id='floatLeft' style='margin-left:25px; width:30%;'>");
         html.append(        "<div class='prodTotal'>");
         html.append(           "<div style='text-align:center;padding:5px 0px;'><strong>Transaction Summary:</strong></div>");
         html.append(           "<div style='border-bottom:2px solid #ccc;background:#eee;'><strong style='padding-left:5px;'>Item:</strong><strong style='margin-left:158px;border-left:1px solid #ccc;padding-left:5px;'>Price:</strong></div>");
         html.append(           "<div id='items'></div>");
         html.append(           "<div id='space' style='height:2px;clear:both;'>&nbsp;</div>");
         html.append(           "<div id='ssTotal'>Total:&nbsp;$<span id='displayPrice'>0</span></div>");
         html.append(           "<img src='{CSS-CLOUD}/img/norton.jpg' class='center'>");
         html.append(        "</div>");
         html.append(     "</div>");
         html.append(  "</div>");
         html.append(  "<iframe width='100%' height='100px' name='buffer' id='buffer' src='' style='clear:both;display:xnone;'></iframe>");
         html.append(  "<!--/$fulltemplate.html$-->" );
         cnt = html.toString();
         cnt = Content.templify( request, cnt );
      }
      out.print( cnt );
   }
   
   public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException
   {
      doGet( request, response );
   }
}
