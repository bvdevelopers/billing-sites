package com.servlets;

import java.io.*;
import java.util.*;
import com.util.*;
import javax.servlet.http.*;
import javax.servlet.*;

public class UserLogin extends HttpServlet
{
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   throws ServletException, IOException
   {
      try
      {
         PrintWriter out   =  response.getWriter();
         String      uri   =  request.getRequestURI().replace( "/user/", "/" );
         String      qs    =  request.getQueryString();
                     qs    =  qs==null?"":("?"+qs);
         String      user  =  (String) request.getSession().getAttribute( "login" );
         String      link  =  "/";
         if( user!=null )
         {
            String cat     =  request.getParameter("cat"    );
            String service =  request.getParameter("service");
            String split[] =  uri.split("/");
            String type    =  split[ split.length - 2 ];
                   link    =  "/checklist/" + type + "/" + cat + (service==null?"":("/" + service)) + "/" + Format.lastToken( uri, "/" );
         }
         else
         {
            link = uri + qs;
         }
         while( link.contains("/null/") )
            link = link.replace( "/null/", "/" );
         Web.redirect( response, 302, link );
      }
      catch( Exception e )
      {
      }
   }
}

