package com.servlets;

import com.util.*;
import java.util.*;
import java.util.concurrent.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class PdfForm extends HttpServlet
{
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   throws ServletException, IOException
   {
      String      uri   =  request.getRequestURI().toString();
      String   domain   =  Web.getDomainName( request );
      if( !uri.endsWith(".pdf") )
      {
         Web.send404( response );
         return;
      }
      response.setContentType( "application/pdf" );
      response.setHeader( "content-disposition", (new StringBuffer("attachment; filename=")).append(Format.lastToken(uri, "/")).toString());
      byte[] b = Cache.getBinary( uri );
      if( b==null )
      {
         String         path = uri.replace("/PDForm/", "/");
         StringBuffer   link = new StringBuffer("/content/").append(domain).append("/pdf/forms/").append(path);
         String     realPath = Web.getRealPath( request, link.toString() );
         try
         {
            b = IO.loadBinaryFile( realPath );

         }
         catch( FileNotFoundException e )
         {
            Web.send404( response );
         }
         if( !request.getRequestURL().toString().contains("://dev.") )
            Cache.setBinary( uri, b );
      }
      try
      {
         
         response.getOutputStream().write( b );
         response.getOutputStream().flush();
      }
      catch( Exception e )
      {
         Web.send404( response );
      }
   }
}

