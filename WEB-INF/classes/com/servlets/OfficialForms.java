package com.servlets;

import com.util.*;
import com.bl.*;
import java.util.*;
import jxl.*;
import java.util.concurrent.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class OfficialForms extends HttpServlet
{
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response ) throws IOException
   {
      response.setContentType( "text/html" );
      
      PrintWriter out = response.getWriter();
      String   domain =  Web.getDomainName( request );
      String     path = getServletContext().getRealPath("/content/"+domain+"/pdf/forms");
      String    ls1[] = (new File( path   )).list();
      String      cnt = "<h1>Official Forms</h1>";


      cnt += "<ul>";
      for( int i=0; i<ls1.length; i++ )
      {
         String    ls2[] = (new File( path + "/" + ls1[i] )).list();
         if( ls2==null || ls2.length==0 )
            continue;
         cnt += "<li>";
         cnt += "<p><h2>" + Format.initCap( ls1[i] ) + "</h2></p>";
         if( ls2!=null )
         {
            cnt += "<ul>";
            for( int j=0; j<ls2.length; j++ )
            {
               cnt += "<li><a href='/PDForm/"+ls1[i]+"/"+ls2[j]+"'>" + Format.initCap(ls2[j].replace(".pdf","")) + "</a></li>";
            }
            cnt += "</ul>";
         }
         cnt += "</li>";
      }
      cnt += "</ul>";
      cnt = Content.templify( request, "<!--$hometemplate.html$-->" + cnt + "<!--/$hometemplate.html$-->" );
      out.print( cnt );
   }
}


