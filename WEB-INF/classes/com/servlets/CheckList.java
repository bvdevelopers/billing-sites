package com.servlets;

import com.util.*;
import com.bl.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class CheckList extends HttpServlet
{
   public void doGet( HttpServletRequest request, HttpServletResponse response ) throws IOException
   {
      String            id    =  request.getParameter( "customerid" );
                        id    =  (id==null)?request.getParameter( "id" ):id;
      String         email    =  request.getParameter( "email"      );
      String       service    =  request.getParameter( "service"    );
      String         state    =  request.getParameter( "state"      );
      String        county    =  request.getParameter( "county"     );
      String       pairs[]    =  Cypher.decode(id).split( "_" );
      String        userId    =  pairs[0];
      String    downloadId    =  pairs[1];
      if( Queries.checkList(downloadId, userId, email) )
      {
         Web.setCookieValue( response, "download",   id      );
         Web.setCookieValue( response, "uid",        userId  );
         request.getSession().setAttribute( "uid",   userId  );
         request.getSession().setAttribute( "id",    userId  );
         request.getSession().setAttribute( "email", email   );
         PrintWriter out = response.getWriter();
         String path = "/checklist/" + service + "/" + state + ".html?fr=mail&" + request.getQueryString();
         if( county!=null )
            path = "/checklist/" + service + "/" + state + "/" + county + ".html?fr=mail&" + request.getQueryString();
         //path += "&__UUIIDD=" + userId;


         response.setContentType( "text/html" );
         
         out.print( "<html><body><script>" );
         out.print( "document.location.replace(\"" + path + "\");" );
         out.print( "</script></body></html>" );
         //Web.redirect( response, Web.MOVE_TEMPORARY, path );
      }
      else
      {
         Web.send404( response );
      }
   }
}
//https://drivers-licenses.org/checklist/download/new-drivers-license/new-york?id=ZAWXA&email=cohana@gmail.com
