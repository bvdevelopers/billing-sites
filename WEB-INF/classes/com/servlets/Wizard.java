package com.servlets;

import com.util.*;
import com.bl.*;
import java.util.*;
import jxl.*;
import java.util.concurrent.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Wizard extends HttpServlet
{
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response ) throws IOException
   {
      PrintWriter    out   =  response.getWriter();
      String pnts    =  request.getParameter( "pnts" );
      String resp    =  request.getParameter( "resp" );
      if( pnts!=null && resp!=null )
      {
         request.getSession().setAttribute( "quiz.points",   pnts                );
         request.getSession().setAttribute( "quiz.response", resp.substring(1)   );
         request.getSession().setMaxInactiveInterval( 900 );
         out.print( "OK" );
         return;
      }
      
      response.setContentType( "text/html" );
      String         uri   =  request.getRequestURI().toString();
      if( uri.contains("poll") )
      {
         String  domain =  com.util.Web.getDomainName( request );
         String  ip     =  request.getRemoteAddr().toString();
         try
         {
            com.bl.Queries.track( "view_poll", ip, domain );
         }
         catch( Exception e )
         {
         }
      }
      
      String     content   =  Cache.get( uri );
      
      if( content==null || request.getParameter("nocache")!=null )
      {
         String     brd       =  "0";
         String         wiz   =  Format.lastToken(uri, "/");
         HashMap      hQuiz   =  DB.getRecord( "select * from quiz where name='" + wiz + "'" );
         String description   =  ((String) hQuiz.get( "description" )).replace("\n", "<br>");
         String      quizId   =   (String) hQuiz.get( "id" );
         content              =  "";
         String       split[] =  uri.split( "/" );
         String       style   =  "";
         if( split.length > 3 )
            style = split[2];
         
         Vector            vQ =  DB.getData(   "select * from quiz_question where quiz_id=" + quizId + " order by sequence" );
         Vector         vFlow =  DB.getData(   "select * from quiz_flow  where quiz_id="    + quizId );
         HashMap       hStyle =  DB.getRecord( "select * from quiz_style where name=\""     + style + "\"" );
         String     borderBox =  "";
         String border_height =  "" + hStyle.get("border_height");
         String   border_logo =  "" + hStyle.get("border_logo");
                  borderBox  +=  "border:1px solid "  +  hStyle.get("border_color")     + ";";
                  borderBox  +=  "padding:"           +  hStyle.get("border_padding")   + ";";
                  if( !border_height.equals("") )
                     borderBox  +=  "height:"            +  border_height    + "px;";
                  else
                     borderBox  +=  "height:auto;";
         
         //-------- header --------
         content += "<html><head>";
         content += "<script src='/jquery.js'></script>";
         content += "<script src='/jquery.tipsy.js'></script>";
         content += "<script src='/wizard.js'></script>";
         content += "<script src='/main.js'></script>";
         content += "<link rel='stylesheet' href='/wizard.css' type='text/css' />";
         content += "<head>";
         content += "<body>";

         
         //-------- get flow logic --------
         content += "<script>\n";
         content +=     "function getNextQuestion(curParentID)";
         content +=     "{\n";
         for( int f=0; f<vFlow.size(); f++ )
         {
            HashMap hmFlow          =  (HashMap) vFlow.elementAt(f);
            String  questionSeq     =  (String)  hmFlow.get( "question_seq"    );
            String  show_condition  =  (String)  hmFlow.get( "show_condition"  );
            String  question_next   =  (String)  hmFlow.get( "question_next"   );
            String  trigger_js_code =  (String)  hmFlow.get( "trigger_js_code" );
            String  condition       = translateCondition( show_condition );
            
            content += "if( (curParentID==\""+questionSeq+"\" ) && " + condition + " )\n";
            content += "{\n";
            content += trigger_js_code + "\n";
            content +=     "return " + question_next + ";\n";
            content += "}\n";
         }
         content +=        "return eval(curParentID) + 1;\n";
         content +=     "}\n";
         content += "var nbQ=" + vQ.size() +";";
         content += "</script>";
         
         content += "<div class='quiz_title' style=\"font-family:"+hStyle.get("title_font")+";height:"+hStyle.get("title_height")+";font-weight:"+hStyle.get("title_weight")+";color:"+hStyle.get("title_color")+";font-size:"+hStyle.get("title_size")+"px\">" + description + "</div>";
         content += "<div id='quiz_page' style=\"font-family:"+hStyle.get("score_font")+";height:"+hStyle.get("score_height")+";font-weight:"+hStyle.get("score_weight")+"; color:"+hStyle.get("score_color")+"; font-size:"+hStyle.get("score_size")+"px;" + (vFlow.size()>0?"display:none;":"") + "\"></div>";


         for( int i=0; i<vQ.size(); i++ )
         {
            HashMap     hmQ   =  (HashMap)   vQ.elementAt( i );
            String sequence   =  (String)    hmQ.get( "sequence"  );
            String     memo   =  (String)    hmQ.get( "memo"      );
                       memo   =  memo.trim();
            Vector       vO   =  DB.getData( "select * from quiz_option where quiz_id=" + quizId + " and question_sequence=" + sequence + " order by sequence" );
            content += "<div class='quiz_question' id='q"+i+"'>\n";

                     if( !border_logo.equals("") )
                        content +=    "<div style=\"float:right; text-align:center; vertical-align:center; Margin-right:20px; margin-top:-50px;\"><img width='100' height='100' src='/content/img/"+border_logo+"'></div>\n";



                     content +=    "<div style='" + borderBox + " position:static; left:0px;' class='quiz_group'>\n";
                              if( vO.size() > 1 )
                              {
                                 content +=        "<table bordercolor='green' cellpadding=0 cellspacing=0 width='100%' border="+brd+"><tr>\n";
                                 content +=        "<td><table bordercolor='blue' cellpadding=0 cellspacing=0 width='100%' border="+brd+"><tr>";
                                 if( memo.length() > 0 )
                                    content +=        "<td width='1' valign='middle' title=\""+hmQ.get("memo")+"\" class='tooltip'><img src='/info.png'></td><td width='20'></td>";
                                 content +=           "<td valign='middle' class='quiz_question_font' style=\"height:"+hStyle.get("question_height")+"px; font-weight:"+hStyle.get("question_weight")+";color:"+hStyle.get("question_color")+"; font-size:"+hStyle.get("question_size")+"px;\">";
                                 content +=           "<div style=\"font-family:"+hStyle.get("question_font")+"\"; class='quiz_question_label'>" + hmQ.get("label") + "</div>";
                                 content +=           "</td>\n";
                                 content +=        "</table></tr></td>";
                                 content +=        "</tr></table>\n";
                              }
                              content +=           "<div>\n";
                                          content     += "<table bordercolor='orange' width='100%' border="+brd+" cellpadding=0 cellspacing=0>\n";
                                          String last  = (i==vQ.size()-1)?"1":"0";
                                          for( int j=0; j<vO.size(); j++ )
                                          {
                                             HashMap hmO       = (HashMap) vO.elementAt(j);
                                             String label      = (String)  hmO.get( "label"  );
                                             String points     = (String)  hmO.get( "points" );
                                             String radioID    = quizId + "_" + i;
                                             content += "<tr><td style='height:"+hStyle.get("option_height")+"px;'></td></tr>\n";
                                             content += "<tr>\n";
                                             String display = (vO.size() <= 1)?" style='display:none' ":"";
                                             content +=     "<td class='quiz_radio_group' valign='middle' " + display + " width='1'><input qlbl=\""+hmQ.get("label")+"\" lbl=\""+label+"\" q=\""+i+"\" last=\"" + last + "\" points=\""+ points +"\" class='radio' value='"+j+"' name='"+radioID+"' id='"+radioID+j+"' type='radio'></td>\n";
                                             content +=     "<td valign='middle' " + display + " style='padding:3px;'><label class='quiz_option' for='"+radioID+j+"' style='font-family:"+hStyle.get("option_font")+";font-size:"+hStyle.get("option_size")+"px;font-weight:"+hStyle.get("option_weight")+";color:"+hStyle.get("option_color")+";'>" + label + "</label></td>\n";
                                             content += "</tr>\n";
                                             content += "<tr class='quiz_extra' id=\"xtra"+radioID+"_"+j+"\"><td></td><td>more question are going here</td></tr>\n";
                                          }
                                          content += "<tr><td class='quiz_sep'></td></tr>\n";
                                          content +=    "</table>\n";
                              content +=    "</div>\n";
                     content +=    "</div>\n";
            content += "</div>\n";
         }
         
         
         
         
         
         content += "</body></html>";
         
         
         



         Cache.set( uri, content );
      }
      out.print( content );
   }
   
   private static String translateCondition( String rawCondition )
   {
      StringTokenizer stk = new StringTokenizer( rawCondition, ".", true );
      String    condition = "";
      while( stk.hasMoreTokens() )
      {
         String next = stk.nextToken();
         if( next.contains("O") )
         {
            next = next.replace( "O", "responses[" );
            condition += next;
         }
         else
         {
            condition += next;
         }
      }
      return condition.replace(".", "]==");
   }
}


