package com.servlets;

import com.util.*;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class JS extends HttpServlet
{
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   {
      ContentInclude.noCache( response );
      response.setContentType( "text/javascript" );
      
      String        url = request.getRequestURL().toString().toLowerCase();
      String   fileName =  Format.lastToken( request.getRequestURI().toString(), "/" );
      String     domain =  Web.getDomainName(request);
      String       path =  Web.getRealPath( request, "/" ) + "content" + File.separator + domain + File.separator + "JS" + File.separator + fileName;
      String     script =  Cache.get( path );
      
      try
      {
         PrintWriter  out  =  response.getWriter();
         
         synchronized(this)
         {
            if( !Web.noWWW( request, response ) )
               return;
            if( script==null )
            {
               Log.log( Log.INFO, "Loading JS file " + path );
               try
               {
                  script = IO.loadASCIIFile( path );
               }
               catch( Exception fnf )
               {
                  script = IO.loadASCIIFile( path.replace("content" + File.separator + domain + File.separator + "JS" + File.separator, "") );
               }
               script = Content.templifyCSSCloud( request, script );
               if( !request.getRequestURL().toString().contains("://dev.") )
                  Cache.set( path, script );
            }
            
            //---- include _jsp version of js file
            if( fileName.contains("main.js") || fileName.contains("form.js") )
            {
               String inc = (new StringBuffer("/")).append( fileName.replace(".js","_js.jsp") ).toString();
               try
               {
                  Log.log( Log.INFO, "Include " + inc );
                  request.getRequestDispatcher( inc ).include( request, response );
               }
               catch( Exception e )
               {
                  Log.log( Log.ERROR, "Include " + inc + " FAILED" );
               }
            }
         }
         out.print( script );
         
         //--- include dynamic content from server
         /*
         try
         {
            String inc = "/content/" + domain + "/JS/" + fileName.replace(".js",".jsp");
            request.getRequestDispatcher( inc ).include( request, response );
         }
         catch( Exception e )
         {
            Log.log( Log.INFO, "Not jsp Include for Business Logic" );
         }
         */
         
      }
      catch( FileNotFoundException e )
      {
         Log.log( Log.INFO, path + " NOT EXISTS" );
      }
      catch( IOException e )
      {
         Log.log( e );
      }
   }
}
