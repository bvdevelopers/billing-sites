package com.servlets;

import java.io.*;
import java.util.*;
import com.util.*;
import com.bl.*;
import javax.servlet.http.*;
import javax.servlet.*;

public class DownloadPDF extends HttpServlet
{
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   throws ServletException, IOException
   {
      try
      {
         String  id    = (String)  request.getSession().getAttribute( "id"     );
                 id    = id==null?(request.getParameter("id")):id;
                                   request.getSession().setAttribute( "id", id );
         String  email = (String)  request.getSession().getAttribute( "email"  );
                 email = email==null?(request.getParameter("email")):email;
                                   request.getSession().setAttribute( "email", email );
         String  sql   = "update payment_transaction         set download=1 where email='" + email + "'";
         DB.executeSQL( sql );
         DB.executeSQL( sql.replace("payment_transaction", "payment_transaction_history") );
      }
      catch( Exception e )
      {
      }
      String uri = request.getRequestURI().toString().replace( "/downloadPDF", "" ).split("\\?")[0];
      Web.redirect( response, Web.MOVE_PERMANENT, uri );
   }
}
