package com.servlets;

import com.bl.*;
import com.util.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Track extends HttpServlet
{
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   {
      if( request.getSession().getAttribute( "returnuser" )!=null )
         return;

      //---- coz if several @ same time tiem between select & update can be caught
      synchronized(this)
      {
         String ip     = request.getRemoteAddr().toString();
         String uri    = request.getRequestURI();
         String domain = Web.getDomainName( request );
         
         ContentInclude.noCache( response );
         response.setContentType( Web.getContentType(uri) );
         try
         {
            if( request.getRequestURL().toString().contains("://m.") )
            {
               Queries.track( "mobile_hit", ip, domain );
            }
            else
            {
               Queries.track( "hit", ip, domain );
            }
            if( uri.endsWith("/form.png") )
            {
               String service = (String) request.getSession().getAttribute( "service" );
                      service = service==null?"":service;
               Queries.track( "impression", service, ip, domain );
            }
         }
         catch( Exception e )
         {
         }
      }
   }
   
   public void doPost( HttpServletRequest request, HttpServletResponse response )
   {
      doGet( request, response );
   }
}

