package com.servlets;

import java.io.*;
import com.util.*;
import java.net.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.bl.*;

public class Redirect extends HttpServlet
{
   protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
   {
      String download   =  "Y";   // request.getParameter("download");
      String domain     =  Web.getDomainName( request );
      String uid        =  Web.getCookieValue( request, "uid" );
      if( uid.equals("") )
      {
         uid = (String) User.getSessionProfile( request ).get("id");
         uid = uid==null?"":uid;
      }
      if( uid.equals("") )
      {
         HashMap data = (HashMap) request.getSession().getAttribute("data");
         if( data!=null )
         {
            uid = (String) data.get("id");
            uid = uid==null?"":uid;
         }
      }


      if( uid.equals("") )
      {
         uid = (String) request.getSession().getAttribute("uid");
         uid = uid==null?"":uid;
      }
      
      
      
      HashMap hm        = Queries.getUserExtra( uid );
      String  xtra      = (String) hm.get( "extra_data" );
      if( xtra!=null || download!=null )
      {
         HashMap data      = new HashMap();
         try
         {
            String  arrData[] = xtra.split( "\n" );
            for( int i=0; i<arrData.length; i++ )
            {
               String split[] = arrData[i].split( "\t" );
               try { data.put( split[0], split[1] ); } catch( Exception e ) {}
            }
         }
         catch( Exception e )
         {
         }
         
         String  service = "";
         String  state   = "";
         String  u       = request.getParameter( "u" );
         String  t       = request.getParameter( "t" );
         
         try
         {
            String form[] = ( (String) data.get("_FORM_") ).split( "/" );
            try { service = Format.initCap( form[1]);                       } catch( Exception e1 ) {}
            try { state   = Format.initCap( form[2].replace(".html", "") ); } catch( Exception e1 ) {}
         }
         catch( Exception e )
         {
            Log.log(e);
         }
         
         StringBuilder sb = new StringBuilder("");
         try
         {
            if( state==null )   state   = "";
            if( service==null ) service = "";
            if( !uid.equals("") && !state.trim().equals("") && !service.trim().equals("") )
            {
               uid = uid.replace( " ", "" ); 
               /*
               DB.executeSQL( "delete from track_download where user_id=" + uid + " and typ='" + t + "'" );
               sb.append( "insert into track_download (user_id, typ, state, service, stamp) values (" );
               sb.append( uid ).append( "," );
               sb.append( "'" ).append(t).append(       "'," );
               sb.append( "'" ).append(state).append(   "'," );
               sb.append( "'" ).append(service).append( "', sysdate())" );
               DB.executeSQL( sb.toString() );
               */

               synchronized(this) { DB.executeSQL( "update payment_transaction_history set download=1 where user_id=" + uid ); }
               synchronized(this) { DB.executeSQL( "update payment_transaction         set download=1 where user_id=" + uid ); }
               synchronized(this) { DB.executeSQL( "update payment_pending             set download=1 where user_id=" + uid ); }

            }
         }
         catch( Exception e )
         {
            Log.log(e);
         }
         
         
         
         /*---------------------------------------------*/
         /*-- JSP notifier ...                       ---*/
         /*---------------------------------------------*/
         try
         { 
            StringBuilder notifier = new StringBuilder( "/content/" );
            notifier.append( domain ).append( "/flow/postProxy.jsp?type=" ).append( t ).append( "&user_id=" ).append( uid );
            request.getRequestDispatcher( notifier.toString() ).include( request, response );
         }
         catch( Exception e )
         {
            Log.log(e);
         }
        Web.redirect( response, Web.MOVE_PERMANENT, u );
      }
      else
      {
         Log.log( Log.ERROR, "Poxy Missing Params : \"" + request.getRequestURL() + "?" + request.getQueryString() );
         Web.send404( response );
      }
   }
}


