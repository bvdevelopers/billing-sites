package com.servlets;

import com.util.*;
import com.bl.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Unsubscribe extends HttpServlet
{
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   {
   }
   
   public void doPost( HttpServletRequest request, HttpServletResponse response )
   {
      response.setContentType( "text/html" );
      try
      {
         PrintWriter out   =  response.getWriter();
         String    uuid    =  request.getParameter( "uuid"  );
         String    email   =  request.getParameter( "email" ).trim();
         boolean  exists   =  (DB.getData("select * from user where id="+uuid+" and email='"+email+"' union select * from user_lead where id="+uuid+" and email='"+email+"'").size()>0);
         if( exists )
         {
            synchronized(this) { try { DB.executeSQL( "insert into deleted select *, sysdate() from user_lead where id="+uuid+" and email='"+email+"'" );   }  catch( Exception e ) { Log.log(e); } }
            synchronized(this) { try { DB.executeSQL( "delete from user_lead where id="+uuid+" and email='"+email+"'" );                                    }  catch( Exception e ) { Log.log(e); } }
            synchronized(this) { try { DB.executeSQL( "insert into deleted select *, sysdate() from user where id="+uuid+" and email='"+email+"'" );        }  catch( Exception e ) { Log.log(e); } }
            synchronized(this) { try { DB.executeSQL( "delete from user where id="+uuid+" and email='"+email+"'" );                                         }  catch( Exception e ) { Log.log(e); } }

            request.getRequestDispatcher( "/unSendGrid.jsp?email="+email ).include( request, response );
            out.print( "<script>alert('You have been succesfully removed');  parent.document.location.replace('/');</script>" );
         }
         else
         {
            out.print( "<script> alert('"+email+" was not found!');</script>" );
         }
      }
      catch( Exception e )
      {
         e.printStackTrace();
      }
   }
}

