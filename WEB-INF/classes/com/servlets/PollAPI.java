package com.servlets;

import java.io.*;
import java.util.*;
import com.util.*;
import com.bl.*;
import javax.servlet.http.*;
import javax.servlet.*;

public class PollAPI extends HttpServlet
{
   private static String  ips[]     =  { "74.212.161.126", "50.63.137.224" };
   private static HashMap whitelist =  new HashMap();;
   static
   {
      for( int i=0; i<ips.length; i++ )
         whitelist.put( ips[i], "" );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   {
      try
      {

         PrintWriter out   =  response.getWriter();

         String      id    =  request.getParameter( "id" );
         if( id==null || whitelist.get( Web.getRemoteIP(request) )==null )
         {
            Web.send404( response );
            return;
         }
         String      currentData    =  (String) Queries.getUserExtra( id ).get( "extra_data" );
         HashMap     currentHash    =  Format.tsv2Hash( currentData );
         HashMap     currentParams  =  Web.getParameters( request, "poll." );
         currentParams.remove( "poll.id" );
         currentHash.putAll( currentParams );
         Queries.saveUserExtra( id, Format.Hash2TSV(currentHash) );
      }
      catch( Exception e )
      {
         Log.log( e );
      }
   }

   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doPost( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException
   {
      doGet( request, response );
   }
}
