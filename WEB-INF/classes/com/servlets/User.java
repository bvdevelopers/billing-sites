package com.servlets;

import java.io.*;
import java.util.*;
import com.util.*;
import javax.servlet.http.*;
import javax.servlet.*;

public class User
{
   private static final int    COOKIE_DAYS      = 30;
   private static final String COOKIE_NAME      = "profile";
   public  static final String PROFILE_STRUCT[] = { "id",
                                                    "first_name",
                                                    "middle_name",
                                                    "last_name",
                                                    "prefix",
                                                    "suffix",
                                                    "date_of_birth",
                                                    "gender",
                                                    "marital_status",
                                                    "address_1",
                                                    "suite_1",
                                                    "city_1",
                                                    "state_1",
                                                    "zip_1",
                                                    "phone_1",
                                                    "phone_2",
                                                    "email",
                                                    "address_2",
                                                    "suite_2",
                                                    "city_2",
                                                    "state_2",
                                                    "usps_forward",
                                                    "moving_type",
                                                    "moving_time",
                                                    "zip_2"
                                                  };
   
   /*-------------------------------------------------------------------------*/
   /*--- getSessionProfile ...                                             ---*/
   /*-------------------------------------------------------------------------*/
   public static HashMap getSessionProfile( HttpServletRequest request )
   {
      try
      {
         String  cookieValue = Web.getRawCookieValue( request, COOKIE_NAME );
         return Crypto.decrypt( cookieValue );
      }
      catch( Exception e )
      {
         Log.log( e );
         return new HashMap();
      }
   }
   

   /*-------------------------------------------------------------------------*/
   /*--- setSessionProfile ...                                             ---*/
   /*-------------------------------------------------------------------------*/
   public static void setSessionProfile( HttpServletResponse response, HashMap data )
   {
      try
      {
         if( data!=null )
         {
            HashMap  hm     = new HashMap();
            for( int i=0; i<PROFILE_STRUCT.length; i++ )
            {
               try
               {
                  String val = (String) data.get(PROFILE_STRUCT[i]);
                  val = val==null?"":val;
                  hm.put( PROFILE_STRUCT[i], val );
               }
               catch( Exception e )
               {
                  // just ignore it
               }
            }
            
            Log.log( Log.INFO, "Gather user profile : " + hm.toString() );
            String crypt = Crypto.encrypt( hm );
            Log.log( Log.INFO, "crypted user profile : " + crypt );
            //Web.setCookieValue( response, COOKIE_NAME, crypt, COOKIE_DAYS );
            Web.setCookieValue( response, COOKIE_NAME, crypt );
         }
      }
      catch( Exception e )
      {
         Log.log( e );
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- logout ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   public static void logout( HttpServletRequest request, HttpServletResponse response )
   {
      Web.destroyCookie( request, response, COOKIE_NAME );
   }

   /*-------------------------------------------------------------------------*/
   /*--- templify ...                                                      ---*/
   /*--- temporary no longer in use ...                                    ---*
   /*-------------------------------------------------------------------------*/
   public static String templify( HttpServletRequest request, String content )
   {
      HashMap userSession = getSessionProfile( request );
      String  newContent  = content==null?"":content;
      for( int i=0; i<PROFILE_STRUCT.length; i++ )
      {
         String val = (String) userSession.get( PROFILE_STRUCT[i] );
         val = val==null?"":val;
         newContent = newContent.replace( "{profile." + PROFILE_STRUCT[i] + "}", val );
      }
      
      
      HashMap data = (HashMap) request.getSession().getAttribute( "data" );
      if( data!=null )
      {
         Iterator it = data.keySet().iterator();
         while( it.hasNext() )
         {
            String k = (String) it.next();
            String v = (String) data.get(k);
            newContent = newContent.replace( "{profile." + v + "}", v );
         }
      }
      
      
      newContent = newContent.replace( "{profile}", userSession.toString() );
      return newContent;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- stripHash ...                                                     ---*/
   /*-------------------------------------------------------------------------*/
   public static HashMap stripHash( HashMap hm )
   {
      HashMap newHm = new HashMap(hm);
      for( int i=0; i<PROFILE_STRUCT.length; i++ )
         newHm.remove( PROFILE_STRUCT[i] );
      return newHm;
   }

   /*-------------------------------------------------------------------------*/
   /*--- jsHash ...                                                        ---*/
   /*-------------------------------------------------------------------------*/
   public static String jsHash( HttpServletRequest request )
   {
      HashMap userSession = getSessionProfile( request );
      StringBuffer jsCode = new StringBuffer();
      for( int i=0; i<PROFILE_STRUCT.length; i++ )
      {
         String v = (String) userSession.get( PROFILE_STRUCT[i] );
         v = v==null?"":v;
         jsCode.append("$('.").append("profile_").append(PROFILE_STRUCT[i]).append("').html(\"").append(v).append("\"); " );
         jsCode.append("_PROFILE[\"").append(PROFILE_STRUCT[i]).append("\"]=\"").append(v).append("\"; ");
      }

      HashMap data = (HashMap) request.getSession().getAttribute( "data" );
      if( data!=null )
      {
         Iterator it = data.keySet().iterator();
         while( it.hasNext() )
         {
            String k = (String) it.next();
            String v = (String) data.get(k);
            jsCode.append("_PROFILE[\"").append(k).append("\"]=\"").append(v).append("\"; ");
         }
      }

      return jsCode.toString();
   }
}
