package com.servlets;

import com.util.*;
import java.util.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class CSS extends HttpServlet
{
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   {
      ContentInclude.noCache( response );
      response.setContentType( "text/css" );
      
      String         protocol  =  request.isSecure()?"https://":"http://";
      String         domain    =  Web.getDomainName( request );
      String         fileName  =  Format.lastToken( request.getRequestURI().toString(), "/" );
      boolean        isDev     =  request.getRequestURL().toString().contains("://dev.");
      StringBuffer   link      =  new StringBuffer( protocol );
      link.append( "s3.amazonaws.com/" ).append( (CR.is(domain)?"n":"") + domain ).append("/").append( fileName );
    
      Web.redirect( response, Web.MOVE_PERMANENT, link.toString() );
   }
}
