package com.servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import com.bl.*;

public class ContentInclude
{
   public static String includesCoreJS( boolean isSecure )
   {
      String  content = "";
      content += "<script src=\"/jquery.js\"></script>";
      content += "<script src=\"/placeholder.js\"></script>";
      content += "<script src=\"/jquery.colorbox.js\"></script>";
      content += "<script src=\"/jquery-ui.js\"></script>";
//    content += "<script src=\"//api.co-dm.com/mediata.jsp\"></script>";
      content += "<script src=\"/main.js\"></script>";
      return content;
   }

   public static String includesJS( boolean isSecure )
   {
      String  content = "";
      //content += "<script src='/main.js'></script>";
      return content;
   }

   public static String includesCSS( boolean isSecure )
   {
      String content = "";
      content  = "<link rel=\"stylesheet\" type=\"text/css\" href=\"/colorbox.css\">";
      content += "<link rel=\"stylesheet\" type=\"text/css\" href=\"/form.css\">";
      content += "<link rel=\"stylesheet\" type=\"text/css\" href=\"/jquery-ui.css\">";
      return content;
   }

   public static void noCache( HttpServletResponse response )
   {
      response.setHeader( "Cache-Control",   "no-cache" );
      response.setHeader( "Cache-Control",   "no-store" );
      response.setHeader( "Pragma",          "no-cache" );
   }
}
