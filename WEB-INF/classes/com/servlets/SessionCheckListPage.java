package com.servlets;

import com.util.*;
import com.bl.*;
import java.util.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class SessionCheckListPage extends HttpServlet
{
   public void doGet( HttpServletRequest request, HttpServletResponse response )
   {
      try
      {
         String u = (String)  request.getSession().getAttribute( "checklist" );
         if( u!=null )
         {
            Web.redirect( response, Web.MOVE_PERMANENT, "/checklist" + u );
            return;
         }
      }
      catch( Exception e )
      {
      }
      Web.send404( response );
   }
}

