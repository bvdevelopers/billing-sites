package com.servlets;

import com.util.*;
import com.bl.*;
import java.util.*;
import jxl.*;
import java.util.concurrent.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.regex.*;
import org.jsoup.*;

public class Content extends HttpServlet
{
// final static String PROD_SERVER = "LAUSAWWW01";
   final static String PROD_SERVER = "ip-10-0-0-233";
   
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response ) throws IOException
   {
      String ref = (String) request.getSession().getAttribute("uri");
      if( ref==null )
      {
         ref = request.getRequestURI().toString() + "?" + request.getQueryString();
         request.getSession().setAttribute( "uri", ref );
      }
      request.getSession().setMaxInactiveInterval( 1800 );
      
      try
      {
         request.getSession().setAttribute( "id",    request.getParameter("id"   ) );
         request.getSession().setAttribute( "email", request.getParameter("email") );
      }
      catch( Exception e )
      {
      }
      
      String __UUIIDD = request.getParameter( "__UUIIDD" );
      if( __UUIIDD != null )
      {
         Web.setCookieValue( response, "uid",      __UUIIDD  );
         request.getSession().setAttribute( "uid", __UUIIDD  );
      }
      
      Traffic.trackSource( request );
      String   uri  = request.getRequestURI().toString().toLowerCase();
      String   type = request.getParameter( "type" );
      
      type = type==null?"html":type;
      
      if( uri.endsWith(".html") )
         response.setContentType( "text/html;charset=UTF-8" );
      else
         response.setContentType( Web.getContentType(uri) );
      
      ContentInclude.noCache( response );
      String     url = request.getRequestURL().toString().toLowerCase();
      String content = Cache.get( url+type );
      if( content==null || content.contains("{State}") || content.contains("{state}") || content.contains("{STATE}") )
      {
         //---- url validation, if state make sure it's properly formated, checks it must be secure, makes sure it's not www.
         if( Web.stateURL( request, response ) )
            return;
         if( !Web.noWWW( request, response ) )
            return;
         if( Web.ssl( request, response ) )
            return;
         
         synchronized(this)
         {
            //--- in case content was loaded while this thread was waiting, no need to reload
            content = Cache.get( url+type );
            if( content==null )
            {
               String   domain   = Web.getDomainName(request);
               String   subDir   = uri.endsWith("html")?"html":"seo";
               String   realPath = Web.getRealPath( request, "/content/" + domain + "/" + subDir + "/" + uri );
               
               try
               {
                  content = IO.loadASCIIFile( realPath );
                  if( !type.equals("plain") )
                     content = templify( request, content );
                  else
                     content = Web.cleanUpHTML( content );
                  content = templifyState(      request,  content );
                  content = templifyCounty(     request,  content );
                  content = templifyProfile( content );
                  content = templifyCCPayment( content );
                  content = templifyPricePoint( domain, content );
                  content = templifyProxy( content );
               }
               catch( FileNotFoundException fnf1 )
               {
                  Log.log( Log.INFO, realPath + " NOT FOUND" );
                  try
                  {
                     if( !Web.isStateURL(uri) )
                     {
                        Log.log( Log.INFO, realPath + " is NOT a State URL" );
                        Web.send404( response );
                        return;
                     }
                     String stateNameURL = Format.lastToken( uri, "/" ).replace( ".html", "" );
                     realPath = realPath.replace( stateNameURL, "#state#" );
                     content  = Cache.get( realPath );
                     if( content==null )
                     {
                        try
                        {
                           content  = IO.loadASCIIFile( realPath, true );
                           content  = templify( request, content );
                           if( !request.getRequestURL().toString().contains("://dev.") )
                              Cache.set( realPath, content );
                        }
                        catch( FileNotFoundException fnf2 )
                        {
                           Web.send404( response );
                           return;
                        }
                     }
                  }
                  catch( FileNotFoundException fnf2 )
                  {
                     Web.send404( response );
                     return;
                  }
                  content = templifyState(      request,  content );
                  content = templifyCounty(     request,  content );
                  content = templifyProfile( content );
                  content = templifyCCPayment( content );
                  content = templifyPricePoint( domain, content );
                  content = templifyProxy( content );
               }
               
               boolean isSecure = request.isSecure();
               content = Web.insertHTML( content, "head", ContentInclude.includesJS( isSecure )        );
               content = Web.insertHTML( content, "head", ContentInclude.includesCoreJS( isSecure )    );
               content = Web.appendHTML( content, "head", ContentInclude.includesCSS( isSecure )       );
               content = Web.appendHTML( content, "body", "<img class=\"imgmain\" src=\"/img/main.png\">"  );
               if( content.contains("<form") )
               {
                  content   = Web.appendHTML( content, "head", "<script src=\"/form.js\"></script>"  );
               }
               if( !request.getRequestURL().toString().contains("://dev.") )
                  Cache.set( url+type, content );
               content = Format.purifyText( content );
            }
         }
      }
      PrintWriter out = response.getWriter();
      out.print( content );
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- templify ...                                                      ---*/
   /*--- the template includes the html unlike teh templify where html     ---*/
   /*--- includes template (see example below ...)                         ---*/
   /*--- also ... merges <head> content and insert templetes               ---*/
   /*---          html code                                                ---*/
   /*---          ---------                                                ---*/
   /*---          <!--$fulltemplate.html$-->                               ---*/
   /*---          blah blah blah                                           ---*/
   /*---          <!--/$fulltemplate.html$-->                              ---*/
   /*---                                                                   ---*/
   /*---          template/fulltemplate.html                               ---*/
   /*---          --------------------------                               ---*/
   /*---          <html>                                                   ---*/
   /*---             <body>                                                ---*/
   /*---                <!--$content$-->                                   ---*/
   /*---             </body>                                               ---*/
   /*---          </html>                                                  ---*/
   /*---                                                                   ---*/
   /*---          also replace url keywords                                ---*/
   /*---          --------------------------                               ---*/
   /*---          http://whatever/folder/subfolder/florida.html            ---*/
   /*---          {url[0]} replaces with Florida                           ---*/
   /*---          {url[1]} replaces with Subfolder                         ---*/
   /*---          {url[2]} replaces with Folder                            ---*/
   /*---                                                                   ---*/
   /*---          output                                                   ---*/
   /*---          ------                                                   ---*/
   /*---          <html>                                                   ---*/
   /*---             <body>                                                ---*/
   /*---                blah blah blah                                     ---*/
   /*---             </body>                                               ---*/
   /*---          </html>                                                  ---*/
   /*-------------------------------------------------------------------------*/
   public static String templify( HttpServletRequest request, String content )
   {
      String domain     = Web.getDomainName(request);
      String newContent = content;
      Log.log( Log.INFO, "templify" );
      String  folder     = Web.getRealPath( request, "/" ) + "/content/" + Web.getDomainName(request);
      boolean need2Cache = !request.getRequestURL().toString().contains("://dev.");
      try
      {
         //--- extracting & stripping head code
         String headContent = Web.extractHTML( newContent, "head" );
         newContent  = Web.stripHTML(   newContent, "head" );
         
         //--- Handling <!--$templification$--> and <!--/$templification$-->
         try
         {
            int             posStart = content.indexOf( "<!--$" );
            int               posEnd = content.indexOf( "$-->"  );
            String           tagName = content.substring( posStart+5, posEnd ).trim();
            Log.log( Log.INFO, "templify Tag Name = \"" + tagName + "\"" );
            String   templateContent = IO.loadASCIIFile( folder + "/template/" + tagName, need2Cache );
            String           flagTag = "<!--$content$-->";
            int           flagTagPos = templateContent.indexOf( flagTag );
            String            before = templateContent.substring( 0, flagTagPos );
            String             after = templateContent.substring( flagTagPos + flagTag.length() );
            newContent = before + newContent.replace( "<!--$" + tagName + "$-->", "" ).replace( "<!--/$" + tagName + "$-->", "" )  + after;
         }
         catch( StringIndexOutOfBoundsException e )
         {
            // --- ignore as page is not templified
         }
         
         //--- Handling <!--templification-->
         String              ls[] = (new File(folder+"/template")).list();
         
         for( int i=0; i<ls.length; i++ )
         {

            if( ls[i].endsWith(".html") )
            {
               String cntTemplate = getStaticTemplate( folder + "/template", ls, ls[i], need2Cache );
               newContent = newContent.replace( "<!--" + ls[i] + "-->", cntTemplate );
            }
         }
         
         //--- merging extracted <head> content to regual <head>
         //newContent = Web.appendHTML( newContent, "head", "<meta charset=\"utf-8\" />" );
         newContent = Web.appendHTML( newContent, "head", headContent );
         
         //--- Templifying state
         if( !request.getRequestURL().toString().toLowerCase().contains("/forms/") )
         {
            //--- Templifying url
            String cleanU  = request.getRequestURL().toString().replace("http://", "").replace("https://", "").replace(domain, "");
            String u[]     = cleanU.split( "/" );
            for( int i=0; i<u.length; i++ )
            {
               newContent = newContent.replace( "{url[" + (u.length - i) + "]}", u[i].replace(".html", "")  );
               newContent = newContent.replace( "url[" + (u.length - i) + "]", u[i].replace(".html", "")  );
               newContent = newContent.replace( "{Url[" + (u.length - i) + "]}", Format.initCap(u[i].replace(".html", "").replace("-", " ")) );
               newContent = newContent.replace( "Url[" + (u.length - i) + "]", Format.initCap(u[i].replace(".html", "").replace("-", " ")) );
               newContent = newContent.replace( "{URL[" + (u.length - i) + "]}", (u[i].replace(".html", "").replace("-", " ")).toUpperCase() );
               newContent = newContent.replace( "URL[" + (u.length - i) + "]", (u[i].replace(".html", "").replace("-", " ")).toUpperCase() );
            }
         }
         newContent = templifyCSSCloud(   request,  newContent );
         newContent = newContent.replace( "{domain}", domain ); 
         newContent = newContent.replace( "{DOMAIN}", domain.toUpperCase() );
         return Web.minimumHTML(newContent);
      }
      catch( Exception e )
      {
         Log.log( e );
         return Web.minimumHTML(content);
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- templifyProfile ...                                               ---*/
   /*--- replace database {Profile.###} with proper spans                  ---*/
   /*-------------------------------------------------------------------------*/
   private static String templifyProfile( String content )
   {
      String newCnt = content;
      for( int i=0; i<User.PROFILE_STRUCT.length; i++ )
         newCnt = newCnt.replace( "{Profile." + User.PROFILE_STRUCT[i] + "}", "<span class=\"profile_"+User.PROFILE_STRUCT[i]+"\"></span>" );
      return newCnt;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- templifyCCPayment ...                                             ---*/
   /*--- replace session {Payment.###} with proper spans                   ---*/
   /*-------------------------------------------------------------------------*/
   private static String templifyCCPayment( String content )
   {
      String newCnt = content;
      for( int i=0; i<Payment.PAYMENT_STRUCT.length; i++ )
         newCnt = newCnt.replace( "{Payment." + Payment.PAYMENT_STRUCT[i] + "}", "<span class=\"payment_"+Payment.PAYMENT_STRUCT[i]+"\"></span>" );
      return newCnt;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- templifyProxy ...                                                 ---*/
   /*--- replace {Proxy.<link>|<trackCode>} with proxy link                ---*/
   /*-------------------------------------------------------------------------*/
   private static String templifyProxy( String content )
   {
      Pattern  regex          =  Pattern.compile( "\\{Proxy([^}]*)\\}", Pattern.MULTILINE );
      Matcher  regexMatcher   =  regex.matcher( content );
      String   newCnt         =  content;
      while( regexMatcher.find() )
      {
         String match    = regexMatcher.group();
         String next     = match.replace( "{", "" ).replace( "}", "" );
         String proxy[]  = next.split( "\\|" );
         String link     = proxy[0].replace("Proxy.", "");
         String track    = "";
         try { track = proxy[1]; } catch( Exception e ) {}
         try { link  = "/proxy?t=" + track + "&u=" + URLEncoder.encode( link, "UTF-8" ); } catch( Exception e ) {}
         newCnt = newCnt.replace( match, link );
      }
      return newCnt;
   }

   
   /*-------------------------------------------------------------------------*/
   /*--- templifyPricePoint ...                                            ---*/
   /*-------------------------------------------------------------------------*/
   private static String templifyPricePoint( String domain, String html )
   {
      Vector  v    = DB.getData( "select type, price from new_pricepoint where domain='"+domain+"'" );
      String  code = html;
      for( int i=0; i<v.size(); i++ )
      {
         try
         {
            HashMap h    = (HashMap) v.elementAt(i);
            String  type = (String) h.get("type");
            String  pr   = (String) h.get("price");
            String  og   = "{pricepoint."+type+"}";
            double  pp   = Double.parseDouble( pr );
            code = code.replace( og, Format.format(pp,2) );
         }
         catch( Exception e )
         {
         }
      }
      return code;
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- templifyCounty ...                                                ---*/
   /*--- replace database #marks# with matching database content           ---*/
   /*-------------------------------------------------------------------------*/
   private static String templifyCounty( HttpServletRequest request, String content )
   {
      String       returnContent =  content;
      String              county = Format.lastToken( request.getRequestURI().toString(), "/" ).replace( ".html", "" );
      String                path =  Web.getRealPath( request, "/" ) + "content/extra/state_lookup";
      ConcurrentHashMap       hm =  ExcelForms.loadExtra( path );
      Vector            allMarks =  Format.extractMarks( content, "{", "}" );
      for( int i=0; i<allMarks.size(); i++ )
      {
         String mark    = (String) allMarks.elementAt(i);
         if( mark.startsWith("county.") )
         {
            String split[] = mark.split( "\\." );
            try
            {
               HashMap hm0 = (HashMap) hm.get(  split[1] );
               HashMap hm1 = (HashMap) hm0.get( split[2] );
               HashMap hm2 = (HashMap) hm1.get( county.toUpperCase() );
               String  val = (String) hm2.get( split[3] );
               returnContent = returnContent.replace( "{"+mark+"}", val );
            }
            catch( Exception e )
            {
               Log.log(e);
            }
         }
      }
      return returnContent;
   }

   /*-------------------------------------------------------------------------*/
   /*--- templifyState ...                                                 ---*/
   /*--- replace database #marks# with matching database content           ---*/
   /*-------------------------------------------------------------------------*/
   static String templifyState( HttpServletRequest request, String content )
   {
      Log.log( Log.INFO, "templifyState" );
      
      String   state     = Format.lastToken( request.getRequestURI().toString(), "/" ).replace( ".html", "" );
      String   stateCode = (String) Static.STATES.get( state );
      //--- it's not a state URL ---
      if( stateCode==null )
         return content;
      
      
      String            path           =  Web.getRealPath( request, "/" ) + "content/extra/state_lookup";
      ConcurrentHashMap hm             =  ExcelForms.loadExtra( path );
      Vector            allMarks       =  Format.extractMarks( content, "{", "}" );
      String            returnContent  =  content;
      for( int i=0; i<allMarks.size(); i++ )
      {
         String mark    = (String) allMarks.elementAt(i);
         String split[] = mark.split( "\\." );
         if( split.length == 3 )
         {
            try
            {
               HashMap hm1 = (HashMap) hm.get(  split[0]  );
               HashMap hm2 = (HashMap) hm1.get( split[1]  );
               HashMap hm3 = (HashMap) hm2.get( stateCode );
               String  val = (String)  hm3.get( split[2] );
               returnContent = returnContent.replace( "{"+mark+"}", val );
            }
            catch( Exception e )
            {
               Log.log( Log.ERROR, "mark " + mark + " not matching" );
            }
         }
      }
      returnContent = returnContent.replace( "{State}",  Format.initCap(state)   );
      returnContent = returnContent.replace( "{state}",  state.toLowerCase()     );
      returnContent = returnContent.replace( "{STATE}",  state.toUpperCase()     );
      returnContent = templifyExtraStateInfo( request, returnContent, state );
      return returnContent;
   }

   /*-------------------------------------------------------------------------*/
   /*--- templifyExtraStateInfo ...                                        ---*/
   /*--- Matches state info with info from Excel                           ---*/
   /*-------------------------------------------------------------------------*/
   public static String templifyExtraStateInfo( HttpServletRequest request, String content, String state )
   {
      boolean isState = (content.indexOf( "{state." )>0);
      int    posStart = 0;
      if( isState )
         posStart = content.indexOf( "{state." );
      else
         posStart = content.indexOf( "{county." );
      if( posStart==-1 )
         return content;
      String newCnt  =  content.substring( posStart );
      int posEnd     =  newCnt.indexOf( "}" );
      String word    =  newCnt.substring( 0, posEnd + 1 );
      String mapWord =  mapWord( request, word, state );
      newCnt = content.replace( word, mapWord );
      newCnt = templifyExtraStateInfo( request, newCnt, state );
      return newCnt;
   }
   
   private static HashMap hmMapMatrix = new HashMap();
   private static String mapWord( HttpServletRequest request, String word, String state )
   {
      String cleanWord[] = word.replace( "{", "" ).replace( "}", "" ).split("\\.");
      String fileName    = Web.getRealPath( request, "/content/extra/state_lookup/" + cleanWord[1] + ".xls" );
      String mapState    = state.trim();
      if( mapState.length()!=2 )
         mapState = ((String) Static.STATE_REVERSE_LOOKUP.get( state )).toUpperCase();
      try
      {
         Sheet sheets[] = (Sheet[]) hmMapMatrix.get( fileName );
         if( sheets==null )
         {
            Log.log( Log.INFO, "Load " + fileName );
            FileInputStream   fip      =  new FileInputStream( fileName );
            Workbook          workbook =  Workbook.getWorkbook( fip );
            sheets =  workbook.getSheets();
            if( !request.getRequestURL().toString().contains("://dev.") )
               hmMapMatrix.put( fileName, sheets );
            fip.close();
         }
         Sheet sheet = null;
         //--- capture the needed sheet
         for( int i=0; i<sheets.length; i++ )
         {
            if( sheets[i].getName().equals(cleanWord[2]) )
            {
               sheet = sheets[i];
               break;
            }
         }
         if( sheet==null )
            return "";
         
         //--- detecting the column needed
         Cell[] firstRow = sheet.getRow(0);
         int    column   = 0;
         for( int i=0; i<firstRow.length; i++ )
         {
            if( firstRow[i].getContents().equals(cleanWord[3]) )
               column=i;
         }
         if( column==0 )
            return "";
         
         
         //--- match word with state
         String matchWord = "";
         for( int i=1; i<sheet.getRows(); i++ )
         {
            try
            {
               Cell[] cells     = sheet.getRow(i);
               String stateCol  = cells[0].getContents().trim();
               if( stateCol.equals(mapState) )
                  matchWord = cells[column].getContents();
            }
            catch( Exception e )
            {
            }
         }
         //fip.close();
         matchWord = matchWord==null?"":matchWord.trim();
         return matchWord;
      }
      catch( Exception e )
      {
         e.printStackTrace();
      }
      return "";
   }

   
   /*-------------------------------------------------------------------------*/
   /*--- getStaticTemplate ...                                             ---*/
   /*--- this used to generate forms as well ...                           ---*/
   /*--- I need to review where I put that method as it does ...           ---*/
   /*--- not really belong here ...                                        ---*/
   /*-------------------------------------------------------------------------*/
   private static String getStaticTemplate( String folder, String allTemplates[], String template, boolean cache )
   {
      try
      {
         String    templateFile     =  folder + File.separator + template;
         if( !templateFile.endsWith(".html") || templateFile.contains("\n") || templateFile.contains("\r") || templateFile.contains("$content$") )
            return "";
         String    templateContent  =  IO.loadASCIIFile( templateFile, cache );
         Vector    comments         =  Web.extractHTMLComments( templateContent );
         for( int i=0; i<comments.size(); i++ )
         {
            String comment = (String) comments.elementAt( i );
            if( comment.contains("[") )
               continue;
            templateContent = templateContent.replace( "<!--" + comment + "-->", getStaticTemplate( folder, allTemplates, comment, cache ) );
         }
         return templateContent;
      }
      catch( FileNotFoundException e )
      {
         // no action needed since we consider it really means an html content
         return "";
      }
      catch( IOException eio )
      {
         Log.log( eio );
         return "NOT FOUND";
      }
   }
   
   /*-------------------------------------------------------------------------*/
   /*--- templifyCSSCloud ...                                              ---*/
   /*--- replace href='@... with proper cloud link accordignily            ---*/
   /*-------------------------------------------------------------------------*/
   static String templifyCSSCloud( HttpServletRequest request, String content )
   {
      String       hostName   =  Web.hostname();
      String       newContent =  content;
      String         protocol =  request.isSecure()?"https://":"http://";
      String           domain =  Web.getDomainName( request );
      StringBuffer       link =  new StringBuffer( protocol );
      String                u =  request.getRequestURL().toString();
      
      if( u.contains("://local.") || u.contains("://dev.") )
      {
         link.append( "s3." ).append(domain).append("/").append(domain);
      }
      else
      {
         link.append( "s3.amazonaws.com/" ).append( (CR.is(domain)?"n":"") +  domain);
      }    
      
      newContent = newContent.replace( " href='@",  " href='"  + link );
      newContent = newContent.replace( " href=\"@", " href=\"" + link );
      newContent = newContent.replace( "{CSS-CLOUD}", link.toString() );
      
      return newContent;
   }
}
