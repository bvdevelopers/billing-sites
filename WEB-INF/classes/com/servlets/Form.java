package com.servlets;

import com.bl.*;
import com.util.*;
import java.util.*;
import java.util.concurrent.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class Form extends HttpServlet
{
   /*-------------------------------------------------------------------------*/
   /*--- doGet ...                                                         ---*/
   /*-------------------------------------------------------------------------*/
   public void doGet( HttpServletRequest request, HttpServletResponse response ) throws IOException
   {
      String ref = (String) request.getSession().getAttribute("uri");
      if( ref==null )
      {
         ref = request.getRequestURI().toString() + "?" + request.getQueryString();
         request.getSession().setAttribute( "uri", ref );
      }
      request.getSession().setMaxInactiveInterval( 1800 );

      String   domain     = Web.getDomainName(request);
      String   uri        = request.getRequestURI().toString().toLowerCase();
      String   splitURI[] = uri.split( "/" );
      String   service    = splitURI[splitURI.length-2];
      
      Traffic.trackSource( request );
      
      request.getSession().setAttribute( "service", service );
      request.getSession().setMaxInactiveInterval( 900 );
      
      response.setContentType( Web.getContentType(uri) );
      ContentInclude.noCache( response );
      
      String url        =  request.getRequestURL().toString().toLowerCase();
      String content    =  Cache.get( url );
      
      if( content==null )
      {
         //---- url validation, if state make sure it's properly formated, checks it must be secure, makes sure it's not www.
         if( Web.stateURL( request, response ) )
            return;
         if( !Web.noWWW( request, response ) )
            return;
         if( Web.ssl( request, response ) )
            return;
         //--- content is on form folder
         boolean  debug    =  request.getParameter( "DEBUG" )!=null;
         
         synchronized(this)
         {
            //--- in case content was loaded while this thread was waiting, no need to reload
            content = Cache.get( url );
            if( content==null )
            {
               //--- ----------------------------------------- ---
               //--- look for excel file, 4 levels of fallback ---
               //--- ----------------------------------------- ---
               String realHtmlPath  = "";
               String templatePath  = "";
               String htmlFile      = "";
               String state         = Format.lastToken( uri, "/" );
               if( realHtmlPath.length() == 0 )
               {
                  String realPath   = Format.replaceToken( uri, "/", -1, state );
                         realPath   = Format.replaceToken( realPath, "/", -3, "" );
                         realPath   = Web.getRealPath(request, "/content/" + domain + "/html" + realPath);
                  if( !(new File(realPath)).exists() )
                     realPath = realPath.replace( state, "form.html" );
                  htmlFile          = IO.loadASCIIFile( realPath );
                  htmlFile          = Content.templifyState( request, htmlFile );
                  templatePath      = uri;
                  
                  realHtmlPath      = Web.getRealPath( request, "/content/" + domain + "/html/" + uri );
               }
               Log.log( Log.INFO, "Loading form " + realHtmlPath );
               

               content = Cache.get( realHtmlPath );
                
               if( content==null )
               {
                  try
                  {
                     String      formCode    = htmlFile;

                     StringBuffer formHTML   =  new StringBuffer();

                     String   templateFile   =  realHtmlPath.replace("form", "template/form").replace(".xls", ".html");
                     boolean templateExist   =  (new File(templateFile)).exists();
                     if( !templateExist )
                     {
                        String tmp = Format.extractLastToken( request.getRequestURI(), "/" ) + "/#state#.html";
                        templateFile   =  Web.getRealPath( request, "/content/" + domain + "/template/" + tmp );
                        templateExist   =  (new File(templateFile)).exists();
                        if( templateExist )
                           templatePath   =  tmp;
                     }

                     templatePath = templatePath.replace( ".xls", ".html" );
                     if( !templateExist )
                        formHTML.append( "<!--$/form/__GENERIC__.html$-->" );
                     else
                        formHTML.append( "<!--$" + templatePath + "$-->" );
                     formHTML.append( "<form name=\"frm\" id=\"frm\" target=\"buffer\" action=\"/postForm\" method=\"POST\">" );
                     formHTML.append( formCode );
                     formHTML.append("<input type=\"hidden\" id=\"_FORM_\" name='_FORM_'>" );
                     formHTML.append("<input type=\"hidden\" name='_CAR_DB_' value='").append(Queries.currentCarTable()).append("'>");
                     formHTML.append( "</form>" );
                     if( !templateExist )
                        formHTML.append( "<!--/$/form/__GENERIC__.html$-->" );
                     else
                        formHTML.append( "<!--/$" + templatePath + "$-->" );
                     
                     content = formHTML.toString();
                     content = Content.templify( request, content );
                     
                     boolean isSecure = request.isSecure(); 
		     //CHANGE HERE FROM CHUCKY
                     content = Web.appendHTML( content, "head", ContentInclude.includesCoreJS(isSecure)    );
                     content = Web.appendHTML( content, "head", ContentInclude.includesJS(isSecure)        );
                     content = Web.appendHTML( content, "head", ContentInclude.includesCSS(isSecure)       );
                     content = Web.appendHTML( content, "body", "<script src=\"/form.js\"></script>"  );
                     content = Web.appendHTML( content, "body", "<iframe width='100%' height='200' style=\"display:" + (debug?"block":"none") + ";\" id=\"buffer\" name=\"buffer\"></iframe><p>"  );
                     content = Web.appendHTML( content, "body", "<img class=\"imgform\" src=\"/img/form.png\">"  );
                     
                     if( !request.getRequestURL().toString().contains("://dev.") )
                        Cache.set( realHtmlPath,  content );
                  }
                  catch( Exception e )
                  {
                     Log.log( e );
                  }
               }
               
               if( !request.getRequestURL().toString().contains("://dev.") )
                  Cache.set( url, content );
            }
         }
      }
      PrintWriter  out  = response.getWriter();
      String   postURL  = request.getRequestURI().toString().replace( "/form/", "/postForm/" ).replace( ".html", "" );
      if( content==null )
         Web.send404( response );
      else
         out.print( content.replace("/postForm", postURL) );
   }
}

