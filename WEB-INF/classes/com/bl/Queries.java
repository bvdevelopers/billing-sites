package com.bl;

import java.util.*;
import com.util.*;
import java.util.concurrent.*;

public class Queries
{
   /*-----------------------------------------------------------------------------------*/
   /*--- checkRemoveUser ...                                                         ---*/
   /*-----------------------------------------------------------------------------------*/
   public static boolean checkRemoveUser( String uuid, String email )
   {
      return (DB.getData( "select 1 from toremove where uid='"+uuid+"' and email='"+email+"'" ).size() > 0);
   }
   
   /*-----------------------------------------------------------------------------------*/
   /*--- removeUser ...                                                              ---*/
   /*-----------------------------------------------------------------------------------*/
   public static void removeUser( String table, String email )
   {
      try { DB.executeSQL( "insert into deleted select *, sysdate() from " + table + " where email='"+email+"'" );   }  catch( Exception e ) { Log.log(e); }
      try { DB.executeSQL( "delete from " + table + " where email='"+email+"'" );                                    }  catch( Exception e ) { Log.log(e); }
   }
   
   /*-----------------------------------------------------------------------------------*/
   /*--- userTables ...                                                              ---*/
   /*-----------------------------------------------------------------------------------*/
   public static Vector userTables()
   {
      Vector tables     =  DB.getData( "show tables like 'user_%'" );
      Vector al         =  new Vector();
      for( int i=0; i<tables.size(); i++ )
      {
         String table   = (String) ((HashMap)tables.elementAt(i)).get( "Tables_in_usa (user_%)" );
         String splt[]  = table.split( "_" );
         if( splt.length==2 && splt[1].length()==4 )
         {
            try
            {
               Integer.parseInt( splt[1] );
               al.add( table );
            }
            catch( Exception e )
            {
               e.printStackTrace();
            }
         }
      }
      return al;
   }
   
   /*-----------------------------------------------------------------------------------*/
   /*--- alertLeads ...                                                              ---*/
   /*-----------------------------------------------------------------------------------*/
   public static String alertLeads() throws Exception
   {
      Vector v          = DB.getData( "call leads.alert()" );
      StringBuffer res  = new StringBuffer( "" );
      for( int i=0; i<v.size(); i++ )
      {
         HashMap hm = (HashMap) v.elementAt( i );
         res.append( hm.get("domain") ).append( " : " ).append( hm.get("post") ).append( " mins" );
      }
      return res.toString();
   }
   
   /*-----------------------------------------------------------------------------------*/
   /*--- setAlert ...                                                                ---*/
   /*-----------------------------------------------------------------------------------*/
   public static void setAlert( String domain ) throws Exception
   {
      DB.executeSQL( "call leads.usp_set_alert('"+domain+"')" );
   }
   

   /*-----------------------------------------------------------------------------------*/
   /*--- removeLead ...                                                              ---*/
   /*-----------------------------------------------------------------------------------*/
   public static void removeLead( String domain, String queue, String id )
   {
      String query = "call leads.usp_remove_lead('"+domain+"', '"+queue+"',"+id+" )";
      if( id==null || id.equals("") )
      {
         Log.log( Log.ERROR, query );
         return;
      }
      try
      {
         DB.executeSQL( query );
      }
      catch( Exception e )
      {
         Log.log( Log.EXCEPTION, "REMOVE LEAD : \"" + query + "\"" );
         Log.log( e );
      }
   }
   
   /*-----------------------------------------------------------------------------------*/
   /*--- add2Queue ...                                                               ---*/
   /*-----------------------------------------------------------------------------------*/
   public static void postLead( String domain, String queue, String id, int minutes, String extra ) throws Exception
   {
      StringBuffer query = new StringBuffer();
      query.append( "call leads.usp_set_pending(" );
      query.append( "\"" ).append( domain ).append( "\"," );
      query.append( "\"" ).append( queue ).append( "\"," );
      query.append( id ).append( "," ).append( minutes ).append( "," );
      query.append( "\"" ).append( extra ).append( "\")" );
      DB.executeSQL( query.toString() );
   }
   
   /*-----------------------------------------------------------------------------------*/
   /*--- track ...                                                                   ---*/
   /*-----------------------------------------------------------------------------------*/
   public static void track( String action, String ip, String domain ) throws Exception
   {
      track( action, "", ip, domain );
   }
   public static synchronized void track( String action, String service, String ip, String domain ) throws Exception
   {
      StringBuffer query = new StringBuffer();
      query.append( "call usp_track(" );
      query.append( "\"" ).append(action).append( "\"," );
      query.append( "\"" ).append(service).append( "\"," );
      query.append( "\"" ).append(ip).append(     "\"," );
      query.append( "\"" ).append(domain).append( "\"," );
      query.append( "\"\",\"\",\"\",\"\" )" );
      DB.executeSQL( query.toString() );
   }
   
   /*-----------------------------------------------------------------------------------*/
   /*--- setDownload ...                                                             ---*/
   /*-----------------------------------------------------------------------------------*/
   public static String setDownload( String domain, String path, String userId )
   {
      HashMap hm   = DB.getRecord( "call usp_set_download ( \"" + domain + "\", \"" + path + "\", " + userId + " )" );
      String  pair = userId + "_" + hm.get( "id" );
      return Cypher.encode( pair );
   }
   
   /*-----------------------------------------------------------------------------------*/
   /*--- getDownload ...                                                             ---*/
   /*-----------------------------------------------------------------------------------*/
   public static HashMap getDownload( String downloadId, String userId )
   {
      return DB.getRecord( "call usp_get_download ( " + downloadId + ", " + userId + " )" );
   }
   
   /*-----------------------------------------------------------------------------------*/
   /*--- updateEmail ...                                                             ---*/
   /*-----------------------------------------------------------------------------------*/
   public static void updateEmail( String id, String email )
   {
      try { DB.executeSQL( "update user set email=\""+email+"\" where id=" + id ); } catch( Exception e ) { }
   }

   /*-----------------------------------------------------------------------------------*/
   /*--- getCars ...                                                                 ---*/
   /*-----------------------------------------------------------------------------------*/
   private static ConcurrentHashMap queries = new ConcurrentHashMap();
   public static Vector getCars( String database, String _year, String _make, String _model )
   {
      String   year  = _year;
      String   make  = _make;
      String   model = _model;
      String   db    = database;
      
      year  = year==null?"":year.trim();
      make  = make==null?"":make.trim();
      model = model==null?"":model.trim();
      if( year.equals("") && make.equals("") && model.equals("") )
         return (new Vector());
      db = db==null?((String) DB.getRecord("select * from car.current_table").get( "name" )):db;
      StringBuffer query   = new StringBuffer ("select distinct * from car.car_data_" + db + " where");
      if( !year.equals("") )
         query.append( " year=").append(year);
      if( !make.equals("") )
         query.append( " and make_value=\"").append(make).append("\"");
      if( !model.equals("") )
         query.append( " and model_value=\"").append(model).append("\"");
      query.append( " order by make, model" );
      String sql = query.toString();
      if( sql.contains("(") || sql.contains(" or ") )
         return null;
      
      Vector v = (Vector) queries.get( sql );
      if( v==null )
      {
         v = DB.getData( sql );
         queries.put( sql, v );
      }
      return v;
   }


   /*-----------------------------------------------------------------------------------*/
   /*--- DMVlookup ...                                                               ---*/
   /*-----------------------------------------------------------------------------------*/
   public static Vector DMVlookup( String sZip )
   {
      int      zip      = 33140;
      try { zip = Integer.parseInt(sZip); } catch( Exception e ) {}
      Vector   v1       = DB.getData( "select * from car.dmv_offices where zip = '" + zip + "' order by office" );
      Vector   v2       = new Vector();
      HashMap  proxy    = DB.getRecord( "call usp_zip_proxy('" + zip + "')" );
      String   proxies  = (String) proxy.get( "proxies" );
      if( proxies != null )
      {
         proxies = "('" + proxies.replace( ",", "','" ) + "')";
         v2      = DB.getData( "select * from car.dmv_offices where zip in " + proxies + " order by zip, office" );
      }
      v1.addAll( v2 );
      return v1;
   }
   
   /*-----------------------------------------------------------------------------------*/
   /*--- ShopRepairLookup ...                                                        ---*/
   /*-----------------------------------------------------------------------------------*/
   public static Vector ShopRepairLookup( String sZip )
   {
      int      zip      = 33140;
      try { zip = Integer.parseInt(sZip); } catch( Exception e ) {}
      Vector   v1       = DB.getData( "select * from car.repair_shops where zip = '" + zip + "' order by name" );
      Vector   v2       = new Vector();
      HashMap  proxy    = DB.getRecord( "call usp_zip_proxy('" + zip + "')" );
      String   proxies  = (String) proxy.get( "proxies" );
      if( proxies != null )
      {
         proxies = "('" + proxies.replace( ",", "','" ) + "')";
         v2      = DB.getData( "select * from car.repair_shops where zip in " + proxies + " order by zip, name" );
      }
      v1.addAll( v2 );
      return v1;
   }
   
   /*-----------------------------------------------------------------------------------*/
   /*--- lookupZIP ...                                                               ---*/
   /*-----------------------------------------------------------------------------------*/
   public static HashMap lookupZIP( String sZip )
   {
      /*
      int      zip      = 33140;
      try { zip = Integer.parseInt(sZip); } catch( Exception e ) {}
      return DB.getRecord( "call usp_zip_lookup('" + zip + "')" );
      */
      return DB.getRecord( "call usp_zip_lookup('" + sZip + "')" );
   }
   
   /*-----------------------------------------------------------------------------------*/
   /*--- checkList ...                                                               ---*/
   /*-----------------------------------------------------------------------------------*/
   public static boolean checkList( String downloadId, String userId, String email )
   {
      String query  = "select        'a' from usa.user      u, usa.downloads d where u.id=d.user_id and d.id=" + downloadId + " and u.id=" + userId + " and email='"+email+"'";
             query += " union select 'a' from usa.user_lead u, usa.downloads d where u.id=d.user_id and d.id=" + downloadId + " and u.id=" + userId + " and email='"+email+"'";
      Log.log( Log.INFO, "checkList : |" + query + "|" );
      return !DB.getRecord( query ).isEmpty();
   }
   
   /*-----------------------------------------------------------------------------------*/
   /*--- currentCarTable ...                                                         ---*/
   /*-----------------------------------------------------------------------------------*/
   public static String currentCarTable()
   {
      return "";
   }
   
   /*-----------------------------------------------------------------------------------*/
   /*--- saveUserExtra ...                                                           ---*/
   /*-----------------------------------------------------------------------------------*/
   public static void saveUserExtra( String id, String extraData )
   {
      try
      {
         StringBuffer query = new StringBuffer( "call usp_save_user_extra (" );
         query.append( id ).append( "," ).append( "\"" ).append(extraData).append( "\")" );
         DB.executeSQL( query.toString() );
      }
      catch( Exception e )
      {
         Log.log(e);
      }
   }
   
   /*-----------------------------------------------------------------------------------*/
   /*--- saveUser ...                                                                ---*/
   /*-----------------------------------------------------------------------------------*/
   public static String saveUser( String userAgent, String service, String domain, String id, String ip, String pageNum, HashMap data, String extraData )
   {
      StringBuffer   query =  new StringBuffer( "" );
      String         ua    =  userAgent;
      if( ua.length() > 200 )
         ua = ua.substring( 0, 190 );
      
      query.append( "call usp_save_user (" );
      query.append( "\"" ).append( ua ).append("\",");
      query.append(id).append( ",\"" ).append(ip).append("\",\"").append(pageNum).append("\"");
      query.append( "," ).append( Format.dbQuote(      Format.HashMapGet(data, "first_name")     ));
      query.append( "," ).append( Format.dbQuote(      Format.HashMapGet(data, "middle_name")    ));
      query.append( "," ).append( Format.dbQuote(      Format.HashMapGet(data, "last_name")      ));
      query.append( "," ).append( Format.dbQuote(      Format.HashMapGet(data, "prefix")         ));
      query.append( "," ).append( Format.dbQuote(      Format.HashMapGet(data, "suffix")         ));
      query.append( "," ).append( Format.dbMySQLDate(  Format.HashMapGet(data, "date_of_birth")  ));
      query.append( "," ).append( Format.dbQuote(      Format.HashMapGet(data, "gender")         ));
      query.append( "," ).append( Format.dbQuote(      Format.HashMapGet(data, "marital_status") ));
      query.append( "," ).append( Format.dbQuote(      Format.HashMapGet(data, "address_1")      ));
      query.append( "," ).append( Format.dbQuote(      Format.HashMapGet(data, "suite_1")        ));
      query.append( "," ).append( Format.dbQuote(      Format.HashMapGet(data, "city_1")         ));
      query.append( "," ).append( Format.dbQuote(      Format.HashMapGet(data, "state_1")        ));
      query.append( "," ).append( Format.dbQuote(      Format.HashMapGet(data, "zip_1")          ));
      query.append( "," ).append( Format.dbQuote(      Format.HashMapGet(data, "phone_1")        ));
      query.append( "," ).append( Format.dbQuote(      Format.HashMapGet(data, "phone_2")        ));
      query.append( "," ).append( Format.dbQuote(      Format.HashMapGet(data, "email")          ));
      query.append( "," ).append( Format.dbQuote(      Format.HashMapGet(data, "password")       ));
      query.append( "," ).append( Format.dbQuote(      service                                   ));
      query.append( "," ).append( Format.dbQuote(      extraData                                 ));
      query.append( "," ).append( Format.dbQuote(      domain                                    ));
      query.append( ")" );
//System.out.println( query );
      return (String) DB.getRecord( query.toString() ).get("id");
   }

   /*-----------------------------------------------------------------------------------*/
   /*--- getUserExtra ...                                                            ---*/
   /*-----------------------------------------------------------------------------------*/
   public static HashMap getUserExtra( String id )
   {
      if( id==null || id.equals("") )
         return (new HashMap());
      StringBuffer query = (new StringBuffer( "call usp_get_user_extra(" )).append(id).append(")");
      return DB.getRecord( query.toString() );
   }
   
   /*-----------------------------------------------------------------------------------*/
   /*--- avatar ...                                                                  ---*/
   /*-----------------------------------------------------------------------------------*/
   public static boolean avatar( String domain )
   {
      try
      {
         StringBuffer query = (new StringBuffer( "select status from avatar where domain=\"")).append(domain).append("\"");
         return ((String) DB.getRecord( query.toString() ).get("status")).equals("Y");
      }
      catch( Exception e )
      {
         Log.log(e);
         return false;
      }
   }

   /*-----------------------------------------------------------------------------------*/
   /*--- pricePoint...                                                               ---*/
   /*-----------------------------------------------------------------------------------*/
   public static HashMap pricePoints( String domain, String state )
   {
      HashMap hm1 = DB.getHashData( "select type, price from new_pricepoint where price>0 and domain='" + domain + "'", "type", "price" );
      HashMap hm2 = DB.getHashData( "select np.type, sp.amount price from state_pricepoint sp, new_pricepoint np where sp.amount>0 and np.id=pricepoint_id and state='"+state+"' and domain='"+domain+"'", "type", "price" );
      hm1.putAll( hm2 );
      return hm1;
   }

   /*-----------------------------------------------------------------------------------*/
   /*--- loadUser ...                                                                ---*/
   /*-----------------------------------------------------------------------------------*/
   public static HashMap loadUser( String id )
   {
      HashMap data = new HashMap();
      try
      {
         String extra_data[] = DB.getRecord( "select * from user where id="+id ).get("extra_data").toString().split("\n");
         for( int i=0; i<extra_data.length; i++ )
         {
            String pair[] = extra_data[i].split("\t");
            try { data.put( pair[0], pair[1] ); } catch( Exception e2 ) {}
         }
      }
      catch( Exception e )
      {
      }
      return data;
   }
}
