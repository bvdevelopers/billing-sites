package com.bl;

import java.util.*;

public class Validation
{
   private static HashMap hmMsg = null;
   
   public static void load()
   {
      hmMsg = new HashMap();
      
      System.out.println("LOAD");
      HashMap hmMsgUS   = new HashMap();
      hmMsgUS.put( "MISSING",          "Missing Data"                );
      hmMsgUS.put( "INVALID_ADDRESS",  "Invalid Address"             );
      hmMsgUS.put( "NO_PO_BOX",        "No PO Box Address"           );
      hmMsgUS.put( "INVALID_EMAIL",    "Invalid e-Mail"              );
      hmMsgUS.put( "INVALID_ZIP",      "Invalid ZIP"                 );
      hmMsgUS.put( "INVALID_PHONE",    "Invalid Phone #"             );
      hmMsgUS.put( "INVALID_DATE",     "Invalid Date"                );
      
      hmMsg.put( "US", hmMsgUS );
      
      
      
      
      HashMap hmMsgBR   = new HashMap();
      hmMsgBR.put( "MISSING",          "Faltando Data"               );
      hmMsgBR.put( "INVALID_ADDRESS",  "Endereco Invalido"           );
      hmMsgBR.put( "NO_PO_BOX",        "No PO Box Address"           );
      hmMsgBR.put( "INVALID_EMAIL",    "e-Mail Invalido"             );
      hmMsgBR.put( "INVALID_ZIP",      "Codigo Postal Invalido"      );
      hmMsgBR.put( "INVALID_PHONE",    "Numero de Telefone Invalido" );
      hmMsgBR.put( "INVALID_DATE",     "Data Invalido"               );
      
      hmMsg.put( "BR", hmMsgBR );
   }
   
   public static String getMessage( String lang, String msg )
   {
      if( hmMsg==null )
         load();
      try
      {
         return ((String) ((HashMap) hmMsg.get(lang)).get(msg));
      }
      catch( Exception e )
      {
         return "";
      }
   }
}

