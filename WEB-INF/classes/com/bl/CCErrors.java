package com.bl;

import java.util.*;
import com.util.*;

public class CCErrors
{
   private static HashMap hmMsg = new HashMap();
   public static String getErrMessage( String code, String line )
   {
      try
      {
         String msg  = (String) hmMsg.get(code);
         if( msg==null )
            hmMsg = getDataHash( "select * from cc_errors", "id", "label" );
         msg  = (String) hmMsg.get(code);
         if( msg==null )
         {
            DB.executeSQL( "insert into cc_unknown values (sysdate(), \""+code+"\", \""+line+"\")" );
            return line;
         }
         return msg;
      }
      catch( Exception exp )
      {
         return line;
      }
   }

   private static HashMap getDataHash( String sql, String key, String val )
   {
      Vector  v = DB.getData(sql);
      HashMap h = new HashMap();
      if( v.size()==0 )
         return h;
      for( int i=0; i<v.size(); i++ )
      {
         HashMap hm = (HashMap) v.elementAt(i);
         h.put( hm.get(key), hm.get(val) );
      }
      return h;
   }
}

