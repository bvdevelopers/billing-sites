package com.bl;

import java.util.*;
import com.util.*;
import java.io.*;
import java.net.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class Downloads
{
   /*-----------------------------------------------------------------------------------*/
   /*--- addForm ..                                                                  ---*/
   /*--- returns encrypted <%=userID%>_<%=downloadID%>                               ---*/
   /*-----------------------------------------------------------------------------------*/
   public static String addForm( String domain, String path, String userId )
   {
      return Queries.setDownload( domain, path, userId );
   }
   
   /*-----------------------------------------------------------------------------------*/
   /*--- getData ..                                                                  ---*/
   /*-----------------------------------------------------------------------------------*/
   public static HashMap getData( String downloadId, String userId )
   {
      return Queries.getDownload( downloadId, userId );
   }
}
