package com.bl;

public class Transaction
{
   public String user_id;
   public String gateway_id;
   public String gateway;
   public String first_name;
   public String last_name;
   public String street;
   public String city;
   public String state;
   public String zip;
   public String phone;
   public String source;
   public String ip;
   public String email;
   public String s_state;
   public String s_service;
   public String four_digit;
   public String subscriber_id;
   public String mid;


   public Transaction(  String _user_id,
                        String _gateway_id,
                        String _gateway,
                        String _first_name,
                        String _last_name,
                        String _street,
                        String _city,
                        String _state,
                        String _zip,
                        String _phone,
                        String _source,
                        String _ip,
                        String _email,
                        String _s_state,
                        String _s_service,
                        String _four_digit,
                        String _subscriber_id,
                        String _mid )
   {
      this.user_id         =  _user_id;
      this.gateway_id      =  _gateway_id;
      this.gateway         =  _gateway;
      this.first_name      =  _first_name;
      this.last_name       =  _last_name;
      this.street          =  _street;
      this.city            =  _city;
      this.state           =  _state;
      this.zip             =  _zip;
      this.phone           =  _phone;
      this.source          =  _source;
      this.ip              =  _ip;
      this.email           =  _email;
      this.s_state         =  _s_state;
      this.s_service       =  _s_service;
      this.four_digit      =  _four_digit;
      this.subscriber_id   =  _subscriber_id;
      this.mid             = _mid;
   }
}

