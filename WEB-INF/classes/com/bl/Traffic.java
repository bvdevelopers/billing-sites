package com.bl;

import com.util.*;
import com.bl.*;
import java.util.*;
import java.util.concurrent.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;


public class Traffic
{
   /*-------------------------------------------------------------------------*/
   /*   track404 ...                                                          */
   /*-------------------------------------------------------------------------*/
   public static void track404( HttpServletRequest request )
   {
      try
      {
         String ip     = request.getRemoteAddr().toString();
         String ua     = (String) request.getHeader( "User-Agent" );
         String domain = com.util.Web.getDomainName( request );
         String source = (String) request.getAttribute( "javax.servlet.error.request_uri" );
         
         ip     = ip==null?"":ip.trim();
         ua     = ua==null?"":ua.trim();
         domain = domain==null?"":domain.trim();
         source = source==null?"":source.trim();
         try { domain = domain.substring(0,255); } catch( Exception e ) {}
         try { source = source.substring(0,255); } catch( Exception e ) {}
         
         StringBuffer   query      =  new StringBuffer();
         query.append( "call usa.usp_track_404 (" );
         query.append( "\"" ).append( domain ).append( "\"," );
         query.append( "\"" ).append( source ).append( "\"," );
         query.append( "\"" ).append( ip ).append( "\"," );
         query.append( "\"" ).append( ua ).append( "\"" );
         query.append( ")" );
         DB.executeSQL( query.toString() );
      }
      catch( Exception e )
      {
         e.printStackTrace();
      }
   }
   

   /*-------------------------------------------------------------------------*/
   /*   trackSource ...                                                       */
   /*-------------------------------------------------------------------------*/
   public static void trackSource( HttpServletRequest request )
   {
      try
      {
         String   source     =  request.getParameter( "utm_source"     );
         String   campaign   =  request.getParameter( "utm_campaign"   );
         String   content    =  request.getParameter( "utm_content"    );
         String   term       =  request.getParameter( "utm_term"       );
         String   ip         =  request.getRemoteAddr().toString();
         String   domain     =  Web.getDomainName( request );
         
         if( source==null && campaign==null && content==null && term==null )
            return;
         
         source     =  source  ==null?"":source;
         campaign   =  campaign==null?"":campaign;
         content    =  content ==null?"":content;
         term       =  term    ==null?"":term;
         
         StringBuffer   query      =  new StringBuffer();
         query.append( "call usa.usp_track_source (" );
         query.append( "\"" ).append( ip       ).append( "\"," );
         query.append( "\"" ).append( domain   ).append( "\"," );
         query.append( "\"" ).append( source   ).append( "\"," );
         query.append( "\"" ).append( campaign ).append( "\"," );
         query.append( "\"" ).append( content  ).append( "\"," );
         query.append( "\"" ).append( term     ).append( "\")" );
         DB.executeSQL( query.toString() );
         try { request.getSession().setAttribute( "utm_source",   source   ); } catch( Exception e ) {}
         try { request.getSession().setAttribute( "utm_campaign", campaign ); } catch( Exception e ) {}
         try { request.getSession().setAttribute( "utm_content",  content  ); } catch( Exception e ) {}
         try { request.getSession().setAttribute( "utm_term",     term     ); } catch( Exception e ) {}
         request.getSession().setMaxInactiveInterval( 900 );
      }
      catch( Exception e )
      {
         e.printStackTrace();
      }
   }
}

